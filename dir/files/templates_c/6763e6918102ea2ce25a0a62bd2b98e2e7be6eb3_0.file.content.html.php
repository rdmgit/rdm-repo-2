<?php
/* Smarty version 3.1.30, created on 2017-06-07 08:18:22
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/content.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59378c9e8b4543_08511040',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6763e6918102ea2ce25a0a62bd2b98e2e7be6eb3' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/content.html',
      1 => 1496812349,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:config.html' => 1,
    'file:article.html' => 1,
    'file:error.html' => 1,
    'file:error_table.html' => 1,
    'file:error_update.html' => 1,
    'file:error_config.html' => 1,
    'file:cron.html' => 1,
    'file:cron_edit.html' => 1,
    'file:strategy_edit.html' => 1,
    'file:strategies.html' => 1,
    'file:users.html' => 1,
    'file:user_edit.html' => 1,
    'file:statistic.html' => 1,
    'file:statistic_price.html' => 1,
    'file:statistic_clicks.html' => 1,
    'file:statistic_count.html' => 1,
    'file:client.html' => 1,
    'file:auction.html' => 1,
  ),
),false)) {
function content_59378c9e8b4543_08511040 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/config") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:config.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/article") {?>	
	<?php $_smarty_tpl->_subTemplateRender("file:article.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/error/index") {?>	
	<?php $_smarty_tpl->_subTemplateRender("file:error.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/error/table") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:error_table.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/error/update") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:error_update.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/error/config") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:error_config.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/cron") {?>	
	<?php $_smarty_tpl->_subTemplateRender("file:cron.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/cron_edit") {?>	
	<?php $_smarty_tpl->_subTemplateRender("file:cron_edit.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/strategy/get" && $_smarty_tpl->tpl_vars['FRAMEWORK']->value['argument']) {?>	
	<?php $_smarty_tpl->_subTemplateRender("file:strategy_edit.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	
<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/strategy/get") {?>	
	<?php $_smarty_tpl->_subTemplateRender("file:strategies.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "user/user/get") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:users.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "user/user/edit") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:user_edit.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/index") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:statistic.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/price") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:statistic_price.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/clicks") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:statistic_clicks.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	
<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/count") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:statistic_count.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
		
<?php } else { ?>
	<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['USER']['ELEMENT'])) {?>
		<?php $_smarty_tpl->_subTemplateRender("file:client.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php } else { ?>
		<?php $_smarty_tpl->_subTemplateRender("file:auction.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php }
}
}
}
