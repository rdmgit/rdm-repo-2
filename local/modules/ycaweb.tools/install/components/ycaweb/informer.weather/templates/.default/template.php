<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<div class="weather">
	<?if($arParams['DISPLAY_DATE'] || $arParams['DISPLAY_TIME']):?>
		<p><?if($arParams['DISPLAY_DATE']):?><?=$arResult['DAY_OF_WEEK']?> <?=$arResult['DATE_FORMATTED']?> <?endif;?><?if($arParams['DISPLAY_TIME']):?><span id="digital-watch"></span><?endif;?></p>
	<?endif;?>
	<?if($arParams['DISPLAY_WEATHER']):?>
		<p><?=$arResult['CITY']['NAME']?> <span><?=$arResult['WEATHER']['NOW']['TEMPERATURE']?>C</span></p>
	<?endif;?>
</div>