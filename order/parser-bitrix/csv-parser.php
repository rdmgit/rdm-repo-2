<?php
function clearOrCreateDir()
{
    global $dir;

    if (!is_dir($dir)) {
        mkdir($dir, 777);
    } else {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            unlink("$dir/$file");
        }
    }
}

$count = isset($_REQUEST['count']) ? $_REQUEST['count'] : 0;

$dir = 'parse-csv';
clearOrCreateDir();

$in = fopen('old.csv', 'r') or die('Failed to open in file!');

$out = null;
$outCount = 0;

$itemCount = 0;

$prevID = null;
$curID = null;

while ($row = fgetcsv($in, 0, ';')) {
    $curID = (int)$row[2];
    if ($prevID !== $curID && $out !== null) {
        $itemCount++;
    }

    if ($itemCount === 0 && $out === null) {
        $out = fopen($dir . '/' . $outCount . '_' . $curID . '-' . ($curID + 99) . '.csv', 'w')
        or die('Failed to open out file!');

        echo('created file #1 ' . $outCount . '_' . $curID . '-' . ($curID + 99));

    } elseif ($itemCount % 100 === 0 && $itemCount > 0 && $prevID !== $curID) {
        fclose($out) or die('Failed to close out file!');

        if ($count !== 0 && $outCount >= $count-1) {
            break;
        }

        $outCount++;
        $out = fopen($dir . '/' . $outCount . '_' . ($curID) . '-' . ($curID + 99) . '.csv', 'w')
        or die('Failed to open out file!');

        echo('created file #2 ' . $curID . '-' . ($curID + 99));
    }

    echo '</br>';

    fputcsv($out, $row, ';');

    $prevID = $curID;
}