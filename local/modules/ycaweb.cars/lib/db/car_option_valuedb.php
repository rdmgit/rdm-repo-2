<?
namespace Ycaweb\Cars;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;

Loc::loadMessages(__FILE__);

class CarOptionValueTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_option_value';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id_car_option_value' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'id_option' => array(
                'data_type' => 'integer',
              //  'required' => true,
            ),
            'id_car_equipment' => array(
                'data_type' => 'integer',
              //  'required' => true,
            ),
            'is_base' => array(
                "data_type" => "integer",
                //"required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_OPTION_VALUE_IS_BASE")
            ),
            'id_car_type'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
        );
    }
}
?>


