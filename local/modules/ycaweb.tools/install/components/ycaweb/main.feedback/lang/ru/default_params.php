<?
$MESS ['MFP_AUTHOR_NAME'] = "Имя";
$MESS ['MFP_AUTHOR_DATE_OF_BIRTH'] = "Дата рождения";
$MESS ['MFP_AUTHOR_CITY'] = "Город";
$MESS ['MFP_AUTHOR_COMPANY'] = "Компания";
$MESS ['MFP_AUTHOR_POST'] = "Должность";
$MESS ['MFP_AUTHOR_PHONE'] = "Телефон";
$MESS ['MFP_AUTHOR_EMAIL'] = "Email";
$MESS ['MFP_MESSAGE'] = "Сообщение";
$MESS ['MFP_FILE'] = "Файл";

//Additional specific fields