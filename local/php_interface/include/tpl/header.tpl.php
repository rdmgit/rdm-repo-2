<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="page__header _js-header">
	<?$APPLICATION->ShowPanel();?>
    <div class="page__header-wrapper _js-header-wrapper"></div>
    <div class="header">
        <div class="header__sidebar-menu _js-header-menu"></div>
        <div class="header__wrapper">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "header", Array(
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                "COMPONENT_TEMPLATE" => ".default",
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "ROOT_MENU_TYPE" => "header",	// Тип меню для первого уровня
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            );?>
            <a href="<?=SITE_DIR?>" class="header__logo"></a>
            <a href="tel:+79039302080" class="header__phone">+7-903-930-20-80</a>

        </div>
    </div>
</section>

<section class="page__middle">
    <div class="page__middle-wrapper _js-page-middle-wrapper"></div>
    <section class="page__sidebar _js-sidebar">
        <div class="sidebar">
            <div class="sidebar__wrapper">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "main-mobile", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "COMPONENT_TEMPLATE" => "",
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "header",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                );?>
                <div class="sidebar__close _js-sidebar-close"></div> 
                <?$APPLICATION->IncludeComponent("bitrix:menu", "main", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "COMPONENT_TEMPLATE" => "",
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                );?>
            </div>
        </div>
    </section>
    <section class="page__main _js-main">
        <div class="page__main-wrapper _breadcrumbs">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "main", Array(
                "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                    "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                    "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                ),
                false
            );?>
        </div>
        <?if($APPLICATION->GetCurPage() != "/"):?>
		<?if(strstr($_SERVER["REQUEST_URI"], "buy")){?>
            <div class="page__main-wrpSearch _search _js-search">
                <div class="search">
                    <a href="#auto-form" class="search__text _js-search-text">Поиск по объявлениям</a>
                </div>
            </div>
		<?}?>
        <?endif;?>