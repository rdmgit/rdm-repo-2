<?
namespace Classes\Handlers;

class Catalog
{
	
	/**
	 OnPriceAdd
	 OnPriceUpdate
	 int ID, array arFields
     @return void
	 * заполняем свойство "PRICE" у товара из Цены 'CATALOG_GROUP_ID' => '1'
    */
	public static function OnPriceAddUpdateHandler($ID, $arFields)
	{
        
// arraytofile($arFields ,$_SERVER['DOCUMENT_ROOT']."/local/php_interface/classes/OnPriceAddUpdate.txt", 'arFields' , 0, true);	
// arraytofile($ID ,$_SERVER['DOCUMENT_ROOT']."/local/php_interface/classes/OnPriceAddUpdate.txt", 'ID' , 0, true);	
		
		$ELEMENT_ID = false;
		$IBLOCK_ID = false;
		//Check for catalog event
		if(is_array($arFields) && $arFields["CATALOG_GROUP_ID"] == 1 && $arFields["PRODUCT_ID"] > 0)
		{	

			\Bitrix\Main\Loader::includeModule('iblock');
            $rsPriceElement = \CIBlockElement::GetList(array(), array( "ID" => $arFields["PRODUCT_ID"]), false, false, array("ID", "IBLOCK_ID"));
			if($arPriceElement = $rsPriceElement->Fetch())
			{				
				$arCatalog = \CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);				
				if(is_array($arCatalog))
				{
					//Check if it is offers iblock
					if($arCatalog["OFFERS"] == "Y")
					{
						$rsElement = \CIBlockElement::GetProperty( $arPriceElement["IBLOCK_ID"], $arPriceElement["ID"], "sort", "asc", array("ID" => $arCatalog["SKU_PROPERTY_ID"])	);
						$arElement = $rsElement->Fetch();
							
						if($arElement && $arElement["VALUE"] > 0)
						{
							$ELEMENT_ID = $arElement["VALUE"];
							$IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
						}
					}
					//or iblock wich has offers
					elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0){
						$ELEMENT_ID = $arPriceElement["ID"];
						$IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
					}
					//or it's regular catalog
					else{
						$ELEMENT_ID = $arPriceElement["ID"];
						$IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
					}
				}
			}
		}
	
		$rsProperty = \CIBlockProperty::GetByID("PRICE", $IBLOCK_ID);
		$arProperty = $rsProperty->Fetch();
	
		if($arProperty && $ELEMENT_ID)
		{
		
			$arPropPrice = array();
			if($arFields["CATALOG_GROUP_ID"] == 1)
				$arPropPrice['PRICE'] = $arFields['PRICE'];			
				
			\CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, $arPropPrice);
		}
	}
	
}