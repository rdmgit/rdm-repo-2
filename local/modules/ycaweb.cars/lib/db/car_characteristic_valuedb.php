<?
namespace Ycaweb\Cars;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;

Loc::loadMessages(__FILE__);

class CarCharacteristicValueTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_characteristic_value';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id_car_characteristic_value' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'id_car_characteristic' => array(
                'data_type' => 'integer'
            ),
            'id_car_modification' => array(
                'data_type' => 'integer'
            ),
            'value' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_CHARACTERISTIC_VALUE_VALUE")
            ),
            'unit' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_CHARACTERISTIC_VALUE_UNIT")
            ),
            'id_car_type'      => array(
                "data_type" => "integer"
            ),
            'date_create'      => array(
                "data_type" => "integer"
            ),
            'date_update'      => array(
                "data_type" => "integer"
            ),
        );
    }
}
?>


