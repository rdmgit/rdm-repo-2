<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(isset($_COOKIE["BITRIX_SM_compare"])){
	
	$strStatus = "<a href='/auto/favorite/' class='favorite__btn favorite-active'>Перейти к сравнению</a>";
	//$strStatus = 
}else{
	
	$strStatus = '<button class="favorite__btn favorite-inactive onCompareHelp">Список сравнения</button>';
}
?>
<?if (!empty($arResult)):?>
	<div class="main__services-links">
		<ul class="services-links">
			<?foreach($arResult as $arItem):?>
				<li class="services-links__item <?=($arItem["SELECTED"]) ? "_active" : ""?> _buy">
					<a href="<?=$arItem["LINK"]?>" class="services-links__item-link" <?if(isset($arItem["PARAMS"]["target"])){?>target="<?=$arItem["PARAMS"]["target"]?>"<?}?>>
						<?=$arItem["TEXT"]?>
					</a>
				</li>
			<?endforeach?>
		</ul>
		<div class="favorite__nav">
		<?if(false){?><a href="/auto/view/" class="favorite__btn favorite-inactive">Список сравнения</a><?}?>
		<?=$strStatus?>
		</div>
	</div>
	
<?endif?>


<div class="fav__help" id="favHelp" style="display:none">

    <div class="fav-help__header"><img class="fav-help__logo" src="/local/assets/img/assets/header/logo.svg" alt="" /></div>
    <div id="fav-help__html" class="fav-help__html">
		<p>Чтобы добавить автомобиль в&nbsp;список сравнения,&nbsp;нажмите<span class="fav__star">Список сравнения</span> на&nbsp;карточке понравившегося автомобиля.</p>
    </div>
	<div class="fav-help__nav">
	<button class="fav-help__submit onHelpClose">Спасибо, понятно!</button>
	</div>
</div>