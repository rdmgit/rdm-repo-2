<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$oAssets = \Bitrix\Main\Page\Asset::getInstance();
$oAssets->addJs(ASSETS_PATH . '/js/plugins/jquery.easing.js');
$oAssets->addJs(ASSETS_PATH . '/js/plugins/jquery.dimensions.js');
$oAssets->addJs(ASSETS_PATH . '/js/plugins/jquery.accordion.js');

$APPLICATION->SetAdditionalCSS(ASSETS_PATH . 'css/sell-auto.css');

$APPLICATION->SetPageProperty("wrapper_class", "autocreditovanie");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetTitle("Автокредит");
?>



<?if(isset($_REQUEST["adm"])){?>
<div class="bg_moz sell__banner" style="background-image: url(/local/assets/img/pasha_c_kartoi.jpg);">
	<!--div class="persons" style=" position: absolute; top: 0; right: 0 ;width: 630px; bottom: 0; background: url(/local/assets/img/persons.png); background-repeat: no-repeat; background-size: initial; background-position: bottom;">
	</div-->
	<div class="sell__wrap">
		
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc" style="    padding-top: 10px;">
				<div class="offers-list__item-text block-ccc">
					<div class="top-banner__info-title">
						<div class="b-title">
							<h1 style="padding-top:15px;">Автокредитование</h1>
						</div>
					</div>
					<p>
						 Работая с автокредитами с 2007 года, компания РДМ-Импорт накопила огромный опыт и наладила партнерские связи с 14 лучшими в вопросах кредитования банками, что позволяет нам получать более выгодные кредитные условия. Также наш опыт позволяет видеть все скрытые в договоре платежи и комиссии, о чем мы сразу предупреждаем и можем подсчитать реальную сумму переплаты.
					</p>
					<p>
						 9 из 10 отправленных заявок получают положительное кредитное решение. Мы оказываем помощь в получение кредита независимо от проживания и трудоустройства обратившегося.
					</p>
					<p>
						 Кредитное решение можно использовать для покупки автомобиля в нашем салоне или любом другом. <br>
					</p>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more">
 <br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
					<p>
						 <!--img src="../local/assets/img/proto_img.png" style="width:80%"-->
					</p>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more">
 <br>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <!-- asdasdasd -->
		<div class="offers-list__item-info">
			<div class="offers-list__item-text">
			</div>
		</div>
	</div>
</div>
<?}else{?>
<div class="bg_moz" style="background: url(/local/assets/img/pasha_c_kartoi.jpg);margin-top: -40px;position:relative; background-size:cover; color:white; min-height:585px;    background-position: center;">
	<!--div class="persons" style=" position: absolute; top: 0; right: 0 ;width: 630px; bottom: 0; background: url(/local/assets/img/persons.png); background-repeat: no-repeat; background-size: initial; background-position: bottom;">
	</div-->
	<div class="offers-list__item">
		
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc" style="    padding-top: 10px;">
				<div class="offers-list__item-text block-ccc">
					<div class="top-banner__info-title">
						<div class="b-title">
							<h1 style="padding-top:15px;">Автокредитование</h1>
						</div>
					</div>
					<p>
						 Работая с автокредитами с 2007 года, компания РДМ-Импорт накопила огромный опыт и наладила партнерские связи с 14 лучшими в вопросах кредитования банками, что позволяет нам получать более выгодные кредитные условия. Также наш опыт позволяет видеть все скрытые в договоре платежи и комиссии, о чем мы сразу предупреждаем и можем подсчитать реальную сумму переплаты.
					</p>
					<p>
						 9 из 10 отправленных заявок получают положительное кредитное решение. Мы оказываем помощь в получение кредита независимо от проживания и трудоустройства обратившегося.
					</p>
					<p>
						 Кредитное решение можно использовать для покупки автомобиля в нашем салоне или любом другом. <br>
					</p>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more">
 <br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
					<p>
						 <!--img src="../local/assets/img/proto_img.png" style="width:80%"-->
					</p>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more">
 <br>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <!-- asdasdasd -->
		<div class="offers-list__item-info">
			<div class="offers-list__item-text">
			</div>
		</div>
	</div>
</div>
<?}?>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Преимущества автокредитования у нас:<br></h2>
		</div>
	</div>
	<div class="offers-list__item-info adventage">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<p class="rassmotr-zayav">
					 Рассмотрение заявки 14 банками в течение 1го дня
				</p>
				<p class="poluchenie">
					 9 из 10 заявок получают положительное решение<br>
				</p>
				<p class="zaschita">
					 100% защита ваших данных от мошенничества
				</p>
				<p class="result-oplata">
					 Оплата только за результат<br>
				</p>
			</div>
		</div>
	</div>
	 <!-- asdasdasd -->
	<div class="offers-list__item-info adventage">
		<div class="offers-list__item-text">
			<p class="procent">
				 9,9% - низкий процент за счет партнерских программ<br>
			</p>
			<p class="rinok">
				 15 лет надежной работы компании<br>
			</p>
			<p class="doc">
				 Простой и понятный договор оказания услуг
			</p>
			<p class="doci">
				 Все документы официально оформлены и в соответствии с законом
			</p>
		</div>
	</div>
</div>

 <div class="gray_bg" style="position: relative; overflow:hidden;">
<div class="offers-list__item vopros-block">
	<div class="top-banner__info-title">
		<div class="b-title">
			 <!--h2>Страхование в Каско</h2-->
		</div>
	</div>
	<div class="auto-block">
		<div class="offers-list__item-desc ">
			<h2>Самые популярные вопросы об автокредите</h2>
			<div class="offers-list__item-text voprosi accordeon">
				 <?$APPLICATION->IncludeComponent(
					"bitrix:support.faq.element.list",
					"mytpl1",
					Array(
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"IBLOCK_ID" => "14",
						"IBLOCK_TYPE" => "services",
						"PATH_TO_USER" => "",
						"RATING_TYPE" => "",
						"SECTION_ID" => "1",
						"SHOW_RATING" => ""
					)
				);?><br>
			</div>
			<div class="offers-list__item-actions btn-lft">
				<div class="offers-list__item-action _calc">
					<div data-src="#creditRequest" class="offers-list__item-action-link fancy-inline">
						 Задать вопрос
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="auto-block">
		<div class="offers-list__item-desc" style="margin-top: 15px;">
			 <!--h2>Потребительское кредитование</h2-->
			<div class="offers-list__item-text" style="display:none">
				 <? if ($USER->IsAuthorized()){ ?>
				<div class="calculator">
					 Кредитный калькулятор
				</div>
				<div class="calculator">
					 Подбор банка
				</div>
				 <? }?> 
				<!--img src="/local/assets/img/question.png" class="faq-vopros-img" style="width:80%;margin-top:10px;"-->
			</div>
		</div>
	</div>
</div>
<div id="trailer" class="is_overlay" style="height:100vh;">
	<video id="video" width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
		<source src="/local/assets/img/credit.webm" type="video/webm"></source>
		<source src="/local/assets/img/credit.mp4" type="video/mp4"></source>
	</video>
	<div class="video-bg-layer"></div>
</div>
</div>
<div class="offers-list__item">
	<h2>Доступно о&nbsp;кредитах</h2>
	<div class="offers-list__item-desc">
		<div class="offers-list__item-text">
			 <!--Lorem.. -->
		</div>
	</div>
	<div class="list_video">
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/qyDVhffuoxg" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Ставка по кредиту от 9.9%, как на новые авто!
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/I31TNX2UfXo" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Что такое "Первый льготный платеж" по кредиту?
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/bVep-XvelEU" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Ответы на вопросы по кредитованию от руководителя кредитного отдела компании РДМ-Импорт
			</div>
		</div>
		<div class="offers-list__item-actions btn-under-video">
			<div class="offers-list__item-action _calc">
				<div data-src="#creditRequest" class="offers-list__item-action-link fancy-inline">
					 Оставить заявку на кредит
				</div>
			</div>
		</div>
	</div>
</div>
 <!--                      BANKS  -->
<div class="offers-list__item">
	<h2>Наши банки&nbsp;-&nbsp;партнеры</h2>
	<div class="offers-list__item-desc">
		<div class="offers-list__item-text">
			 <!-- Lorem.. -->
		</div>
	</div>
	<div class="list_bank">
		<div class="bank_item">
 <img src="/local/assets/img/bank/1.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/2.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/3.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/4.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/5.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/6.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/7.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/8.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/9.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/10.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/11.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/12.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/13.gif">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/14.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/15.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/16.png">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/17.gif">
		</div>
		<div class="bank_item">
 <img src="/local/assets/img/bank/18.png">
		</div>
		<div class="offers-list__item-actions">
			<div class="offers-list__item-action _calc">
				<div data-src="#creditRequest" class="offers-list__item-action-link fancy-inline">
					 Подобрать банк
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
<?$APPLICATION->IncludeFile(
	SITE_DIR."include/forms/creditRequest.php",
	Array(),
	Array("MODE"=>"text")
);?>
 <br>
 <script>
$(".accordeon .accordeon_slide").hide().prev().click(function() {
	$(this).parents(".accordeon").find(".accordeon_slide").not(this).slideUp().prev().removeClass("active");
	$(this).next().not(":visible").slideDown().prev().addClass("active");
});
</script> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>