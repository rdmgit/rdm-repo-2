<?
define("NO_PHOTO_PATH", SITE_DIR."local/assets/img/no_photo.jpg");

CModule::IncludeModule('ycaweb.tools');

if(is_file($_SERVER['DOCUMENT_ROOT']."/local/php_interface/YcawebHelper.php")){
    require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/YcawebHelper.php");
}

if(is_file(sprintf('%s/functions.php', __DIR__))){
    require (sprintf('%s/functions.php', __DIR__));
}

if(is_file(sprintf('%s/constants.php', __DIR__))){
    require (sprintf('%s/constants.php', __DIR__));
}


\Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    '\Classes\Handlers\Reviews' => '/local/php_interface/classes/reviews.php',
    '\Classes\Handlers\Catalog' => '/local/php_interface/classes/catalog.php',
    '\Classes\Handlers\Iblock' => '/local/php_interface/classes/iblock.php',
    '\Classes\Handlers\SubscribeTovar' => '/local/php_interface/classes/subscribetovar.php',
    '\HLBlock' => '/local/php_interface/classes/HLClass.php',
));

$eventManager = \Bitrix\Main\EventManager::getInstance();
/*товары*/
$eventManager->addEventHandlerCompatible('iblock', 'OnAfterIBlockElementUpdate', array('\Classes\Handlers\Iblock', 'BXIBlockAfterSave'));
$eventManager->addEventHandlerCompatible('iblock', 'OnAfterIBlockElementAdd', array('\Classes\Handlers\Iblock', 'BXIBlockAfterSave'));

// для оповещений подписанных пользователей о новом товаре
$eventManager->addEventHandlerCompatible('iblock', 'OnAfterIBlockElementAdd', array('\Classes\Handlers\SubscribeTovar', 'OnAfterIBlockElementAddHandler'));

/*отзывы*/
$eventManager->addEventHandlerCompatible('iblock', 'OnAfterIBlockElementAdd', array('\Classes\Handlers\Reviews', 'OnAfterIBlockElementAddHandler'));
$eventManager->addEventHandlerCompatible('iblock', 'OnAfterIBlockElementUpdate', array('\Classes\Handlers\Reviews', 'OnAfterIBlockElementUpdateHandler'));
$eventManager->addEventHandlerCompatible('iblock', 'OnBeforeIBlockElementUpdate', array('\Classes\Handlers\Reviews', 'OnBeforeIBlockElementUpdateHandler'));
$eventManager->addEventHandlerCompatible('iblock', 'OnBeforeIBlockElementDelete', array('\Classes\Handlers\Reviews', 'OnBeforeIBlockElementDeleteHandler'));

$eventManager->addEventHandlerCompatible('catalog', 'OnPriceAdd', array('\Classes\Handlers\Catalog', 'OnPriceAddUpdateHandler'));
$eventManager->addEventHandlerCompatible('catalog', 'OnPriceUpdate', array('\Classes\Handlers\Catalog', 'OnPriceAddUpdateHandler'));

/* Раздел в админке для управления рассылкой */
$eventManager->addEventHandlerCompatible('main', 'OnBuildGlobalMenu', 'ASDFavoriteOnBuildGlobalMenu');


 function ASDFavoriteOnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {   

// Раздел в админке, для настройки попапа exit popup
    $aModuleMenu[] = Array(
                         "parent_menu" => "global_menu_services"
                        ,"section" => "subscribe"
                        ,"sort" => 6
                        ,"text" => "Рассылки"
                        ,"title" => "Рассылки"
                        ,"url" => "_subscribe.php"
                        ,"icon" => "bizproc_menu_icon"
                        ,"page_icon" => "catalog_page_icon"
                        ,"items_id" => "mnu_catalog"
                        ,"items" => Array()
            ); 
      
    }