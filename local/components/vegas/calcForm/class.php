<?include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/scripts/HLClass.php');
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class calcForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {

            $hLBlock = new HLBlock('CarMark');
            $arResultMark = $hLBlock->getFields(array(), array('UF_NAME' => 'ASC'));   


            $this->arResult["MARK"] = $arResultMark;


            $this->includeComponentTemplate();

        }    

        public function onPrepareComponentParams($arParams)
        { 
            return $arParams;
        }
        
    }
?>