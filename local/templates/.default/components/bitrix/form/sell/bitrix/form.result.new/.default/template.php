<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?//if ($arResult["isFormErrors"] == "Y"):?>

<script>
var edataChecked = false;
var errors_fields = <?=json_encode($arResult["FORM_ERRORS"]);?>;
var errorFlag = true;
$(document).ready(function(){
	
	$(".order-input-fullrow").removeClass('error');
	
	for(var i in errors_fields){
		errorFlag = false;
		$('#'+i).addClass('error')
		
	}
	

	
	$('.in-buy-data').change(function() {
		if ($(this).prop("checked")) {
			
			edataChecked = true;
			$(".form_buy_submit").removeAttr("disabled");
			$(".form_buy_submit").removeClass("disabled");
		   
		}	else{
			edataChecked = false;
			$(".form_buy_submit").attr("disabled","disabled");
			$(".form_buy_submit").addClass("disabled");	
		}
	
	});	
});
</script>
<?if ($arResult["isFormNote"] == "Y"):?>
<script>
$(document).ready(function(){

	if(errorFlag && errors_fields==null){
		
		console.log(target);
		yaCounter4175221.reachGoal(target);
	}
});
</script>
<?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>

<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>


	<? 

	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
	?>
		<div class="order-input-fullrow" id="<?=$FIELD_SID?>">
		<?
		if($arQuestion["REQUIRED"]=="Y"){
			
			$placeholder = 'placeholder="'.$arQuestion['CAPTION'].'*"';
		}else{
			$placeholder = 'placeholder="'.$arQuestion['CAPTION'].'"';
			
		}			
			
		$arQuestion["HTML_CODE"] = str_replace('placeholder',$placeholder,$arQuestion["HTML_CODE"]);

		if(strstr("placeholder",$arQuestion["HTML_CODE"])){
			
			$title = "Y";
		}
		?>

		<?if(!strstr($arQuestion["HTML_CODE"],"placeholder") && $arQuestion["STRUCTURE"][0]["FIELD_TYPE"]!="checkbox"){
			
			echo $arQuestion["CAPTION"];
		}?>
		
			<?=$arQuestion["HTML_CODE"]?>
			<div class="error__block">
			 <div class="error__block-text">Заполните это поле</div>
			</div>
		</div>
	<?
	} //endwhile
	?>
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<div>
			<div colspan="2"><b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b></div>
		</div>
		<div>
			<div>&nbsp;</div>
			<div><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" /></div>
		</div>
		<div>
			<div><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></div>
			<div><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /></div>
		</div>
<?
} // isUseCaptcha
?>

	
		<div class="order-input-fullrow order-input-row-center">
			
				<?if ($arResult["F_RIGHT"] >= 15):?>
				&nbsp;<input type="hidden" name="web_form_apply" value="Y" />
				<input class="form_buy_submit" type="submit" name="web_form_apply" value="Отправить" />
				<?endif;?>
		
			
		</div>
	
</div>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>