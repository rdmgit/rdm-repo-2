<?			
	/*
	*  Принимаем со стороны некий код и сверяем его со списком
	*  существующих эл. ящиков в веб форме
	*/ 
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;

    CModule::IncludeModule("form"); 
	 
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class regForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {

		    $rsResults = CFormResult::GetList($this->arParams["FORM_ID"], 
			($by="s_timestamp"),($order="desc"), 
			array(),$is_filtered, 
			"N");

			$this->arResult["unscribe"] = false;
		
			while ($arRes = $rsResults->Fetch())
			{	

				$temp  = CFormResult::GetDataByID( $arRes["ID"], array("your_email"));
				if(md5('u'.$arRes["ID"].$temp["your_email"][0]["USER_TEXT"]) == $this->arParams["SECURE_CODE"]){					
					$this->arResult["unscribe"] = true;

					if (CFormResult::SetStatus($arRes["ID"], 10,"N"))
					{			
						$this->arResult = array("starus"=>true,"msg"=>"Вы отписались от новостей");
					}
					else // ошибка
					{
						$this->arResult = array("starus"=>false,"msg"=>"Неудалось отписаться от новостей");					
					}
					
					break;
				}
				
			}
		
			$this->includeComponentTemplate();
                    
        }

        public function onPrepareComponentParams($arParams)
        {
            
            
            if(!isset($arParams["CACHE_TIME"]))
                $arParams["CACHE_TIME"] = 36000000;

            $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
            if(strlen($arParams["IBLOCK_TYPE"])<=0)
                $arParams["IBLOCK_TYPE"] = "news";
            
            $arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
            $arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
            $arParams["AJAX_MODE"] = "N";

            return $arParams;
        }
        
    }
?>