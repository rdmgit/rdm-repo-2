<?
use Bitrix\Main\Loader,
    Bitrix\Main,
    Bitrix\Iblock,
    Bitrix\Catalog,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc,
    Ycaweb\Cars;

/** @global CUser $USER */
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/prolog.php');

Loader::includeModule('iblock');

Main\Page\Asset::getInstance()->addJs('/bitrix/js/ycaweb.cars/jquery-1.9.1.min.js');
Main\Page\Asset::getInstance()->addJs('/bitrix/js/ycaweb.cars/editPage.js');

$moduleID = "ycaweb.cars";
Loader::includeModule($moduleID);

$io = CBXVirtualIo::GetInstance();

/*Change any language identifiers carefully*/
/*because of user customized forms!*/
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/iblock/admin/iblock_element_edit_compat.php");

IncludeModuleLangFile(__FILE__);

$IBLOCK_ID = 0;
if (isset($_REQUEST['IBLOCK_ID']))
    $IBLOCK_ID = (int)$_REQUEST["IBLOCK_ID"]; //information block ID
define("MODULE_ID", "iblock");
define("ENTITY", "CIBlockDocument");
define("DOCUMENT_TYPE", "iblock_".$IBLOCK_ID);

/* autocomplete */
$strLookup = '';
if (isset($_REQUEST['lookup']))
    $strLookup = preg_replace("/[^a-zA-Z0-9_:]/", "", $_REQUEST["lookup"]);
if ('' != $strLookup)
{
    define('BT_UT_AUTOCOMPLETE', 1);
}
$bAutocomplete = defined('BT_UT_AUTOCOMPLETE') && (BT_UT_AUTOCOMPLETE == 1);

/* property ajax */
$bPropertyAjax = (isset($_REQUEST["ajax_action"]) && $_REQUEST["ajax_action"] === "section_property");
if ($bPropertyAjax)
{
    CUtil::JSPostUnescape();
}

$strWarning = '';
$bVarsFromForm = false;

$ID = 0;
if (isset($_REQUEST['ID']))
    $ID = (int)$_REQUEST['ID']; //ID of the persistent record

/* copy element */
$bCopy = false;
$copyID = 0;
if (!$bAutocomplete)
{
    $bCopy = (isset($_REQUEST['action']) && $_REQUEST["action"] == "copy");
}
$copyID = (int)(isset($_REQUEST['copyID']) ? $_REQUEST['copyID'] : 0);

if($ID<=0 && intval($PID)>0)
    $ID = intval($PID);

$PREV_ID = intval($PREV_ID);

$WF_ID = $ID; //This is ID of the current copy

$arShowTabs = array(
    'edit_rights' => false,
    'workflow' => false,
    'bizproc' => false,
    'sections' => false,
    'catalog' => false,
    'sku' => false,
    'product_set' => false,
    'product_group' => false
);

$bWorkflow = Loader::includeModule("workflow") && (CIBlock::GetArrayByID($IBLOCK_ID, "WORKFLOW") != "N");
$bBizproc = Loader::includeModule("bizproc") && (CIBlock::GetArrayByID($IBLOCK_ID, "BIZPROC") != "N");

$bCatalog = Loader::includeModule('catalog');
$arMainCatalog = false;
$arCatalogTabs = false;
$bOffers = false;
$boolCatalogRead = false;
$boolCatalogPrice = false;
if ($bCatalog)
{
    $boolCatalogRead = $USER->CanDoOperation('catalog_read');
    $boolCatalogPrice = $USER->CanDoOperation('catalog_price');
    $arMainCatalog = CCatalogSku::GetInfoByIBlock($IBLOCK_ID);
    if (!empty($arMainCatalog))
    {
        $bOffers = ($arMainCatalog['CATALOG_TYPE'] == CCatalogSku::TYPE_PRODUCT || $arMainCatalog['CATALOG_TYPE'] == CCatalogSku::TYPE_FULL);
        CCatalogAdminTools::setProductFormParams();

        $arCatalogTabs = CCatalogAdminTools::getShowTabs($IBLOCK_ID, ($copyID > 0 && $ID == 0 ? $copyID : $ID), $arMainCatalog);
        if (!empty($arCatalogTabs))
        {
            $arShowTabs['catalog'] = $arCatalogTabs[CCatalogAdminTools::TAB_CATALOG];
            $arShowTabs['sku'] = $arCatalogTabs[CCatalogAdminTools::TAB_SKU];
            $arShowTabs['product_set'] = $arCatalogTabs[CCatalogAdminTools::TAB_SET];
            $arShowTabs['product_group'] = $arCatalogTabs[CCatalogAdminTools::TAB_GROUP];
        }
    }
}
$str_TMP_ID = 0;
if ($bOffers && (0 == $ID || $bCopy))
{
    if ('GET' == $_SERVER['REQUEST_METHOD'] && (0 >= $ID || $bCopy))
    {
        $str_TMP_ID = CIBlockOffersTmp::Add($IBLOCK_ID, $arMainCatalog['IBLOCK_ID']);
    }
    else
    {
        if (isset($_REQUEST['TMP_ID']))
            $str_TMP_ID = (int)$_REQUEST['TMP_ID'];
    }
}
$TMP_ID = $str_TMP_ID;

if(($ID <= 0 || $bCopy) && $bWorkflow)
    $WF = "Y";
elseif(!$bWorkflow)
    $WF = "N";
else
    $WF = ($_REQUEST["WF"] === "Y")? "Y": "N";

$historyId = intval($history_id);
if ($historyId > 0 && $bBizproc)
    $view = "Y";
else
    $historyId = 0;

Main\Page\Asset::getInstance()->addJs('/bitrix/js/iblock/iblock_edit.js');

$error = false;

$view = ($view=="Y") ? "Y" : "N"; //view mode

$return_url = (isset($return_url) ? (string)$return_url : '');

if ($bAutocomplete)
{
    $return_url = '';
}
else
{
    if ($return_url != '' && strtolower(substr($return_url, strlen($APPLICATION->GetCurPage())))==strtolower($APPLICATION->GetCurPage()))
        $return_url = '';
    if ($return_url == '')
    {
        if ($from=="iblock_section_admin")
            $return_url = CIBlock::GetAdminSectionListLink($IBLOCK_ID, array('find_section_section'=>intval($find_section_section)));
    }
}

do{ //one iteration loop

    $errorTriger = false;
    if ($historyId > 0)
    {
        $arErrorsTmp = array();
        $arResult = CBPDocument::GetDocumentFromHistory($historyId, $arErrorsTmp);

        if (!empty($arErrorsTmp))
        {
            foreach ($arErrorsTmp as $e)
            {
                $error = new _CIBlockError(1, $e["code"], $e["message"]);
                break;
            }
        }

        $canWrite = CBPDocument::CanUserOperateDocument(
            CBPCanUserOperateOperation::WriteDocument,
            $USER->GetID(),
            $arResult["DOCUMENT_ID"],
            array("UserGroups" => $USER->GetUserGroupArray())
        );
        if (!$canWrite)
        {
            $error = new _CIBlockError(1, "ACCESS_DENIED", GetMessage("IBLOCK_ACCESS_DENIED_STATUS"));
            break;
        }

        $type = $arResult["DOCUMENT"]["FIELDS"]["IBLOCK_TYPE_ID"];
        $IBLOCK_ID = $arResult["DOCUMENT"]["FIELDS"]["IBLOCK_ID"];
    }

    $arIBTYPE = CIBlockType::GetByIDLang($type, LANGUAGE_ID);
    if($arIBTYPE===false)
    {
        $error = new _CIBlockError(1, "BAD_IBLOCK_TYPE", GetMessage("IBLOCK_BAD_BLOCK_TYPE_ID"));
        break;
    }

    $MENU_SECTION_ID = intval($IBLOCK_SECTION_ID)? intval($IBLOCK_SECTION_ID): intval($find_section_section);

    $bBadBlock = true;
    $arIBlock = CIBlock::GetArrayByID($IBLOCK_ID);
    if($arIBlock)
    {
        if(($ID > 0 && !$bCopy) && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "iblock_admin_display"))
            $bBadBlock = true;
        elseif(($ID <= 0 || $bCopy) && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "iblock_admin_display"))
            $bBadBlock = true;
        elseif(CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit_any_wf_status"))
            $bBadBlock = false;
        elseif(!$bWorkflow && CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit"))
            $bBadBlock = false;
        elseif($bWorkflow && ($WF=="Y" || $view=="Y"))
            $bBadBlock = false;
        elseif($bBizproc)
            $bBadBlock = false;
        elseif(
            (($ID <= 0) || $bCopy)
            && CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
        )
            $bBadBlock = false;
        elseif(($ID > 0 && !$bCopy) && CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "iblock_admin_display"))
            $bBadBlock = false;
    }

    if($bBadBlock)
    {
        $error = new _CIBlockError(1, "BAD_IBLOCK", GetMessage("IBLOCK_BAD_IBLOCK"));
        $APPLICATION->SetTitle($arIBTYPE["ELEMENT_NAME"].": ".GetMessage("IBLOCK_EDIT_TITLE"));
        break;
    }

    $bEditRights = $arIBlock["RIGHTS_MODE"] === "E" && (
            CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_rights_edit")
            || (($ID <= 0 || $bCopy) && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "element_rights_edit"))
        );

    $arShowTabs['sections'] = ('Y' == $arIBTYPE["SECTIONS"]);
    $arShowTabs['workflow'] = $bWorkflow;
    $arShowTabs['bizproc'] = $bBizproc && (0 >= $historyId);
    $arShowTabs['edit_rights'] = $bEditRights;
    if ('Y' == $view || $bPropertyAjax)
    {
        $arShowTabs['sku'] = false;
        $arShowTabs['product_set'] = false;
        $arShowTabs['product_group'] = false;
    }

    $aTabs = array(
        array(
            "DIV" => "edit1",
            "TAB" => $arIBlock["ELEMENT_NAME"],
            "ICON" => "iblock_element",
            "TITLE" => htmlspecialcharsEx($arIBlock["ELEMENT_NAME"])
        ),
        array(
            "DIV" => "edit5",
            "TAB" => GetMessage("IBEL_E_TAB_PREV"),
            "ICON" => "iblock_element",
            "TITLE" => GetMessage("IBEL_E_TAB_PREV_TITLE")
        ),
        array(
            "DIV" => "edit6",
            "TAB" => GetMessage("IBEL_E_TAB_DET"),
            "ICON" => "iblock_element",
            "TITLE" => GetMessage("IBEL_E_TAB_DET_TITLE")
        ),
        array(
            "DIV" => "edit14",
            "TAB" => GetMessage("IBEL_E_TAB14"),
            "ICON" => "iblock_iprops",
            "TITLE" => GetMessage("IBEL_E_TAB14_TITLE"),
            "ONSELECT" => "InheritedPropertiesTemplates.onTabSelect();",
        ),
    );
    if($arShowTabs['sections'])
        $aTabs[] = array(
            "DIV" => "edit2",
            "TAB" => $arIBlock["SECTIONS_NAME"],
            "ICON" => "iblock_element_section",
            "TITLE" => htmlspecialcharsEx($arIBlock["SECTIONS_NAME"]),
        );
    if ($arShowTabs['catalog'])
        $aTabs[] = array(
            "DIV" => "edit10",
            "TAB" => GetMessage("IBLOCK_TCATALOG"),
            "ICON" => "iblock_element",
            "TITLE" => GetMessage("IBLOCK_TCATALOG"),
            "required" => true,
        );
    if($arShowTabs['sku'])
        $aTabs[] = array(
            "DIV" => "edit8",
            "TAB" => GetMessage("IBLOCK_EL_TAB_OFFERS"),
            "ICON" => "iblock_element",
            "TITLE" => GetMessage("IBLOCK_EL_TAB_OFFERS_TITLE"),
            "required" => true,
        );
    if ($arShowTabs['product_set'])
        $aTabs[] = array(
            "DIV" => "edit11",
            "TAB" => GetMessage("IBLOCK_EL_TAB_PRODUCT_SET"),
            "ICON" => "iblock_element",
            "TITLE" => GetMessage("IBLOCK_EL_TAB_PRODUCT_SET_TITLE"),
            "required" => true,
        );
    if ($arShowTabs['product_group'])
        $aTabs[] = array(
            "DIV" => "edit12",
            "TAB" => GetMessage("IBLOCK_EL_TAB_PRODUCT_GROUP"),
            "ICON" => "iblock_element",
            "TITLE" => GetMessage("IBLOCK_EL_TAB_PRODUCT_GROUP_TITLE"),
            "required" => true,
        );
    if($arShowTabs['workflow'])
        $aTabs[] = array(
            "DIV" => "edit4",
            "TAB" => GetMessage("IBLOCK_EL_TAB_WF"),
            "ICON" => "iblock_element_wf",
            "TITLE" => GetMessage("IBLOCK_EL_TAB_WF_TITLE")
        );
    if($arShowTabs['bizproc'])
        $aTabs[] = array(
            "DIV" => "edit7",
            "TAB" => GetMessage("IBEL_E_TAB_BIZPROC"),
            "ICON" => "iblock_element_bizproc",
            "TITLE" => GetMessage("IBEL_E_TAB_BIZPROC")
        );
    if($arShowTabs['edit_rights'])
        $aTabs[] = array(
            "DIV" => "edit9",
            "TAB" => GetMessage("IBEL_E_TAB_RIGHTS"),
            "ICON" => "iblock_element_rights",
            "TITLE" => GetMessage("IBEL_E_TAB_RIGHTS_TITLE")
        );

    if (strlen($arIBlock["EDIT_FILE_AFTER"])>0 && is_file($_SERVER["DOCUMENT_ROOT"].$arIBlock["EDIT_FILE_AFTER"]))
    {
        $bCustomForm = true;
        $customFormFile = $arIBlock["EDIT_FILE_AFTER"];
    }
    elseif (strlen($arIBTYPE["EDIT_FILE_AFTER"])>0 && is_file($_SERVER["DOCUMENT_ROOT"].$arIBTYPE["EDIT_FILE_AFTER"]))
    {
        $bCustomForm = true;
        $customFormFile = $arIBTYPE["EDIT_FILE_AFTER"];
    }
    else
    {
        $bCustomForm = false;
        $customFormFile = '';
    }

    if($ID>0)
    {
        $rsElement = CIBlockElement::GetList(array(), array("ID" => $ID, "IBLOCK_ID" => $IBLOCK_ID, "SHOW_HISTORY"=>"Y"), false, false, array("ID", "CREATED_BY"));
        if(!($arElement = $rsElement->Fetch()))
        {
            $error = new _CIBlockError(1, "BAD_ELEMENT", GetMessage("IBLOCK_BAD_ELEMENT"));
            $APPLICATION->SetTitle($arIBTYPE["ELEMENT_NAME"].": ".GetMessage("IBLOCK_EDIT_TITLE"));
            $errorTriger = true;
        }
    }

    if (!$errorTriger)
    {
        // workflow mode
        $isLocked = false;
        if($ID>0 && $WF=="Y")
        {
            // get ID of the last record in workflow
            $WF_ID = CIBlockElement::WF_GetLast($ID);

            // check for edit permissions
            $STATUS_ID = CIBlockElement::WF_GetCurrentStatus($WF_ID, $STATUS_TITLE);
            $STATUS_PERMISSION = CIBlockElement::WF_GetStatusPermission($STATUS_ID);

            if($STATUS_ID>1 && $STATUS_PERMISSION<2)
            {
                $error = new _CIBlockError(1, "ACCESS_DENIED", GetMessage("IBLOCK_ACCESS_DENIED_STATUS"));
                $errorTriger = true;
            }
            elseif($STATUS_ID==1)
            {
                $WF_ID = $ID;
                $STATUS_ID = CIBlockElement::WF_GetCurrentStatus($WF_ID, $STATUS_TITLE);
                $STATUS_PERMISSION = CIBlockElement::WF_GetStatusPermission($STATUS_ID);
            }

            if (!$errorTriger)
            {
                // check if document is locked
                $isLocked = CIBlockElement::WF_IsLocked($ID, $locked_by, $date_lock);
                if($isLocked)
                {
                    if($locked_by > 0)
                    {
                        $rsUser = CUser::GetList(($by="ID"), ($order="ASC"), array("ID_EQUAL_EXACT" => $locked_by));
                        if($arUser = $rsUser->GetNext())
                            $locked_by = rtrim("[".$arUser["ID"]."] (".$arUser["LOGIN"].") ".$arUser["NAME"]." ".$arUser["LAST_NAME"]);
                    }
                    $error = new _CIBlockError(2, "BLOCKED", GetMessage("IBLOCK_DOCUMENT_LOCKED", array("#ID#"=>$locked_by, "#DATE#"=>$date_lock)));
                    $errorTriger = true;
                }
            }
        }
        elseif ($bBizproc)
        {
            $arDocumentStates = CBPDocument::GetDocumentStates(
                array(MODULE_ID, ENTITY, DOCUMENT_TYPE),
                ($ID > 0) ? array(MODULE_ID, ENTITY, $ID) : null,
                "Y"
            );

            $arCurrentUserGroups = $USER->GetUserGroupArray();
            if ($ID > 0 && is_array($arElement))
            {
                if ($USER->GetID() == $arElement["CREATED_BY"])
                    $arCurrentUserGroups[] = "Author";
            }
            else
            {
                $arCurrentUserGroups[] = "Author";
            }

            if ($ID > 0)
            {
                $canWrite = CBPDocument::CanUserOperateDocument(
                    CBPCanUserOperateOperation::WriteDocument,
                    $USER->GetID(),
                    array(MODULE_ID, ENTITY, $ID),
                    array("AllUserGroups" => $arCurrentUserGroups, "DocumentStates" => $arDocumentStates)
                );
                $canRead = CBPDocument::CanUserOperateDocument(
                    CBPCanUserOperateOperation::ReadDocument,
                    $USER->GetID(),
                    array(MODULE_ID, ENTITY, $ID),
                    array("AllUserGroups" => $arCurrentUserGroups, "DocumentStates" => $arDocumentStates)
                );
            }
            else
            {
                $canWrite = CBPDocument::CanUserOperateDocumentType(
                    CBPCanUserOperateOperation::WriteDocument,
                    $USER->GetID(),
                    array(MODULE_ID, ENTITY, DOCUMENT_TYPE),
                    array("AllUserGroups" => $arCurrentUserGroups, "DocumentStates" => $arDocumentStates, 'sectionId' => $MENU_SECTION_ID)
                );
                $canRead = false;
            }

            if (!$canWrite && !$canRead)
            {
                $error = new _CIBlockError(1, "ACCESS_DENIED", GetMessage("IBLOCK_ACCESS_DENIED_STATUS"));
                $errorTriger = true;
            }
        }
        else
        {
            if($ID > 0 && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit"))
            {
                $error = new _CIBlockError(2, "ACCESS_DENIED", GetMessage("IBLOCK_ACCESS_DENIED_STATUS"));
                $errorTriger = true;
            }
        }
    }

    $denyAutosave = false;
    if ($bWorkflow)
    {
        $denyAutosave = CIBlockElement::WF_IsLocked($ID, $locked_by1, $date_lock1);
    }
    else
    {
        $denyAutosave = ($view=="Y")
            || (
                (($ID <= 0) || $bCopy)
                && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
            )
            || (
                (($ID > 0) && !$bCopy)
                && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit")
            )
            || (
                $bBizproc
                && !$canWrite
            );
    }

    $tabControl = new CAdminForm("form_element_".$IBLOCK_ID, $aTabs, !$bPropertyAjax, $denyAutosave);
    $customTabber = new CAdminTabEngine("OnAdminIBlockElementEdit", array("ID" => $ID, "IBLOCK"=>$arIBlock, "IBLOCK_TYPE"=>$arIBTYPE));
    $tabControl->AddTabs($customTabber);

    if($bCustomForm)
    {
        $tabControl->SetShowSettings(false);
        if ($bCatalog && !empty($arMainCatalog))
        {
            $arMainCatalog['OFFERS_PROPERTY_ID'] = 0;
            $arMainCatalog['OFFERS_IBLOCK_ID'] = 0;
            if ($arMainCatalog['CATALOG_TYPE'] == CCatalogSku::TYPE_FULL || $arMainCatalog['CATALOG_TYPE'] == CCatalogSku::TYPE_PRODUCT)
            {
                $arMainCatalog['OFFERS_PROPERTY_ID'] = $arMainCatalog['SKU_PROPERTY_ID'];
                $arMainCatalog['OFFERS_IBLOCK_ID'] = $arMainCatalog['IBLOCK_ID'];
            }
        }
    }

    if (!$errorTriger)
    {
        //Find out files properties
        $arFileProps = array();
        $propertyIterator = Iblock\PropertyTable::getList(array(
            'select' => array('ID'),
            'filter' => array('=IBLOCK_ID' => $IBLOCK_ID, '=PROPERTY_TYPE' => Iblock\PropertyTable::TYPE_FILE, '=ACTIVE' => 'Y')
        ));
        while ($property = $propertyIterator->fetch())
            $arFileProps[] = $property['ID'];
        unset($property, $propertyIterator);

        //Assembly properties values from $_POST and $_FILES
        $PROP = array();
        if (isset($_POST['PROP']))
            $PROP = $_POST['PROP'];

        //Recover some user defined properties
        if(is_array($PROP))
        {
            foreach($PROP as $k1 => $val1)
            {
                if(is_array($val1))
                {
                    foreach($val1 as $k2 => $val2)
                    {
                        $text_name = preg_replace("/([^a-z0-9])/is", "_", "PROP[".$k1."][".$k2."][VALUE][TEXT]");
                        if(array_key_exists($text_name, $_POST))
                        {
                            $type_name = preg_replace("/([^a-z0-9])/is", "_", "PROP[".$k1."][".$k2."][VALUE][TYPE]");
                            $PROP[$k1][$k2]["VALUE"] = array(
                                "TEXT" => $_POST[$text_name],
                                "TYPE" => $_POST[$type_name],
                            );
                        }
                    }
                }
            }

            foreach($PROP as $k1 => $val1)
            {
                if(is_array($val1))
                {
                    foreach($val1 as $k2 => $val2)
                    {
                        if(!is_array($val2))
                            $PROP[$k1][$k2] = array("VALUE" => $val2);
                    }
                }
            }
        }

        //transpose files array
        // [property id] [value id] = file array (name, type, tmp_name, error, size)
        $files = $_FILES["PROP"];
        if(is_array($files))
        {
            if(!is_array($PROP))
                $PROP = array();
            CAllFile::ConvertFilesToPost($_FILES["PROP"], $PROP);
        }

        foreach($arFileProps as $k1)
        {
            if (isset($PROP_del[$k1]) && is_array($PROP_del[$k1]))
            {
                if (!is_array($PROP[$k1]))
                    $PROP[$k1] = array();
                foreach ($PROP_del[$k1] as $prop_value_id => $tmp)
                {
                    if (!array_key_exists($prop_value_id, $PROP[$k1]))
                        $PROP[$k1][$prop_value_id] = null;
                }
            }

            if (isset($PROP[$k1]) && is_array($PROP[$k1]))
            {
                foreach ($PROP[$k1] as $prop_value_id => $prop_value)
                {
                    $PROP[$k1][$prop_value_id] = CIBlock::makeFilePropArray(
                        $PROP[$k1][$prop_value_id],
                        $PROP_del[$k1][$prop_value_id] === "Y",
                        isset($_POST["DESCRIPTION_PROP"][$k1][$prop_value_id])? $_POST["DESCRIPTION_PROP"][$k1][$prop_value_id]: $_POST["PROP_descr"][$k1][$prop_value_id]
                    );
                }
            }
        }

        $DESCRIPTION_PROP = $_POST["DESCRIPTION_PROP"];
        if(is_array($DESCRIPTION_PROP))
        {
            foreach($DESCRIPTION_PROP as $k1=>$val1)
            {
                foreach($val1 as $k2=>$val2)
                {
                    if(is_set($PROP[$k1], $k2) && is_array($PROP[$k1][$k2]) && is_set($PROP[$k1][$k2], "DESCRIPTION"))
                        $PROP[$k1][$k2]["DESCRIPTION"] = $val2;
                    else
                        $PROP[$k1][$k2] = Array("VALUE"=>$PROP[$k1][$k2], "DESCRIPTION"=>$val2);
                }
            }
        }

        function _prop_value_id_cmp($a, $b)
        {
            if(substr($a, 0, 1)==="n")
            {
                $a = (int)substr($a, 1);
                if(substr($b, 0, 1)==="n")
                {
                    $b = (int)substr($b, 1);
                    if($a < $b)
                        return -1;
                    elseif($a > $b)
                        return 1;
                    else
                        return 0;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                if(substr($b, 0, 1)==="n")
                {
                    return -1;
                }
                else
                {
                    if(preg_match("/^(\\d+):(\\d+)$/", $a, $a_match))
                        $a = (int)$a_match[2];
                    else
                        $a = (int)$a;

                    if(preg_match("/^(\\d+):(\\d+)$/", $b, $b_match))
                        $b = (int)$b_match[2];
                    else
                        $b = (int)$b;

                    if($a < $b)
                        return -1;
                    elseif($a > $b)
                        return 1;
                    else
                        return 0;
                }
            }
        }

        //Now reorder property values
        if(
            is_array($PROP)
            && !empty($arFileProps)
            && !class_exists('\Bitrix\Main\UI\FileInput', true)
        )
        {
            foreach($arFileProps as $id)
            {
                if(is_array($PROP[$id]))
                    uksort($PROP[$id], "_prop_value_id_cmp");
            }
            unset($id);
        }

        if(strlen($arIBlock["EDIT_FILE_BEFORE"])>0 && is_file($_SERVER["DOCUMENT_ROOT"].$arIBlock["EDIT_FILE_BEFORE"]))
        {
            include($_SERVER["DOCUMENT_ROOT"].$arIBlock["EDIT_FILE_BEFORE"]);
        }
        elseif(strlen($arIBTYPE["EDIT_FILE_BEFORE"])>0 && is_file($_SERVER["DOCUMENT_ROOT"].$arIBTYPE["EDIT_FILE_BEFORE"]))
        {
            include($_SERVER["DOCUMENT_ROOT"].$arIBTYPE["EDIT_FILE_BEFORE"]);
        }

        if (
            $bBizproc
            && $canWrite
            && $historyId <= 0
            && $ID > 0
            && $_SERVER['REQUEST_METHOD'] == "GET"
            && isset($_REQUEST["stop_bizproc"]) && strlen($_REQUEST["stop_bizproc"]) > 0
            && check_bitrix_sessid()
        )
        {
            CBPDocument::TerminateWorkflow(
                $_REQUEST["stop_bizproc"],
                array(MODULE_ID, ENTITY, $ID),
                $ar
            );

            if (!empty($ar))
            {
                $str = "";
                foreach ($ar as $a)
                    $str .= $a["message"];
                $error = new _CIBlockError(2, "STOP_BP_ERROR", $str);
            }
            else
            {
                LocalRedirect($APPLICATION->GetCurPageParam("", array("stop_bizproc", "sessid")));
            }
        }

        if(
            $historyId <= 0
            && 'GET' == $_SERVER['REQUEST_METHOD']
            && $bCatalog
            && 0 < $ID
            && check_bitrix_sessid()
        )
        {
            if (CCatalogAdminTools::changeTabs($IBLOCK_ID, $ID, $arMainCatalog))
            {
                $arUrlParams = array(
                    "find_section_section" => intval($find_section_section)
                );
                if ('Y' == $WF)
                    $arUrlParams['WF'] = 'Y';
                if ('' != $return_url)
                    $arUrlParams['return_url'] = $return_url;
                if ($bAutocomplete)
                    $arUrlParams['lookup'] = $strLookup;
                CCatalogAdminTools::addTabParams($arUrlParams);
                LocalRedirect("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, $arUrlParams, "&".$tabControl->ActiveTabParam()));
            }
        }

        if(
            $historyId <= 0
            && 'POST' == $_SERVER['REQUEST_METHOD']
            && strlen($Update) > 0
            && $view != "Y"
            && (!$error)
            && empty($dontsave)
        )
        {
            $DB->StartTransaction();

            if(isset($_POST["IBLOCK_SECTION"]))
            {
                if(is_array($_POST["IBLOCK_SECTION"]))
                {
                    foreach($_POST["IBLOCK_SECTION"] as $i => $parent_section_id)
                    {
                        if(!CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $parent_section_id, "section_element_bind"))
                            unset($_POST["IBLOCK_SECTION"][$i]);
                    }

                    if(empty($_POST["IBLOCK_SECTION"]))
                        unset($_POST["IBLOCK_SECTION"]);
                }
                else
                {
                    if(!CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $_POST["IBLOCK_SECTION"], "section_element_bind"))
                        unset($_POST["IBLOCK_SECTION"]);
                }
            }

            if(!check_bitrix_sessid() && !$bCustomForm)
            {
                $strWarning .= GetMessage("IBLOCK_WRONG_SESSION")."<br>";
                $error = new _CIBlockError(2, "BAD_SAVE", $strWarning);
                $bVarsFromForm = true;
            }
            elseif($WF=="Y" && $bWorkflow && intval($_POST["WF_STATUS_ID"])<=0)
                $strWarning .= GetMessage("IBLOCK_WRONG_WF_STATUS")."<br>";
            elseif($WF=="Y" && $bWorkflow && CIBlockElement::WF_GetStatusPermission($_POST["WF_STATUS_ID"])<1)
                $strWarning .= GetMessage("IBLOCK_ACCESS_DENIED_STATUS")." [".$_POST["WF_STATUS_ID"]."]."."<br>";
            elseif(0 >= $ID && !isset($_POST["IBLOCK_SECTION"]) && !CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "section_element_bind"))
                $strWarning .= GetMessage("IBLOCK_ACCESS_DENIED_SECTION")."<br>";
            elseif(!$customTabber->Check())
            {
                if($ex = $APPLICATION->GetException())
                    $strWarning .= $ex->GetString();
                else
                    $strWarning .= "Error. ";
            }
            else
            {
                if ($bCatalog)
                {
                    $arCatalogItem = array(
                        'IBLOCK_ID' => $IBLOCK_ID,
                        'SECTION_ID' => $MENU_SECTION_ID,
                        'ID' => (!$bCopy ? $ID : 0),
                        'PRODUCT_ID' => (0 < $ID && !$bCopy ? CIBlockElement::GetRealElement($ID) : 0)
                    );
                    if (
                        $arShowTabs['catalog']
                        && file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/catalog/admin/templates/product_edit_validator.php")
                    )
                    {
                        // errors'll be appended to $strWarning;
                        $boolSKUExists = false;
                        if (CCatalogSku::TYPE_FULL == $arMainCatalog['CATALOG_TYPE'])
                        {
                            $boolSKUExists = CCatalogSku::IsExistOffers(($ID > 0 && !$bCopy ? $ID : '-'.$str_TMP_ID), $IBLOCK_ID);
                        }
                        include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/catalog/admin/templates/product_edit_validator.php");
                    }
                    if ($arShowTabs['product_set'])
                    {
                        CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_SET));
                        if (!CCatalogAdminProductSetEdit::checkFormValues($arCatalogItem))
                        {
                            $strWarning .= implode('<br>', CCatalogAdminProductSetEdit::getErrors());
                        }
                    }
                    if ($arShowTabs['product_group'])
                    {
                        CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_GROUP));
                        if (!CCatalogAdminProductSetEdit::checkFormValues($arCatalogItem))
                        {
                            $strWarning .= implode('<br>', CCatalogAdminProductSetEdit::getErrors());
                        }
                    }
                }
                if ($bBizproc)
                {
                    if($canWrite)
                    {
                        $arBizProcParametersValues = array();
                        foreach ($arDocumentStates as $arDocumentState)
                        {
                            if (strlen($arDocumentState["ID"]) <= 0)
                            {
                                $arErrorsTmp = array();

                                $arBizProcParametersValues[$arDocumentState["TEMPLATE_ID"]] = CBPDocument::StartWorkflowParametersValidate(
                                    $arDocumentState["TEMPLATE_ID"],
                                    $arDocumentState["TEMPLATE_PARAMETERS"],
                                    array(MODULE_ID, ENTITY, DOCUMENT_TYPE),
                                    $arErrorsTmp
                                );

                                if (!empty($arErrorsTmp))
                                {
                                    foreach ($arErrorsTmp as $e)
                                        $strWarning .= $e["message"]."<br />";
                                }
                            }
                        }
                    }
                    else
                    {
                        $strWarning .= GetMessage("IBLOCK_ACCESS_DENIED_STATUS")."<br />";
                    }
                }

                if ($strWarning == '')
                {
                    $bs = new CIBlockElement;

                    $arPREVIEW_PICTURE = CIBlock::makeFileArray(
                        array_key_exists("PREVIEW_PICTURE", $_FILES)? $_FILES["PREVIEW_PICTURE"]: $_REQUEST["PREVIEW_PICTURE"],
                        ${"PREVIEW_PICTURE_del"} === "Y",
                        ${"PREVIEW_PICTURE_descr"}
                    );
                    if ($arPREVIEW_PICTURE["error"] == 0)
                        $arPREVIEW_PICTURE["COPY_FILE"] = "Y";

                    $arDETAIL_PICTURE = CIBlock::makeFileArray(
                        array_key_exists("DETAIL_PICTURE", $_FILES)? $_FILES["DETAIL_PICTURE"]: $_REQUEST["DETAIL_PICTURE"],
                        ${"DETAIL_PICTURE_del"} === "Y",
                        ${"DETAIL_PICTURE_descr"}
                    );
                    if ($arDETAIL_PICTURE["error"] == 0)
                        $arDETAIL_PICTURE["COPY_FILE"] = "Y";

                    $arFields = array(
                        "ACTIVE" => $_POST["ACTIVE"],
                        "MODIFIED_BY" => $USER->GetID(),
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "ACTIVE_FROM" => $_POST["ACTIVE_FROM"],
                        "ACTIVE_TO" => $_POST["ACTIVE_TO"],
                        "SORT" => $_POST["SORT"],
                        "NAME" => $_POST["NAME"],
                        "CODE" => trim($_POST["CODE"], " \t\n\r"),
                        "TAGS" => $_POST["TAGS"],
                        "PREVIEW_PICTURE" => $arPREVIEW_PICTURE,
                        "PREVIEW_TEXT" => $_POST["PREVIEW_TEXT"],
                        "PREVIEW_TEXT_TYPE" => $_POST["PREVIEW_TEXT_TYPE"],
                        "DETAIL_PICTURE" => $arDETAIL_PICTURE,
                        "DETAIL_TEXT" => $_POST["DETAIL_TEXT"],
                        "DETAIL_TEXT_TYPE" => $_POST["DETAIL_TEXT_TYPE"],
                        "TMP_ID" => $str_TMP_ID,
                        "PROPERTY_VALUES" => $PROP,
                    );

                    if(isset($_POST["IBLOCK_SECTION"]) && is_array($_POST["IBLOCK_SECTION"]))
                    {
                        $arFields["IBLOCK_SECTION"] = $_POST["IBLOCK_SECTION"];
                    }

                    if($arIBlock["FIELDS"]["IBLOCK_SECTION"]["DEFAULT_VALUE"]["KEEP_IBLOCK_SECTION_ID"] === "Y")
                    {
                        $arFields["IBLOCK_SECTION_ID"] = intval($_POST["IBLOCK_ELEMENT_SECTION_ID"]);
                    }

                    if(COption::GetOptionString("iblock", "show_xml_id", "N")=="Y" && is_set($_POST, "XML_ID"))
                    {
                        $arFields["XML_ID"] = trim($_POST["XML_ID"], " \t\n\r");
                    }

                    if($bEditRights)
                    {
                        if(is_array($_POST["RIGHTS"]) )
                            $arFields["RIGHTS"] = CIBlockRights::Post2Array($_POST["RIGHTS"]);
                        else
                            $arFields["RIGHTS"] = array();
                    }

                    if (is_array($_POST["IPROPERTY_TEMPLATES"]))
                    {
                        $ELEMENT_PREVIEW_PICTURE_FILE_NAME = \Bitrix\Iblock\Template\Helper::convertArrayToModifiers($_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_NAME"]);
                        $ELEMENT_DETAIL_PICTURE_FILE_NAME = \Bitrix\Iblock\Template\Helper::convertArrayToModifiers($_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_NAME"]);

                        $arFields["IPROPERTY_TEMPLATES"] = array(
                            "ELEMENT_META_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_TITLE"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_TITLE"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_META_KEYWORDS" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_KEYWORDS"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_KEYWORDS"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_META_DESCRIPTION" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_DESCRIPTION"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_META_DESCRIPTION"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_PAGE_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PAGE_TITLE"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PAGE_TITLE"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_PREVIEW_PICTURE_FILE_ALT" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_PREVIEW_PICTURE_FILE_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_PREVIEW_PICTURE_FILE_NAME" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_PREVIEW_PICTURE_FILE_NAME"]["INHERITED"]==="N"?
                                $ELEMENT_PREVIEW_PICTURE_FILE_NAME:
                                ""
                            ),
                            "ELEMENT_DETAIL_PICTURE_FILE_ALT" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_DETAIL_PICTURE_FILE_TITLE" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]["INHERITED"]==="N"?
                                $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]["TEMPLATE"]:
                                ""
                            ),
                            "ELEMENT_DETAIL_PICTURE_FILE_NAME" => (
                            $_POST["IPROPERTY_TEMPLATES"]["ELEMENT_DETAIL_PICTURE_FILE_NAME"]["INHERITED"]==="N"?
                                $ELEMENT_DETAIL_PICTURE_FILE_NAME:
                                ""
                            ),
                        );
                    }

                    if ($bWorkflow)
                    {
                        $arFields["WF_COMMENTS"] = $_POST["WF_COMMENTS"];
                        if(intval($_POST["WF_STATUS_ID"])>0)
                        {
                            $arFields["WF_STATUS_ID"] = $_POST["WF_STATUS_ID"];
                        }
                    }

                    if($bBizproc)
                    {
                        $BP_HISTORY_NAME = $arFields["NAME"];
                        if($ID <= 0)
                            $arFields["BP_PUBLISHED"] = "N";
                    }

                    if($ID > 0)
                    {
                        $bCreateRecord = false;
                        $res = $bs->Update($ID, $arFields, $WF=="Y", true, true);
                    }
                    else
                    {
                        $bCreateRecord = true;
                        $ID = $bs->Add($arFields, $bWorkflow, true, true);
                        $res = ($ID > 0);
                        $PARENT_ID = $ID;

                        if ($res)
                        {
                            if ($arShowTabs['sku'])
                            {
                                $arFilter = array('IBLOCK_ID' => $arMainCatalog['IBLOCK_ID'],'=PROPERTY_'.$arMainCatalog['SKU_PROPERTY_ID'] => '-'.$str_TMP_ID);
                                $rsOffersItems = CIBlockElement::GetList(
                                    array(),
                                    $arFilter,
                                    false,
                                    false,
                                    array('ID')
                                );
                                while ($arOfferItem = $rsOffersItems->Fetch())
                                {
                                    CIBlockElement::SetPropertyValues(
                                        $arOfferItem['ID'],
                                        $arMainCatalog['IBLOCK_ID'],
                                        $ID,
                                        $arMainCatalog['SKU_PROPERTY_ID']
                                    );
                                }
                                $boolFlagClear = CIBlockOffersTmp::Delete($str_TMP_ID);
                                $boolFlagClearAll = CIBlockOffersTmp::DeleteOldID($IBLOCK_ID);
                            }
                        }
                    }

                    if (!$res)
                    {
                        $strWarning .= $bs->LAST_ERROR."<br>";
                    }
                    else
                    {
                        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($IBLOCK_ID, $ID);
                        $ipropValues->clearValues();
                    }

                    if ('' == $strWarning && $bCatalog)
                    {
                        $arCatalogItem = array(
                            'IBLOCK_ID' => $IBLOCK_ID,
                            'SECTION_ID' => $MENU_SECTION_ID,
                            'ID' => $ID,
                            'PRODUCT_ID' => CIBlockElement::GetRealElement($ID)
                        );
                        if ($arShowTabs['catalog'])
                        {
                            include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/catalog/admin/templates/product_edit_action.php");
                        }
                        elseif ($arShowTabs['sku'])
                        {
                            CPrice::DeleteByProduct($arCatalogItem['PRODUCT_ID']);
                        }
                        if ($arShowTabs['product_set'])
                        {
                            CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_SET));
                            CCatalogAdminProductSetEdit::saveFormValues($arCatalogItem);
                        }
                        if ($arShowTabs['product_group'])
                        {
                            CCatalogAdminProductSetEdit::setProductFormParams(array('TYPE' => CCatalogProductSet::TYPE_GROUP));
                            CCatalogAdminProductSetEdit::saveFormValues($arCatalogItem);
                        }
                    }
                } // if ($strWarning)

                if ($bBizproc)
                {
                    if ($strWarning == '')
                    {
                        $arBizProcWorkflowId = array();
                        foreach ($arDocumentStates as $arDocumentState)
                        {
                            if (strlen($arDocumentState["ID"]) <= 0)
                            {
                                $arErrorsTmp = array();

                                $arBizProcWorkflowId[$arDocumentState["TEMPLATE_ID"]] = CBPDocument::StartWorkflow(
                                    $arDocumentState["TEMPLATE_ID"],
                                    array(MODULE_ID, ENTITY, $ID),
                                    $arBizProcParametersValues[$arDocumentState["TEMPLATE_ID"]],
                                    $arErrorsTmp
                                );

                                if (!empty($arErrorsTmp))
                                {
                                    foreach ($arErrorsTmp as $e)
                                        $strWarning .= $e["message"]."<br />";
                                }
                            }
                        }
                    }

                    if ($strWarning == '')
                    {
                        $bizprocIndex = intval($_REQUEST["bizproc_index"]);
                        if ($bizprocIndex > 0)
                        {
                            for ($i = 1; $i <= $bizprocIndex; $i++)
                            {
                                $bpId = trim($_REQUEST["bizproc_id_".$i]);
                                $bpTemplateId = intval($_REQUEST["bizproc_template_id_".$i]);
                                $bpEvent = trim($_REQUEST["bizproc_event_".$i]);

                                if (strlen($bpEvent) > 0)
                                {
                                    if (strlen($bpId) > 0)
                                    {
                                        if (!array_key_exists($bpId, $arDocumentStates))
                                            continue;
                                    }
                                    else
                                    {
                                        if (!array_key_exists($bpTemplateId, $arDocumentStates))
                                            continue;
                                        $bpId = $arBizProcWorkflowId[$bpTemplateId];
                                    }

                                    $arErrorTmp = array();
                                    CBPDocument::SendExternalEvent(
                                        $bpId,
                                        $bpEvent,
                                        array("Groups" => $arCurrentUserGroups, "User" => $USER->GetID()),
                                        $arErrorTmp
                                    );

                                    if (!empty($arErrorsTmp))
                                    {
                                        foreach ($arErrorsTmp as $e)
                                            $strWarning .= $e["message"]."<br />";
                                    }
                                }
                            }
                        }

                        $arDocumentStates = null;
                        CBPDocument::AddDocumentToHistory(array(MODULE_ID, ENTITY, $ID), $BP_HISTORY_NAME, $USER->GetID());
                    }
                }
            }

            if($strWarning == '')
            {
                if(!$customTabber->Action())
                {
                    if ($ex = $APPLICATION->GetException())
                        $strWarning .= $ex->GetString();
                    else
                        $strWarning .= "Error. ";
                }
            }

            if($strWarning != '')
            {
                $error = new _CIBlockError(2, "BAD_SAVE", $strWarning);
                $bVarsFromForm = true;
                $DB->Rollback();
            }
            else
            {
                if($bWorkflow)
                    CIBlockElement::WF_UnLock($ID);

                $arFields['ID'] = $ID;
                if (function_exists('BXIBlockAfterSave'))
                    BXIBlockAfterSave($arFields);

                $DB->Commit();

                if(strlen($apply) <= 0 && strlen($save_and_add) <= 0)
                {
                    if ($bAutocomplete)
                    {
                        if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                        {
                            ?><script type="text/javascript">
                            top.<? echo $strLookup; ?>.AddValue(<? echo $ID;?>);
                            top.BX.WindowManager.Get().AllowClose(); top.BX.WindowManager.Get().Close();
                        </script><?
                            die();
                        }
                        else
                        {
                            ?><script type="text/javascript">
                            window.opener.<? echo $strLookup; ?>.AddValue(<? echo $ID;?>);
                            window.close();
                        </script><?
                        }
                    }
                    elseif(strlen($return_url) > 0)
                    {
                        if(strpos($return_url, "#")!==false)
                        {
                            $rsElement = CIBlockElement::GetList(array(), array(
                                "ID" => $ID,
                                "SHOW_NEW" => "Y",
                            ), false, false, array("DETAIL_PAGE_URL"));
                            $arElement = $rsElement->Fetch();
                            if($arElement)
                                $return_url = CIBlock::ReplaceDetailUrl($return_url, $arElement, true, "E");
                        }

                        if(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                        {
                            if($return_url === "reload_absence_calendar")
                            {
                                echo '<script type="text/javascript">top.jsBXAC.__reloadCurrentView();</script>';
                                die();
                            }
                            else
                            {
                                LocalRedirect($return_url);
                            }
                        }
                        else
                        {
                            LocalRedirect($return_url);
                        }
                    }
                    else
                    {
                        LocalRedirect("/bitrix/admin/".CIBlock::GetAdminElementListLink($IBLOCK_ID, array('find_section_section'=>intval($find_section_section))));
                    }
                }
                elseif(strlen($save_and_add) > 0)
                {
                    $params = array(
                        "WF" => ($WF=="Y"? "Y": null),
                        "find_section_section" => intval($find_section_section),
                        "return_url" => (strlen($return_url) > 0? $return_url: null),
                    );
                    if ($IBLOCK_SECTION_ID > 0)
                    {
                        $params["IBLOCK_SECTION_ID"] = intval($IBLOCK_SECTION_ID);
                    }
                    elseif(isset($arFields["IBLOCK_SECTION"]) && !empty($arFields["IBLOCK_SECTION"]))
                    {
                        foreach($arFields["IBLOCK_SECTION"] as $i => $id)
                            $params["IBLOCK_SECTION_ID[".$i."]"] = $id;
                    }
                    if (!empty($arMainCatalog))
                        $params = CCatalogAdminTools::getFormParams($params);

                    if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                    {
                        while(ob_end_clean());
                        $l = CUtil::JSEscape(CIBlock::GetAdminElementEditLink($IBLOCK_ID, 0, array_merge($params, array(
                            "from_module" => "iblock",
                            "bxpublic" => "Y",
                            "nobuttons" => "Y",
                        )), "&".$tabControl->ActiveTabParam()));
                        ?>
                        <script type="text/javascript">
                            top.BX.ajax.get(
                                '/bitrix/admin/<? echo $l; ?>',
                                function (result) {
                                    top.BX.closeWait();
                                    top.window.reloadAfterClose = true;
                                    top.BX.WindowManager.Get().SetContent(result);
                                }
                            );
                        </script>
                        <?
                        die();
                    }
                    else
                    {
                        LocalRedirect("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, 0, $params, "&".$tabControl->ActiveTabParam()));
                    }
                }
                else
                {
                    LocalRedirect("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, array(
                            "WF" => ($WF=="Y"? "Y": null),
                            "find_section_section" => intval($find_section_section),
                            "return_url" => (strlen($return_url) > 0? $return_url: null),
                            "lookup" => $bAutocomplete ? $strLookup : null,
                        ), "&".$tabControl->ActiveTabParam()));
                }
            }
        }

        if(!empty($dontsave) && check_bitrix_sessid())
        {
            if($bWorkflow)
                CIBlockElement::WF_UnLock($ID);

            if(strlen($return_url)>0)
            {
                if ($bAutocomplete)
                {
                    ?><script type="text/javascript">
                    window.opener.<? echo $strLookup; ?>.AddValue(<? echo $ID;?>);
                    window.close();
                </script><?
                }
                elseif(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
                {
                    echo '<script type="text/javascript">top.BX.closeWait(); top.BX.WindowManager.Get().AllowClose(); top.BX.WindowManager.Get().Close();</script>';
                    die();
                }
                else
                {
                    $rsElement = CIBlockElement::GetList(array(), array("=ID" => $ID), false, array("nTopCount" => 1), array("DETAIL_PAGE_URL"));
                    $arElement = $rsElement->Fetch();
                    if($arElement)
                        $return_url = CIBlock::ReplaceDetailUrl($return_url, $arElement, true, "E");
                    LocalRedirect($return_url);
                }
            }
            else
            {
                if ($bAutocomplete)
                {
                    ?><script type="text/javascript">
                    window.opener.<? echo $strLookup; ?>.AddValue(<? echo $ID;?>);
                    window.close();
                </script><?
                }
                else
                {
                    LocalRedirect("/bitrix/admin/".CIBlock::GetAdminElementListLink($IBLOCK_ID, array('find_section_section'=>intval($find_section_section))));
                }
            }
        }
    }

}while(false);

if($error && $error->err_level==1)
{
    if ($bAutocomplete)
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php");
    else
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

    CAdminMessage::ShowOldStyleError($error->GetErrorText());
}
else
{
    if(!$arIBlock["ELEMENT_NAME"])
        $arIBlock["ELEMENT_NAME"] = $arIBTYPE["ELEMENT_NAME"]? $arIBTYPE["ELEMENT_NAME"]: GetMessage("IBEL_E_IBLOCK_ELEMENT");
    if(!$arIBlock["SECTIONS_NAME"])
        $arIBlock["SECTIONS_NAME"] = $arIBTYPE["SECTION_NAME"]? $arIBTYPE["SECTION_NAME"]: GetMessage("IBEL_E_IBLOCK_SECTIONS");

    ClearVars("str_");
    ClearVars("str_prev_");
    ClearVars("prn_");
    $str_SORT="500";

    if (
        !$error
        && $bWorkflow
        && $view != "Y"
        && CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit")
    )
    {
        if(!$bCopy)
            CIBlockElement::WF_Lock($ID);
        else
            CIBlockElement::WF_UnLock($ID);
    }

    if($historyId <= 0 && $view=="Y")
    {
        $WF_ID = $ID;
        $ID = CIBlockElement::GetRealElement($ID);

        if($PREV_ID)
        {
            $prev_result = CIBlockElement::GetByID($PREV_ID);
            $prev_arElement = $prev_result->ExtractFields("str_prev_");
            if(!$prev_arElement)
                $PREV_ID = 0;
        }
    }

    $str_IBLOCK_ELEMENT_SECTION = Array();
    $str_ACTIVE = $arIBlock["FIELDS"]["ACTIVE"]["DEFAULT_VALUE"] === "N"? "N": "Y";
    $str_NAME = htmlspecialcharsbx($arIBlock["FIELDS"]["NAME"]["DEFAULT_VALUE"]);

    $currentTime = time() + CTimeZone::GetOffset();
    if ($arIBlock["FIELDS"]["ACTIVE_FROM"]["DEFAULT_VALUE"] === "=now")
        $str_ACTIVE_FROM = ConvertTimeStamp($currentTime, "FULL");
    elseif ($arIBlock["FIELDS"]["ACTIVE_FROM"]["DEFAULT_VALUE"] === "=today")
        $str_ACTIVE_FROM = ConvertTimeStamp($currentTime, "SHORT");

    $dayOffset = (int)$arIBlock["FIELDS"]["ACTIVE_TO"]["DEFAULT_VALUE"];
    if ($dayOffset > 0)
        $str_ACTIVE_TO = ConvertTimeStamp($currentTime + $dayOffset*86400, "FULL");
    unset($dayOffset);
    unset($currentTime);

    $str_PREVIEW_TEXT_TYPE = $arIBlock["FIELDS"]["PREVIEW_TEXT_TYPE"]["DEFAULT_VALUE"] !== "html"? "text": "html";
    $str_PREVIEW_TEXT = htmlspecialcharsbx($arIBlock["FIELDS"]["PREVIEW_TEXT"]["DEFAULT_VALUE"]);
    $str_DETAIL_TEXT_TYPE = $arIBlock["FIELDS"]["DETAIL_TEXT_TYPE"]["DEFAULT_VALUE"] !== "html"? "text": "html";
    $str_DETAIL_TEXT = htmlspecialcharsbx($arIBlock["FIELDS"]["DETAIL_TEXT"]["DEFAULT_VALUE"]);

    if ($historyId > 0)
    {
        $view = "Y";
        foreach ($arResult["DOCUMENT"]["FIELDS"] as $k => $v)
            ${"str_".$k} = $v;
    }
    else
    {
        $result = CIBlockElement::GetByID($WF_ID);

        if($arElement = $result->ExtractFields("str_"))
        {
            if($str_IN_SECTIONS=="N")
            {
                $str_IBLOCK_ELEMENT_SECTION[] = 0;
            }
            else
            {
                $result = CIBlockElement::GetElementGroups($WF_ID, true, array('ID', 'IBLOCK_ELEMENT_ID'));
                while($ar = $result->Fetch())
                    $str_IBLOCK_ELEMENT_SECTION[] = $ar["ID"];
            }
            $ipropTemlates = new \Bitrix\Iblock\InheritedProperty\ElementTemplates($IBLOCK_ID, $WF_ID);
        }
        else
        {
            $WF_ID=0;
            $ID=0;
            if(is_array($IBLOCK_SECTION_ID))
            {
                foreach($IBLOCK_SECTION_ID as $id)
                    if($id > 0)
                        $str_IBLOCK_ELEMENT_SECTION[] = $id;
            }
            elseif($IBLOCK_SECTION_ID > 0)
            {
                $str_IBLOCK_ELEMENT_SECTION[] = $IBLOCK_SECTION_ID;
            }
            $ipropTemlates = new \Bitrix\Iblock\InheritedProperty\ElementTemplates($IBLOCK_ID, 0);
            $ipropTemlates->getValuesEntity()->setParents($str_IBLOCK_ELEMENT_SECTION);
        }
        $str_IPROPERTY_TEMPLATES = $ipropTemlates->findTemplates();
        $str_IPROPERTY_TEMPLATES["ELEMENT_PREVIEW_PICTURE_FILE_NAME"] = \Bitrix\Iblock\Template\Helper::convertModifiersToArray($str_IPROPERTY_TEMPLATES["ELEMENT_PREVIEW_PICTURE_FILE_NAME"]);
        $str_IPROPERTY_TEMPLATES["ELEMENT_DETAIL_PICTURE_FILE_NAME"] = \Bitrix\Iblock\Template\Helper::convertModifiersToArray($str_IPROPERTY_TEMPLATES["ELEMENT_DETAIL_PICTURE_FILE_NAME"]);
    }

    if($bCopy)
    {
        $str_XML_ID = "";
    }

    if($ID > 0 && !$bCopy)
    {
        if($view=="Y" || ($bBizproc && !$canWrite))
            $APPLICATION->SetTitle($arIBlock["NAME"].": ".$arIBlock["ELEMENT_NAME"].": ".$arElement["NAME"]." - ".GetMessage("IBLOCK_ELEMENT_EDIT_VIEW"));
        else
            $APPLICATION->SetTitle($arIBlock["NAME"].": ".$arIBlock["ELEMENT_NAME"].": ".$arElement["NAME"]." - ".GetMessage("IBLOCK_EDIT_TITLE"));
    }
    else
    {
        $APPLICATION->SetTitle($arIBlock["NAME"].": ".$arIBlock["ELEMENT_NAME"].": ".GetMessage("IBLOCK_NEW_TITLE"));
    }

    if($arIBTYPE["SECTIONS"]=="Y")
        $sSectionUrl = CIBlock::GetAdminSectionListLink($IBLOCK_ID, array('find_section_section'=>0));
    else
        $sSectionUrl = CIBlock::GetAdminElementListLink($IBLOCK_ID, array('find_section_section'=>0));

    if(!defined("CATALOG_PRODUCT"))
    {
        $adminChain->AddItem(array(
            "TEXT" => htmlspecialcharsex($arIBlock["NAME"]),
            "LINK" => htmlspecialcharsbx($sSectionUrl),
        ));

        if($find_section_section > 0)
            $sLastFolder = $sSectionUrl;
        else
            $sLastFolder = '';

        if($find_section_section > 0)
        {
            $nav = CIBlockSection::GetNavChain($IBLOCK_ID, $find_section_section);
            while($ar_nav = $nav->GetNext())
            {
                $sSectionUrl = CIBlock::GetAdminSectionListLink($IBLOCK_ID, array('find_section_section'=>$ar_nav["ID"]));
                $adminChain->AddItem(array(
                    "TEXT" => $ar_nav["NAME"],
                    "LINK" => htmlspecialcharsbx($sSectionUrl),
                ));

                if($ar_nav["ID"] != $find_section_section)
                    $sLastFolder = $sSectionUrl;
            }
        }
    }

    if ($bAutocomplete)
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php");
    elseif ($bPropertyAjax)
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
    else
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

    if($bVarsFromForm)
    {
        if(!isset($ACTIVE)) $ACTIVE = "N"; //It is checkbox. So it is not set in POST.
        $DB->InitTableVarsForEdit("b_iblock_element", "", "str_");
        $str_IBLOCK_ELEMENT_SECTION = $IBLOCK_SECTION;
        $str_IPROPERTY_TEMPLATES = $_POST["IPROPERTY_TEMPLATES"];
    }

    if ($bPropertyAjax)
        $str_IBLOCK_ELEMENT_SECTION = $_REQUEST["IBLOCK_SECTION"];

    if (is_array($str_IBLOCK_ELEMENT_SECTION))
        $str_IBLOCK_ELEMENT_SECTION = array_unique($str_IBLOCK_ELEMENT_SECTION);

    $clearedByCopyProperties = array();
    if ($bCopy)
    {
        $clearedByCopyProperties = CIBlockPropertyTools::getClearedPropertiesID($IBLOCK_ID);
    }
    $arPROP_tmp = array();
    $properties = CIBlockProperty::GetList(
        array("SORT"=>"ASC", "NAME"=>"ASC", "ID" => "ASC"),
        array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "CHECK_PERMISSIONS"=>"N")
    );
    while($prop_fields = $properties->Fetch())
    {
        $prop_values = array();
        $prop_values_with_descr = array();
        if (
            $bPropertyAjax
            && is_array($PROP)
            && array_key_exists($prop_fields["ID"], $PROP)
        )
        {
            $prop_values = $PROP[$prop_fields["ID"]];
            $prop_values_with_descr = $prop_values;
        }
        elseif ($bVarsFromForm)
        {
            if($prop_fields["PROPERTY_TYPE"]=="F")
            {
                $db_prop_values = CIBlockElement::GetProperty($IBLOCK_ID, $WF_ID, "id", "asc", Array("ID"=>$prop_fields["ID"], "EMPTY"=>"N"));
                while($res = $db_prop_values->Fetch())
                {
                    $prop_values[$res["PROPERTY_VALUE_ID"]] = $res["VALUE"];
                    $prop_values_with_descr[$res["PROPERTY_VALUE_ID"]] = array("VALUE"=>$res["VALUE"],"DESCRIPTION"=>$res["DESCRIPTION"]);
                }
            }
            elseif(is_array($PROP))
            {
                if(array_key_exists($prop_fields["ID"], $PROP))
                    $prop_values = $PROP[$prop_fields["ID"]];
                else
                    $prop_values = $PROP[$prop_fields["CODE"]];
                $prop_values_with_descr = $prop_values;
            }
            else
            {
                $prop_values = "";
                $prop_values_with_descr = $prop_values;
            }
        }
        else
        {
            if ($historyId > 0)
            {
                $vx = $arResult["DOCUMENT"]["PROPERTIES"][(strlen(trim($prop_fields["CODE"])) > 0) ? $prop_fields["CODE"] : $prop_fields["ID"]];

                $prop_values = array();
                if (is_array($vx["VALUE"]) && is_array($vx["DESCRIPTION"]))
                {
                    for ($i = 0, $cnt = count($vx["VALUE"]); $i < $cnt; $i++)
                        $prop_values[] = array("VALUE" => $vx["VALUE"][$i], "DESCRIPTION" => $vx["DESCRIPTION"][$i]);
                }
                else
                {
                    $prop_values[] = array("VALUE" => $vx["VALUE"], "DESCRIPTION" => $vx["DESCRIPTION"]);
                }

                $prop_values_with_descr = $prop_values;
            }
            elseif($ID>0)
            {
                if (empty($clearedByCopyProperties) || !in_array($prop_fields["ID"], $clearedByCopyProperties))
                {
                    $db_prop_values = CIBlockElement::GetProperty($IBLOCK_ID, $WF_ID, "id", "asc", array("ID"=>$prop_fields["ID"], "EMPTY"=>"N"));
                    while($res = $db_prop_values->Fetch())
                    {
                        if($res["WITH_DESCRIPTION"]=="Y")
                            $prop_values[$res["PROPERTY_VALUE_ID"]] = array("VALUE"=>$res["VALUE"], "DESCRIPTION"=>$res["DESCRIPTION"]);
                        else
                            $prop_values[$res["PROPERTY_VALUE_ID"]] = $res["VALUE"];
                        $prop_values_with_descr[$res["PROPERTY_VALUE_ID"]] = array("VALUE"=>$res["VALUE"], "DESCRIPTION"=>$res["DESCRIPTION"]);
                    }
                }
            }
        }

        $prop_fields["VALUE"] = $prop_values;
        $prop_fields["~VALUE"] = $prop_values_with_descr;
        $arPROP_tmp[$prop_fields["ID"]] = $prop_fields;
    }
    $PROP = $arPROP_tmp;

    $aMenu = array();
    if ( !$bAutocomplete && !$bPropertyAjax )
    {
        $aMenu = array(
            array(
                "TEXT" => htmlspecialcharsEx($arIBlock["ELEMENTS_NAME"]),
                "LINK" => CIBlock::GetAdminElementListLink($IBLOCK_ID, array('find_section_section'=>intval($find_section_section))),
                "ICON" => "btn_list",
            )
        );

        if (!$bCopy)
        {
            $aMenu[] = array(
                "TEXT"=>GetMessage("IBEL_E_COPY_ELEMENT"),
                "TITLE"=>GetMessage("IBEL_E_COPY_ELEMENT_TITLE"),
                "LINK"=>"/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, array(
                        "IBLOCK_SECTION_ID" => $MENU_SECTION_ID,
                        "find_section_section" => intval($find_section_section),
                        "action" => "copy",
                    )),
                "ICON"=>"btn_copy",
            );
        }

        if ($bCatalog)
        {
            $arCatalogBtns = CCatalogAdminTools::getIBlockElementContentMenu(
                $IBLOCK_ID,
                $ID,
                $arMainCatalog,
                array(
                    "IBLOCK_SECTION_ID" => $MENU_SECTION_ID,
                    "find_section_section" => intval($find_section_section),
                )
            );
            if (!empty($arCatalogBtns))
            {
                $aMenu[] = array('SEPARATOR' => 'Y');
                $aMenu[] = $arCatalogBtns;
            }
        }

        if($ID > 0 && !$bCopy)
        {
            $arSubMenu = array();
            $arSubMenu[] = array(
                "TEXT" => htmlspecialcharsEx($arIBlock["ELEMENT_ADD"]),
                "LINK" => "/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, 0, array(
                        "IBLOCK_SECTION_ID" => $MENU_SECTION_ID,
                        "find_section_section" => intval($find_section_section),
                    )),
                'ICON' => 'edit',
            );
            if (CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_delete"))
            {
                $urlDelete = CIBlock::GetAdminElementListLink($IBLOCK_ID, array('find_section_section'=>intval($find_section_section), 'action'=>'delete'));
                $urlDelete .= '&'.bitrix_sessid_get();
                $urlDelete .= '&ID='.(preg_match('/^iblock_list_admin\.php/', $urlDelete)? "E": "").$ID;
                $arSubMenu[] = array(
                    "TEXT" => htmlspecialcharsEx($arIBlock["ELEMENT_DELETE"]),
                    "ACTION" => "if(confirm('".GetMessageJS("IBLOCK_ELEMENT_DEL_CONF")."'))window.location='".CUtil::JSEscape($urlDelete)."';",
                    'ICON' => 'delete',
                );
            }

            if($bWorkflow && !defined("CATALOG_PRODUCT"))
            {
                $arSubMenu[] = array(
                    "TEXT" => GetMessage("IBEL_HIST"),
                    "LINK" => '/bitrix/admin/iblock_history_list.php?ELEMENT_ID='.$ID.'&type='.urlencode($arIBlock["IBLOCK_TYPE_ID"]).'&lang='.LANGUAGE_ID.'&IBLOCK_ID='.$IBLOCK_ID.'&find_section_section='.intval($find_section_section),
                );
            }

            if (!empty($arSubMenu))
            {
                $aMenu[] = array("SEPARATOR"=>"Y");
                $aMenu[] = array(
                    "TEXT" => GetMessage('IBEL_E_ACTIONS'),
                    "TITLE" => GetMessage('IBEL_E_ACTIONS_TITLE'),
                    "MENU" => $arSubMenu,
                    'ICON' => 'btn_new'
                );
            }
        }

        $context = new CAdminContextMenu($aMenu);
        $context->Show();
    }

    if($error)
        CAdminMessage::ShowOldStyleError($error->GetErrorText());

    $bFileman = Loader::includeModule("fileman");
    $arTranslit = $arIBlock["FIELDS"]["CODE"]["DEFAULT_VALUE"];
    $bLinked = (!strlen($str_TIMESTAMP_X) || $bCopy) && $_POST["linked_state"]!=='N';

    if($customFormFile):
        include($_SERVER["DOCUMENT_ROOT"].$customFormFile);
    else:

        $arMarks = array(
            'CODE' => 'MANUFACTURER',
            'VALUES' => Cars\Cars::getManufacturers()
        );
        $arAutoOptions = Cars\Cars::getAutoOptions();
        $arProperties = Cars\Cars::getChosenOptions();

        $elementID = $_REQUEST['ID'];
        $iblockID = $_REQUEST['IBLOCK_ID'];
        $submitDisabled = true;

        foreach($arAutoOptions as $key=>$option)
        {
            $arAutoOptions[$key]['JS_CLASS'] = '_js-select';

            switch($option['IBLOCK_CODE'])
            {
                case 'MANUFACTURER':
                    $arAutoOptions[$key]['VALUES'] = Cars\Cars::getManufacturers();
                    $arAutoOptions[$key]['JS_CLASS'] .= ' _manufacturer';
                    $arAutoOptions[$key]['REQUIRED'] = true;
                    break;
                case 'MODEL':
                    $arAutoOptions[$key]['JS_CLASS'] .= ' _model';
                    $arAutoOptions[$key]['REQUIRED'] = true;
                    break;
                case 'GENERATION':
                    $arAutoOptions[$key]['JS_CLASS'] .= ' _generation';
                    break;
                case 'SERIE':
                    $arAutoOptions[$key]['JS_CLASS'] .= ' _serie';
                    break;
                case 'MODIFICATION':
                    $arAutoOptions[$key]['JS_CLASS'] .= ' _modification';
                    break;
            }
        }

        if($elementID != 0)
        {
            $elementExists = Cars\Cars::elementExists($elementID);

            if($elementExists)
            {
                $arSelectedProps = array();
                $arElementModelProps =  Cars\Cars::getAdProps($elementID);

                foreach($arAutoOptions as $key=>$option)
                {
                    if(!empty($arElementModelProps[$option['IBLOCK_CODE']]))
                    {
                        $propertyValue = trim($arElementModelProps[$option['IBLOCK_CODE']]);

                        if(!empty($arAutoOptions[$key]['VALUES']))
                        {
                            foreach($arAutoOptions[$key]['VALUES'] as $valKey=>$arVal)
                            {
                                $chosenValue = ($arVal['NAME'] == $propertyValue);
                                $arAutoOptions[$key]['VALUES'][$valKey]['selected'] = $chosenValue;
                                if($chosenValue)
                                {
                                    $arSelectedProps[$option['IBLOCK_CODE']] = $arVal['ID'];
                                }
                            }
                        }
                        else
                        {
                            switch($option['IBLOCK_CODE'])
                            {
                                case 'MODEL':
                                    $arAutoOptions[$key]['VALUES'] = \Ycaweb\Cars\Cars::getModels($arSelectedProps['MANUFACTURER']);

                                    foreach($arAutoOptions[$key]['VALUES'] as $valKey=>$arVal)
                                    {
                                        $chosenValue = ($arVal['NAME'] == $arElementModelProps['MODEL']);
                                        $arAutoOptions[$key]['VALUES'][$valKey]['selected'] = $chosenValue;
                                        if($chosenValue)
                                        {
                                            $arSelectedProps[$option['IBLOCK_CODE']] = $arVal['ID'];
                                            $submitDisabled = false;
                                        }
                                    }
                                    break;
                                case 'GENERATION':
                                    $arAutoOptions[$key]['VALUES'] = \Ycaweb\Cars\Cars::getGenerations($arSelectedProps['MODEL']);

                                    foreach($arAutoOptions[$key]['VALUES'] as $valKey=>$arVal)
                                    {
                                        $chosenValue = ($arVal['NAME'] == $arElementModelProps['GENERATION']);
                                        $arAutoOptions[$key]['VALUES'][$valKey]['selected'] = $chosenValue;
                                        if($chosenValue)
                                        {
                                            $arSelectedProps[$option['IBLOCK_CODE']] = $arVal['ID'];
                                        }
                                    }
                                    break;
                                case 'SERIE':
                                    $arAutoOptions[$key]['VALUES'] = \Ycaweb\Cars\Cars::getSeries($arSelectedProps['GENERATION']);

                                    foreach($arAutoOptions[$key]['VALUES'] as $valKey=>$arVal)
                                    {
                                        $chosenValue = ($arVal['NAME'] == $arElementModelProps['SERIE']);
                                        $arAutoOptions[$key]['VALUES'][$valKey]['selected'] = $chosenValue;
                                        if($chosenValue)
                                        {
                                            $arSelectedProps[$option['IBLOCK_CODE']] = $arVal['ID'];
                                        }
                                    }
                                    break;
                                case 'MODIFICATION':
                                    $arAutoOptions[$key]['VALUES'] = \Ycaweb\Cars\Cars::getModifications($arSelectedProps['SERIE']);

                                    foreach($arAutoOptions[$key]['VALUES'] as $valKey=>$arVal)
                                    {
                                        $chosenValue = ($arVal['NAME'] == $arElementModelProps['MODIFICATION']);
                                        $arAutoOptions[$key]['VALUES'][$valKey]['selected'] = $chosenValue;
                                        if($chosenValue)
                                        {
                                            $arSelectedProps[$option['IBLOCK_CODE']] = $arVal['ID'];
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }

                foreach($arProperties as $key=>$arProp)
                {
                    $elementPropVal = Cars\Cars::getAdProp($elementID, $arProp['IBLOCK_CODE']);

                    if(!empty($elementPropVal))
                    {
                        if( $arProp['TYPE'] == 'N' || $arProp['TYPE'] == 'S' || $arProp['TYPE'] == 'L')
                        {
                            $arProperties[$key]['VALUE'] = $elementPropVal;
                        }
                        elseif($arProp['TYPE'] == 'E' && !empty($arProp['LINK_IBLOCK_ID']))
                        {
                            $dbEl = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $arProp['LINK_IBLOCK_ID'], 'ID' => $elementPropVal), false, false, array('IBLOCK_ID', 'ID', 'NAME'));
                            while($arEl = $dbEl->fetch())
                            {
                                $arProperties[$key]['VALUE'] = $arEl['NAME'];
                            }
                        }
                    }
                }
            }
        }
        else
        {

        }
    ?>
        <div class="adm-detail-content-item-block">
            <form method="POST" id="ad-form">
                <table class="adm-detail-content-table edit-table">
                    <tr class="heading">
                        <td colspan="2"><?=Loc::getMessage('IBLOCK_EDIT_PAGE_CHOOSE_CAR')?></td>
                    </tr>
                    <?foreach($arAutoOptions as $option):?>
                        <tr class="adm-detail-file-row _js-prop-row">
                            <td class="adm-detail-valign-top adm-detail-content-cell-l" width="40%">
                                <?if($option['REQUIRED']):?>
                                    <span class="adm-required-field">
                                        <?=$option['NAME']?>
                                    </span>
                                <?else:?>
                                    <?=$option['NAME']?>
                                <?endif;?>
                            </td>
                            <td width="60%" class="adm-detail-content-cell-r">
                                <select <?=($option['REQUIRED']) ? "required" : ""?> data-prop="<?=$option['IBLOCK_CODE']?>" name="AD_PROPERTY[<?=$option['IBLOCK_CODE']?>]" <?=(empty($option['VALUES'])) ? "disabled" : ""?> class="<?=$option['JS_CLASS']?>">
                                    <option value="">--</option>
                                    <?if(!empty($option['VALUES'])):?>
                                        <?foreach ($option['VALUES'] as $arValue):?>
                                            <option data-val="<?=$arValue['ID']?>" value="<?=$arValue['NAME']?>" <?if($arValue["selected"]):?> selected="selected"<?endif;?>>
                                                <?=$arValue['NAME']?>
                                            </option>
                                        <?endforeach;?>
                                    <?endif;?>
                                </select>
                            </td>
                        </tr>
                    <?endforeach;?>
                    <?if(!empty($arProperties)):?>
                        <tr class="heading">
                            <td colspan="2"><?=Loc::getMessage('IBLOCK_EDIT_PAGE_CAR_CHOSEN_PROPS')?></td>
                        </tr>
                        <?foreach($arProperties as $tableID=>$arProp):?>
                            <tr class="adm-detail-file-row _js-prop-row">
                                <td class="adm-detail-valign-top adm-detail-content-cell-l" width="40%">
                                    <?=$arProp['NAME']?>
                                </td>
                                <td width="60%" class="adm-detail-content-cell-r">
                                   <input name="PROPERTY[<?=$tableID?>]" data-prop="<?=$arProp['IBLOCK_CODE']?>" value="<?=$arProp['VALUE']?>" type="text" />
                                </td>
                            </tr>
                        <?endforeach;?>
                    <?endif;?>
                    <input type="hidden" name="ELEMENT_ID" value="<?=$elementID?>"/>
                </table>
                <input <?if ($submitDisabled) echo "disabled";?> type="submit" class="button submit-btn" id="submit-ad" value="<?echo GetMessage('IBLOCK_APPLY')?>">
            </form>
        </div>
    <?endif;
}
if ($bAutocomplete)
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");
elseif ($bPropertyAjax)
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
else
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");