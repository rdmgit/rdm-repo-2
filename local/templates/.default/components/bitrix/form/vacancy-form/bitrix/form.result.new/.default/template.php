<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?//if ($arResult["isFormErrors"] == "Y"):?>

<script>
var edataChecked = false;
var errors_fields = <?=json_encode($arResult["FORM_ERRORS"]);?>;
$(document).ready(function(){

	$(".order-input-fullrow").removeClass('error');
	for(var i in errors_fields){
		
		$('#'+i).addClass('error')
		
	}
	
	$('.in-vacancy-data').change(function() {
		if ($(this).prop("checked")) {
			
			edataChecked = true;
			$(".form_vacancy_submit").removeAttr("disabled");
			$(".form_vacancy_submit").removeClass("disabled");
		   
		}	else{
			edataChecked = false;
			$(".form_vacancy_submit").attr("disabled","disabled");
			$(".form_vacancy_submit").addClass("disabled");	
		}
	
	});	
});
</script>
<?//=$arResult["FORM_ERRORS_TEXT"];?>

<?//endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>

<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>


	<? 

	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
		
		if(isset($arQuestion["HTML_MODIFY"])){
			
			echo $arQuestion["HTML_MODIFY"];
		}else{
	?>
	
		<div class="order-input-fullrow" id="<?=$FIELD_SID?>">
		<?
		//echo"<pre>";print_r($arQuestion);echo"</pre>";
		if($arQuestion["REQUIRED"]=="Y"){
			
			$placeholder = 'placeholder="'.$arQuestion['CAPTION'].'*"';
		}else{
			$placeholder = 'placeholder="'.$arQuestion['CAPTION'].'"';
			
		}			
			
		$arQuestion["HTML_CODE"] = str_replace('placeholder',$placeholder,$arQuestion["HTML_CODE"]);
		//$arQuestion["HTML_CODE"] = str_replace('#inc#','id="'.$FIELD_SID.'"',$arQuestion["HTML_CODE"]);
		
		if(strstr("placeholder",$arQuestion["HTML_CODE"])){
			
			$title = "Y";
		}
		?>
		<?if(false){?>
			<div>
				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
				<?endif;?>
				<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
				<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
			</div>
		<?}?>
		<?if(!strstr($arQuestion["HTML_CODE"],"placeholder") && $arQuestion["STRUCTURE"][0]["FIELD_TYPE"]!="checkbox"){
			
			echo $arQuestion["CAPTION"];
		}?>
			<?//if(isset($arQuestion["HTML_MODIFY"])){?>
			<?//=$arQuestion["HTML_MODIFY"]?>
			<?//}else{?>
			<?=$arQuestion["HTML_CODE"]?>
			<?//}?>
			<div class="error__block">
			 <div class="error__block-text">Заполните это поле</div>
			</div>
		</div>
	<?
	}} //endwhile
	?>
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<div>
			<div colspan="2"><b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b></div>
		</div>
		<div>
			<div>&nbsp;</div>
			<div><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" /></div>
		</div>
		<div>
			<div><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></div>
			<div><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /></div>
		</div>
<?
} // isUseCaptcha
?>

	
		<div class="order-input-fullrow order-input-row-center">
			
				<?if ($arResult["F_RIGHT"] >= 15):?>
				&nbsp;<input type="hidden" name="web_form_apply" value="Y" />
				<input class="form_vacancy_submit" type="submit" name="web_form_apply" value="Отправить" />
				<?endif;?>
		
			
		</div>
	
</div>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>