<?php
/* Smarty version 3.1.30, created on 2017-06-09 15:59:56
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/statistic_price_filter.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a9bcc2ebae0_43396344',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3648fb9c836ad7410fc1aa4d2d0b2f1a75d3556e' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/statistic_price_filter.html',
      1 => 1496812353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:statistic_export_button.html' => 1,
  ),
),false)) {
function content_593a9bcc2ebae0_43396344 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/home/a/amazuref/new.rdm-import.ru/public_html/dir/framework/module/template/template/smarty/plugins/modifier.date_format.php';
?>

					<div class="data-table-toolbar"><form class="filter" role="form" method="GET">
							<div class="row">
								<div class="col-md-3">
									<select name="user" class="filter_user form-control" data-toggle="tooltip" title="Клиент">
										<option value="">Клиент</option>
										<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['USER']['ELEMENT'])) {?>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['USER']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['login'];?>
 - <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										<?php }?>
									</select>
								</div>
								<div class="col-md-3">
									<select name="company" class="filter_company form-control" data-toggle="tooltip" title="Кампания">
										<option value="">Кампания</option>
										<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['COMPANY']['ELEMENT'])) {?>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['COMPANY']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										<?php }?>
									</select>
								</div>
								<div class="col-md-3">
									<select name="group" class="filter_group form-control" data-toggle="tooltip" title="Группа">
										<option value="">Группа</option>
										<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['GROUP']['ELEMENT'])) {?>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['GROUP']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										<?php }?>
									</select>
								</div>
								<div class="col-md-3">
									<select name="phrase" class="filter_phrase form-control" data-toggle="tooltip" title="Фраза">
										<option value="">Фраза</option>
										<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['PHRASE']['ELEMENT'])) {?>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['PHRASE']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['phrase']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										<?php }?>
									</select>
								</div>
							</div>

							<div class="row" style="margin-top: 2px;">
								<div class="col-md-3">
									<select name="date_start" class="form-control" data-toggle="tooltip" title="Дата начала статистики">
										<option value="">Дата начала</option>
										<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['DATE'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['date_start']) {?> selected<?php }?>><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['VALUE']->value,'%d.%m.%Y');?>
</option>
										<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

									</select>
								</div>							
								<div class="col-md-3">
									<select name="date_end" class="form-control" data-toggle="tooltip" title="Дата окончания статистики">
										<option value="">Дата окончания</option>
										<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['DATE'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['date_end']) {?> selected<?php }?>><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['VALUE']->value,'%d.%m.%Y');?>
</option>
										<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

									</select>
								</div>	
								<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] != 'direct/statistic/price') {?><div class="col-md-3">
									<select name="where" class="form-control" data-toggle="tooltip" title="Где искать">
										<option value="1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 1) {?> selected<?php }?>>Кампании</option>
										<option value="2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 2) {?> selected<?php }?>>Группы</option>
										<option value="3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 3) {?> selected<?php }?>>Фразы</option>
									</select>
								</div>
								<?php } else { ?><div class="col-md-3">
								<?php $_smarty_tpl->_subTemplateRender("file:statistic_export_button.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

								</div><?php }?>
								
								<div class="col-md-1">
									<input type="text" class="form-control" name="number" value="<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'];
} else {
echo $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['number'];
}?>" data-toggle="tooltip" title="Количество записей" placeholder="Количество">
								</div>	
								<div class="col-md-2">	
									<button class="btn btn-default" type="submit">Искать &#128269;</button>
								</div>
		
							</div>
							<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] != 'direct/statistic/price') {?>
							<div class="row" style="margin-top: 2px;">
								<div class="col-md-3">
									<select name="sort" class="form-control" data-toggle="tooltip" title="Сортировка">
										<option value="0"<?php if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'])) {?> selected<?php }?>>Сортировка по умолчанию</option>
										<option value="position"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position') {?> selected<?php }?>>Сортировка по «позиции»&uarr;</option>
										<option value="-position"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position') {?> selected<?php }?>>Сортировка по «позиции»&darr;</option>
										<option value="position_show"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position_show') {?> selected<?php }?>>Сортировка по «Позиция показа»&uarr;</option>
										<option value="-position_show"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position_show') {?> selected<?php }?>>Сортировка по «Позиция показа»&darr;</option>
										<option value="position_click"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position_click') {?> selected<?php }?>>Сортировка по «Позиция клика»&uarr;</option>
										<option value="-position_click"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position_click') {?> selected<?php }?>>Сортировка по «Позиция клика»&darr;</option>
										<option value="position_value"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position_value') {?> selected<?php }?>>Сортировка по «Позиция XML»&uarr;</option>
										<option value="-position_value"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position_value') {?> selected<?php }?>>Сортировка по «Позиция XML»&darr;</option>
										<option value="position_visible"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position_visible') {?> selected<?php }?>>Сортировка по «Видимость, %»&uarr;</option>
										<option value="-position_visible"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position_visible') {?> selected<?php }?>>Сортировка по «Видимость, %»&darr;</option>
										
										<option value="position1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position1') {?> selected<?php }?>>Сортировка по «Ставка 1 место»&uarr;</option>
										<option value="-position1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position1') {?> selected<?php }?>>Сортировка по «Ставка 1 место»&darr;</option>
										<option value="position2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position2') {?> selected<?php }?>>Сортировка по «Ставка 2 место»&uarr;</option>
										<option value="-position2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position2') {?> selected<?php }?>>Сортировка по «Ставка 2 место»&darr;</option>
										<option value="position3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position3') {?> selected<?php }?>>Сортировка по «Ставка 3 место»&uarr;</option>
										<option value="-position3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position3') {?> selected<?php }?>>Сортировка по «Ставка 3 место»&darr;</option>
										<option value="position4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position4') {?> selected<?php }?>>Сортировка по «Ставка 4 место»&uarr;</option>
										<option value="-position4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position4') {?> selected<?php }?>>Сортировка по «Ставка 4 место»&darr;</option>
										<option value="position5"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position5') {?> selected<?php }?>>Сортировка по «Ставка 5 место»&uarr;</option>
										<option value="-position5"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position5') {?> selected<?php }?>>Сортировка по «Ставка 5 место»&darr;</option>
										<option value="position6"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position6') {?> selected<?php }?>>Сортировка по «Ставка 6 место»&uarr;</option>
										<option value="-position6"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position6') {?> selected<?php }?>>Сортировка по «Ставка 6 место»&darr;</option>
										<option value="position7"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'position7') {?> selected<?php }?>>Сортировка по «Ставка 7 место»&uarr;</option>
										<option value="-position7"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-position7') {?> selected<?php }?>>Сортировка по «Ставка 7 место»&darr;</option>
										<option value="price1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price1') {?> selected<?php }?>>Сортировка по «Списываемая цена 1 место»&uarr;</option>
										<option value="-price1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price1') {?> selected<?php }?>>Сортировка по «Списываемая цена 1 место»&darr;</option>
										<option value="price2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price2') {?> selected<?php }?>>Сортировка по «Списываемая цена 2 место»&uarr;</option>
										<option value="-price2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price2') {?> selected<?php }?>>Сортировка по «Списываемая цена 2 место»&darr;</option>
										<option value="price3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price3') {?> selected<?php }?>>Сортировка по «Списываемая цена 3 место»&uarr;</option>
										<option value="-price3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price3') {?> selected<?php }?>>Сортировка по «Списываемая цена 3 место»&darr;</option>
										<option value="price4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price4') {?> selected<?php }?>>Сортировка по «Списываемая цена 4 место»&uarr;</option>
										<option value="-price4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price4') {?> selected<?php }?>>Сортировка по «Списываемая цена 4 место»&darr;</option>
										<option value="price5"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price5') {?> selected<?php }?>>Сортировка по «Списываемая цена 5 место»&uarr;</option>
										<option value="-price5"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price5') {?> selected<?php }?>>Сортировка по «Списываемая цена 5 место»&darr;</option>
										<option value="price6"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price6') {?> selected<?php }?>>Сортировка по «Списываемая цена 6 место»&uarr;</option>
										<option value="-price6"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price6') {?> selected<?php }?>>Сортировка по «Списываемая цена 6 место»&darr;</option>
										<option value="price7"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price7') {?> selected<?php }?>>Сортировка по «Списываемая цена 7 место»&uarr;</option>
										<option value="-price7"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price7') {?> selected<?php }?>>Сортировка по «Списываемая цена 7 место»&darr;</option>
										
				
										<option value="price"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'price') {?> selected<?php }?>>Сортировка по «ставке»&uarr;</option>
										<option value="-price"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-price') {?> selected<?php }?>>Сортировка по «ставке»&darr;</option>
										<option value="real_price"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'real_price') {?> selected<?php }?>>Сортировка по «списываемой цене»&uarr;</option>
										<option value="-real_price"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-real_price') {?> selected<?php }?>>Сортировка по «списываемой цене»&darr;</option>	
										<option value="sum"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'sum') {?> selected<?php }?>>Сортировка по «расходу»&uarr;</option>
										<option value="-sum"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-sum') {?> selected<?php }?>>Сортировка по «расходу»&darr;</option>
										<option value="show"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'show') {?> selected<?php }?>>Сортировка по «показам»&uarr;</option>
										<option value="-show"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-show') {?> selected<?php }?>>Сортировка по «показам»&darr;</option>
										<option value="click"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'click') {?> selected<?php }?>>Сортировка по «кликам»&uarr;</option>
										<option value="-click"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-click') {?> selected<?php }?>>Сортировка по «кликам»&darr;</option>
										<option value="ctr"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'ctr') {?> selected<?php }?>>Сортировка по «CTR»&uarr;</option>
										<option value="-ctr"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-ctr') {?> selected<?php }?>>Сортировка по «CTR»&darr;</option>
										<option value="conversion"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'conversion') {?> selected<?php }?>>Сортировка по «конверсиям»&uarr;</option>
										<option value="-conversion"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-conversion') {?> selected<?php }?>>Сортировка по «конверсиям»&darr;</option>
										<option value="roi"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'roi') {?> selected<?php }?>>Сортировка по «ROI»&uarr;</option>
										<option value="-roi"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-roi') {?> selected<?php }?>>Сортировка по «ROI»&darr;</option>
										<option value="revenue"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'revenue') {?> selected<?php }?>>Сортировка по «доходу»&uarr;</option>
										<option value="-revenue"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-revenue') {?> selected<?php }?>>Сортировка по «доходу»&darr;</option>										

									</select>
								</div>

								<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where']) && $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 3) {?>
								<div class="col-md-3">
									<select name="place" class="form-control" data-toggle="tooltip" title="Позиция">
										<option value="0"<?php if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'])) {?> selected<?php }?>>Все позиции</option>
										<option class="color_red" value="-1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == -1) {?> selected<?php }?>>Не видно или 2 страница &empty;</option>
										<option class="color_yellow" value="1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 1) {?> selected<?php }?>>1 место спец. &uarr;</option>
										<option class="color_yellow" value="2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 2) {?> selected<?php }?>>2 место спец. &uarr;</option>
										<option class="color_yellow" value="3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 3) {?> selected<?php }?>>3 место спец. &uarr;</option>
										<option class="color_green" value="4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 4) {?> selected<?php }?>>4 место гарантии &darr;</option>
										<option class="color_green" value="5"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 5) {?> selected<?php }?>>5 место гарантии &darr;</option>
										<option class="color_green" value="6"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 6) {?> selected<?php }?>>6 место гарантии &darr;</option>
										<option class="color_green" value="7"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 7) {?> selected<?php }?>>7 место гарантии &darr;</option>

									</select>
								</div>
								<div class="col-md-1" title="Отобрать по списываемой цене">
									<span data-toggle="tooltip" title="Отобрать по списываемой цене">Цена</span> <input type="checkbox" name="real" value="1"<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['real'])) {?> checked<?php }?> />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" name="price1" value="<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price1'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price1'];
}?>" data-toggle="tooltip" title="Ставка больше" placeholder=">">
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" name="price2" value="<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price1'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price2'];
}?>" data-toggle="tooltip" title="Ставка меньше" placeholder="<">
								</div>
								<?php }?>							
							</div><?php }?>
						</form></div>
					<?php }
}
