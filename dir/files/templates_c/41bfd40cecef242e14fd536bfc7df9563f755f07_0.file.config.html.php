<?php
/* Smarty version 3.1.30, created on 2017-06-09 15:54:41
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/config.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a9a9172cf83_02143724',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '41bfd40cecef242e14fd536bfc7df9563f755f07' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/config.html',
      1 => 1496812349,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a9a9172cf83_02143724 (Smarty_Internal_Template $_smarty_tpl) {
?>
				<!-- Your awesome content goes here -->
				<div class="row">
					
					
				
				
				<div class="box-info animated fadeInDown">
				<h2><strong>Настройки</strong> </h2>
					
					<form class="form-horizontal save" role="form" action="/direct/save/config/">
					  <div class="form-group" data-toggle="tooltip" title="Пример автовычисленного задания для планировщика задач crontab в панели управления хостингом">
						<label for="input-text" class="col-sm-2 control-label">Планировщик задач</label>
						<div class="col-sm-10">
						  <?php echo $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['cron'];?>
 > /dev/null 2>&1
						</div>
					  </div>
					  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
if (empty($_smarty_tpl->tpl_vars['VALUE']->value['type'])) {?>
					  <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</label>
						<div class="col-sm-10" data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['description'];?>
">
						  <input type="<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['key'] == 'password') {?>password<?php } else { ?>text<?php }?>" class="form-control" id="input-text" name="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['key'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['value'];?>
" placeholder="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['status'] == 2) {?> disabled<?php }?>>
						</div>
					  </div>
					  <?php } else { ?>
					  <div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</label>
						<div class="col-sm-10" data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['description'];?>
">
							<div class="radio">
							  <label>
								<input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['key'];?>
" id="optionsRadios1" value="1"<?php if (!empty($_smarty_tpl->tpl_vars['VALUE']->value['value'])) {?> checked<?php }?>>
								Включено
							  </label>
							</div>
							<div class="radio">
							  <label>
								<input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['key'];?>
" id="optionsRadios2" value="0"<?php if (empty($_smarty_tpl->tpl_vars['VALUE']->value['value'])) {?> checked<?php }?>>
								Выключено
							  </label>
							</div>
						</div>
					  </div>
					  <?php }?>
					  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					    <center><button type="submit" class="btn btn-default"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Save<?php } else { ?>Сохранить<?php }?></button></center>
					</form>
					
				</div>
				<!-- End of your awesome content --><?php }
}
