<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<ul class="sidebar__list">
	<?
	foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
			continue;
	?>
	<?$notActive = ($arItem['PARAMS']['IS_ACTIVE'] == "N");?>
		<li class="sidebar__list-item">
			<a href="<?=$arItem["LINK"]?>" class="sidebar__list-item-link _<?=$arItem['PARAMS']['CLASS_MOD']?> <?=($notActive ? "_disabled" : "")?>">
				<?=$arItem["TEXT"]?>
			</a>
		</li>
	<?endforeach?>

	</ul>
<?endif?>