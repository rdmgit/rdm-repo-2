<?php

//Front-end resource folder
define('ASSETS_PATH', '/local/assets/');

require_once(__DIR__ . '/helper_functions.php');

// Автозагрузка классов
\Bitrix\Main\Loader::registerAutoLoadClasses('ycaweb.tools', array(
	'\Ycaweb\Tools'						=> 'lib/tools.php',
	'Ycaweb\Tools'						=> 'lib/tools.php',
	'\Ycaweb\Holidays'					=> 'lib/holidays.php',
	'\Ycaweb\MobileDetect'				=> 'lib/mobile_detect.php',
	'\Ycaweb\CUserTypeHTMLEditor'		=> 'lib/usertype_html.php',
	'\Ycaweb\CIBlockPropertyMap2GIS'	=> 'lib/iblock_prop_map2gis.php',
	'\Ycaweb\CIBlockPropertyBoolean'	=> 'lib/iblock_prop_boolean.php',
	'\Ycaweb\CIBlockPropertyColor'		=> 'lib/iblock_prop_color.php',
));