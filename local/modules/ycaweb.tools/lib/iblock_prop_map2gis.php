<?php

namespace Ycaweb;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

Loc::loadMessages(__FILE__);

class CIBlockPropertyMap2GIS{

	public static function GetUserTypeDescription(){
		return array(
			'PROPERTY_TYPE'			=> 'S',
			'USER_TYPE'				=> 'map2gis',
			'DESCRIPTION'			=> Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS'),
			'PrepareSettings'		=> array(__CLASS__, 'PrepareSettings'),
			'GetSettingsHTML' 		=> array(__CLASS__, 'GetSettingsHTML'),
			'GetAdminListViewHTML'	=> array(__CLASS__, 'GetAdminListViewHTML'),
			'GetPropertyFieldHtml'	=> array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetPublicViewHTML'		=> array(__CLASS__, 'GetPublicViewHTML'),
			'GetPublicEditHTML'		=> array(__CLASS__, 'GetPublicEditHTML'),
			'ConvertToDB'			=> array(__CLASS__, 'ConvertToDB'),
			'ConvertFromDB'			=> array(__CLASS__, 'ConvertFromDB'),
		);
	}

	/*
	 * @param array $arProperty
	 * @return array
	 */
	public static function PrepareSettings($arProperty){
		$width = '100%';
		$height = '340';
		if(is_array($arProperty['USER_TYPE_SETTINGS'])){
			if(!empty($arProperty['USER_TYPE_SETTINGS']['WIDTH'])){
				$width = (int)$arProperty['USER_TYPE_SETTINGS']['WIDTH'].(preg_match('/[0-9]+%$/i', $arProperty['USER_TYPE_SETTINGS']['WIDTH']) ? '%' : '');
			}
			if(!empty($arProperty['USER_TYPE_SETTINGS']['HEIGHT'])){
				$height = (int)$arProperty['USER_TYPE_SETTINGS']['HEIGHT'].(preg_match('/[0-9]+%$/i', $arProperty['USER_TYPE_SETTINGS']['HEIGHT']) ? '%' : '');
			}
		}

		return array(
			'WIDTH'		=> $width,
			'HEIGHT'	=> $height,
		);
	}

	/**
	 * @param $arProperty
	 * @param $strHTMLControlName
	 * @param $arPropertyFields
	 * @return string
	 */
	public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields){
		$strReturn = '';

		$arSettings = self::PrepareSettings($arProperty);

		$arPropertyFields = array(
			'HIDE' => array('ROW_COUNT', 'COL_COUNT', 'WITH_DESCRIPTION', 'SMART_FILTER')
		);

		ob_start();
		?>
		<tr>
			<td colspan="2" style="padding-bottom: 10px; font-size: 11px;">
				<?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_SETTINGS_WARNING')?>
			</td>
		</tr>
		<tr>
			<td class="adm-detail-content-cell-l"><?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_SETTINGS_WIDTH')?>:</td>
			<td class="adm-detail-content-cell-r">
				<input type="text" name="<?=$strHTMLControlName['NAME']?>[WIDTH]" value="<?=$arSettings['WIDTH']?>">
			</td>
		</tr>
		<tr>
			<td class="adm-detail-content-cell-l"><?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_SETTINGS_HEIGHT')?>:</td>
			<td class="adm-detail-content-cell-r">
				<input type="text" name="<?=$strHTMLControlName['NAME']?>[HEIGHT]" value="<?=$arSettings['HEIGHT']?>">
			</td>
		</tr>
		<?
		$strReturn = ob_get_contents();
		ob_end_clean();

		return $strReturn;
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @param $strHTMLControlName
	 * @return mixed
	 */
	public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName){
		return $value['VALUE'];
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName){
		global $APPLICATION;

		if(isset($GLOBALS['ycaweb_map2GISLastNumber'])){
			$GLOBALS['ycaweb_map2GISLastNumber']++;
		}else{
			$GLOBALS['ycaweb_map2GISLastNumber'] = 0;

			if(!isset($_SERVER['HTTP_BX_AJAX']) && $_SERVER['HTTP_BX_AJAX'] != true){
				$APPLICATION->AddHeadString('<script src="http://maps.api.2gis.ru/2.0/loader.js?pkg=full&skin=light&lazy=true" data-id="dgLoader"></script>', true);
			}
			$APPLICATION->AddHeadScript('/bitrix/js/ycaweb.tools/prop_map2gis.min.js', true);
		}

		if($strHTMLControlName["MODE"] != "FORM_FILL")
			return '<input type="text" name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" value="'.htmlspecialcharsbx($value['VALUE']).'" />';

		$bHasValue = false;

		if(!empty($value['VALUE'])){
			list($POINT_LAT, $POINT_LON, $ZOOM) = explode(',', $value['VALUE'], 3);
			if($POINT_LAT && $POINT_LON)
				$bHasValue = true;
		}

		$MAP_ID = 'ycaweb_map2gis_'.$arProperty['ID'].'_'.$GLOBALS['ycaweb_map2GISLastNumber'];
		$MAP_ID = strtolower($MAP_ID);
		$MAP_OBJ = 'obj_'.$MAP_ID;
		?>
		<script type="text/javascript">
			DG.then(function(){
				window.<?=$MAP_OBJ?> = new propYcaweb2GISMap('<?=$MAP_ID?>'<?echo ', '.(($bHasValue) ? '['.$POINT_LAT.','.$POINT_LON.']' : 'null');?><?if(!empty($ZOOM)):?>, <?=$ZOOM?><?endif;?>);
			});
		</script>
		<div<?if($arProperty["MULTIPLE"] == "Y"):?> style="margin-bottom: 15px;"<?endif;?>>
			<div id="bx_map_hint_<?=$MAP_ID?>" style="margin-bottom: 15px;">
				<div id="bx_map_hint_value_<?=$MAP_ID?>" style="display: <?echo $bHasValue ? 'block' : 'none'?>;">
					<?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_INSTR');?><br /><br />
					<a href="javascript:void(0);" onclick="<?=$MAP_OBJ?>.toMarker(); return false;"><?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_GOTO_POINT')?></a> | <a href="javascript:void(0);" onclick="<?=$MAP_OBJ?>.clearValue(); return false;"><?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_REMOVE_POINT')?></a>
				</div>
				<div id="bx_map_hint_novalue_<?=$MAP_ID?>" style="display: <?echo $bHasValue ? 'none' : 'block'?>;">
					<?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_INSTR_VALUE');?>
				</div>
			</div>
			<div id="<?=$MAP_ID?>" style="width:95%; height:400px"></div>
			<div id="bx_address_search_control_<?=$MAP_ID?>" style="margin-top: 10px; display: none;">
				<?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_SEARCH')?>
				<input id="bx_address_<?=$MAP_ID?>" type="text" name="bx_address_<?=$MAP_ID?>" value="" style="width: 300px;" autocomplete="off" />
			</div>
			<div style="margin-top: 10px;">
				<input id="point_lat_<?=$MAP_ID;?>" type="text" name="point_lat_<?=$MAP_ID;?>" value="<?=$POINT_LAT?>" onchange="">,
				<input id="point_lon_<?=$MAP_ID;?>" type="text" name="point_lon_<?=$MAP_ID;?>" value="<?=$POINT_LON?>" onchange="">
			</div>
			<input id="value_<?=$MAP_ID;?>" type="hidden" name="<?=htmlspecialcharsbx($strHTMLControlName["VALUE"])?>" value="<?=htmlspecialcharsEx($value["VALUE"])?>" />
		</div>
	<?
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @param $arParams
	 * @return string
	 */
	public static function GetPublicViewHTML($arProperty, $value, $arParams){
		$arSettings = self::PrepareSettings($arProperty);
		$strReturn = '';

		if(!empty($value["VALUE"]))
		{
			global $APPLICATION;

			if(isset($GLOBALS['ycaweb_map2GISLastNumber_'.$arProperty['ID']])){
				$GLOBALS['ycaweb_map2GISLastNumber_'.$arProperty['ID']]++;
			}else{
				$GLOBALS['ycaweb_map2GISLastNumber_'.$arProperty['ID']] = 0;

				$APPLICATION->AddHeadString('<script src="http://maps.api.2gis.ru/2.0/loader.js?pkg=basic&skin=light&lazy=true" data-id="dgLoader"></script>', true);
				$APPLICATION->AddHeadScript('/bitrix/js/ycaweb.map2gis/prop_map2gis.min.js');
			}

			$MAP_WIDTH = (int)$arProperty['USER_TYPE_SETTINGS']['WIDTH'].(preg_match('/[0-9]+%$/i', $arProperty['USER_TYPE_SETTINGS']['WIDTH']) ? '%' : 'px');
			$MAP_HEIGHT = (int)$arProperty['USER_TYPE_SETTINGS']['HEIGHT'].(preg_match('/[0-9]+%$/i', $arProperty['USER_TYPE_SETTINGS']['HEIGHT']) ? '%' : 'px');

			if($arProperty['MULTIPLE'] == 'Y'){

				$propertyValueID = $arProperty['PROPERTY_VALUE_ID'][$GLOBALS['ycaweb_map2GISLastNumber_'.$arProperty['ID']]];
			}else{
				$propertyValueID = $arProperty['PROPERTY_VALUE_ID'];
			}

			list($POINT_LAT, $POINT_LON, $ZOOM) = explode(',', $value['VALUE'], 3);

			$arPoints = array(
				array(
					'coord' => array('lat' => $POINT_LAT, 'lng' => $POINT_LON)
				)
			);

			$MAP_ID = 'ycaweb_map2gis_'.$arProperty['ID'].'_'.$propertyValueID;
			$MAP_ID = strtolower($MAP_ID);
			$MAP_OBJ = 'obj_'.$MAP_ID;

			ob_start();
			?>
			<script type="text/javascript">
				DG.then(function(){
					window.<?=$MAP_OBJ?> = new displayPropYcaweb2GISMap('<?=$MAP_ID?>', <?=json_encode($arPoints);?><?if(!empty($ZOOM)):?>, <?=$ZOOM?><?endif;?>);
				});
			</script>
			<div id="<?=$MAP_ID?>" style="width:<?=$MAP_WIDTH?>; height:<?=$MAP_HEIGHT?>"><?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_MAP_LOADING');?></div>
			<?
			$strReturn = ob_get_clean();
		}

		return $strReturn;
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPublicEditHTML($arProperty, $value, $strHTMLControlName){
		$strReturn = '';
		$arSettings = self::PrepareSettings($arProperty);

		global $APPLICATION;

		if(isset($GLOBALS['ycaweb_map2GISLastNumber'])){
			$GLOBALS['ycaweb_map2GISLastNumber']++;
		}else{
			$GLOBALS['ycaweb_map2GISLastNumber'] = 0;

			$APPLICATION->AddHeadString('<script src="http://maps.api.2gis.ru/2.0/loader.js?pkg=full&skin=light&lazy=true" data-id="dgLoader"></script>', true);
			$APPLICATION->AddHeadScript('/bitrix/js/ycaweb.map2gis/prop_map2gis.min.js', true);
		}

		$bHasValue = false;

		if(!empty($value['VALUE'])){
			list($POINT_LAT, $POINT_LON, $ZOOM) = explode(',', $value['VALUE'], 3);
			if($POINT_LAT && $POINT_LON)
				$bHasValue = true;
		}

		$MAP_ID = 'ycaweb_map2gis_'.$arProperty['ID'].'_'.$GLOBALS['ycaweb_map2GISLastNumber'];
		$MAP_ID = strtolower($MAP_ID);
		$MAP_OBJ = 'obj_'.$MAP_ID;

		ob_start();
		?>
		<script type="text/javascript">
			DG.then(function(){
				window.<?=$MAP_OBJ?> = new propYcaweb2GISMap('<?=$MAP_ID?>'<?echo ', '.(($bHasValue) ? '['.$POINT_LAT.','.$POINT_LON.']' : 'null');?><?if(!empty($ZOOM)):?>, <?=$ZOOM?><?endif;?>);
			});
		</script>
		<div<?if($arProperty["MULTIPLE"] == "Y"):?> style="margin-bottom: 15px;"<?endif;?>>
			<div id="bx_map_hint_<?=$MAP_ID?>" style="margin-bottom: 15px;">
				<div id="bx_map_hint_value_<?=$MAP_ID?>" style="display: <?echo $bHasValue ? 'block' : 'none'?>;">
					<?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_INSTR');?><br /><br />
					<a href="javascript:void(0);" onclick="<?=$MAP_OBJ?>.toMarker(); return false;"><?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_GOTO_POINT')?></a> | <a href="javascript:void(0);" onclick="<?=$MAP_OBJ?>.clearValue(); return false;"><?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_REMOVE_POINT')?></a>
				</div>
				<div id="bx_map_hint_novalue_<?=$MAP_ID?>" style="display: <?echo $bHasValue ? 'none' : 'block'?>;">
					<?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_INSTR_VALUE');?>
				</div>
			</div>
			<div id="<?=$MAP_ID?>" class="ycaweb_map2gis_render-area" style="width:100%; height:400px"></div>
			<div id="bx_address_search_control_<?=$MAP_ID?>" style="margin-top: 10px; display: none;">
				<?=Loc::getMessage('IBLOCK_PROP_YCAWEB_MAP2GIS_SEARCH')?>
				<input id="bx_address_<?=$MAP_ID?>" type="text" name="bx_address_<?=$MAP_ID?>" value="" style="width: 300px;" autocomplete="off" />
			</div>
			<div style="margin-top: 10px;">
				<input id="point_lat_<?=$MAP_ID;?>" type="text" name="point_lat_<?=$MAP_ID;?>" value="<?=$POINT_LAT?>" onchange="">,
				<input id="point_lon_<?=$MAP_ID;?>" type="text" name="point_lon_<?=$MAP_ID;?>" value="<?=$POINT_LON?>" onchange="">
			</div>
			<input id="value_<?=$MAP_ID;?>" type="hidden" name="<?=htmlspecialcharsbx($strHTMLControlName["VALUE"])?>" value="<?=htmlspecialcharsEx($value["VALUE"])?>" />
		</div>
		<?
		$strReturn = ob_get_clean();

		return $strReturn;
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return array
	 */
	public static function ConvertToDB($arProperty, $value){
		$arResult = array('VALUE' => '');

		if (strlen($value['VALUE']) > 0)
		{
			$arCoords = explode(',', $value['VALUE'], 3);

			$lat = doubleval($arCoords[0]);
			$lng = doubleval($arCoords[1]);
			$zoom = intval($arCoords[2]);

			if($lat && $lng){
				$arResult['VALUE'] = $lat.','.$lng;

				if($zoom){
					$arResult['VALUE'] .= ','.$zoom;
				}
			}
		}

		return $arResult;
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return array
	 */
	public static function ConvertFromDB($arProperty, $value){
		$arResult = array('VALUE' => '');

		if (strlen($value['VALUE']) > 0)
		{
			$arCoords = explode(',', $value['VALUE'], 3);

			$lat = doubleval($arCoords[0]);
			$lng = doubleval($arCoords[1]);
			$zoom = intval($arCoords[2]);

			if($lat && $lng){
				$arResult['VALUE'] = $lat.','.$lng;

				if($zoom){
					$arResult['VALUE'] .= ','.$zoom;
				}
			}
		}

		return $arResult;
	}

}