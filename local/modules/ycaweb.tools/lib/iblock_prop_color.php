<?php

namespace Ycaweb;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

Loc::loadMessages(__FILE__);

class CIBlockPropertyColor{

	public static function GetUserTypeDescription(){
		return array(
			'PROPERTY_TYPE'			=> 'S',
			'USER_TYPE'				=> 'color',
			'DESCRIPTION'			=> Loc::getMessage('YCAWEB_IBLOCK_PROP_COLOR'),
			'PrepareSettings'		=> array(__CLASS__, 'PrepareSettings'),
			'GetSettingsHTML' 		=> array(__CLASS__, 'GetSettingsHTML'),
			'GetAdminListViewHTML'	=> array(__CLASS__, 'GetAdminListViewHTML'),
			'GetPropertyFieldHtml'	=> array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetPublicViewHTML'		=> array(__CLASS__, 'GetPublicViewHTML'),
			'GetPublicEditHTML'		=> array(__CLASS__, 'GetPublicEditHTML'),
			'ConvertToDB'			=> array(__CLASS__, 'ConvertToDB'),
			'ConvertFromDB'			=> array(__CLASS__, 'ConvertFromDB'),
		);
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return mixed
	 */
	public static function ConvertToDB($arProperty, $value){
		$value['VALUE'] = trim($value['VALUE']);

		return $value;
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return mixed
	 */
	public static function ConvertFromDB($arProperty, $value){
		return $value;
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPropertyFieldHTML($arProperty, $arValue, $strHTMLControlName){
		global $APPLICATION;
		$APPLICATION->AddHeadScript('/bitrix/js/ycaweb.tools/jscolor/jscolor.js', true);

		if($arValue['VALUE'] === null || $arValue['VALUE'] === false || $arValue['VALUE'] === ''){
			$arValue['VALUE'] = $arProperty['DEFAULT_VALUE'];
		}

		$strReturn = '';
		ob_start();
	?>
		<div class="ycaweb-prop-color">
			<span class="ycaweb-prop-color__inner">
				<input type="text" class="ycaweb-prop-color_input {hash:true, pickerPosition:'right'}" name="<?=htmlspecialcharsbx($strHTMLControlName['VALUE'])?>" value="<?=$arValue['VALUE']?>" size="5">
			</span>
		</div>
	<?
		$strReturn = ob_get_clean();

		return $strReturn;
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName){

		if(empty($arValue['VALUE'])){
			return Loc::getMessage('YCAWEB_IBLOCK_PROP_COLOR_ABSENT');
		}

		return '<div style="display:inline-block; width:35px; height:15px; background-color:'.$arValue['VALUE'].'"></div>';
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPublicEditHtml($arProperty, $arValue, $strHTMLControlName){
		$strResult = '';
		//@TODO: Редактирование в публичном интерфейсе
		return $strResult;
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPublicViewHTML($arProperty, $arValue, $strHTMLControlName){
		return '<div class="public-color-view" style="display:inline-block; width:35px; height:15px; vertical-align: middle; background-color:'.$arValue['VALUE'].'"></div>';
	}

}