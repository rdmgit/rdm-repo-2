<?    define("NO_KEEP_STATISTIC", true);
    define("NO_AGENT_CHECK", true);
    define('PUBLIC_AJAX_MODE', true);
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
    $APPLICATION->ShowIncludeStat = false;
 
    
    if(isset($_REQUEST["data"])){
        
        $REQUEST = array();
        $data = $_REQUEST["data"];
      
        foreach($data as $field){
            
            if(!empty($field["name"]) && strstr($field["name"], 'zayavka_')){
                
                $REQUEST[$field["name"]] = htmlspecialchars($field["value"],ENT_QUOTES);
                
            }
            
        }
        
        $type = !empty($_REQUEST["type"])?htmlspecialchars($_REQUEST["type"],ENT_QUOTES):"osago";
        

        CModule::IncludeModule('iblock');
        CModule::IncludeModule("form");
    
    
        $rsFieldList = CFormField::GetList(10, "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
        $arFieldList = array();
        $arEventFields = array();
        $arFields = array();
        $arErrors = array();
       
        while ($arField = $rsFieldList->GetNext())
        {

            if(!empty($REQUEST[$arField["SID"]])){
                $val = htmlspecialchars($REQUEST[$arField["SID"]], ENT_QUOTES);
                $arFieldList[$arField["SID"]] = $val;
                $arEventFields["form_".$arField["TITLE_TYPE"]."_".$arField["ID"]] = $val;
        
            }else{
                $arErrors[$arField["SID"]] = $arField["SID"];
                
            }
            
        }
        //$title = "Заказать расчет КАСКО";
        if($type=="osago"){
			
			$title = "Хочу заказать расчет ОСАГО";
			
		}elseif($type=="kasko"){
			
			$title = "Хочу заказать расчет КАСКО";
		}
		
        /* Cогласие обработки персональных данных */        
        if(isset($arFieldList["zayavka_edata"])){$edata = "Да";}else{$edata = "Нет";}
        $arFieldList["zayavka_edata"] = $edata;
        $arFieldList["title"] = $title;
        $arEventFields["form_text_36"] = $edata;
        $arEventFields["form_hidden_67"] = $title;
        
        /* Тип формы */
        CEvent::Send("SIMPLE_FORM_4", 's1', $arFieldList,"N",63);

        if ($RESULT_ID = CFormResult::Add(10, $arEventFields))
        {
           $arResult["send"] = "Y";
        }else{
           $arResult["send"] = "N";
        }
                    
		$arResult = array("status"=>true);
    
    
    }else{
        
        $arResult = array("status"=>false);
        
    }
    
    echo json_encode($arResult);
?>