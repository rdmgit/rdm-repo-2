<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

/*interface DB {
    public function getFields(array $arFilter, array $arSort, $count);
    public function addFields(array $arFields);
    public function updateFields($id, array $arFields);
    public function remove($id);
}*/

class HLBlock /*implements DB*/ {


    protected $id;

    public function __construct($id) {
        $this->id = $id;
    }


    /**
     * Получаем записи из таблицы
     * @param array $arFilter фильтр по полям
     * @return array список записей
     */
    public function getField(array $arFilter = array(), array $arSelect = array('*'),array $arSort = array("ID" => "DESC"), $count = 0) {

        $hlblock = HL\HighloadBlockTable::getList(array('filter'=>array('NAME'=>$this->id)))->fetch();
// p($this->id, 'id'); 
// p($hlblock, 'hlblock'); 
        $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
        $entity_data_class = $entity->getDataClass();

        $entity_table_name = $hlblock['TABLE_NAME'];//получим имя таблицы
//p($entity_table_name, 'entity_table_name');
        $sTableID = 'tbl_'.$entity_table_name;
//p($arFilter, 'arFilter');
        $rsData = $entity_data_class::getList(array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arSort, // сортировка по полю, будет работать только если вы завели такое поле в hl'блоке
            "limit" => $count
        ));
        $rsData = new CDBResult($rsData, $sTableID);
        $arData = array();
        if($arRes = $rsData->Fetch()){
            return $arRes;
        }
        else
            return false;
    }
    
    /**
     * Получаем записи из таблицы
     * @param array $arFilter фильтр по полям
     * @return array список записей
     */
    public function getFields(array $arFilter = array(),array $arSort = array("ID" => "DESC"), $count = 0) {

        $hlblock = HL\HighloadBlockTable::getList(array('filter'=>array('NAME'=>$this->id)))->fetch();
// p($this->id, 'id'); 
// p($hlblock, 'hlblock'); 
        $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
        $entity_data_class = $entity->getDataClass();

        $entity_table_name = $hlblock['TABLE_NAME'];//получим имя таблицы
//p($entity_table_name, 'entity_table_name');
        $sTableID = 'tbl_'.$entity_table_name;
//p($arFilter, 'arFilter');
        $rsData = $entity_data_class::getList(array(
            "select" => array('*'), //выбираем все поля
            "filter" => $arFilter,
            "order" => $arSort, // сортировка по полю, будет работать только если вы завели такое поле в hl'блоке
            "limit" => $count
        ));
        $rsData = new CDBResult($rsData, $sTableID);
        $arData = array();
        while($arRes = $rsData->Fetch()){
            $arData[$arRes['ID']] = $arRes;
        }
            
        return $arData;
    }

    /**
     * Добавляет запись в таблицу
     * @param array $arFields Поля таблицы
     * @return bool
     */
    public function addFields(array $arFields = array()){
        if(is_array($arFields)){
            $hlblock = HL\HighloadBlockTable::getList(array('filter'=>array('NAME'=>$this->id)))->fetch();
            $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
            $entity_data_class = $entity->getDataClass();


            $result = $entity_data_class::add($arFields);
            if(!$result->isSuccess()){
                return $result->getErrorMessages(); //выведем ошибку
            } else {
                //echo $result->getId();
                return $result->getId();
            }


        } else {
            return false;
        }
    }

    /**
     * Обновляем запись
     * @param $id int запись
     * @param array $arFields поля записи
     * @return bool
     */
    public function updateFields($id ,array $arFields = array()){
        if(is_array($arFields)){
            $hlblock = HL\HighloadBlockTable::getList(array('filter'=>array('NAME'=>$this->id)))->fetch();
            $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
            $entity_data_class = $entity->getDataClass();

            if ($id){
                $result = $entity_data_class::update($id, $arFields);
                if(!$result->isSuccess()){ //произошла ошибка
                    return $result->getErrorMessages(); //выведем ошибку
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Удаляет запись в таблице
     * @param $id int запись
     * @return bool
     */
    public function remove($id){
        if($id){
            $hlblock = HL\HighloadBlockTable::getList(array('filter'=>array('NAME'=>$this->id)))->fetch();
            $entity   = HL\HighloadBlockTable::compileEntity( $hlblock );
            $entity_data_class = $entity->getDataClass();

            $result = $entity_data_class::delete($id);
            if(!$result->isSuccess()){ //произошла ошибка
                echo $result->getErrorMessages(); //выведем ошибку
            }
        } else {
            return false;
        }
    }
}
?>