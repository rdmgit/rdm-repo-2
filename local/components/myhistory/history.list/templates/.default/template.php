<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<div class="page-history__wrap">
<?if($arResult["ITEMS"]){
    $arItems = $arResult["ITEMS"];
    $ind=0;
    foreach($arItems as $arItem){                
        $style = ($ind%2)?'history__item-right':'history__item-left';
        $this->AddEditAction($arItem['ID'], $arItem['ADD_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_ADD"));
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
	<div class="history__item <?=$style?> history__x-45"  data-ind="<?=($ind%2)?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">		
		<div class="history__item-head">
		<div class="history__head-empty"></div>
		<h2><?=$arItem["~NAME"]?></h2>
		</div>
		<div class="history__item-media">
        <?if(count($arItem["ALBUM"])>0){?>            
		<div class="chrono-list__media">
			<a id="<?=$arItem["ID"]?>" class="fancybox-HistoryPopup" href="javascript:;"><img src="<?=$arItem["ALBUM"][0]["SRC"]?>" alt="..." /></a>
		</div>            
        <?if(!empty($arItem["ALBUM"][0]["DESCRIPTION"])){?>
        <div class="history__item-description"><?=$arItem["ALBUM"][0]["DESCRIPTION"]?></div>        
        <?}?>
        <?}?>
        </div>

		<div class="history__item-text">
         
        <?=$arItem["~DETAIL_TEXT"]?> 
			</div>
	</div>
<?$ind++;}}?>
</div>
<script>var jsHistoryPic = <?=json_encode($arResult["JSON"]);?></script>