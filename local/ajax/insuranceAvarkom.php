<?    define("NO_KEEP_STATISTIC", true);
    define("NO_AGENT_CHECK", true);
    define('PUBLIC_AJAX_MODE', true);
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
    $APPLICATION->ShowIncludeStat = false;

    if(isset($_REQUEST["data"])){
		
       
        $REQUEST = array();
        $data = $_REQUEST["data"];

        foreach($data as $field){
            
            if(!empty($field["name"]) && strstr($field["name"], 'order_')){
                
                $REQUEST[$field["name"]] = htmlspecialchars($field["value"],ENT_QUOTES);
                
            }
            
        }
        
        CModule::IncludeModule('iblock');
        CModule::IncludeModule("form");
    
    
        $rsFieldList = CFormField::GetList(11, "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
        $arFieldList = array();
        $arEventFields = array();
        $arFields = array();
        $arErrors = array();
       
        while ($arField = $rsFieldList->GetNext())
        {

            if(!empty($REQUEST[$arField["SID"]])){
                $val = htmlspecialchars($REQUEST[$arField["SID"]], ENT_QUOTES);
                $arFieldList[$arField["SID"]] = $val;
                $arEventFields["form_".$arField["TITLE_TYPE"]."_".$arField["ID"]] = $val;
        
            }else{
                $arErrors[$arField["SID"]] = $arField["SID"];
                
            }
            
        }
        
        
        /* Cогласие обработки персональных данных */        
        CEvent::Send("ORDER_FORM", 's1', $arFieldList,"N",64);

        if ($RESULT_ID = CFormResult::Add(11, $arEventFields))
        {
           $arResult["send"] = "Y";
        }else{
           $arResult["send"] = "N";
        }
	              
    $arResult = array("status"=>true);
    
    
    }else{
        
        $arResult = array("status"=>false);
        
    }
    
    echo json_encode($arResult);
?>