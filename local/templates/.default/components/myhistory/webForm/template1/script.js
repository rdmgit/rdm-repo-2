$(document).ready(function(){
    
    $("body").on("click",".webform-event", function(){
        
        $(".modalform__shadow").css({opacity:0,display:"block"}).animate({opacity:.2},400,function(){
            
            $(".modalForm").css({opacity:0,display:"block"}).animate({opacity:1},400);
			
            $('input[name="respondTo"]').val($(".modalForm__btn-eventTo.modalForm__btn-select").attr("id"));
			
        });
        
        return false;
    });
	
	$("body").on("click",".modalForm__btn-eventTo", function(){
		
		//console.log($(this).attr("id"));
		
		$(".modalForm__btn-eventTo").removeClass("modalForm__btn-select");
		$(this).addClass("modalForm__btn-select");
		
		$('input[name="respondTo"]').val($(this).attr("id"));
		return false;
	});
		
	
    $("body").on("click",".modalForm__eventCancel", function(){
        
        $(".modalForm").animate({opacity:0},400,function(){
            
            $(this).css({display:"none"});
            
            $(".modalform__shadow").animate({opacity:0},400,function(){
                
                $(this).css({display:"none"});
                
            });
            
        });        
        
    });

    $("body").on("click",".modalform__shadow", function(){
        
        $(".modalForm").animate({opacity:0},400,function(){
            
            $(this).css({display:"none"});
            
            $(".modalform__shadow").animate({opacity:0},400,function(){
                
                $(this).css({display:"none"});
                
            });
            
        });
        
        
    });

// Проверка формы
var v = $("#formConsultation");

//Show modal form

//jQuery validate
	$.validator.setDefaults({

	});

	$("#formConsultation").validate({
		rules:
			{
				yourname: {required: true},			
				yourphone: {required: true},
                yourcomment: {required: true},
			},
		messages:
			{
				yourname: { required: 'Заполните поле'},				
				yourphone: { required: 'Заполните поле'},
                yourcomment: { required: 'Заполните поле'},

			},
		submitHandler: function(form)
			{

				$.post($(form).attr('action'),$(form).serialize(),function(data){
					
					//console.log(data);
					
					$(".modalForm").animate({opacity:0},400,function(){
						
						$(this).css({display:"none"});
						
						$(".modalform__shadow").animate({opacity:0},400,function(){
							
							$(this).css({display:"none"});
							$(".webform__wrap").html("<div class='webForm__msg'>Ваше сообщение отправлено!</div>");
							$(".modalForm").remove();
							$(".modalform__shadow").remove();
						});
						
					});
								
								
								},"json");

			}
	});

	v.validate({
		focusCleanup: true,
		focusInvalid: false
	});

    
});