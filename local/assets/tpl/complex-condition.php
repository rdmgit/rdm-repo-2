<div class="complex__header">
	<img class="complex__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
</div>		
<div class="complexForm__title">Условия кредитования</div>
<div class="credit__body">
<span class="credit__body-title">Первоначальный взнос:</span>
<p>20% от стоимости авто
(но можно и без первоначального взноса)</p>

<span class="credit__body-title">Процент:</span>
<p>от 7,5% годовых</p>

<span class="credit__body-title">Срок кредита:</span>
<p>От 3 до 7 лет</p>

<span class="credit__body-title">Требуемые документы:</span>
<p>Паспорт, водительское, ПТС</p>

<span class="credit__body-title">Время рассмотрения:</span>
<p>24 часа</p>


<span class="credit__body-title">Наши преимущества:</span>
<ol>
	<li>Можно БЕЗ ПЕРВОНАЧАЛЬНОГО ВЗНОСА</li>
	<li>ДВА ДОКУМЕНТА без подтверждения дохода</li>
	<li>Как ПЕРВОНАЧАЛЬНЫЙ взнос подойдет ВАШ АВТО!</li>
	<li>Можно с ВРЕМЕННОЙ РЕГИСТРАЦИЕЙ</li>
	<li>КРЕДИТ ПЕНСИОНЕРАМ</li>
	<li>Можно без КАСКО</li>
	<li>Оформление через банки: ВТБ, Сбербанк, Локобанк,
	Русфинансбанк, Связьбанк, Плюсбанк и другие (более 20 банков)</li>
	<li>Кредитуем ПО ВСЕЙ РОССИИ!</li>
	<li>Выдача кредита в день обращения</li>
</ol>
</div>
<button class="btn-submit" id="onCreditBack">Подать заявку</button>
