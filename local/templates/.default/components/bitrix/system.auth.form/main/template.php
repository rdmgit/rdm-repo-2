<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init();
?>

<div class="page__authorization-wrapper" id="authorization">
	<div class="authorization">
		<div class="authorization__title">
			<div class="authorization__title-wrapper">
				Авторизация
			</div>
		</div>
		<?
		if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
			ShowMessage($arResult['ERROR_MESSAGE']);
		?>

		<?if($arResult["FORM_TYPE"] == "login"):?>
		<div class="authorization__wrapper">
			<form class="authorization__form" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
				<?if($arResult["BACKURL"] <> ''):?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?endif?>
				<?foreach ($arResult["POST"] as $key => $value):?>
					<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
				<?endforeach?>
					<input type="hidden" name="AUTH_FORM" value="Y" />
					<input type="hidden" name="TYPE" value="AUTH" />

				<div class="authorization__form-label">
					Электронная почта
				</div>
				<input type="text" class="authorization__form-input" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" maxlength="50 size="17" />
				<div class="authorization__form-pass">
					<a href="#" class="authorization__form-forget">Забыли пароль</a>
					<div class="authorization__form-label">
						Пароль
					</div>
					<input class="authorization__form-input" type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" />
				</div>

				<div class="authorization__form-wrapper">
					<button class="authorization__form-button" type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>">
						Войти
					</button>
					<a href="" class="authorization__form-registr eventOpenReg"><?=GetMessage("AUTH_REGISTER")?></a>
					<a href="registration.html" class="authorization__form-registr _page"><?=GetMessage("AUTH_REGISTER")?></a>
				</div>

<?if($arResult["AUTH_SERVICES"]){
	?>
				<div class="authorization__social">
					<div class="authorization__social-label">
						Авторизоваться через соц. сети
					</div>
	<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
}
?></div>
			</form>
			<?if($arResult["AUTH_SERVICES"]):?>

				<?
				$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
					array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
					),
					$component,
					array("HIDE_ICONS"=>"Y")
				);
				?>
				
				<!--div class="authorization__social">
					<div class="authorization__social-label">
						Войти с помощью
					</div>
					<div class="authorization__social-links">
						<a href="#" class="authorization__social-links-link _facebook"></a>
						<a href="#" class="authorization__social-links-link _odn"></a>
						<a href="#" class="authorization__social-links-link _vk"></a>
						<a href="#" class="authorization__social-links-link _g"></a>
						<a href="#" class="authorization__social-links-link _twitter"></a>
						<a href="#" class="authorization__social-links-link"></a>
					</div>
				</div-->

			<?endif?>
		</div>
		<div class="authorization__close _js-authorization-close"></div>
		<?
		elseif($arResult["FORM_TYPE"] == "otp"):
		?>
		<?
		else:
		?>

		<form action="<?=$arResult["AUTH_URL"]?>">
			<table width="95%">
				<tr>
					<td align="center">
						<?=$arResult["USER_NAME"]?><br />
						[<?=$arResult["USER_LOGIN"]?>]<br />
						<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
					</td>
				</tr>
				<tr>
					<td align="center">
					<?foreach ($arResult["GET"] as $key => $value):?>
						<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
					<?endforeach?>
					<input type="hidden" name="logout" value="yes" />
					<input type="submit" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" />
					</td>
				</tr>
			</table>
		</form>
		<?endif?>
	</div>
</div>







