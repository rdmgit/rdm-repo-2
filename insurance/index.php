<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetAdditionalCSS("/local/assets/css/insurance.css");
$APPLICATION->SetTitle("Страхование");
?>
<div class="insure__banner" style="background-image: url(/local/assets/img/banner_2.jpg);">
	<div class="insure__wrap insure-banner">

		<div class="banner-col">		
			<h1>Страхование</h1>	
			<p>
				 Выбрать надежную страховку так&nbsp;же непросто как и&nbsp;исправный подержанный автомобиль: 
				 часто знание этой области приобретается горьким опытом. В&nbsp;выборе страховой компании 
				 может помочь РДМ&nbsp;-&nbsp;Импорт. Мы годами копили практический опыт и&nbsp;партнерские связи для 
				 получения специальных более выгодных программ нашим покупателям. Специалисты компании 
				 помогают сориентироваться в&nbsp;плюсахи и&nbsp;минусах сотрудничества с&nbsp;разными страховщиками, 
				 понятно объясняют, как происходят выплаты.
			</p>
		
				 
				<a href="#" class="insure__link insure__colorChange">Узнать подробности</a>
				
		
			<div class="insure__info-btns">
				
					<a href="#osago" class="insure-btns__item">ОСАГО</a>						
				
					<a href="#kasko" class="insure-btns__item">КАСКО</a>						
				
					<a href="#avarkom" class="insure-btns__item">АварКом</a>
				
			</div>			
		</div>
	</div>
</div>
 <!-- ОСАГО -->
<div class="insure__wrap" id="osago">
	<div class="insure-subtitle">
		<div class="b-title">
			<h2>ОСАГО</h2>
		</div>
	</div>
	 <!-- image -->
	<div class="insure__col-right insure-img">		
		<img src="../local/assets/img/osago.jpg" style="width:100%">	
	</div>
	<div class="insure__col-left insure-text">	
			
		<p>
			Полис ОСАГО (Обязательное Страхование АвтоГражданской Ответственности) - необходимое условие эксплуатации автомобиля. 
			Наличие полиса у&nbsp;каждого участника дорожного движения, контролируется законодательством. Данный полис компенсирует ущерб, 
			который может быть причинен только в&nbsp;процессе дорожного движения и&nbsp;только невиновной стороне. От&nbsp;выбора страховой компании 
			будет зависеть срок и&nbsp;объем выплат.
		</p>
		<? if ($USER->IsAuthorized()){ ?>
		<a href="#" class="insure__link insure__link-red">Узнать подробности</a>				
		<?}?>
		<button class="insure-btn eventCalc" data-form="osago" data-target="yazakazosago">Получить расчет ОСАГО</button>
	</div>

</div>

 <!-- КАСКО -->
 
<div class="insure__wrap" id="kasko">
	<div class="insure-subtitle">
		<div class="b-title">
			<h2>КАСКО</h2>
		</div>
	</div>
	<div class="insure__col-left insure-img">				
		<img src="../local/assets/img/kasko.jpg" style="width:100%" />	
	</div>

	<div class="insure__col-right insure-text">		
		<p>
			КАСКО - это добровольное автострахование. В&nbsp;отличие от&nbsp;ОСАГО, страховых случаев для&nbsp;получения компенсацию намного больше, 
			вплоть до&nbsp;кражи автомобиля. Но&nbsp;страховые компании часто отвечают отказом на&nbsp;заявление о&nbsp;возмещение ущерба. Как не&nbsp;попасть 
			в&nbsp;подобную ситуацию, знают наши специалисты. Перед подписанием мы&nbsp;тщательно проверяются все&nbsp;пункты договора, 
			знаем законодательство - делаем&nbsp;все, чтобы защитить Ваши интересы.
		</p>
		
		<?if($USER->IsAuthorized()){?><a href="#" class="insure__link insure__link-red">Узнать подробности</a><?}?>				
		
		<button class="insure-btn eventCalc" data-form="kasko" data-target="yazakazkasko">Получить расчет КАСКО</button>
				
	</div>
</div>

 <!-- АварКом -->
<div class="insure__wrap" id="avarkom">
	<div class="insure-subtitle">
		<div class="b-title">
			<h2>АварКом</h2>
		</div>
	</div>
	 <!-- image -->
	<div class="insure__col-right insure-img">		
		<img src="../local/assets/img/avarkom.jpg" style="width:100%" />	
	</div>
	
	<div class="insure__col-left insure-text">	
			
		<p>
		Услуги Аварийного комиссара необходимы, когда важно быстро и&nbsp;правильно зафиксировать дорожно транспортное происшествие, 
		составить рапорт и&nbsp;схему, взять объяснения с&nbsp;участников, сделать фотографии. Помимо этого аварийный комиссар передает 
		документы в&nbsp;дежурную часть ГИБДД, затем получает постановление для&nbsp;начисления страхового возмещения.
		</p>
		
		<?if($USER->IsAuthorized()){?>
		<a href="#" class="insure__link insure__link-red">Узнать подробности</a>				
		<?}?>
		<button class="insure-btn onEventOrder" data-form="avarkom" data-target="yazakazavarkom">Получить расчет АварКом</button>		
		
	</div>

</div>
<?$APPLICATION->IncludeComponent(
	"vegas:calcForm",
	"insurance",
	Array(
		"AJAX_MODE" => "Y"
	)
);?>
 <?$APPLICATION->IncludeComponent(
	"vegas:orderForm", 
	"insurance", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"EMAIL_TEMPLATE" => "54",
		"REQUIRED_FIELDS" => "order_email",
		"WEB_FORM_ID" => "ORDER_FORM"
	),
	false
);?>
<script>
$(document).ready(function(){
	$('a[href^="#"]').on('click', function(event) {

		var target = $( $(this).attr('href') );

		if( target.length ) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top-50
			}, 500);
		}

	});
});
</script>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>