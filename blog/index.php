<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Статьи об автомобилях");

\Bitrix\Main\Page\Asset::getInstance()->addCss(ASSETS_PATH . '/css/blog.css');
?><style>
.page__main-wrpSearch._search._js-search {
    display: none;
}
section.page__header._fix._js-header._buy-auto._visible{display:none !important;}
.myTitle h1 {
    font: 34px/38px 'RobotoLight', Helvetica, Arial, sans-serif;
    margin-bottom: 11px;
}
.offers-list__item {
    position: relative;
    max-width: 940px;
    margin: 0 auto;
    padding-bottom: 9px;
    font-size: 15px;
    box-sizing: border-box;
}
</style>

<?
	// Фильтр статей по тегам
	$tags = !empty($_REQUEST["tag"])?htmlspecialchars($_REQUEST["tag"], ENT_QUOTES):false;
	if($tags!=false){	$GLOBALS['arrFilter']=array("?TAGS" => $tags);	}

?>
<div class="offers-list__item myTitle"> 
<h1>Статьи о&nbsp;автомобилях</h1>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"blog-list", 
	array(
		"COMPONENT_TEMPLATE" => "blog-list",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "22",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "TAGS",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => "blog",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>
	 <?$APPLICATION->IncludeComponent(
	"vegas:subscription.registration.blog", 
	"", 
	array(
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"FORM_ID" => "9",
		
	),
	false
);?> 
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>