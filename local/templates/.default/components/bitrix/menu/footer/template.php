<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if (!empty($arResult)):?>
	<ul class="links__list">
		<?if(!empty($arResult["TITLE"])):?>
			<?foreach($arResult["TITLE"] as $arTitle):?>
				<div class="links__list-title">
					<?=$arTitle['TEXT'];?>
				</div>
			<?endforeach;?>
		<?endif;?>
	<?
	foreach($arResult["LINKS"] as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
			continue;
	?>
		<li class="links__list-item">
			<a href="<?=$arItem["LINK"]?>" class="links__list-item-link" <?if(isset($arItem["PARAMS"]["target"])){?>target="<?=$arItem["PARAMS"]["target"]?>"<?}?>>
				<?=$arItem["TEXT"]?>
			</a>
		</li>
	<?endforeach?>

		<?if($arParams['ROOT_MENU_TYPE'] == "links"):?>
			<?$APPLICATION->ShowViewContent('social_links');?>
		<?endif;?>
	</ul>
<?endif?>
