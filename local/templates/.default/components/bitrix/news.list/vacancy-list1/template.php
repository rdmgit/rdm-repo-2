<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if( count($arResult["ITEMS"]) <= 0 ){?>
	<div class="b-title"><h2>К сожалению на сегодня нет вакансий.</h2></div>
<?}else{?>
	<div class="b-title"><h2>Открытые вакансии</h2></div>
	<div class="vacancy-list">
	<?foreach($arResult["ITEMS"] as $arItem){
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="vacancy-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if( isset($arItem["DETAIL_PICTURE"]) && $arItem["DETAIL_PICTURE"] ){
				$img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width'=>270, 'height'=>200), BX_RESIZE_IMAGE_EXACT); 
			?>
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="vacancy-img-href">
				<img src="<?=$img['src']?>" alt="<?=$arItem['NAME']?>" />
			</a>
			<?}?>
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="vacancy-item-title">
				<?=$arItem['NAME']?>
			</a>
			<p class="date_auto">
				ДАТА ПУБЛИКАЦИИ<br>
				<?=FormatDate("d.m.Y", MakeTimeStamp($arItem["ACTIVE_FROM"]))?>
			</p>
			<p class="date_auto">ЗАРАБОТНАЯ ПЛАТА</p>
			<div class="auto__item-cell _price">
				<span class="auto__item-cell-important _price _show _rub">
					<?=$arItem['DISPLAY_PROPERTIES']['ZP']['VALUE'];?>
				</span>
			</div>
			<div class="class_manager">
				ТРЕБУЕМЫЙ ОПЫТ<br>
				<?=$arItem['DISPLAY_PROPERTIES']['EXPERIENCE']['VALUE'];?>
			</div>			
		</div>
	<?}?>
	</div>
<?}?>