<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/scripts/HLClass.php');
$APPLICATION->SetTitle("О компании");
$APPLICATION->SetAdditionalCSS(ASSETS_PATH . 'css/sell-auto.css');
?> 
<script type="text/javascript">
$(document).ready(function() {
	$('a[href^="#"]').click(function(){
			var el = $(this).attr('href');
			if( el != '#' ){
				$('body').animate({
					scrollTop: $(el).offset().top-50}, 1000);
			}
			return false;
	});

});
</script>
<style>
section.page__header._fix._js-header._buy-auto._visible{display:none !important;}
</style>

<div class="bg_moz sell__banner sell__height sell__pos" style="background-image: url(/local/assets/img/sell_auto2.png);">
	<div class="sell__wrap" style="z-index:2;">
		<div class="sell__half bground-white black-text" style="z-index:2;">

			<h1 class="sell__h1">Как продать авто?</h1>
			<p>
				 Сколько людей столько и способов продажи авто существует. 
				 И все они не похожи друг на друга, как и сами люди. 
				 Можно пройти длинный путь и разработать собственную технологию, 
				 но зачем, если это не станет делом Вашей жизни тратить на это драгоценное время? 
				 Компания РДМ-Импорт может помочь не только в приобретение автомобиля, 
				 но и в его продаже. Мы используем свой опыт, который накопили за 15 лет, 
				 и можем рекомендовать 4 варианта: удобные, понятные, выгодные для продавца.
			</p>

			<p class="em15">
				<a href="#vikup" class="black-text"><b>Срочный выкуп</b></a>
			</p>
			<p class="em15">
				<a href="#trade_in" class="black-text"><b>Trade-in (обмен)</b></a>
			</p>
			<p class="em15">
				<a href="#commis" class="black-text"><b>Поставь на комиссию </b></a>
			</p>
			<p class="em15">
				<a href="#fifty" class="black-text"><b>Авто на 50 сайтов - разом</b></a>
			</p>
			
			
		</div>
	</div>

	<div id="trailer" class="is_overlay" style="height:100vh;width:100%;">
	<video id="video" width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
		<source src="/local/assets/img/videi_yan.webm" type="video/webm"></source>
		<source src="/local/assets/img/videi_yan.mp4" type="video/mp4"></source>
	</video>
	<div class="video-bg-layer" style="background: rgba(0,0,0,0.07);"></div>
	</div>
</div>

<div class="offers-list__item mrg-t40" id="vikup">
	 <!--div class="top-banner__info-title">
		<div class="b-title">
			<h2>У нас работают профессионалы и те, кто стремится к этому</h2>
		</div>
	</div-->

	<div class="offers-list__item-info pd-lft-88 offers-posRight" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<div class="top-banner__info-title">
					<div class="b-title"><h2>Срочный выкуп</h2></div>
				</div>
				<div>
					<p>
						 Срочно продать машину по рыночной цене - это редкая удача. 
						 Если время не терпит, то лучше обратится в компанию РДМ-Импорт. 
						 Наши специалисты оперативно проведут все необходимые как технические так 
						 и юридические проверки, автомобиль получит справедливую рыночную оценку. 
						 Сделка будет оформлена по всем требованиям законодательства и произведен расчет.
					</p>
					<?if( $USER->IsAuthorized() ){?>
						<div class="offers-list-links">
							<div class="offers-list-links__item _read-more">
								<a href="#" class="offers-list-links__item-link">Подробнее</a>
							</div>
						</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="offers-list__item-info offers-btns offers-posRight" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<div class="top-banner__info-title">
					<div class="offers-list__item-actions">
						<div class="offers-list__item-action _calc">
							<button class="offers-list__item-action-link eventCalc fancy-inline" data-src="#calcForm">Калькулятор выкупа</button>
						</div>
						<div class="offers-list__item-action _calc">
							 <a id="zayav-vikup" data-src="#bl_list" class="offers-list__item-action-link fancy-inline msg-vikup" >Отправить заявку</a>
						</div>
						 <!-- calc --> <!-- video -->
						<!--div class="media-content">
 <button class="media-content__button eventVideo" data-src="ZZ32BZk5nso" data-time="25">Посмотрите видео</button>
						</div>
						 <!-- video -->
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<div class="offers-list__item marg0 mrg-t40 " id="trade_in">
	 <!--div class="top-banner__info-title">
		<div class="b-title">
			<h2>У нас работают профессионалы и те, кто стремится к этому</h2>
		</div>
	</div-->

	<div class="offers-list__item-info pd-lft-88 offers-posRight">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<div class="top-banner__info-title">
					<div class="b-title">
						<h2>Trade-in (обмен)</h2>
					</div>
				</div>
				<div>
					<p>
						 Обмен авто - это безопасно и комфортно: не дожидаясь продажи 
						 старого автомобиля можно получить новый в одном месте и в течение 
						 нескольких часов; разница в цене оплачивается наличными или оформляется в кредит. 
						 Для обмена в наличии всегда более 200 машин в отличном состоянии и с гарантией! 
						 Сел и поехал!
					</p>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more">
							<a href="/trade-in/" class="offers-list-links__item-link ">Подробнее </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="offers-list__item-info offers-btns offers-posRight" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<div class="top-banner__info-title">
					<div class="offers-list__item-actions">
						<div class="offers-list__item-action _calc">
							<button href="#" class="offers-list__item-action-link fancy-inline eventCalc" data-src="#calcForm">Калькулятор обмена</button>
						</div>
						<div class="offers-list__item-action _calc">
							 <a data-src="#bl_list" class="offers-list__item-action-link fancy-inline msg-trade" >Отправить заявку</a>
						</div>
						 <!-- calc --> <!-- video -->
						<div class="media-content">
							<button class="media-content__button eventVideo" data-src="ZZ32BZk5nso" data-time="25">Посмотрите видео</button>
						</div>
						 <!-- video -->
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
 <!-- Nagradi -->
<div id="commis-layer">
<div class="offers-list__item" id="commis">
	<div class="back-list">
	<div class="top-banner__info-title">
		<div class="b-title center">
			<h2>Поставить на комиссию</h2>
		</div>
	</div>
	<div class="offers-list__item-text full-width-align">
		<p>
			 Утомительный процесс продажи доверьте компании РДМ-Импорт: 
			 еженедельно мы общаемся с более чем 4 000 людей, которым предложим купить Ваш автомобиль. 
			 Мы возьмем на себя работу по подготовке, хранению, демонстрации машины. 
			 Ваш автомобиль может быть продан в кредит - это значительно увеличит количество покупателей.
		</p>
	</div>

	<div class="offers-list__item-info offers-posRight" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				 <? if ($USER->IsAuthorized() || isset($_REQUEST["adm"])){ ?>
				<div>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more right">
							<a href="#" class="offers-list-links__item-link right">Подробнее </a>
						</div>
					</div>
				</div>
				 <? } ?>
			</div>
		</div>
	</div>

	<div class="offers-list__item-info offers-btns offers-posLeft" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<div class="top-banner__info-title">
					<div class="offers-list__item-actions">
						 
						<div class="offers-list__item-action _calc">
							<button href="#" class="offers-list__item-action-link fancy-inline eventCalc" data-src="#calcForm">Комиссионный калькулятор</button>
						</div>
						
						<div class="offers-list__item-action _calc">
							 <a data-src="#bl_list" class="offers-list__item-action-link fancy-inline msg-commis" >Отправить заявку</a>
						</div>
						 <!-- calc --> <!-- video -->
						<!--div class="media-content">
							<button class="media-content__button eventVideo" data-src="ZZ32BZk5nso" data-time="25">Посмотрите видео</button>
						</div>
						 <!-- video -->
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
</div>
 <!--Выданные авто-->&nbsp;
<div class="offers-list__item" id="fifty">
	<div class="top-banner__info-title">
		<div class="b-title center">
			<h2>Разместить авто на 50 автосайтов одним разом</h2>
		</div>
	</div>
	<!--div class="offers-list__item"-->
		<div class="offers-list__item-info offers-btns" style="padding-left:0">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
					<div class="top-banner__info-title">
						<div class="b-title">
							<h3>Более, чем 50 сайтов — для вас</h3>
						</div>
					</div>
					<p>
						 Мы знаем, как быстро продать Ваш автомобиль! Нужно показать 
						 и рассказать о машине всем: размещаем объявления на наших корпоративных 
						 сайтах с посещаемостью более 6 000 пользователей в неделю. 
						 Дополнительно объявления выгружаем на самые популярные площадки 
						 по продаже авто в Новосибирске, области и Сибири!
					</p>
					<div class="top-banner__info-title">
						<div class="b-title">
							<h3>Используйте видеоканал</h3>
						</div>
					</div>
					<p>
						 Впечатляют видео обзоры машин? А если обзор будет презентовать именно Вашу машину, 
						 то и количество претендентов на ее покупку возрастет. Проверено! Наши видеографиы 
						 помогут создать профессиональное видео, разместить его на нашем сайте в объявление 
						 и на популярном канале you tube, где у нас несколько тысяч подписчиков и 80 тысяч 
						 просмотров еженедельно!
					</p>
					<div class="media-content">
							<button class="media-content__button eventVideo" data-src="WIg8zy86LCo" data-time="0">Посмотрите видео</button>
						</div>
					 <? if ($USER->IsAuthorized() || isset($_REQUEST["adm"])){ ?>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more right">
 <a href="#" class="offers-list-links__item-link right mgr-rght30">
							Узнать больше </a>
						</div>
					</div>
					 <? } ?>
				</div>
			</div>
		</div>
		<div class="offers-list__item-info offers-btns">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
 <img src="/local/assets/img/_MG_8232.jpg" style="width:100%;">
				</div>
			</div>
		</div>
	<!--/div-->
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Профессиональные фото лучше продают!</h2>
		</div>
	</div>
	<div class="offers-list__item-text full-width-align">
		<p>
			 В нашем штате есть профессиональные фотографы, которые знают не понаслышке о продаже авто: 
			 на что обращают внимание потенциальные покупатели, как показать целостность кузова, 
			 комфортабельность салона, комплектацию и установленное оборудование. 
			 Продуманные фотографии обязательно ускорят продажу: Ваши объявления сразу привлекут 
			 внимание и дадут исчерпывающую информацию об автомобиле!
		</p>
		<div style="text-align:center; max-width:1240px; margin:40px auto 15px;">
		<?$APPLICATION->IncludeComponent(
			"hmweb:medialibrary.slider",
			"template1",
			Array(
				"CACHE_GROUPS" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"CAROUSEL_WRAP" => "circular",
				"COLLECTIONS" => array(0=>"3",),
				"COMPONENT_TEMPLATE" => ".default",
				"COUNT_ELEMENT_DEFAULT" => "4",
				"RESIZE_TYPE" => "BX_RESIZE_IMAGE_EXACT",
				"START_ELEMENT_SLIDER" => "1",
				"TEMPLATE_THEME" => "blue",
				"THUMB_IMG_HEIGHT" => "300",
				"THUMB_IMG_WIDTH" => "150",
				"USE_AUTOSCROLL" => "N",
				"USE_JQUERY" => "N",
				"USE_PAGINATION" => "N",
				"USE_THUMB_IMG" => "Y"
			)
		);?>
		</div>
		<?if ($USER->IsAuthorized() || isset($_REQUEST["adm"])){?>
			<div>
				<a href="#" class="sell-auto-btn">Подать объявление</a>
			</div>
		<?}?>
	</div>
	<script>
	$(document).ready(function(){
		if($('font.errortext').length > 0){
			//$('.black_list').show();
			$('.notetext').hide();
			$('font.errortext').text(' Поле "Телефон" необходимо для заполнения')
		}

		$('.msg-commis').click(function(){
			$('.sell-title').text('Поставить на комиссию');
		})
		$('.msg-trade').click(function(){
			$('.sell-title').text('Trade-in');
		})
		$('.msg-vikup').click(function(){
			$('.sell-title').text('Заявка на выкуп');
		})

	});
	</script>
</div>
 <?$APPLICATION->IncludeComponent(
	"vegas:winVideo",
	".default",
	Array()
);?>
<?$APPLICATION->IncludeFile(
	SITE_DIR."include/forms/obmenCalculate.php",
	Array(),
	Array("MODE"=>"text")
);?>

<?/*$APPLICATION->IncludeFile(
	SITE_DIR."include/forms/saleRequest.php",
	Array(),
	Array("MODE"=>"text")
);*/?>

<div class="form_back" id="bl_list">
	<img src="/local/assets/img/logosm.png"><br>
	
	
<?$APPLICATION->IncludeComponent(
	"bitrix:form",
	"sell",
	array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => "sell",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "N",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"NOT_SHOW_FILTER" => array(
			0 => "",
			1 => "",
		),
		"NOT_SHOW_TABLE" => array(
			0 => "",
			1 => "",
		),
		"RESULT_ID" => $_REQUEST[RESULT_ID],
		"SEF_MODE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_STATUS" => "N",
		"SHOW_VIEW_PAGE" => "Y",
		"START_PAGE" => "new",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"WEB_FORM_ID" => "3",
		"VARIABLE_ALIASES" => array(
			"action" => "action",
		)
	)
);?>
</div>


<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>