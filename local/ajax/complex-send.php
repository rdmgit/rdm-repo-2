<?
if(!empty($_REQUEST['type']))
{
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");	
    CModule::IncludeModule("form");			
    CModule::IncludeModule("iblock");

	$temp = $_REQUEST['fields'];
	
	$arUser = array();	
	$arAnswer = array();
	$arCredit = array();
	$autoID=0;
	$creditSumma=0;
	$msg = "";
	/* ------------------------------ */
	// Фильтруем полученные данные
	foreach($temp as $k=>$v){
		
		if(strstr($v["name"],'your_')){
			
			$arUser[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
			
		}elseif(strstr($v["name"],'result__')){
			
			$arCredit[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
			
		}else{
			//if(is_array()){}
			$arData[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);

			if(strstr($v["name"],'_autolink')){ $autoID = (int)$v["value"];}
			if(strstr($v["name"],'_summa')){ $creditSumma = (int)$v["value"];}

		}
		
	
	}

	/* ------------------------------ */
	// Определяем форму, для почтовых уведомлений
	$rsForms = CForm::GetList($by="s_id", $order="desc", array("SID"=>$_REQUEST["type"]));

	if($arForm = $rsForms->Fetch())
	{
				
		$rsFieldList = CFormField::GetList($arForm["ID"], "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	   
	    $arFormFields = array();
	    $arError = array();
		while ($arFieldList = $rsFieldList->GetNext())
		{
			
			$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);
			$arAnswer[$arFieldList["ID"]] = $rsAnswer->Fetch();
			$arFieldList["ANSWER"] = $arAnswer;
			
			$arFormFields[] = $arFieldList;
			
			if($arFieldList["REQUIRED"]=="Y"){
				
				if(empty($arUser[$arFieldList["SID"]])){
					
					$arError[] = $arFieldList["SID"];
				}
				
			}
			
			
		}
		
	/* ------------------------------ */
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID"=>5, "ID"=>$autoID,"ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arRes = $ob->GetFields();
            $arAuto = '<a href="https://'.$_SERVER["HTTP_HOST"].$arRes["DETAIL_PAGE_URL"].'" >'.$arRes["NAME"].'</a>';
            //$arAuto["name"] = $arRes["NAME"];
        }
	/* ------------------------------ */

		// Проверяем массив с ошибками		
		if(count($arError)==0){
			
				$arWebFormListField = array();
				foreach($arFormFields as $arVal){
            
					$arElem = $arAnswer[$arVal["ID"]];
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] =$arUser[$arVal["SID"]];
					
					if($arVal["SID"] == "auto_link"){
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] = $arAuto;
						
					}
					
					if(isset($arCredit['result__'.$arVal["SID"]])){
						
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] = $arCredit['result__'.$arVal["SID"]];
						
					}
					
				}
				
	
				
				
				
			if ($RESULT_ID = CFormResult::Add($arForm["ID"], $arWebFormListField))
			{
			   $send = "Y";
     
                // создадим почтовое событие для отсылки по EMail данных результата
                if (CFormResult::Mail($RESULT_ID))
                {
                    //echo "Почтовое событие успешно создано.";
                }
                else // ошибка
                {
                    //global $strError;
                    //echo $strError;
                }

			}else{
			   $send = "N";
                global $strError;
                 //$strError;
			} 

			$arResult = array("msg"=>$arForm["DESCRIPTION"],"status"=>true);

		}else{ // Если поля не заполнены
						
			$arResult = array("status"=>false,"msg"=>"errorForm","error"=>$arError);
						
		}

			
		}else{ // Если поля не заполнены
						
			$arResult = array("status"=>false,"msg"=>"errorForm","error"=>$arError);
						
		}
	

	/* ------------------------------ */

	
}else{ // Если не пришли нужные данные
	
	$arResult = array("status"=>false,"msg"=>"nodata");
}


echo json_encode($arResult);
?>