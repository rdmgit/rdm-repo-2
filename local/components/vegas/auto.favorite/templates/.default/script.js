$(document).ready(function(){
	
    $("body").on("click",".onCompareRemove", function(){
        

		var th = $(this);
		
		$.post('/compare-remove/',{id:th.data('id')},function(data){
	
			$('.elem'+th.data('id')).animate({opacity:0},400,function(){	
				$(".elem"+th.data('id')).remove();
				if($('.favorite-auto__item').length==0){
								
					$('#favorite-auto__main').html('<div class="favorite__text"><p>Чтобы добавить автомобиль в список сравнения,'+ 
					'нажмите <span class="fav__star">Список сравнения</span> на карточке понравившегося автомобиля.</p></div>');
				}			
			});
			
		},'json');
		

		
        return false;
    });
	    
});