<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/*
 * class CWeatherInformer
 */

class CWeatherInformer extends CBitrixComponent{

	public $arParams = array();

	/*
	 * Prepare parameters
	 *
	 * @param array $arParams
	 * @return array
	 */
	public function onPrepareComponentParams($arParams){
		global $DB;

		$arParams['DISPLAY_DATE'] = ($arParams['DISPLAY_DATE'] == 'Y');
		$arParams['DISPLAY_TIME'] = ($arParams['DISPLAY_TIME'] == 'Y');
		$arParams['DISPLAY_WEATHER'] = ($arParams['DISPLAY_WEATHER'] == 'Y');

		$arParams['DATE_FORMAT'] = trim($arParams['DATE_FORMAT']);
		if(strlen($arParams['DATE_FORMAT']) <= 0)
			$arParams['DATE_FORMAT'] = $DB->DateFormatToPHP(CSite::GetDateFormat('SHORT'));


		return $arParams;
	}

	/**
	 * @param $cityId
	 * @return array
	 */
	public function getCityData($cityId){
		$arCity = array();
		if(!empty($cityId)){
			$arCities = include(dirname(__FILE__).'/lang/'.LANGUAGE_ID.'/city.php');
			$arCity = array(
				'ID'	=> $cityId,
				'NAME'	=> $arCities[$cityId]
			);
		}
		return $arCity;
	}

	/*
	 * Получить информацию о погоде по id города (используется сервис yandex)
	 *
	 * @param string $cityId
	 * @return array
	 */
	public function getWeather($cityId){
		$arWeather = array();
		if(!empty($cityId)){
			global $APPLICATION;

			$objCache = new CPHPCache();
			if($objCache->InitCache($this->arParams['CACHE_TIME'], $cityId, '/'.SITE_ID.'/ots/informer/')){
				$arWeather = $objCache->GetVars();
			}elseif($objCache->StartDataCache()){
				$arWeather = array();
				$objHttp = new Bitrix\Main\Web\HttpClient;
				$objHttp->setTimeout(10);
				$result = $objHttp->get('http://export.yandex.ru/bar/reginfo.xml?region='.substr($cityId, 1));

				$result = str_replace("\xE2\x88\x92", "-", $result);

				$xml = new CDataXML();
				$xml->LoadString($APPLICATION->ConvertCharset($result, 'UTF-8', SITE_CHARSET));

				$arWeather['NOW'] = array(
					'TEMPERATURE' 		=> $xml->SelectNodes('/info/weather/day/day_part/temperature')->content,	//температура
					'TYPE'				=> $xml->SelectNodes('/info/weather/day/day_part/weather_type')->content,	//описание
					'WIND_SPEED'		=> $xml->SelectNodes('/info/weather/day/day_part/wind_speed')->content, 	//скорость ветра, м/сек.
					'WIND_DIRECTION'	=> $xml->SelectNodes('/info/weather/day/day_part/wind_direction')->content, //направлеине ветра
					'PRESSURE'			=> $xml->SelectNodes('/info/weather/day/day_part/pressure')->content,		//давление, мм.рт.ст.
					'DAMPNESS'			=> $xml->SelectNodes('/info/weather/day/day_part/dampness')->content,		//влажность, %
					'ICON'				=> $xml->SelectNodes('/info/weather/day/day_part/image-v3')->content		//иконка
				);
				$arWeather['TOMORROW']= array(
					'TEMPERATURE' 		=> $xml->SelectNodes('/info/weather/day/tomorrow/temperature')->content,	//температура
					'TYPE'				=> $xml->SelectNodes('/info/weather/day/tomorrow/weather_type')->content,	//описание
					'WIND_SPEED'		=> $xml->SelectNodes('/info/weather/day/tomorrow/wind_speed')->content, 	//скорость ветра, м/сек.
					'WIND_DIRECTION'	=> $xml->SelectNodes('/info/weather/day/tomorrow/wind_direction')->content, //направлеине ветра
					'PRESSURE'			=> $xml->SelectNodes('/info/weather/day/tomorrow/pressure')->content,		//давление, мм.рт.ст.
					'DAMPNESS'			=> $xml->SelectNodes('/info/weather/day/tomorrow/dampness')->content,		//влажность, %
					'ICON'				=> $xml->SelectNodes('/info/weather/day/tomorrow/image-v3')->content		//иконка
				);

				$objCache->EndDataCache($arWeather);
			}
		}

		return $arWeather;
	}

	/*
	 * Выполнение компонента
	 */
	public function executeComponent(){

		$timestamp = time() + CTimeZone::GetOffset();
		$this->arResult['DAY_OF_WEEK'] = FormatDate('D', $timestamp);
		$this->arResult['DATE_FORMATTED'] = strtolower(FormatDate($this->arParams['DATE_FORMAT'], $timestamp));

		$this->arResult['CITY'] = $this->getCityData($this->arParams['CITY']);
		$this->arResult['WEATHER'] = $this->getWeather($this->arParams['CITY']);

		$this->IncludeComponentTemplate();

	}

} 