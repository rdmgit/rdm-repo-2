<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS($templateFolder."/jquery.validate.js");?>
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer'></script>
<div class="tradeForm" id="_js-orderForm">
<?
$siteKey = "6Lfy-SgUAAAAAHUFP8mCx8lsf5GowSPmeOKaS7b9";
$secret = "6Lfy-SgUAAAAAM-effqlqdfMsGHzRxXlZ6-ba9fC";
$lang = "ru";
?>
	<form class="order-form" id="trade-form" method="post">
		
        <div class="complexForm-group">

		<?foreach($arResult["FIELDS"] as $arFields){

		    $placeholder=!empty($arFields["ANSWER"]["FIELD_PARAM"])?$arFields["ANSWER"]["FIELD_PARAM"]:$arFields["TITLE"];

			if($arFields["ANSWER"]["FIELD_TYPE"]=="text"){
		?>
		<div class="complexForm-input-fullrow formfield <?if($arFields["REQUIRED"]=="Y"){?>requir<?}?>" id="<?=$arFields["SID"]?>">
		
			<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?> value="" />
		<?//if($arFields["REQUIRED"]=="Y"){?>
			<div class="error-block">Заполните это поле</div>
		<?//}?>
		</div>
		
			<?}elseif($arFields["ANSWER"]["FIELD_TYPE"]=="textarea"){?>
				<div class="complexForm-input-fullrow formfield <?if($arFields["REQUIRED"]=="Y"){?>requir<?}?>" id="<?=$arFields["SID"]?>">

				<textarea name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?>></textarea>
			<?if($arFields["REQUIRED"]=="Y"){?>
				<div class="error-block">Заполните это поле</div>
			<?}?>
		</div>
	
		<?}else{?>			
			<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" value="<?=$arFields["ANSWER"]["VALUE"]?>" />
		  <?}
		}?>

        </div>
		
		<div class="complexForm-input-fullrow edata">					
			<input type="checkbox" class="in-text-data data-style onData" name="order_edata" checked="checked">
			<span class="data-text">Подтверждаю согласие на обработку <a href="/personal-data/" target="_blank">персональных&nbsp;данных</a></span>
		</div>
		<div class="captcha__wrap formfield" id="captcha">
		<div class="g-recaptcha" data-sitekey="6Lfy-SgUAAAAAHUFP8mCx8lsf5GowSPmeOKaS7b9"></div>
		<div class="error-block trade__error">Вы пропустили капчу</div>
		</div>
		<div class="complexForm-input-fullrow order-input-row-center">    
			<button id="onTradeSubmit" data-type="trade_whit_us">Отправить</button>
		</div>
	</form>
</div>