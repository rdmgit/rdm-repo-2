<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$manufacturerID = trim($_REQUEST["manufacturerID"]);

$arResult = array();
if($manufacturerID)
{
	$arResult['MODELS'] = YcawebHelper::getModels($manufacturerID);
}
echo json_encode($arResult);
