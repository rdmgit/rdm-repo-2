(function ($) {

    var defaults = {};
    var options = {};

    var methods = {

        init: function (params) {

           var options = $.extend({}, defaults, options, params);

            if (!this.data('apiFeedbackex')) {
                this.data('apiFeedbackex', options);

                var submitWrapper = $(options.wrapperId);
                var submitForm = $(options.formId);

                submitForm.on('submit',function(e){
					
					console.log(target);
					if(target == undefined){
					target = $('button[name="API_FEX_SUBMIT_BUTTON"]').data('target');
					}
					//console.log(target);
                    var submitFormData = submitForm.serialize() +'&'+ $.param(options.params);

                    submitForm.find('button').attr('disabled',true);

                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        data: submitFormData,
                        error: function(request,error) {
                            alert('Server '+ error +'...');
                        },
                        success: function(data) {

                            submitForm.find('button').attr('disabled',false);

                            if(data.result == 'ok'){
                                submitForm.replaceWith(data.html);							
								            
								console.log(target);
								//yaCounter4175221.reachGoal(target);
							}
                            if(data.result == 'error')
                            {
                                if( typeof data.message.danger != 'undefined' && Object.keys(data.message.danger).length)
                                {
                                    var messageDanger = data.message.danger;
                                    for(var field in messageDanger)
                                    {
                                        if(messageDanger[field].length)
                                            submitForm.find(options.formId + '_ROW_' + field + ' .api-field-error').html(messageDanger[field]).slideDown('fast');
                                        else
                                            submitForm.find(options.formId + '_ROW_' + field + ' .api-field-error').html('').slideUp('fast');
                                    }
                                }

                                if( typeof data.message.warning != 'undefined' && Object.keys(data.message.warning).length)
                                {
                                    submitForm
                                        .find('.api-field-warning')
                                        .html(data.message.warning.join('<br>'))
                                        .slideDown('fast');
                                }
                                else
                                    submitForm.find('.api-field-warning:visible').hide();
                            }
                        }
                    });

                    e.preventDefault();
                })

            }

            return this;
        }
    };

    $.fn.apiFeedbackex = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Error! Method "' + method + '" not found in plugin $.fn.apiFeedbackex');
        }
    };

})(jQuery);