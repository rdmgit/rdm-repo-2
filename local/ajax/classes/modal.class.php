<?php

/**
 * Modal windows
 *
 * @author
 *
 */
class modal extends \Ycaweb\ajaxModules{
	
	public function login(){
		global $USER;
		
		$arResult = array(
			'BACK_URL' => '',
			'ERRORS' => array()
		);
		
		if(isset($_REQUEST['submit_auth'])){
			$arFields = array(
				'EMAIL'		=> htmlspecialchars(trim($_REQUEST['USER_LOGIN'])),
				'PASSWORD'	=> htmlspecialchars(trim($_REQUEST['USER_PASSWORD'])),
				'REMEMBER'	=> (htmlspecialchars(trim($_REQUEST['REMEMBER'])) == 'Y') ? 'Y' : 'N'
			);
			
			if(empty($arFields['EMAIL']) || !check_email($arFields['EMAIL'])){
				$arResult['ERRORS']['EMAIL'] = 'Неверный email.';
			}
			if(empty($arFields['PASSWORD'])){
				$arResult['ERRORS']['PASSWORD'] = 'Неверный пароль.';
			}

			$arAuthResult = $USER->Login($arFields['EMAIL'], $arFields['PASSWORD'], $arFields['REMEMBER']);

			if($arAuthResult === true){
				$arResult['OK'] = 'Вы успешно авторизованы';
				$arResult['BACK_URL'] = $_REQUEST['backurl'];
			}else{
				$arResult['ERRORS']['ALL'] = 'Неверный email или пароль.';
			}

			$arResult["FIELDS"] = $arFields;
		}

		$this->moduleResult(array(
			'result' => $arResult,
			'backurl' => $arResult['BACK_URL']
		));
	}

	public function forgotPass(){
		global $USER;

		$arResult = array(
			'BACK_URL' => '',
			'ERRORS' => array()
		);

		if(isset($_REQUEST['submit_forgot'])){
			$arFields = array(
				'EMAIL'		=> htmlspecialchars(trim($_REQUEST['USER_EMAIL'])),
			);

			if(empty($arFields['EMAIL']) || !check_email($arFields['EMAIL'])){
				$arResult['ERRORS']['ALL'] = 'Неверный email.';
			}

			if(empty($arResult['ERRORS'])){
				$arAuthResult = $USER->SendPassword('', $arFields['EMAIL']);

				if($arAuthResult['TYPE'] == 'OK'){
					$arResult['OK'] = 'На ваш email отправлена ссылка для смены пароля.';
					$arResult['BACK_URL'] = $_REQUEST['backurl'];
				}else{
					$arResult['ERRORS']['ALL'] = 'Неверный email.';
				}
			}

			$arResult["FIELDS"] = $arFields;
		}

		$this->moduleResult(array(
			'result' => $arResult,
			'backurl' => $arResult['BACK_URL']
		));
	}

}