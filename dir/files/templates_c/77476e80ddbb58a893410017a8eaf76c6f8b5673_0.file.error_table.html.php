<?php
/* Smarty version 3.1.30, created on 2017-06-09 15:55:47
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/error_table.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a9ad3285b85_01848255',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '77476e80ddbb58a893410017a8eaf76c6f8b5673' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/error_table.html',
      1 => 1496812349,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a9ad3285b85_01848255 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/home/a/amazuref/new.rdm-import.ru/public_html/dir/framework/module/template/template/smarty/plugins/modifier.date_format.php';
?>
				<!-- Your awesome content goes here -->				
				
				<div class="box-info full">
					<h2><strong>Таблицы</strong> (<?php echo $_smarty_tpl->tpl_vars['DATA']->value['TABLE']['mb'];?>
 МБ)</h2>
							
					<div class="table-responsive"><form class="save" action="">
						<table data-sortable class="table table-hover table-striped">
							<thead>
								<tr>
									<th>Таблица</th>
									<th>Размер</th>
									<th>Лишние данные</th>
									<th>Кол-во записей</th>
									<th>Время обновления</th>									
									<th>Статус</th>	
									<th data-sortable="false">Операции</th>									
								</tr>
							</thead>
							
							<tbody>
							<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['TABLE']['ELEMENT'])) {?>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['TABLE']['ELEMENT'], 'ELEMENT');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ELEMENT']->value) {
?>
									<tr>
										<td><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
</td>
										<td><?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['mb'])) {
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['mb'];?>
 Мб<?php } else {
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['kb'];?>
 Кб<?php }?></td>
										<td><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['optimize'];?>
</td>
										<td><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['count'];?>
</td>
										<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ELEMENT']->value['time'],'%H:%M:%S %d.%m.%Y');?>
</td>
										<td><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['status']) {?>Работает<?php } else { ?><font class="color_red">Повреждена</font><?php }?></td>
										<td>
											<div class="btn-group btn-group-xs">
												<a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/table/param/repair/1/table/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
/" title="Починить" class="btn btn-default"><i class="fa fa-wrench"></i></a>
												<a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/table/param/optimize/1/table/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
/" title="Оптимизировать" class="btn btn-default"><i class="fa fa-level-down"></i></a>
												<?php if (in_array($_smarty_tpl->tpl_vars['ELEMENT']->value['name'],$_smarty_tpl->tpl_vars['DATA']->value['TRUNCATE'])) {?><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/table/param/truncate/1/table/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
/" title="Очистить" class="btn btn-default"><i class="fa fa-minus-circle"></i></a><?php }?>
											</div>
										</td>
									</tr>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							<?php }?>
							</tbody>
						</table>
						</form>
					</div>
	
					<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['pages']) && $_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['pages'] > 1) {?>	
					<div class="data-table-toolbar">
						<ul class="pagination">
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['first']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/error/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['first'];?>
/">&laquo;</a></li>
						  <?php
$__section_page_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_page']) ? $_smarty_tpl->tpl_vars['__smarty_section_page'] : false;
$__section_page_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['pages']) ? count($_loop) : max(0, (int) $_loop));
$__section_page_0_start = min(0, $__section_page_0_loop);
$__section_page_0_total = min(($__section_page_0_loop - $__section_page_0_start), $__section_page_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_page'] = new Smarty_Variable(array());
if ($__section_page_0_total != 0) {
for ($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] = 1, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] = $__section_page_0_start; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] <= $__section_page_0_total; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']++, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']++){
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['current'] == (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null)) {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/error/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] : null);?>
/"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null);?>
</a></li>
						  <?php
}
}
if ($__section_page_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_page'] = $__section_page_0_saved;
}
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['last']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/error/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['ERROR']['PAGE']['last'];?>
/">&raquo;</a></li>
						</ul>
					</div>
					<?php }?>
					
				</div>
				
				
				
				
				
				<!-- End of your awesome content --><?php }
}
