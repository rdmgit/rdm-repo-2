<?php
/* Smarty version 3.1.30, created on 2017-06-07 08:18:24
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/cron.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59378ca030de85_27590189',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea9aff5b0441a0e4862f56e679660a82ff542b90' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/cron.html',
      1 => 1496812349,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59378ca030de85_27590189 (Smarty_Internal_Template $_smarty_tpl) {
?>
				<!-- Your awesome content goes here -->				
				
				<div class="box-info full">
					<h2><strong>Планировщик заданий</strong></h2>
						
						<div class="data-table-toolbar">
							<div class="row">
								<div class="col-md-4">
									
								</div>
								<div class="col-md-8">
									<div class="toolbar-btn-action">
										<a class="btn btn-success" href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/cron_edit/"><i class="fa fa-plus-circle"></i> Добавить задание</a>
									</div>
								</div>
							</div>
						</div>
					<form class="save" action="/direct/save/cron/">	
					<div class="table-responsive"><form class="save" action="/direct/save/company/">
						<table data-sortable class="table table-hover table-striped">
							<thead>
								<tr>
									<th>№</th>
									
									<th>Наименование</th>
									<th>Время старта</th>
									<th>Время завершения</th>
									<th>Время выполнения, мин.</th>
									<th data-toggle="tooltip" title="Запускать каждые указанное количество минут">Интервал, мин.</th>
									<th>Статус</th>
									<th>Перезапуск</th>
									<th data-sortable="false">Сохранить</th>
									
								</tr>
							</thead>
							
							<tbody>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['CRON'], 'ELEMENT', false, NULL, 'element', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ELEMENT']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_element']->value['iteration']++;
?>
								<tr>
									<td data-toggle="tooltip" title="Память: <?php echo round(($_smarty_tpl->tpl_vars['ELEMENT']->value['memory']/1024/1024));?>
 Мб."><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_foreach_element']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_element']->value['iteration'] : null);?>
</td>
									<td data-toggle="tooltip" title="Процесс: <?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['pid'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
</strong></td>
									<td><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['time'];?>
</td>
									<td><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['time_end'];?>
</td>
									<td data-toggle="tooltip" title="Время последнего выполнения: <?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['time_difference'];?>
 сек."><?php $_smarty_tpl->_assignInScope('time_difference_min', ((string)$_smarty_tpl->tpl_vars['ELEMENT']->value['time_difference']/60));
echo ceil($_smarty_tpl->tpl_vars['time_difference_min']->value);?>
</td>
									<td data-toggle="tooltip" title="Максимальное время выполнения: <?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['time_limit'])) {
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['time_limit'];?>
 сек.<?php } elseif (!empty($_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['max_execution_time'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['max_execution_time'];?>
 сек.<?php } else { ?>без ограничений<?php }?>">
										<input type="text" name="interval[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['interval']);?>
" size="6">
									</td>
									<td><?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['expire'])) {?><font color="color_red">Выполняется дольше обычного</font><?php } elseif ($_smarty_tpl->tpl_vars['ELEMENT']->value['status']) {?>Запущено<?php } else { ?>Ожидание<?php }?></td>
									<td><input type="checkbox" name="status[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="0"></td>
									<td>
										<div class="btn-group btn-group-xs">
											<button title="Сохранить" class="btn btn-default" type="submit"><i class="fa fa-save"></i></button>
											<a title="Редактировать" class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/cron_edit/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
"><i class="fa fa-edit"></i></a>											
										</div>
									</td>
								</tr>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</tbody>
						</table>
						<br><center><button class="btn btn-default" type="submit">Сохранить</button></center><br>
					</div>
					</form>
						
					
					
				</div>
				
				
				
				
				
				<!-- End of your awesome content --><?php }
}
