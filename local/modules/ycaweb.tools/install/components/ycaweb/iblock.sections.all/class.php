<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/*
 * class CIBlockSectionsAllComponent
 */

class CIBlockSectionsAllComponent extends CBitrixComponent{

	public $arParams = array();
	protected $needModules = array('iblock');
	protected $arrFilter = array();

	/*
	 * Prepare parameters
	 *
	 * @param array $arParams
	 * @return array
	 */
	public function onPrepareComponentParams($arParams){
		global $USER;

		if(!isset($arParams['CACHE_TIME']))
			$arParams['CACHE_TIME'] = 36000000;

		$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
		$arParams['SECTION_ID'] = intval($arParams['SECTION_ID']);
		$arParams['SECTION_CODE'] = trim($arParams['SECTION_CODE']);

		if(strlen($arParams['ELEMENT_SORT_FIELD'])<=0)
			$arParams['ELEMENT_SORT_FIELD']='sort';
		if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams['ELEMENT_SORT_ORDER']))
			$arParams['ELEMENT_SORT_ORDER']='asc';

		if(strlen($arParams['SECTION_SORT_FIELD'])<=0)
			$arParams['SECTION_SORT_FIELD']='sort';
		$arParams['SECTION_SORT_ORDER'] = strtolower($arParams['SECTION_SORT_ORDER']);
		if($arParams['SECTION_SORT_ORDER']!='desc')
			$arParams['SECTION_SORT_ORDER']='asc';

		if(strlen($arParams['FILTER_NAME'])>0)
		{
			global $$arParams['FILTER_NAME'];
			$this->arrFilter = ${$arParams['FILTER_NAME']};
		}
		if(!is_array($this->arrFilter))
			$this->arrFilter = array();

		$arParams['SECTION_URL']=trim($arParams['SECTION_URL']);
		$arParams['DETAIL_URL']=trim($arParams['DETAIL_URL']);

		$arParams['SECTION_ID_VARIABLE']=trim($arParams['SECTION_ID_VARIABLE']);
		if(strlen($arParams['SECTION_ID_VARIABLE'])<=0|| !preg_match('/^[A-Za-z_][A-Za-z01-9_]*$/', $arParams['SECTION_ID_VARIABLE']))
			$arParams['SECTION_ID_VARIABLE'] = 'SECTION_ID';

		$arParams['SET_TITLE'] = $arParams['SET_TITLE']!='N';
		$arParams['DISPLAY_COMPARE'] = $arParams['DISPLAY_COMPARE']=='Y';

		$arParams['SECTION_COUNT'] = intval($arParams['SECTION_COUNT']);
		if($arParams['SECTION_COUNT']<=0)
			$arParams['SECTION_COUNT']=20;
		$arParams['ELEMENT_COUNT'] = intval($arParams['ELEMENT_COUNT']);
		if($arParams['ELEMENT_COUNT']<=0)
			$arParams['ELEMENT_COUNT']=20;

		if(!is_array($arParams['PROPERTY_CODE']))
			$arParams['PROPERTY_CODE'] = array();
		foreach($arParams['PROPERTY_CODE'] as $k=>$v)
			if($v==='')
				unset($arParams['PROPERTY_CODE'][$k]);

		if(!is_array($arParams['PRODUCT_PROPERTIES']))
			$arParams['PRODUCT_PROPERTIES'] = array();
		foreach($arParams['PRODUCT_PROPERTIES'] as $k=>$v)
			if($v==='')
				unset($arParams['PRODUCT_PROPERTIES'][$k]);

		$arParams['CACHE_FILTER']=$arParams['CACHE_FILTER']=='Y';
		if(!$arParams['CACHE_FILTER'] && count($this->arrFilter)>0)
			$arParams['CACHE_TIME'] = 0;

		return $arParams;
	}

	public function getSectionElements($arSection = array()){
		$arElements = array();

		$arElementSort = array(
			$this->arParams['ELEMENT_SORT_FIELD'] => $this->arParams['ELEMENT_SORT_ORDER'],
			'ID' => 'DESC',
		);
		// list of the element fields that will be used in selection
		$arElementSelect = array(
			'ID',
			'NAME',
			'CODE',
			'ACTIVE_FROM',
			'ACTIVE_TO',
			'DATE_CREATE',
			'CREATED_BY',
			'IBLOCK_ID',
			'IBLOCK_SECTION_ID',
			'DETAIL_PAGE_URL',
			'DETAIL_TEXT',
			'DETAIL_TEXT_TYPE',
			'DETAIL_PICTURE',
			'PREVIEW_TEXT',
			'PREVIEW_TEXT_TYPE',
			'PREVIEW_PICTURE',
			'TAGS',
			'PROPERTY_*',
		);
		$arElementFilter = array(
			'ACTIVE'			=> 'Y',
			'IBLOCK_ID'			=> $this->arParams['IBLOCK_ID'],
			'SECTION_ID'		=> (!empty($arSection) ? $arSection['ID'] : false),
			'ACTIVE_DATE'		=> 'Y',
			'CHECK_PERMISSIONS' => 'Y',
		);

		$arNavStartParams = false;
		if($this->arParams['ELEMENT_COUNT']){
			$arNavStartParams = array(
				'iNumPage'	=> 1,
				'nPageSize'	=> $this->arParams['ELEMENT_COUNT']
			);
		}

		//Get elements
		$rsElements = CIBlockElement::GetList($arElementSort, array_merge($arElementFilter, $this->arrFilter), false, $arNavStartParams, $arElementSelect);
		$rsElements->SetUrlTemplates($this->arParams['DETAIL_URL']);
		if(!empty($arSection)){
			$rsElements->SetSectionContext($arSection);
		}
		while($obElement = $rsElements->GetNextElement())
		{
			$arItem = $obElement->GetFields();

			$arButtons = CIBlock::GetPanelButtons(
				$arItem['IBLOCK_ID'],
				$arItem['ID'],
				$arSection['ID'],
				array('SECTION_BUTTONS'=>false, 'SESSID'=>false, 'CATALOG'=>true)
			);
			$arItem['EDIT_LINK'] = $arButtons['edit']['edit_element']['ACTION_URL'];
			$arItem['DELETE_LINK'] = $arButtons['edit']['delete_element']['ACTION_URL'];

			$arItem['PREVIEW_PICTURE'] = CFile::GetFileArray($arItem['PREVIEW_PICTURE']);
			$arItem['DETAIL_PICTURE'] = CFile::GetFileArray($arItem['DETAIL_PICTURE']);

			if(count($this->arParams['PROPERTY_CODE']))
				$arItem['PROPERTIES'] = $obElement->GetProperties();

			$arItem['DISPLAY_PROPERTIES'] = array();
			foreach($this->arParams['PROPERTY_CODE'] as $pid)
			{
				$prop = &$arItem['PROPERTIES'][$pid];
				if((is_array($prop['VALUE']) && count($prop['VALUE'])>0) ||
					(!is_array($prop['VALUE']) && strlen($prop['VALUE'])>0))
				{
					$arItem['DISPLAY_PROPERTIES'][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, 'catalog_out');
				}
			}

			$arElements[] = $arItem;
		}

		return $arElements;
	}

	protected function checkModules(){
		if(!empty($this->needModules)){
			foreach($this->needModules as $module){
				if(!CModule::IncludeModule($module)){
					throw new \Bitrix\Main\LoaderException('Module '.$module.' is not found');
				}
			}
		}
	}

	public function executeComponent(){
		global $USER, $APPLICATION;

		try{
			$this->checkModules();
		}catch(\Exception $e){
			//echo $e->getMessage();
			return;
		}

		if($this->StartResultCache(false, array($this->arrFilter, CDBResult::NavStringForCache($this->arParams['PAGE_ELEMENT_COUNT']), ($this->arParams['CACHE_GROUPS']==='N'? false: $USER->GetGroups()))))
		{
			global $CACHE_MANAGER;

			$arSectionFilter = array(
				'ACTIVE'		=> 'Y',
				'GLOBAL_ACTIVE'	=> 'Y',
				'IBLOCK_ID'		=> $this->arParams['IBLOCK_ID'],
				'IBLOCK_ACTIVE'	=> 'Y',
			);
			if($this->arParams['SECTION_ID'] > 0){
				$arSectionFilter['SECTION_ID'] = $this->arParams['SECTION_ID'];
			}elseif('' != $this->arParams['SECTION_CODE']){
				$arSectionFilter['SECTION_ID'] = CIBlockFindTools::GetSectionID($this->arParams['SECTION_ID'], $this->arParams['SECTION_CODE']);
			}

			//ORDER BY
			$arSectionSort = array(
				'LEFT_MARGIN' => 'ASC',
				$this->arParams['SECTION_SORT_FIELD'] => $this->arParams['SECTION_SORT_ORDER'],
				'ID' => 'ASC',
			);
			//SELECT
			$arSectionSelect = array();
			if(isset($this->arParams['SECTION_FIELDS']) && is_array($this->arParams['SECTION_FIELDS']))
			{
				foreach($this->arParams['SECTION_FIELDS'] as $field)
					if(is_string($field) && !empty($field))
						$arSectionSelect[] = $field;
			}

			if(!empty($arSectionSelect))
			{
				$arSectionSelect[] = 'ID';
				$arSectionSelect[] = 'NAME';
				$arSectionSelect[] = 'LIST_PAGE_URL';
				$arSectionSelect[] = 'SECTION_PAGE_URL';
			}

			if(isset($this->arParams['SECTION_USER_FIELDS']) && is_array($this->arParams['SECTION_USER_FIELDS']))
			{
				foreach($this->arParams['SECTION_USER_FIELDS'] as $field)
					if(is_string($field) && preg_match('/^UF_/', $field))
						$arSectionSelect[] = $field;
			}

			$arNavStartParams = false;
			if($this->arParams['SECTION_COUNT']){
				$arNavStartParams = array(
					'iNumPage'	=> 1,
					'nPageSize'	=> $this->arParams['SECTION_COUNT']
				);
			}

			//Get sections
			$rsSections = CIBlockSection::GetList($arSectionSort, $arSectionFilter, false, $arSectionSelect, $arNavStartParams);
			$rsSections->SetUrlTemplates('', $this->arParams['SECTION_URL']);
			while($arSection = $rsSections->GetNext())
			{
				if(isset($arSection['PICTURE']))
					$arSection['PICTURE'] = CFile::GetFileArray($arSection['PICTURE']);
				if(isset($arSection['DETAIL_PICTURE']))
					$arSection['DETAIL_PICTURE'] = CFile::GetFileArray($arSection['DETAIL_PICTURE']);

				$arSection['ITEMS'] = $this->getSectionElements($arSection);

				$arButtons = CIBlock::GetPanelButtons(
					$arSection["IBLOCK_ID"],
					0,
					$arSection["ID"],
					array("SESSID"=>false, "CATALOG"=>true)
				);
				$arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
				$arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];

				$this->arResult['SECTIONS'][] = $arSection;
			}

			$this->arResult['SECTIONS'][] = array(
				'ID'	=> 0,
				'ITEMS'	=> $this->getSectionElements(array())
			);

			$arReturnUrl = array(
				"add_section" => (strlen($this->arParams["SECTION_URL"]) ? $this->arParams["SECTION_URL"] : CIBlock::GetArrayByID($this->arParams["IBLOCK_ID"], "SECTION_PAGE_URL")),
			);
			$arButtons = CIBlock::GetPanelButtons(
				$this->arParams["IBLOCK_ID"],
				0,
				($arSectionFilter['SECTION_ID'] ? $arSectionFilter['SECTION_ID'] : false),
				array("RETURN_URL" => $arReturnUrl, "CATALOG"=>true)
			);

			if($APPLICATION->GetShowIncludeAreas())
				$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

			$this->IncludeComponentTemplate();
		}
	}

}