<?php
/* Smarty version 3.1.30, created on 2017-08-14 07:30:40
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/group.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59912770e44c74_36537792',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cc6c686189ea002cb08caaed54d0b5e53d71fca4' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/group.html',
      1 => 1496812350,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:client_info.html' => 1,
    'file:auction_filter.html' => 1,
    'file:strategy_title.html' => 1,
    'file:strategy.html' => 1,
  ),
),false)) {
function content_59912770e44c74_36537792 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/home/a/amazuref/new.rdm-import.ru/public_html/dir/framework/module/template/template/smarty/plugins/modifier.date_format.php';
?>
				<!-- Your awesome content goes here -->				
				
				<div class="box-info full">
					<h2><strong>Группы объявлений</strong></h2>
					<?php $_smarty_tpl->_subTemplateRender("file:client_info.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	
					<?php $_smarty_tpl->_subTemplateRender("file:auction_filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

						
					<form class="save" action="/direct/save/group/">	
					<div class="table-responsive">
						<table data-sortable class="table table-hover table-striped">
							<thead>
								<tr>
									<th>ID</th>
									
									<th>Наименование</th>
									<?php $_smarty_tpl->_subTemplateRender("file:strategy_title.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

									<th data-sortable="false">Сохранить</th>
								</tr>
							</thead>
							
							<tbody>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['ELEMENT'], 'ELEMENT');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ELEMENT']->value) {
?>
								<tr>
									<td data-toggle="tooltip" title="<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['group'])) {?>Группа: №<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['group'];
}?>"><?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['id']) && $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['id'] != -1 && $_smarty_tpl->tpl_vars['ELEMENT']->value['expire']) {?><font class="color_red"><?php }?><strong><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
</strong></font><br>
									<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['status'] == 2) {?><font class="color_red">Остановлена</font><br><?php }?>
									<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['rarely'] == 1) {?><font class="color_red">Мало показов</font><br><?php }?>
									<nobr>Расход: <?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['sum'],2);?>
</nobr><br>
									<nobr>Валюта: <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?></nobr><br>
									<nobr>Показов: <?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['show']+$_smarty_tpl->tpl_vars['ELEMENT']->value['show_context'];?>
</nobr><br>
									<nobr>Кликов: <?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['click']+$_smarty_tpl->tpl_vars['ELEMENT']->value['click_context'];?>
</nobr><br>
									<nobr>CTR: <?php echo round((($_smarty_tpl->tpl_vars['ELEMENT']->value['click']+$_smarty_tpl->tpl_vars['ELEMENT']->value['click_context'])/($_smarty_tpl->tpl_vars['ELEMENT']->value['show']+$_smarty_tpl->tpl_vars['ELEMENT']->value['show_context'])*100),2);?>
%</nobr>
									</td>
									
									<td><a name="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
"></a><strong><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/phrase/param/where/3/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'])) {?>user/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'];?>
/<?php }?>company/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['company'];?>
/group/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
</a></strong><br><strong><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['title'];?>
</strong><br><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['body'];?>
<br><a href="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['url'];?>
" target="_blank" title="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['domain'];?>
</a></td>
									<?php $_smarty_tpl->_subTemplateRender("file:strategy.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

									
									<td>
										<div class="btn-group btn-group-xs">
											
											<button title="Сохранить" class="btn btn-default"><i class="fa fa-save"></i></button>
											<a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/index/param/group/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/" title="Статистика" class="btn btn-default"><i class="fa fa-bar-chart-o"></i></a>
											<br>
											<center>
											<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['id']) && $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['id'] != -1) {?>
											<br><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['expire']) {?><font class="color_red">Давно не обновлялись: <?php } else { ?><font class="color_yellow">Последнее обновление ставок: <?php }?></font><br>
											<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['expire']) {?><font class="color_red"><?php }
if ($_smarty_tpl->tpl_vars['ELEMENT']->value['phrase_max_time']) {
echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ELEMENT']->value['phrase_max_time'],'<strong>%H:%M</strong><br>%d.%m.%Y');
} else { ?>не обновлялись<?php }?></font><?php } else { ?><br>Не обновлять ставки<?php }?>
											</center>
										</div>
									</td>
								</tr>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</tbody>
						</table>
						<br><center><button class="btn btn-default" type="submit">Сохранить <?php echo $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['elements'];?>
 из <?php echo $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['count'];?>
</button></center><br>
					</div>
					</form>	
					<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['pages']) && $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['pages'] > 1) {?>	
					<div class="data-table-toolbar">
						<ul class="pagination">
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['first']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/group/param/where/2/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'])) {?>status/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['search'])) {?>search/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['search'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'])) {?>number/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'])) {?>sort/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'])) {?>strategy/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'])) {?>user/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'])) {?>company/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['tag'])) {?>tag/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['tag'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['first'];?>
/">&laquo;</a></li>
						  <?php
$__section_page_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_page']) ? $_smarty_tpl->tpl_vars['__smarty_section_page'] : false;
$__section_page_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['pages']) ? count($_loop) : max(0, (int) $_loop));
$__section_page_0_start = min(0, $__section_page_0_loop);
$__section_page_0_total = min(($__section_page_0_loop - $__section_page_0_start), $__section_page_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_page'] = new Smarty_Variable(array());
if ($__section_page_0_total != 0) {
for ($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] = 1, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] = $__section_page_0_start; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] <= $__section_page_0_total; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']++, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']++){
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['current'] == (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null)) {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/group/param/where/2/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'])) {?>status/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['search'])) {?>search/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['search'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'])) {?>number/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'])) {?>sort/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'])) {?>strategy/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'])) {?>user/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'])) {?>company/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['tag'])) {?>tag/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['tag'];?>
/<?php }?>page/<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] : null);?>
/"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null);?>
</a></li>
						  <?php
}
}
if ($__section_page_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_page'] = $__section_page_0_saved;
}
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['last']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/group/param/where/2/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'])) {?>status/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['search'])) {?>search/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['search'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'])) {?>number/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'])) {?>sort/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'])) {?>strategy/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'])) {?>user/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'])) {?>company/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['tag'])) {?>tag/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['tag'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['GROUP']['PAGE']['last'];?>
/">&raquo;</a></li>
						</ul>
					</div>
					<?php }?>
				</div>
				
				
				
				
				
				<!-- End of your awesome content --><?php }
}
