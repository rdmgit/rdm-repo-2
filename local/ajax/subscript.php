<?
if(!empty($_REQUEST['get']) && !empty($_REQUEST['fields'])){
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");	
    CModule::IncludeModule('subscribe');
    CModule::IncludeModule("form");			
	$get = htmlspecialchars($_REQUEST['get'], ENT_QUOTES);	
	$temp = $_REQUEST['fields'];
	
	$arUser = array();	
	$arAnswer = array();
	$arKuzov = array();
	$arKuzovID = "";
	$msg = "";
	/* ------------------------------ */
	// Фильтруем полученные данные
	foreach($temp as $k=>$v){
		
		if(strstr($v["name"],'kuzov_')){
			
			$arKuzov[] = htmlspecialchars($v["value"], ENT_QUOTES);
		
		}elseif(strstr($v["name"],'your_')){
			
			$arUser[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
			
		}else{
			
			$arData[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
		}
	
	}
	
	$arKuzovID = implode(',',$arKuzov);

	/* ------------------------------ */
	// Определяем форму, для почтовых уведомлений
	$form_id_md5 = $arData['data'];
	$rsForms = CForm::GetList($by="s_id", $order="desc", array(), $is_filtered);
	$arT = array();
	
	while ($arForm = $rsForms->Fetch())
	{	
		if($arData['data'] == md5('id_'.$arForm['ID'])){

			break;
		}

	} 

	if($arForm){
		
		
		$rsFieldList = CFormField::GetList($arForm["ID"], "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	   
	    $arFormFields = array();
	    $arError = array();
		while ($arFieldList = $rsFieldList->GetNext())
		{
			
			$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);
			$arAnswer[$arFieldList["ID"]] = $rsAnswer->Fetch();
			$arFieldList["ANSWER"] = $arAnswer;
			
			$arFormFields[] = $arFieldList;
			
			if($arFieldList["REQUIRED"]=="Y"){
				
				if(empty($arUser[$arFieldList["SID"]])){
					
					$arError[] = $arFieldList["SID"];
				}
				
			}
		}
	/* ------------------------------ */

		$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
		$arFilter = Array("IBLOCK_ID"=>6,"ID"=>$arKuzov, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false,true, $arSelect);
	
		$arKuzovFull = array();
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$arKuzovFull[] = $arFields["NAME"];

		}
		$arKuzovFull = implode(',',$arKuzovFull);
	/* ------------------------------ */
	
	
	
	
		// Проверяем массив с ошибками
		if(count($arError)==0){
			
				/* ------------------------------ */
				$arWebFormListField = array();
				foreach($arFormFields as $arVal){
					$arElem = $arAnswer[$arVal["ID"]];
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] =$arUser[$arVal["SID"]];
					
					if($arVal["SID"] == "your_kuzov"){
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] = $arKuzovFull;	
						
					}
					if($arVal["SID"] == "your_kuzov_type"){
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] = $arKuzovID;	
						
					}					
					
					
				}
				
				
				$arEventFields = array(
					"your_name" =>"Антон",
					"your_phone" =>"2222",
					"your_email" =>"www@www.et",
				);
				
				
			if ($RESULT_ID = CFormResult::Add($arForm["ID"], $arWebFormListField))
			{
			   $send = "Y";
			}else{
			   $send = "N";
			} 	
			
			$REQUEST = array(
			"get"=>$get,
			"arUser"=>$arUser,
			"arKuzov"=>$arKuzov,
			"arUserFields"=>$arUserFields,
			"arData"=>$arData,
			"arForm"=>$arForm,
			"arFormFields"=>$arFormFields,
			"arAnswer"=>$arAnswer,
			"arWebFormListField"=>$arWebFormListField,
			"arKuzovFull"=>$arKuzovFull,
			"arError"=>$arError,
			"msg"=>$msg,
			"send"=>$send,
			);
			
			
			
			$arResult = array("status"=>true);
			
			
		}else{ // Если поля не заполнены
			
			
			$arResult = array("status"=>false,"msg"=>"errorForm","error"=>$arError);
			
			
		}
	
	
	
	}else{ // Если нет вебформы
		
		$arResult = array("status"=>false,"arForm"=>$arForm,"arT"=>$arT, "msg"=>"nowebform");
	}
	/* ------------------------------ */

	
}else{ // Если не пришли нужные данные
	
	$arResult = array("status"=>false,"msg"=>"nodata");
}


echo json_encode($arResult);
?>