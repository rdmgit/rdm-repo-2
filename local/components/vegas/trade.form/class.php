<?//include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/scripts/HLClass.php');
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class orderForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {
			CModule::IncludeModule("form");	
			$rsForms = CForm::GetList($by="s_id", $order="desc", array("SID"=>$req["type"]));
			
			if ($arForm = $rsForms->Fetch())
			{	
				$rsFieldList = CFormField::GetList(15, "ALL", $by="s_sort", $order="asc", array("ACTIVE"=>"Y"), $is_filtered);
			   
				$this->arResult = array();
				$arError = array();
				$isRequired = '';
				while ($arFieldList = $rsFieldList->GetNext())
				{
					
					$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);

					$arFieldList["ANSWER"] = $rsAnswer->Fetch();
					
					if($arFieldList["SID"]=="auto_link"){
						
						$arFieldList["ANSWER"]["VALUE"]=$this->arParams["ELEMENT_ID"];
						
					}
					$this->arResult["FIELDS"][] = $arFieldList;					
	
				}
				
				
			}
		
            $this->includeComponentTemplate();

        }    

        public function onPrepareComponentParams($arParams)
        { 
            return $arParams;
        }
        
    }
?>