<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS($templateFolder."/jquery.validate.js");?>

<section class="page__registration _js-registration">
    <div class="page__registration-wrapper" id="registration">
        <div class="registration">
            <div class="registration__title">
                <div class="registration__title-wrapper">
                    Регистрация
                </div>
            </div>
            <div class="registration__wrapper">
                <form action="#" class="registration__form">
                    <div class="registration__form-label">
                        Представьтесь
                    </div>
                    <input type="text" class="registration__form-input">
                    <div class="registration__form-label">
                        Номер телефона
                    </div>
                    <input type="text" class="registration__form-input" placeholder="+7">
                    <div class="registration__form-label">
                        Электронная почта
                    </div>
                    <input type="text" class="registration__form-input">
                    <div class="registration__form-pass">
                        <div class="registration__form-pass-wrapper">
                            <label class="registration__form-pass-label">
                                Пароль
                            </label>
                            <input type="password" class="registration__form-pass-input _left">
                        </div>
                        <div class="registration__form-pass-wrapper">
                            <label class="registration__form-pass-label">
                                Повторите пароль
                            </label>
                            <input type="password" class="registration__form-pass-input _right">
                        </div>
                    </div>
                    <div class="registration__form-captcha">

                    </div>
                    <div class="registration__form-in">
                        <a href="#authorization" class="registration__form-in-authorization _js-authorization-link">Уже есть аккаунт</a>
                        <a href="authorization.html" class="registration__form-in-authorization _page">Уже есть аккаунт</a>
                        <button class="registration__form-in-button">
                            Регистрация
                        </button>
                    </div>
                </form>
            </div>
            <div class="registration__close _js-registration-close">

            </div>
        </div>
    </div>
</section>