<?php

function sendEmptyResponse($code)
{
    http_response_code($code);
    die();
}

function removeDirectory($dir)
{
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file"))
            ? removeDirectory("$dir/$file")
            : unlink("$dir/$file");
    }
    return rmdir($dir);
}

removeDirectory('upload');
unlink('log.txt');
unlink('old.csv');

sendEmptyResponse(200);