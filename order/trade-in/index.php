<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обмен Авто");
$APPLICATION->SetPageProperty("wrapper_class", "trade-in");

include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/scripts/HLClass.php');
?><style>
.offers-list__item h2 {padding: 27px 0 0px;}
.page__main-wrapper._search._js-search{display:none;}
.page__header._fix .header{display:none !important}
.page__main-wrpSearch._search._js-search {display: none;}
</style>
<div class="bg_moz" style="background: url('/local/assets/img/_MG_8207-1_(3).jpg');margin-top: -40px; background-size:cover; color:white; min-height:635px; background-position-x:right;">
	<div class="offers-list__item" style="color:#000;">
		
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text" style="background:rgba(255,255,255,0.7) !important; border-radius:5px; padding:2px 15px!important;    margin-top: 70px;">
					<div class="top-banner__info-title">
						<div class="b-title">
							<h1 style=" padding-top: 13px;">Обмен авто (Trade-in)</h1>
						</div>
					</div>
					<p style="text-align: left;">
						 Автосалон РДМ-Импорт предлагает известную во всем мире услугу по покупке, а лучше сказать, обмену старого автомобиля на новый. Услуга trade-in позволяет приобрести любую машину из нашего салона, доплатив только разницу в цене от старого и нового автомобиля. Можно использовать Ваш старый автомобиль в качестве первоначального взноса при оформлении кредита.<br>
					</p>
					<p style="text-align: left;">
						 Trade-in - это простое решение вопросов быстрого и безопасного приобретения автомобиля
					</p>
				</div>
			</div>
		</div>
		<div class="offers-list__item-info">
			<div class="offers-list__item-text">
			</div>
		</div>
	</div>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Обменяйте свой старый автомобиль на новый!</h2>
		</div>
	</div>
	 <!--ol class="trade-shag-list">
		<li>Вы приезжаете к нам
		<div class="mini-desc-shag">
			 Каждый день с 9:00 до 20:00
		</div>
 </li>
		<li>Мы принимаем ваше авто
		<div class="mini-desc-shag">
			 Каждый день с 9:00 до 20:00
		</div>
 </li>
		<li>Вы выбираете свой автомобиль
		<div class="mini-desc-shag">
			 Каждый день с 9:00 до 20:00
		</div>
 </li>
		<li>Оформляем документы и вы едете домой на новом авто!
		<div class="mini-desc-shag">
			 Каждый день с 9:00 до 20:00
		</div>
 </li>
	</ol-->
	<ul class="timeline">
		<li class="num1" data-num="1">
		<div class="ps-abs">
			<div style="padding-bottom:10px;">
 <img src="/local/assets/img/_MG_3834_(6) (1).jpg" style="width:100%;">
				Выбирайте один из 200 автомобилей, представленных в нашем Автосалоне РДМ-Импорт
			</div>
		</div>
 </li>
		<li class="num2" data-num="2">
		<div class="ps-abs ps-abs-btm">
			<div style="padding-bottom:10px;">
 <img src="/local/assets/img/timeline2.jpg" style="width:100%;">
				Для вас бесплатная диагностика старого автомобиля и определение его рыночной цены
				<div class="offers-list__item-actions btn-dg" style="text-align:center;">
					<div class="offers-list__item-action" style="cursor:pointer;">
						<div data-src="#formDiagnostic" class="fancy-inline diagn">
							Записаться на диагностику
						</div>
					</div>
				</div>
			</div>
		</div>
 </li>
		<li class="num3" data-num="3">
		<div class="ps-abs">
			<div style="padding-bottom:10px;">
 <img src="/local/assets/img/timeline3.jpg" style="width:100%;">
				Оформление договора купли-продажи по всем требованиям законодательства
			</div>
		</div>
 </li>
		<li class="num4" data-num="4">
		<div class="ps-abs ps-abs-btm">
			<div style="padding-bottom:10px;">
 <img src="/local/assets/img/_MG_2567-Edit.jpg" style="width:100%;">
				Оплата наличными разницы между ценой старого и нового автомобиля либо заключение кредитного договора
			</div>
		</div>
 </li>
		<li class="num5" data-num="5">
		<div class="ps-abs ">
			<div style="padding-bottom:10px;">
				<div class="ps-abs__media-wrap">
					<div class="ps-abs__media">
 <img src="http://img.youtube.com/vi/rYdcMQpRly0/0.jpg" class="ps-abs__thumbnail" alt="">
						<div class="ps-abs__overlay-play eventVideo" data-src="rYdcMQpRly0">
						</div>
					</div>
				</div>
				 И вот вы уже управляете своим новым автомобилем! <br>
				<br>
			</div>
		</div>
 </li>
	</ul>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Оставить заявку на обмен</h2>
		</div>
	</div>
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<form class="form-trade-zayavka">
					<div class="title-form">
						 Опишите ваш автомобиль
					</div>
					<div class="input-trade-form require" id="marka_form">
                        <div class="input-trade-form require" style="margin-right:1%;">
                            Марка
                            <select id="marka" class="auto-form__item-select _js-select _js-mark-select _js-form-field">
                                <option class="auto-form__item-select-var" value="">--</option>
                                <?
                                $hLBlock = new HLBlock('CarMark');
                                $arRes = $hLBlock->getFields(array(), array('UF_NAME' => 'ASC'));   
     
                                foreach($arRes as $res){
                                    
                                    if($res['UF_NAME'])
                                        $name = $res['UF_NAME'];
                                    elseif($res['UF_NAME_RUS'])
                                        $name = $res['UF_NAME_RUS'];
                                    else
                                        continue;?>
                                    
                                    <option class="auto-form__item-select-var" value="<?=$name?>" data-id="<?=$res['ID']?>">
                                        <?=$name?>
                                    </option> 
                                <?}?>
                            </select>
						</div>     
                        <div class="blockError"><div class="errorMsg">Выберите марку</div></div>
					</div>
					<div class="input-trade-form require" id="model_form">
						<div class="input-trade-form require" style="margin-right:1%;">
							 Модель
							<select id="model">
								<option value="">--</option>
							</select>
						</div>
						<div class="blockError">
							<div class="errorMsg">
								Выберите модель
							</div>
						</div>
					</div>
					<div class="input-trade-form">
						 <script>
$(document).ready(function(){
	var god="2017";
	var marka=false;
	var model=false;
	var probeg=false;
	var info=false;
	var name=false;
	var phone=false;
	var email=false;
	var edata=false;
                        
	$('#god').change(function(){

		god=$(this).val();
	})
	$( "#god" ).keyup(function() {
	  god=$(this).val();
	});
	
	$('#marka').change(function(){

		 marka = $(this).val();
		 $.ajax({
			type:"POST",
			url:"getModels.php",
			data:{
				marka_id: $(this).find('option:selected').data('id')			
			},
			success:function(data){

				$('#model').html(data);
				$('#model').selectOrDie("update");
				$('#model_form .sod_select').removeClass('disabled').find('.sod_list').css({'max-height': '165px'});


				if(marka != false){ $('#marka_form').find(".errorMsg").removeClass("form_error"); }
			}
		});
	});
						
	$('#model').change(function(){
		//alert($(this).val());
		 model=$(this).val();

		 if(model != false){ $('#model_form').find(".errorMsg").removeClass("form_error"); }
	});

	$('#probeg').change(function(){
		 probeg=$(this).val();
	});

	$('#info').change(function(){
		 info=$(this).val();
	});

	$('#name').change(function(){
		 name=$(this).val();
	});

	$('#phone').change(function(){
		 phone=$(this).val();
		 console.log(phone);
	});

	$('#email').change(function(){
		 email=$(this).val();
	});

	$('#edata').change(function(){
		 edata=$(this).val();
	});

	$('#phone').keyup(function(){ 

		phone = $(this).val();
		if(phone.length>=6){
			$('#phone_require')
				.find(".errorMsg")
				.removeClass("form_error")
				.addClass("form_descr")
				.html("Не менее 6 символов");   
		}

	});
                    
	$('#go_mail').click(function(){

		//$(".sod_select").removeClass("form_error");

		$(".errorMsg").removeClass("form_error");

		$(".in-text-trade").removeClass("form_error");

		if(marka == false){$('#marka_form').find(".errorMsg").addClass("form_error");}
		if(model == false){$('#model_form').find(".errorMsg").addClass("form_error");}
		if(phone == false){
			$('#phone_require')
			.find(".errorMsg")
			.removeClass("")
			.addClass("form_error")
			.html("Обязательное поле");
		}else{

			$('#phone_require')
			.find(".errorMsg")
			.removeClass("form_error")
			.addClass("form_descr")
			.html("Не менее 6 символов");    

		}


	   if(marka != false && model != false && phone.length>=6 ){

					
	
			$.ajax({
				type:"POST",
				url:"mail.php",
				data:{
					name:name,
					phone:phone,
					marka:marka,
					model:model,
					god:god,
					probeg:probeg,
					info:info,
					email:email,
					edata:edata
				},
				success:function(){
					$.fancybox.open([$('#winCB')], {wrapCSS:'styleWrap',padding : 0});	
					//alert("Письмо отправлено!");
					$('.sod_label').html('&nbsp;');
					$('.form-trade-zayavka').trigger("reset");                                        

					 god="2017";
					 marka = false;
					 model = false;
					 probeg = false;
					 info = false;
					 name = false;
					 phone = false;
					 email = false;
					 edata = false;                                                                               

				}
			});
	   

			}
			return false;
	});
		/*				
	$("body").on("click",".modalForm__eventCancel", function(){

		$(".winCallback").animate({opacity:0},400,function(){
			$(this).css({display:"none"});
			$(".modalform__shadow").animate({opacity:0},400,function(){
				$(this).css({display:"none"});
			});
		});
	});

    $("body").on("click",".modalform__shadow", function(){
        $(".winCallback").animate({opacity:0},400,function(){            
            $(this).css({display:"none"});            
            $(".modalform__shadow").animate({opacity:0},400,function(){                
                $(this).css({display:"none"});                
            });
        });
    });
	*/
/* ------------------------- */    
});
</script> Год
						<select id="god">
                            <?for($year=2017; $year > 1989; $year--){?>
                                <option value="><?=$year?>"><?=$year?></option>
                            <?}?>
						</select>
					</div>
					<div class="input-trade-form">
						 Пробег (км) <input type="text" class="in-text-trade" id="probeg">
					</div>
					<div>
						 Доп. информация:<br>
 <textarea class="desc-tarea" id="info" style="height: 90px;"></textarea>
					</div>
					<div class="input-trade-form require" style="margin-right:1%;">
						 Имя <input type="text" class="in-text-trade" id="name">
						<div class="blockError">
							<div class="errorMsg">
							</div>
						</div>
					</div>
					<div class="input-trade-form require">
						 Телефон <input type="text" class="in-text-trade" id="phone">
						<div class="blockError" id="phone_require">
							<div class="errorMsg form_descr">
								Не менее 6 символов
							</div>
						</div>
					</div>
					<div class="input-trade-form-full">
						 E-Mail <input type="text" class="in-text-trade" id="email">
					</div>
					<div class="input-trade-form-full">
 <input type="checkbox" class="in-text-data" checked="checked" id="edata">
						Подтверждаю согласие на обработку <a href="http://new.rdm-import.ru/personal-data/" target="_blank">персональных данных</a>
					</div>
					<div style="text-align:center;">
 <button id="go_mail">Узнать цену вашего авто</button>
					</div>
				</form>
				<div class="top-banner__info-btns">
				</div>
			</div>
		</div>
	</div>
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text" style="font: 15px/19px 'RobotoLight', Helvetica, Arial, sans-serif;">
				<p>
					<b>Личная и финансовая безопасность</b>
				</p>
				<p>
					Мы берем на себя вопросы по продаже старого автомобиля: подготовки, хранения, поиска и демонстрации его потенциальным покупателям. Оформляем договоры купли-продажи по всем требованиям законодательства РФ. Гарантируем отсутствие проблем с налоговой отчетностью и постановкой на учет нового автомобиля.
				</p>
				<p>
 <b>Удобная форма оплаты</b></p><p>
					 Разница между ценой старого и нового автомобиля оплачивается наличными. Так же старый автомобиль может использоваться в качестве первоначального взноса по кредиту. Партнерские связи с банками помогают получать для наших клиентов минимальные ставки от 9,9%.<br>
				</p>
				<p>
 <b>Экономия вашего времени</b></p><p>
 Не зависите от срока продажи старого автомобиля, сезонных колебаний спроса, возможной потери его стоимости, Вы начинаете пользоваться новой машиной. Все действия по обмену могут быть проведены в один день: утром приезжает на старом автомобиле, а вечером уезжаете на новом.<br>
				</p>
				<p>
					<b>Комфорт</b>
				</p>
				<p>
					В одном месте можете обменять свой старый автомобиль на новый. Наш Автосалон предлагает самый большой выбор авто в Сибири! В наличии 200 проверенных машин с пробегом. Каждый автомобиль проверен по более 100 пунктам качества, исправен, подготовлен, юридически проверен и готов служить каждый день для Вашего комфортного движения вперед!
				</p>
			</div>
		</div>
	</div>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Смотреть видеотзывы наших клиентов</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		<p style="text-align: left;">
			 Можно много читать информации за и против использования услуги trade-in, можно узнать, как происходит все на практике. Посмотрите часть видеоотзывов наших покупателей, которые так же как и Вы стояли перед выбором, но сделали выбор в пользу нашего Автосалона и остались довольны.<br>
		</p>
	</div>
	<div class="list_video">
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/4GN1SmLZtVY" frameborder="0" allowfullscreen=""></iframe>
			<div class="author_video">
				 Отзыв Стаса из Якутии
			</div>
			<div class="video_item-play eventVideo" data-src="4GN1SmLZtVY">
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/fLZ5u3YBv_Y" frameborder="0" allowfullscreen=""></iframe>
			<div class="author_video">
				 Отзыв Юрия о гарантийном случае
			</div>
			<div class="video_item-play eventVideo" data-src="fLZ5u3YBv_Y">
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/FgoRbJ8xxoc" frameborder="0" allowfullscreen=""></iframe>
			<div class="author_video">
				 Отзыв Владимира из Ачинска
			</div>
			<div class="video_item-play eventVideo" data-src="FgoRbJ8xxoc">
			</div>
		</div>
	</div>
	<div class="offers-list__item-actions btn-under-video" style="text-align:center;">
		<div class="offers-list__item-action _calc" style="cursor:pointer;">
			<div data-src="#formObmen" class="fancy-inline offers-list__item-action-link">
				 Оставить заявку на обмен
			</div>
		</div>
	</div>
	 <style>
		.offers-list__item-action-link:before {
			content: '';
			width: 0px;
			height: 0;
			position: absolute;
			left: 16px;
			top: 10px;
		}
		.offers-list__item-action-link {
			display: block;
			font-size: 16px;
			color: #FFFFFF;
			text-decoration: none;
			position: relative;
			border-radius: 5px;
			background: #4790c6 !important;
			padding: 12px 20px 12px 20px;
		}
	</style>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Выбрать автомобиль для обмена</h2>
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"ycaweb:ads.filter",
	"trade",
	Array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"FILTER_NAME" => "arrAdsFilter",
		"SAVE_IN_SESSION" => "N"
	)
);?>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Варианты автомобилей для обмена</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		<p style="text-align: left;">
			 РДМ-Импорт предлагает на обмен более 200 автомобилей. Каждый юридически и технически проверен. Все автомобили продаются с гарантией на важнейшие узлы, такие как двигатель, коробка, электрика, и лакокрасочное покрытие. Любой из выбранных автомобилей можно посмотреть в нашем автосалоне, мы работаем ежедневно.<br>
		</p>
	</div>
	<div class="list_video">
		 <?
    global $arFilterObmen;
    $arFilterObmen = array(
        'PROPERTY_OBMEN' => 1
    );
    ?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"obmen",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"COMPONENT_TEMPLATE" => "obmen",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"SORT",1=>"",),
		"FILTER_NAME" => "arFilterObmen",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "ads",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"Марка",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"vegas:winVideo",
	".default",
Array()
);?>
<div class="winCB" id="winCB" style="display: none;">

	<div class="cb__head">
		<img src="/local/assets/img/assets/header/logo.svg" class="modalForm__logo" alt="">
	</div>
	<div class="cb__body">
 <strong>Спасибо!</strong> <span>Мы рассмотрим сообщение и обязательно свяжемся с вами.<br>
		 Пожалуйста, дождитесь ответа.</span>
	</div>
</div>
<div class="modalform__shadow">
</div>
 <?$APPLICATION->IncludeFile(
	SITE_DIR."include/forms/diagnostic.php",
	Array(),
	Array("MODE"=>"text")
);?> <?$APPLICATION->IncludeFile(
	SITE_DIR."include/forms/obmen.php",
	Array(),
	Array("MODE"=>"text")
);?> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>