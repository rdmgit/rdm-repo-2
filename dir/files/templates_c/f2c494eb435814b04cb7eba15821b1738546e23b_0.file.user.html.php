<?php
/* Smarty version 3.1.30, created on 2017-06-07 08:18:18
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/user.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59378c9aaf4b75_65586333',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f2c494eb435814b04cb7eba15821b1738546e23b' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/user.html',
      1 => 1496812354,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.html' => 1,
    'file:first.html' => 1,
    'file:user_remember.html' => 1,
    'file:user_login.html' => 1,
    'file:footer.html' => 1,
  ),
),false)) {
function content_59378c9aaf4b75_65586333 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('language', "ru");
$_smarty_tpl->_subTemplateRender("file:header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php if (!empty($_smarty_tpl->tpl_vars['USER']->value)) {?>
	<?php $_smarty_tpl->_subTemplateRender("file:first.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "user/controller/remember") {?>
	<?php $_smarty_tpl->_subTemplateRender("file:user_remember.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } else { ?>
	<?php $_smarty_tpl->_subTemplateRender("file:user_login.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
$_smarty_tpl->_subTemplateRender("file:footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
