<?php
/* Smarty version 3.1.30, created on 2017-06-09 15:59:56
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/statistic_price.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a9bcc1a9e55_44413541',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '77242911836d6481c8bc45866cd712b1d5b60844' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/statistic_price.html',
      1 => 1496812353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:statistic_price_filter.html' => 3,
    'file:statistic_export_button.html' => 1,
  ),
),false)) {
function content_593a9bcc1a9e55_44413541 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('statistic_url', "/direct/statistic/index/");
$_smarty_tpl->_assignInScope('statistic_name', "Клики");
if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['phrase'])) {?>
				<div class="box-info animated fadeInDown">
					<h2><strong>График ставок</strong></h2>
					<?php $_smarty_tpl->_subTemplateRender("file:statistic_price_filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

					<center><font class="color_pink">Необходимо выбрать клиента, кампанию, группу и фразу</font></center>
				</div>
<?php } else { ?>
				<div class="box-info animated fadeInDown">
					<h2><strong>График изменения ставок</strong> - №<?php echo $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['SELECT']['PHRASE']['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['SELECT']['PHRASE']['name'];?>
</h2>
					<?php $_smarty_tpl->_subTemplateRender("file:statistic_price_filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

					<div id="morris-area-chart" style="height: 500px;"></div>
				</div>
<?php echo '<script'; ?>
>
	
		Morris.Line({
		  element: 'morris-area-chart',
		  data: [
		  
		  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
			{ y: '<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['datetime'];?>
', a: <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['position1'];?>
, b: <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['position3'];?>
, c: <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['position4'];?>
, d: <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['position7'];?>
, e: <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['price'];?>
, f: <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['real_price'];?>
 },
		  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		  
		  ],
		  xkey: 'y',
		  ykeys: ['a', 'b', 'c', 'd', 'e', 'f'],
		  labels: ['1 место спец.', '3 место спец.', '1 место гарантии', 'Вход в гарантию', 'Ставка', 'Цена клика' ],
		  resize: true,
		  lineColors: ['#f99518', '#f99518', '#5CB85C', '#5CB85C', '#2a6496', '#f8346a' ]
		});
	
<?php echo '</script'; ?>
>	
				
				<div class="box-info full">
					<h2><strong>Средние значения ставок</strong> - №<?php echo $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['SELECT']['PHRASE']['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['SELECT']['PHRASE']['name'];?>
</h2>
						
					<form class="save" action="/direct/save/phrase/">	
					<div class="table-responsive">
						<table data-sortable class="table table-hover table-striped" id="ver_realtrue">
							<thead>
								<tr style="height: 100px;">
									<th>Кол-во</th>
									<th class="color_blue vert"><span class="text_vertical"><span class="rotated-text__inner">Ставка</span></span></th>
									<th class="color_yellow">1</th>
									<th class="color_yellow">2</th>
									<th class="color_yellow">3</th>
									<th class="color_green">4</th>
									<th class="color_green">5</th>
									<th class="color_green">6</th>
									<th class="color_green">7</th>
									<th class="color_pink vert"><span class="text_vertical"><span class="rotated-text__inner">Цена клика</span></span></th>									
									<th class="color_yellow">1</th>
									<th class="color_yellow">2</th>
									<th class="color_yellow">3</th>
									<th class="color_green">4</th>
									<th class="color_green">5</th>
									<th class="color_green">6</th>
									<th class="color_green">7</th>									
									<th class="vert"><span class="text_vertical"><span class="rotated-text__inner">Позиция</span></span></th>
								</tr>
							</thead>
							
							<tbody>
							
								<tr>
									<td>
									<strong>Avg</strong> <?php echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['count'];?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price'],2);?>
</strong>
									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position7'],2);?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['real_price'],2);?>
</strong>
									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['price7'],2);?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] <= 3) {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'];
} elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'] > 3) {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'];
} else {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['position'];
}?></strong>
									</td>
								</tr>
								<tr>
									<td>
									<strong>Max</strong>
									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price'],2);?>
</strong>
									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position7'],2);?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['real_price'],2);?>
</strong>
									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['price7'],2);?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] <= 3) {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'];
} elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'] > 3) {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'];
} else {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MAX']['position'];
}?></strong>
									</td>
								</tr>
								<tr>
									<td>
									<strong>Min</strong>
									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price'],2);?>
</strong>
									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position7'],2);?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['real_price'],2);?>
</strong>
									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['price7'],2);?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php if ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 0 && $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] <= 3) {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'];
} elseif ($_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'] > 3) {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'];
} else {
echo $_smarty_tpl->tpl_vars['DATA']->value['ANALIZE']['MIN']['position'];
}?></strong>
									</td>
								</tr>
							
							</tbody>
						</table>
						
					</div></form>
					
				</div>
				
				<div class="box-info full">
					<h2><strong>Лог ставок</strong> - №<?php echo $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['SELECT']['PHRASE']['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['SELECT']['PHRASE']['name'];?>
</h2>
					
						<?php $_smarty_tpl->_subTemplateRender("file:statistic_price_filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

						
					<form class="save" action="/direct/save/phrase/">	
					<div class="table-responsive">
						<table data-sortable class="table table-hover table-striped" id="ver_realtrue">
							<thead>
								<tr style="height: 100px;">
									<th>Дата</th>
									<th class="color_blue vert"><span class="text_vertical"><span class="rotated-text__inner">Ставка</span></span></th>
									<th class="color_yellow">1</th>
									<th class="color_yellow">2</th>
									<th class="color_yellow">3</th>
									<th class="color_green">4</th>
									<th class="color_green">5</th>
									<th class="color_green">6</th>
									<th class="color_green">7</th>
									<th class="color_pink vert"><span class="text_vertical"><span class="rotated-text__inner">Цена клика</span></span></th>
									<th class="color_yellow">1</th>
									<th class="color_yellow">2</th>
									<th class="color_yellow">3</th>
									<th class="color_green">4</th>
									<th class="color_green">5</th>
									<th class="color_green">6</th>
									<th class="color_green">7</th>									
									<th class="vert"><span class="text_vertical"><span class="rotated-text__inner">Позиция</span></span></th>
								</tr>
							</thead>
							
							<tbody>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['ELEMENT'], 'ELEMENT');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ELEMENT']->value) {
?>
								<tr>
									<td class="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 0 && $_smarty_tpl->tpl_vars['ELEMENT']->value['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['datetime'];?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 0 && $_smarty_tpl->tpl_vars['ELEMENT']->value['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price'],2);?>
</strong>
									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['position1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['position2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['position3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['position4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['position5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['position6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['position7'],2);?>

									</td>
									<td class="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 0 && $_smarty_tpl->tpl_vars['ELEMENT']->value['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['real_price'],2);?>
</strong>
									</td>	
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price1'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price2'],2);?>

									</td>
									<td class="color_yellow">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price3'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price4'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price5'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price6'],2);?>

									</td>
									<td class="color_green">
									<?php echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['price7'],2);?>

									</td>									
									<td class="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 0 && $_smarty_tpl->tpl_vars['ELEMENT']->value['position'] <= 3) {?>color_yellow<?php } elseif ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 3) {?>color_green<?php } else { ?>color_red<?php }?>">
									<strong><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 0 && $_smarty_tpl->tpl_vars['ELEMENT']->value['position'] <= 3) {
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['position'];
} elseif ($_smarty_tpl->tpl_vars['ELEMENT']->value['position'] > 3) {
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['position'];
} else {
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['position'];
}?></strong>
									</td>
								</tr>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</tbody>
						</table>
					
					</div></form>
						
					<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['pages']) && $_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['pages'] > 1) {?>	
					<div class="data-table-toolbar">
						<ul class="pagination">
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['first']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/price/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'])) {?>user/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'])) {?>company/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['first'];?>
/">&laquo;</a></li>
						  <?php
$__section_page_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_page']) ? $_smarty_tpl->tpl_vars['__smarty_section_page'] : false;
$__section_page_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['pages']) ? count($_loop) : max(0, (int) $_loop));
$__section_page_0_start = min(0, $__section_page_0_loop);
$__section_page_0_total = min(($__section_page_0_loop - $__section_page_0_start), $__section_page_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_page'] = new Smarty_Variable(array());
if ($__section_page_0_total != 0) {
for ($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] = 1, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] = $__section_page_0_start; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] <= $__section_page_0_total; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']++, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']++){
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['current'] == (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null)) {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/price/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'])) {?>user/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'])) {?>company/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] : null);?>
/"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null);?>
</a></li>
						  <?php
}
}
if ($__section_page_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_page'] = $__section_page_0_saved;
}
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['last']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/price/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'])) {?>user/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'])) {?>company/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company'];?>
/<?php }
if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['STATISTIC']['PAGE']['last'];?>
/">&raquo;</a></li>
						</ul>
					</div>
					<?php }?>
				<br><center><?php $_smarty_tpl->_subTemplateRender("file:statistic_export_button.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</center><br>	
				</div>
<?php }?>						<?php }
}
