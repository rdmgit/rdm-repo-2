<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="main__auto-form">

	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?=$arParams["FOLDER"];?>" class="auto-form _js-auto-form"  method="GET">

		<div class="auto-form__wrapper">
			
			<div class="auto-form__item _first">
				<div class="auto-form__item-title">
					Марка
				</div>
				<div class="auto-form__item-select-wrapper _js-select-wrapper">
					<select class="auto-form__item-select _js-select _js-mark-select _js-form-field" name="filter[MANUFACTURER]">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["MANUFACTURER"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			
			<div class="auto-form__item">
				<div class="auto-form__item-title">
					Модель
				</div>
				<div class="auto-form__item-select-wrapper _model">
					<select class="auto-form__item-select _js-model-select _js-form-field" name="filter[MODEL]" <?=(!$arResult["MANUFACTURER_IS_SET"]) ? "disabled='disabled'" : ""?>>
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["MODEL"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item">
				<div class="auto-form__item-title">
					Цена
				</div>
				<div class="auto-form__item-input-wrapper">
					<input type="text" placeholder="200 000" class="auto-form__item-input _js-price-input _js-form-field" name="filter[PRICE][FROM]" value="<?=$arResult["SET"]["PRICE"]["FROM"]?>">
				</div>
				<span class="auto-form__item-text">—</span>
				<div class="auto-form__item-input-wrapper">
					<input type="text" placeholder="3 000 000" class="auto-form__item-input _js-price-input _js-form-field" name="filter[PRICE][TO]" value="<?=$arResult["SET"]["PRICE"]["TO"]?>">
				</div>
			</div>
			<div class="auto-form__item _year">
				<div class="auto-form__item-title">
					Год
				</div>
				<div class="auto-form__item-select-wrapper _year">
					<select class="auto-form__item-select _js-form-field" name="filter[YEAR][FROM]">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["YEAR"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]['from']):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
				<span class="auto-form__item-text">—</span>
				<div class="auto-form__item-select-wrapper _year">
					<select class="auto-form__item-select _js-form-field" name="filter[YEAR][TO]">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["YEAR"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]['to']):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item _volume _js-mobile-volume">
				<div class="auto-form__item-title">
					Объем
				</div>
				<div class="auto-form__item-select-wrapper _volume">
					<select class="auto-form__item-select _js-form-field" name="filter[ENGINE][FROM]">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["ENGINE"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]['from']):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
				<span class="auto-form__item-text">—</span>
				<div class="auto-form__item-select-wrapper _volume">
					<select class="auto-form__item-select _js-form-field" name="filter[ENGINE][TO]">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["ENGINE"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]['to']):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
		</div>
		<div class="auto-form__wrapper">
			<!-- <div class="auto-form__item-wrapper"> -->
			<div class="auto-form__item _first _js-mobile-fuel">
				<div class="auto-form__item-title">
					Топливо
				</div>
				<div class="auto-form__item-select-wrapper _fuel">
					<select class="auto-form__item-select _js-form-field" name="<?=$arResult["SET"]["FUEL"][0]["name"]?>">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["FUEL"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item _js-mobile-unit">
				<div class="auto-form__item-title">
					Привод
				</div>
				<div class="auto-form__item-select-wrapper _unit">
					<select class="auto-form__item-select _js-form-field" name="<?=$arResult["SET"]["DRIVE"][0]["name"]?>">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["DRIVE"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item _js-mobile-kpp">
				<div class="auto-form__item-title">
					КПП
				</div>
				<div class="auto-form__item-select-wrapper _kpp">
					<select class="auto-form__item-select _js-form-field" name="<?=$arResult["SET"]["TRANSMISSION"][0]["name"]?>">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arResult["SET"]["TRANSMISSION"] as $item):?>
							<option class="auto-form__item-select-var" value="<?=$item["value"]?>"<?if($item["selected"]):?> selected="selected"<?endif;?>><?=$item["label"]?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<?/*?>
			<!-- 		<div class="auto-form__item _js-mobile-status">
                <div class="auto-form__item-title">
                    Статус
                </div>
                <div class="auto-form__item-select-wrapper _status">
                    <select class="auto-form__item-select _js-form-field">
                        <option class="auto-form__item-select-var" value="">--</option>
                        <option class="auto-form__item-select-var" value="">1</option>
                        <option class="auto-form__item-select-var" value="">2</option>
                        <option class="auto-form__item-select-var" value="">3</option>
                    </select>
                </div>
            </div> -->
			<?*/?>
			<div class="auto-form__item-controls">
				<div class="auto-form__item _rudder _js-mobile-rudder">
					<input type="checkbox" class="auto-form__item-check _js-form-field" <?=($arResult['SET']['STEERING_WHEEL']['LEFT']['selected'] ? "checked" : "")?> id="rudder" value="<?=$arResult['SET']['STEERING_WHEEL']['LEFT']['value']?>" name="filter[STEERING_WHEEL][LEFT]" />
					<label class="auto-form__item-label" for="rudder">Левый руль</label>
				</div>
				<div class="auto-form__item _foreign _js-mobile-foreign">
					<input type="checkbox" class="auto-form__item-check _js-form-field" <?=($arResult["SET"]["IS_FOREIGN"]['selected'] ? "checked" : "")?> id="foreign" value="Y" name="filter[IS_FOREIGN]" />
					<label class="auto-form__item-label" for="foreign">Иномарки</label>
				</div>
				<div class="auto-form__more _js-more">
					<span class="auto-form__more-text">Больше параметров</span>
				</div>
				<a href="#" class="auto-form__full _js-form-show">Расширенный поиск</a>
				<div class="auto-form__item-wrapper _mobile _js-mobile _hidden">
				</div>
				<button class="auto-form__submit _js-auto-submit" type="submit" name="start_filter" value="filter">
					Подобрать
				</button>
			</div>
		</div>
	<?if(true){?>
		<div class="auto-form__full-form _hidden _js-full-form-hide">
			<div class="full-form">
				<div class="full-form__item">
					<div class="full-form__item-title">
						Цвет автомобиля
					</div>
					<div class="full-form__item-wrapper">
						<div class="full-form__item-checkbox _color">
							<input type="checkbox" class="full-form__item-checkbox-input _js-form-field" id="check" />
							<label class="full-form__item-checkbox-label _any" for="check">Любой</label>
						</div>
						<?foreach($arResult["SET"]["CAR-COLORS"] as $key=>$item):?>
							<div class="full-form__item-checkbox _color">
								<input type="checkbox" <?=($item['selected'] ? 'checked' : '')?> name="<?=$item['name']?>[]" class="full-form__item-checkbox-input _js-form-field" value="<?=$item['id']?>" id="<?=$item['for']?>" />
								<label class="full-form__item-checkbox-label" for="<?=$item['for']?>" style="background-color: <?=$item['color']?>"></label>
							</div>
						<?endforeach;?>
					</div>
				</div>
				
				<div class="full-form__item">
					<div class="full-form__item-title">
						Тип кузова
					</div>
					<div class="full-form__item-wrapper">
						<?foreach($arResult["SET"]["CAR_BODY"] as $item):?>
							<div class="full-form__item-checkbox _common _<?=$item['code']?>">
								<input type="checkbox" <?=($item['selected'] ? 'checked' : '')?> value="<?=$item['id']?>" name="filter[CAR_BODY][]" class="full-form__item-checkbox-input _js-form-field" id="<?=$item['for']?>" />
								<label class="full-form__item-checkbox-label" for="<?=$item['for']?>">
									<?=$item['label']?>
									<div class="full-form__item-checkbox-label-pic"></div>
								</label>
							</div>
						<?endforeach;?>
					</div>
				</div>

				<div class="full-form__item _power">
					<div class="full-form__item-title">
						Мощность по ПТС
					</div>
					<div class="full-form__item-wrapper">
						<div class="full-form__item-select-wrapper">
							<select class="full-form__item-select _js-form-field" name='filter[POWER]'>
								<option value=''>--</option>
								<?foreach($arResult["SET"]["POWER"] as $item):?>
									<option value="<?=$item['value']?>" <?=($item["selected"]) ? "selected='selected'" : ""?>>
										до <?=$item['value']?>
									</option>
								<?endforeach;?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="full-form__item">
					<div class="full-form__item-title">
						Дополнительно
					</div>
					<div class="full-form__item-wrapper">
						<div class="full-form__item-checkbox _common">
							<input type="checkbox" class="full-form__item-checkbox-input _js-form-field" <?=($arResult["SET"]["IS_NEW"]['selected'] ? "checked" : "")?> value="Y" id="more1" name="filter[IS_NEW]" />
							<label class="full-form__item-checkbox-label" for="more1">Новые</label>
						</div>
						<div class="full-form__item-checkbox _common">
							<input type="checkbox" class="full-form__item-checkbox-input _js-form-field" <?=($arResult["SET"]["IS_HYBRID"]['selected'] ? "checked='checked'" : "")?> value="Y" id="more2" name="filter[IS_HYBRID]" />
							<label class="full-form__item-checkbox-label" for="more2">Гибрид</label>
						</div>
						<div class="full-form__item-checkbox _common">
							<input type="checkbox" class="full-form__item-checkbox-input _js-form-field" <?=($arResult['SET']['STEERING_WHEEL']['RIGHT']['selected'] ? "checked" : "")?> id="more3" value="<?=$arResult['SET']['STEERING_WHEEL']['RIGHT']['value']?>" name="filter[STEERING_WHEEL][RIGHT]" />
							<label class="full-form__item-checkbox-label" for="more3">Правый руль</label>
						</div>
						<div class="full-form__item-checkbox _common">
							<input type="checkbox" class="full-form__item-checkbox-input _js-form-field" id="more4" <?=($arResult["SET"]["HAS_RF_MILEAGE"]['selected'] ? "checked" : "")?> value="Y" name="filter[HAS_RF_MILEAGE]" />
							<label class="full-form__item-checkbox-label" for="more4">Без пробега по РФ</label>
						</div>
					</div>
				</div>
				
				<div class="full-form__controls">
					<a class="full-form__button _js-button-reset" href="http://<?=$_SERVER["HTTP_HOST"]?>/buy/">
						Сбросить фильтры
					</a>
					<a href="#" class="full-form__hide _js-form-hide">Скрыть расширенный поиск</a>
				</div>
				
				<button class="full-form__submit " type="submit" name="start_filter" value="filter">
					Подобрать
				</button>
			</div>
		</div>
		<!-- </div> -->
		<?}?>
	</form>
</div>


