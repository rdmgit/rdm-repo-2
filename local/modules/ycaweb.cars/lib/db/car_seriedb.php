<?php

namespace Ycaweb\Cars;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class CarSerieTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_serie';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
             'id_car_serie' => array(
                'data_type' => 'integer',
                'primary' => true,                
             ),   
             'id_car_model'=> array(
                'data_type' => 'integer',
                "required" => false
             ), 
             'name' => array(
                "data_type" => "string",
                "required" => false,
                "title"	=> Loc::getMessage("YCAWEB_CAR_SERIE_NAME")
             ), 
             'id_car_generation' => array(
                "data_type" => "integer",
                "required" => false
             ),
            'id_car_type'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
        );
    }
}