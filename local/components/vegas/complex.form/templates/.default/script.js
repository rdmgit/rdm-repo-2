var edataChecked = true;

var strType;
var	arParams = {};

$(document).ready(function(){
	
	var body = document.getElementsByTagName('body')[0];
	var bodyScrollTop = null;
	var locked = false;
	
	$('body').find('.mask-phone').mask('+7(999) 999-99-99');
	
	var body = document.getElementsByTagName('body')[0];
	var bodyScrollTop = null;
	var locked = false;

	// Заблокировать прокрутку страницы
	function lockScroll(){
		if (!locked) {
			bodyScrollTop = (typeof window.pageYOffset !== 'undefined') ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
			body.classList.add('scroll-locked');
			body.style.top = `-${bodyScrollTop}px`;
			locked = true;
		};
	}

	// Включить прокрутку страницы
	function unlockScroll(){
		if (locked) {
			body.classList.remove('scroll-locked');
			body.style.top = null;
			window.scrollTo(0, bodyScrollTop);
			locked = false;
		}
	}
	
	var block1 = $('#complex__form');
	var block2 = $('#complexForm__callback');
	
	
	$('body').on('click', '.onFormOpen', function(){
	
        arParams = {};
		target = $(this).data('target');
		
		$(this).find('input').each(function(){

            arParams[$(this).attr("name")] = $(this).val();

		});

		strType = $(this).attr("data-type");
		boolCapth = $(this).attr("data-cpth");
		strLayout = $(this).attr("data-layout");
	
		$.post('/complexform/',{type:strType,params:arParams},function(data){
			
			$("#complexForm__html").removeAttr('class').addClass('complexForm-html');	
			
			if(strLayout == 'layout-3'){
				
				$("#complex__form")
				.removeClass('layout-1')
				.removeClass('layout-2')
				.addClass('layout-3'); 
				
				}else{
					
					$("#complex__form")
					.removeClass('layout-3')
					.removeClass('layout-2')
					.addClass('layout-1');
					
				}
				
			lockScroll();
			$("#complexForm__html").html(data.html);	
			$.fancybox.open([block1], {
				baseClass:'complexWrap',
				padding : 0,
				autoSize:true,
				afterClose: function( instance, slide ) {unlockScroll();}
			});

			if(boolCapth == 'true'){onloadCallback(); }
		},'json');
		
	/*

		*/
	});
		
		
	$('body').on('click', '.onSubmit', function(){
		var comlexForm = $('#complex-form').serializeArray();

		$.post('/complex-send/',{type:strType,fields:comlexForm},function(data){
	
            if(data.status){
						
				if(target != undefined && target!=false){					
					console.log(target);
					yaCounter4175221.reachGoal(target);
				}
				
                $('.complexForm-input-fullrow').removeClass('error');
                $("#complexForm__callback-html").html(data.msg);
                $.fancybox.close( block1 );      
                $.fancybox.open([block2], {
                    wrapCSS:'styleWrap',
                    padding : 0
                });
				
            }else if(!data.status && data.msg == 'errorForm'){
				$('.complexForm-input-fullrow').removeClass('error');
				var arError = data.error;
				for(var i in arError){				
					$('#'+arError[i]).addClass('error');
				}
			}

				
		},"json");

		return false;
	});
	
	
	
	$('body').on('change','#complex-form .onData',function() {	
	 if ($(this).prop("checked")) {			
			edataChecked = true;		   
		}else{			
			edataChecked = false;		}
		
		cx_lockSubmit();
	});
	
	cx_lockSubmit();

/* for creditovanie */

	$('body').on('click','.onAdvanced', function(){
		strType = $(this).attr('id');
		$.post('/complex-advanced/',{type:strType},function(data){
			
			$('#complex__form')
			.find('.fancybox-close-small')
			.removeAttr('data-fancybox-close')
			.addClass('onComplexBackForm');
			
			if(strType == 'onDiff'){
				$("#complex__form").removeClass('layout-1').addClass('layout-2');
			}

			$("#complexForm__html").removeAttr('class').addClass('complexForm-'+strType);
			$("#complexForm__html").html(data.html);	
			
		},"json");	
		return false;
	});
	
	// Кнопка "Х" - возвращает к прошлой форме
	$('body').on('click','.onComplexBackForm', function(){
		
		$('#complex__form')
		.find('.fancybox-close-small')
		.attr('data-fancybox-close','')
		.removeClass('onComplexBackForm');
			
		strType = 'credit';
	
		$.post('/complexform/',{type:'credit'},function(data){
			
			$("#complexForm__html").removeAttr('class').addClass('complexForm-html');
			$("#complex__form").removeClass('layout-2').addClass('layout-1');
			$("#complexForm__html").html(data.html);	
			
		},'json');		

		return false;
	});
	

	// 
	$('body').on('click','#onCalculator', function(){
		
		$('#complex__form')
		.find('.fancybox-close-small')
		.removeAttr('data-fancybox-close')
		.addClass('onComplexBackForm');
		
		//carName
		strType = 'creditadv';
		
		$.post('/complexform/',{type:'creditadv'},function(data){
		
			$("#complexForm__html").removeAttr('class').addClass('complexForm-html');
			$("#complex__form").removeClass('layout-2').addClass('layout-1');
			$("#complexForm__html").html(data.html);
			$('#autoid').html(carName);			
			$('#auto__price').html('Стоимость автомобиля, руб:&nbsp;'+cPrice);
			
			onloadCallback();
			
			$('#result__vznos').keyup(function(eventObject){			
				result_calc();
			});
			$('#result__srok').keyup(function(eventObject){			
				result_calc();
			});
			
			
		},'json');
		
		return false;
	});		
	
	// Кнопка "Закрыть" - возвращает к прошлой форме
	$('body').on('click','#onCreditBack', function(){
		$('#complex__form')
		.find('.fancybox-close-small')
		.attr('data-fancybox-close','')
		.removeClass('onComplexBackForm');
	
		strType = 'credit';
		
		$.post('/complexform/',{type:'credit'},function(data){
			
			$("#complexForm__html").removeAttr('class').addClass('complexForm-html');
			$("#complex__form").removeClass('layout-2').addClass('layout-1');
			
			$("#complexForm__html").html(data.html);	
			onloadCallback();
		},'json');
		
		return false;
	});	


// =======================
function result_calc(){
	var v1 = $('#result__vznos').val();
	var v2 = $('#result__srok').val();
	
	if(v1 != '' && v2 != ''){
		$('.credit__desc-msg').css({display:'none'});
		$('.credit__desc-min').css({display:'block'});
		$('.credit__desc-max').css({display:'block'});

		var procent = .13;
		var kred__summa=cPrice-v1;
		var year = v2;
		var mounth = year * 12;
		var B1 = .20 * year;
		var A2 = procent * year;
		var B3 = kred__summa/mounth;
		var C2 = B3 * A2;
		var C1 = B3 * B1;
		var min = B3 + C2;
		var max = B3 + C1;
			min = min.toFixed(2);
			max = max.toFixed(2);
			
		$('#result__min').html('От: ' + min);
		$('#result__max').html('До: ' + max);
		
		$('input[name="result__min"]').val(min);
		$('input[name="result__max"]').val(max);
		$('input[name="result__price"]').val(cPrice);
		$('input[name="_autolink"]').val(autoID);

	}

}
//########################	
	function cx_lockSubmit(){
			
		if (edataChecked) {
		 
			$(".btn-submit").removeAttr("disabled").removeClass("disabled");
			
		}else{

			$(".btn-submit").attr("disabled","disabled").addClass("disabled");
		}	
		
	}

});


 var widId = "";
 
   var onloadCallback = function ()
   {
	   var response = grecaptcha.getResponse();
		widId = grecaptcha.render('recapchaWidget', {
		'sitekey':'6Lfy-SgUAAAAAHUFP8mCx8lsf5GowSPmeOKaS7b9'
        });
   };
