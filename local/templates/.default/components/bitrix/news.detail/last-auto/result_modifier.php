<?



if(isset($arResult["ITEMS"])){
	
	$arItems = $arResult["ITEMS"];
	
	foreach($arItems as $k => $arItem){
	
		if(isset($arItem["PROPERTIES"]["MORE_PHOTO"])){
			
			
			// Если в MORE_PHOTO есть фотографии
			if(is_array($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"])){
			

			$MORE_PHOTO =  $arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"];
			$arMorePhoto = array();
			$arSrc = array();
			
			if(isset($arItem["PREVIEW_PICTURE"]["SRC"])){
				
				$arMorePhoto[] = $arItem["PREVIEW_PICTURE"];
				$arSrc[] = array('src' => $arItem["PREVIEW_PICTURE"]["SRC"]);
				
			}
			
			foreach($MORE_PHOTO as $k=>$v){
				
				$arFile = CFile::GetFileArray($v);
				$arMorePhoto[] = $arFile;
				$arSrc[] = array('src' => $arFile["SRC"]);
			}

			// Если нет фотографий, то берем фото из превью
			}elseif(isset($arResult["PREVIEW_PICTURE"]["SRC"])){
				
				$arMorePhoto[] = $arResult["PREVIEW_PICTURE"];
				$arSrc[] = $arResult["PREVIEW_PICTURE"]["SRC"];
				
			}else{
				
				$arMorePhoto = false;
				$arSrc = false;
				
			}
			
			$arResult["ITEMS"][$k]["PROPERTIES"]["MORE_PHOTO"]["MODIFY"] = $arMorePhoto;
			$arResult["ITEMS"][$k]["PROPERTIES"]["MORE_PHOTO"]["_SRC"] = $arSrc;
			
		}
	}

}
?>