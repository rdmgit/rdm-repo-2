<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$sCurPage = $APPLICATION->GetCurPage(false);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>" class="no-js">
	<head>
		<?\Ycaweb\Tools::getTpl('head');?>
		<? if (!$USER->IsAuthorized()){ ?>
			<style>
				._disabled{display:none};
			</style>
		<?}?>
	</head>
<?\Ycaweb\Tools::getTpl('iebody');?>
	<body class="page">
	<script>
		if( document.getElementById('map') ){
			ymaps.ready(function () {
				var myMap = new ymaps.Map('map', {
						center: [55.040636, 82.952120],
						zoom: 14.8
					}, {
						searchControlProvider: 'yandex#search'
					}),

				// Создаём макет содержимого.
				MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
					'<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
				),
				myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
					hintContent: 'РДМ-ИМПОРТ',
					balloonContent: 'РДМ-ИМПОРТ'
				}, {
					// Опции.
					// Необходимо указать данный тип макета.
					iconLayout: 'default#image',

					// Размеры метки.
					iconImageSize: [30, 42],
					// Смещение левого верхнего угла иконки относительно
					// её "ножки" (точки привязки).
					iconImageOffset: [-5, -38]
				});
				myMap.behaviors.disable('scrollZoom');
				if( myPlacemark.length > 0 ){;
					myMap.geoObjects.add(myPlacemark)
				}
				if( typeof myPlacemarkWithContent != "undefined" && myPlacemarkWithContent.length > 0 ){;
					myMap.geoObjects.add(myPlacemarkWithContent)
				}
			});
		}
	</script>
	<style>
		#map{height:340px;}
	</style>
<?if( $sCurPage != '/kompany/history/'){?>	
<section class="page__wrapper" date-page="<?=$sCurPage?>">
<?}?>

<?if( $sCurPage == '/kompany/history/'){?>
	<? \Ycaweb\Tools::getTpl('header-history');?>
<?}else{?>
	<? \Ycaweb\Tools::getTpl('header');?>
<?}?>
	
<?if( $sCurPage == '/' ){?>
	<?\Ycaweb\Tools::showH1Title()?>
	<div class="page__main-wrapper">
		<div class="main">
			<div class="main__wrapper">
				<div class="main__title">
					<?$APPLICATION->IncludeFile(SITE_DIR."include/cars_title.php", Array(), Array("MODE" => "html", "NAME"  => GetMessage("COPYRIGHT"),));?>
				</div>
<?}?>
				
<?if( $sCurPage == '/creditovanie/' 
		|| $sCurPage == '/creditovanie/autocreditovanie/' 
		|| $sCurPage == '/kompany/' 
		|| $sCurPage == '/contacts/' 
		&& $sCurPage != '/kompany/history/'
){?>
	<div class="page__main-wrapper page__wrapper-full <?$APPLICATION->ShowProperty("wrapper_class");?>">
		<div class="offers-list-wrapper">
			<div class="offers-list">
			<script>
				$(document).ready(function(){
		
					$("#menu a, .anim").hover( function() {
						$(this).animate({"paddingLeft" : "10px"}, 300);
					},
					function() {
						$(this).animate({"paddingLeft" : "0"}, 300);
					});
				});
			</script>
			<div class="top-banner__info-title"><?// \Ycaweb\Tools::showH1Title()?></div>
<?}else{?>

<?}?>