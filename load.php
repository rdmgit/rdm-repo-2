<?php

/** oldArr[ID]=hash; */
$string = file_get_contents("json/old.json") OR die('Error #1');
$oldArr = json_decode($string);
$oldArr = (array)$oldArr;

/** newArr[hash] = ID; */
$string = file_get_contents("json/new.json") OR die('Error #2');
$newArr = json_decode($string);
$newArr = (array)$newArr;

$countHasMatch = 0;
$countHasNoMatch = 0;

$matches = [];
$count = 0;
$prevNewItemID = null;

foreach ($oldArr as $oldItemID => $oldItemHash) {
    if (isset($newArr[$oldItemHash])) {
        $newArrID = $newArr[$oldItemHash];
        $matches[$oldItemID] = $newArrID;

        $prevNewItemID = $newArrID;

        $countHasMatch++;
    } else {
        $matches[$oldItemID] = $prevNewItemID;

        $countHasNoMatch++;
    }
}

echo '<pre>';
print_r('has match: ' . $countHasMatch);
echo '</pre>';
echo '<pre>';
print_r('has no match: ' . $countHasNoMatch);
echo '</pre>';

$host = 'localhost';
$database = 'amazuref_bd';        
$tableName = 'old_new_rdm'; 
$user = 'amazuref_bd';             
$password = 'rdmpuck123';             

$link = mysqli_connect($host, $user, $password, $database) or die("err0 " . mysqli_error($link));

$query = 'SHOW TABLES FROM ' . $database . ' LIKE "' . $tableName . '";';
$result = mysqli_query($link, $query) or die("err1" . mysqli_error($link));
$countTables = mysqli_num_rows($result);

if ($countTables == 0) {
    $query = "CREATE TABLE " . $tableName . "(old_id INT PRIMARY KEY, new_id INT);";
    mysqli_query($link, $query) or die("err1" . mysqli_error($link));
} else {
    $query = "DELETE FROM " . $tableName . ";";
    mysqli_query($link, $query) or die("err2" . mysqli_error($link));
}

$queryDataSize = 50;
$queryData = [];
foreach ($matches as $oldItemID => $newItemID) {
    if (count($queryData) > 0 && count($queryData) % $queryDataSize == 0) {
        echo '<pre>';
        print_r($queryData);
        echo '</pre>';

        $queryString = "";
        $count = 0;
        foreach ($queryData as $oldItemID => $newItemID) {
            $count++;
            $queryString = $queryString . "(" . $oldItemID . ", " . $newItemID . "), ";
        }
        $queryString = substr($queryString, 0, strlen($queryString) - 2);
        $query = "INSERT INTO " . $tableName . "(old_id, new_id) VALUES " . $queryString . ";";
        mysqli_query($link, $query) or die("err3" . mysqli_error($link)) or die("err1 " . mysqli_error($link));

        $queryData = [];
    } else {
        $queryData[$oldItemID] = $newItemID;
    }
}

mysqli_close($link);