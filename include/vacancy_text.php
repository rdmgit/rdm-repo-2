<div class="vacancy-about">
 <img alt="Роман, РДМ-Импорт" src="/local/assets/img/rdm-import-roman.jpg">
	<div class="offers-list__item-text">
		<p>
			Здравствуйте, меня зовут Роман Облецов, я 15 лет управляющий автосалона РДМ-Импорт Мы лидеры в Сибири по продаже подержанных автомобилей.
		</p>
		<p>
			Наше предназначение - обеспечивать жителей Новосибирска и регионов Сибири ТОЛЬКО качественными и проверенными автомобилями, чтобы выбор и покупка были удобными, безопасными, и приятными, а с машинами не было сюрпризов и проблем в будущем.
		</p>
		<p>
			Мы создаем цивилизованный рынок продажи автотранспорта: без обмана, без лжи, без унижения и хамства. Мы предоставляем покупателю всю известную информацию об автомобиле «как есть», до покупки. Не смотря на то что автомобили не новые, на каждый из них мы предоставляем гарантию - в этом наша особенность.
		</p>
		<p>
			На данный момент в связи с расширением персонала я набираю дополнительных людей в свою команду.
		</p>
		<p>
			Мне требуются особенные люди: активные, нацеленные на результат, желающие хорошо зарабатывать и готовые для этого эффективно работать.
		</p>
		<p>
			У нас сильная команда, нет текучести кадров, но при этом мы очень требовательны, хотя и всячески помогаем тем, кто к нам присоединяется.
		</p>
		<p>
			Если вы имеете цели в жизни, если вы способны достигать поставленных задач, работать в команде, мы будем рады видеть вас, и не важно какое у вас образование. Главное для нас - это ваше желание работать и обучаться.
		</p>
	</div>
</div>
<br>