<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!strstr($_SERVER['HTTP_HOST'],'rdm-import.ru')){	
	$domain = 'https://rdm-import.ru';	
}else{	
	$domain = '';
}

if(!empty($arResult['ITEMS']))
{
	$arCurrencies = YcawebHelper::getCurrencyPrice();

	foreach($arResult['ITEMS'] as &$arItem)
	{
		if(!empty($arItem['PROPERTIES']['PRICE']['VALUE']))
		{
			$arItem["FORMATTED_PRICE_RUB"] = YcawebHelper::priceFormat($arItem['PROPERTIES']['PRICE']['VALUE']);
			foreach($arCurrencies as $code=>$arCurrency)
			{
				$price = round( (int)$arItem['PROPERTIES']['PRICE']['VALUE'] / (int)$arCurrency['RATE'], 0 );
				$arItem['PRICE'][$code]['VALUE'] = YcawebHelper::priceFormat($price);
				$arItem['PRICE'][$code]['SYMBOL'] = $arCurrency['SYMBOL'];
			}
		}
		
		//if(empty($arItem["PREVIEW_PICTURE"]["SRC"]))
		if($arItem["PREVIEW_PICTURE"]["SRC"]){
             
            $arImgTmp = CFile::ResizeImageGet(
				$arItem["PREVIEW_PICTURE"],
				array("width" => 155, "height" => 155),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true
			);
			
			$arItem["PREVIEW_PICTURE_MIN"]["SRC"] = $domain.$arImgTmp["src"];

			/*
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$arImgTmp["src"])){
				$arItem["PREVIEW_PICTURE_MIN"]["SRC"] = 'http://rdm-import.ru'.$arImgTmp["src"]; 
			}else{
				$arItem["PREVIEW_PICTURE_MIN"]["SRC"] = NO_PHOTO_PATH;
			}
				*/		
        }    
        else
		{
			//$arItem["PREVIEW_PICTURE"]["SRC"] = NO_PHOTO_PATH;
			$arItem["PREVIEW_PICTURE_MIN"]["SRC"] = NO_PHOTO_PATH;
		}
	}
}
