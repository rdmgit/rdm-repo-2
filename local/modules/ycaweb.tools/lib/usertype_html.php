<?php

namespace Ycaweb;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

Loc::loadMessages(__FILE__);

class CUserTypeHTMLEditor extends \CUserTypeString{

	public function GetUserTypeDescription(){
		return array(
			'USER_TYPE_ID'	=> 'text_html',
			'CLASS_NAME'	=> 'CUserTypeHTMLEditor',
			'DESCRIPTION'	=> Loc::getMessage('TYPE_DESCRIPTION'),
			'BASE_TYPE'		=> 'string',
		);
	}

	//Этот метод вызывается для показа значений в списке
	public function GetAdminListViewHTML($arUserField, $arHtmlControl){
		$arHtmlControl["VALUE"] = unserialize($arHtmlControl["VALUE"]);
		if(strlen($arHtmlControl["VALUE"]["TEXT"]) > 0)
			return $arHtmlControl["VALUE"]["TEXT"];
		else
			return ' ';
	}

	//Эта функция вызывается при выводе формы настройки свойства.
	function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm){
		$result = '';
		if($bVarsFromForm)
			$value = htmlspecialcharsbx($GLOBALS[$arHtmlControl["NAME"]]["DEFAULT_VALUE"]);
		elseif(is_array($arUserField))
			$value = htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
		else
			$value = "";
		$result .= '
		<tr>
			<td>'.Loc::getMessage("USER_TYPE_STRING_DEFAULT_VALUE").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[DEFAULT_VALUE]" size="20"  maxlength="225" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = intval($GLOBALS[$arHtmlControl["NAME"]]["SIZE"]);
		elseif(is_array($arUserField))
			$value = intval($arUserField["SETTINGS"]["SIZE"]);
		else
			$value = 20;
		$result .= '
		<tr>
			<td>'.Loc::getMessage("USER_TYPE_STRING_SIZE").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[SIZE]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = intval($GLOBALS[$arHtmlControl["NAME"]]["ROWS"]);
		elseif(is_array($arUserField))
			$value = intval($arUserField["SETTINGS"]["ROWS"]);
		else
			$value = 1;
		if($value < 1) $value = 1;
		$result .= '
		<tr>
			<td>'.Loc::getMessage("USER_TYPE_STRING_ROWS").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[ROWS]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = intval($GLOBALS[$arHtmlControl["NAME"]]["MIN_LENGTH"]);
		elseif(is_array($arUserField))
			$value = intval($arUserField["SETTINGS"]["MIN_LENGTH"]);
		else
			$value = 0;
		$result .= '
		<tr>
			<td>'.Loc::getMessage("USER_TYPE_STRING_MIN_LEGTH").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[MIN_LENGTH]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = intval($GLOBALS[$arHtmlControl["NAME"]]["MAX_LENGTH"]);
		elseif(is_array($arUserField))
			$value = intval($arUserField["SETTINGS"]["MAX_LENGTH"]);
		else
			$value = 0;
		$result .= '
		<tr>
			<td>'.Loc::getMessage("USER_TYPE_STRING_MAX_LENGTH").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[MAX_LENGTH]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = htmlspecialcharsbx($GLOBALS[$arHtmlControl["NAME"]]["REGEXP"]);
		elseif(is_array($arUserField))
			$value = htmlspecialcharsbx($arUserField["SETTINGS"]["REGEXP"]);
		else
			$value = "";
		$result .= '
		<tr>
			<td>'.Loc::getMessage("USER_TYPE_STRING_REGEXP").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[REGEXP]" size="20"  maxlength="200" value="'.$value.'">
			</td>
		</tr>
		';
		return $result;
	}

	//Эта функция вызывается при выводе формы редактирования значения свойства
	public function GetEditFormHTML($arUserField, $arHtmlControl){
		$arValue = unserialize(htmlspecialcharsback($arHtmlControl["VALUE"]));

		if(strToLower($arValue["TYPE"]) != "text"){
			$arValue["TYPE"] = "html";
		}else{
			$arValue["TYPE"] = "text";
		}
		ob_start();
		?>
		<table>
			<?if(COption::GetOptionString("iblock", "use_htmledit", "Y") == "Y" && CModule::IncludeModule("fileman")):?>
				<tr>
					<td colspan="2" align="center">
						<input type="hidden" name="<?=$arHtmlControl["NAME"]?>" value="">
						<?
						$text_name = preg_replace("/([^a-z0-9])/is", "_", $arHtmlControl["NAME"]."[TEXT]");
						$text_type = preg_replace("/([^a-z0-9])/is", "_", $arHtmlControl["NAME"]."[TYPE]");
						CFileMan::AddHTMLEditorFrame($text_name, $arValue["TEXT"], $text_type, strToLower($arValue["TYPE"]), array("height"=>350), "N", 0, "", "");
						?>
					</td>
				</tr>
			<?else:?>
				<tr>
					<td><?echo Loc::getMessage("IBLOCK_DESC_TYPE")?></td>
					<td>
						<input type="radio" name="<?=$arHtmlControl["NAME"]?>[TYPE]" id="<?=$arHtmlControl["NAME"]?>_TYPE_TEXT" value="text"<?if($arValue["TYPE"]=="text")echo " checked"?>>
						<label for="<?=$arHtmlControl["NAME"]?>_TYPE_TEXT"><?echo Loc::getMessage("IBLOCK_DESC_TYPE_HTML")?></label>
						<input type="radio" name="<?=$arHtmlControl["NAME"]?>[TYPE]" id="<?=$arHtmlControl["NAME"]?>_TYPE_HTML" value="html"<?if($arValue["TYPE"]=="html")echo " checked"?>>
						<label for="<?=$arHtmlControl["NAME"]?>_TYPE_HTML"><?echo Loc::getMessage("IBLOCK_DESC_TYPE_HTML")?></label>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><textarea cols="60" rows="10" name="<?=$arHtmlControl["NAME"]?>[TEXT]" style="width:100%"><?=$arValue["TEXT"]?></textarea></td>
				</tr>
			<?endif;?>
		</table>
		<?
		$return = ob_get_contents();
		ob_end_clean();
		return  $return;
	}

	public function OnBeforeSave($arUserField, $value) {
		global $DB;

		$value = serialize(array("TYPE" => $_REQUEST[$arUserField["FIELD_NAME"]."_TYPE_"], "TEXT" => trim($_REQUEST[$arUserField["FIELD_NAME"]."_TEXT_"])));
		return $value;
	}

}