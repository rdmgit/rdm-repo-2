<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$defCity = 'c65';
$arCities = include(dirname(__FILE__).'/lang/'.LANGUAGE_ID.'/city.php');

$arComponentParameters = array(
	"PARAMETERS" => array(
		'DISPLAY_DATE' => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("WEATHER_INFORMER_DISPLAY_DATE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "Y",
		),
		'DISPLAY_TIME' => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("WEATHER_INFORMER_DISPLAY_TIME"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		'DISPLAY_WEATHER' => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("WEATHER_INFORMER_DISPLAY_WEATHER"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "Y",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>60),
	)
);

if($arCurrentValues["DISPLAY_DATE"] == "Y"){
	$arComponentParameters["PARAMETERS"]["DATE_FORMAT"] = CIBlockParameters::GetDateFormat(GetMessage("WEATHER_INFORMER_DATE_FORMAT"), "DATA_SOURCE");
}

if($arCurrentValues["DISPLAY_WEATHER"] == "Y"){
	$arComponentParameters["PARAMETERS"]["CITY"] = array(
		"PARENT" => "BASE",
		"NAME" => GetMessage("WEATHER_INFORMER_CITY"),
		"TYPE" => "LIST",
		"MULTIPLE" => "N",
		"DEFAULT" => $defCity,
		"VALUES"=>$arCity,
	);
}