<?
namespace Ycaweb\Cars;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;

Loc::loadMessages(__FILE__);

class CarEquipmentTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_equipment';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id_car_equipment' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'id_car_modification' => array(
                'data_type' => 'integer',
              //  'required' => true,
            ),
            'price_min' => array(
                'data_type' => 'integer',
                'required' => false,
                'title' => Loc::getMessage("YCAWEB_CAR_EQUIPMENT_PRICE_MIN")
            ),
            'year' => array(
                'data_type' => 'integer',
                'required' => false,
                'title' => Loc::getMessage("YCAWEB_CAR_EQUIPMENT_YEAR")
            ),
            'name' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_EQUIPMENT_NAME")
            ),
            'id_car_type'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
        );
    }
}
?>


