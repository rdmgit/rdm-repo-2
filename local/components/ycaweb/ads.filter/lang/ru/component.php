<?
$MESS ['CC_BCF_ALL'] = "(все)";
$MESS ['CC_BCF_TILL'] = "по";
$MESS ['CC_BCF_TOP_LEVEL'] = "Верхний уровень";
$MESS ['CC_BCF_INCLUDE_SUBSECTIONS'] = "включая подразделы";
$MESS ['CC_BCF_MODULE_NOT_INSTALLED'] = "Модуль Информационных блоков не установлен";

$MESS ['MF_FIO_NOT_VALID'] = "Вы не указали ФИО.";
$MESS ['MF_PHONE_NOT_VALID'] = "Вы не указали телефон.";
$MESS ['MF_EMAIL_NOT_VALID'] = "Вы не указали Email.";
$MESS ['MF_PRICE_NOT_VALID'] = "Вы не указали цену объекта.";
$MESS ['MF_STREET_NOT_VALID'] = "Вы не указали адрес объекта.";

$MESS ['MF_SEND_OK'] = "Ваша заявка отправлена, в ближайшее время мы свяжемся с Вами.";

$MESS ['MF_OK_TPL'] = '<p class="formOk">%s</p>';
$MESS ['MF_ERROR_TPL'] = '<p class="formError">%s</p>';
?>