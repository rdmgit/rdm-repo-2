<?php
/* Smarty version 3.1.30, created on 2017-06-07 08:18:22
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/auction_filter.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59378c9eb1f1d0_90242554',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea64da126d43624dd1de1c2886ab7556362c0ac9' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/auction_filter.html',
      1 => 1496812348,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59378c9eb1f1d0_90242554 (Smarty_Internal_Template $_smarty_tpl) {
?>

					<div class="data-table-toolbar"><form class="filter" role="form" action="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/auction/" method="GET"><input type="hidden" name="page" value="0">
							<div class="row">
								<div class="col-md-3">
									<select name="user" class="filter_user form-control" data-toggle="tooltip" title="Клиент">
										<option value="">Все аккаунты</option>
										<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['USER']['ELEMENT'])) {?>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['USER']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['user']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['login'];
if (!empty($_smarty_tpl->tpl_vars['VALUE']->value['name'])) {?> - <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];
}?></option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										<?php }?>
									</select>
								</div>
								<div class="col-md-3">
									<select name="company" class="filter_company form-control" data-toggle="tooltip" title="Кампания">
										<option value="">Все кампании</option>
										<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['COMPANY']['ELEMENT'])) {?>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['COMPANY']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['company']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										<?php }?>
									</select>
								</div>
								<div class="col-md-3">
									<select name="group" class="filter_group form-control" data-toggle="tooltip" title="Объявление">
										<option value="">Все группы объявлений</option>
										<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['GROUP']['ELEMENT'])) {?>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['GROUP']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										<?php }?>
									</select>
								</div>
								<div class="col-md-3">
									<select name="status" class="form-control" data-toggle="tooltip" title="Статус">
										<option value="0">Все статусы</option>
										<option value="1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'] == 1) {?> selected<?php }?>>Запущенные</option>
										<option value="2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['status'] == 2) {?> selected<?php }?>>Остановленные</option>
									</select>
								</div>
							</div>

							<div class="row" style="margin-top: 2px;">
								<div class="col-md-3">
									<input type="text" class="form-control" name="search" value="<?php echo htmlspecialchars(urldecode($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['search']));?>
" data-toggle="tooltip" title="Поиск по тексту" placeholder="Поиск">
								</div>
								<div class="col-md-3">
									<select name="where" class="form-control" data-toggle="tooltip" title="Где искать">
										<option value="1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 1) {?> selected<?php }?>>Искать в «Кампании»</option>
										<option value="2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 2) {?> selected<?php }?>>Искать в «Группы»</option>
										<option value="3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 3) {?> selected<?php }?>>Искать в «Фразы»</option>
										<option value="4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 4) {?> selected<?php }?>>Искать в «Ретаргетинг»</option>
									</select>
								</div>	
								<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['FILTER']['TAG']['ELEMENT'])) {?><div class="col-md-3">
									<select name="tag" class="form-control" data-toggle="tooltip" title="Метки">
										<option value="">Все метки</option>
										
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['FILTER']['TAG']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
												<option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['tag']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
</option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										
									</select>
								</div><?php }?>							
								<div class="col-md-1">
									<input type="text" class="form-control" name="number" value="<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['number'];
} else {
echo $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['number'];
}?>" data-toggle="tooltip" title="Количество записей" placeholder="Количество">
								</div>												
								<div class="col-md-1">	
									<button class="btn btn-default" type="submit">Искать &#128269;</button>
								</div>
							</div>
							<div class="row" style="margin-top: 2px;">
								<div class="col-md-3">
									<select name="sort" class="form-control" data-toggle="tooltip" title="Сортировка">
										<option value="0"<?php if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'])) {?> selected<?php }?>>Сортировка по умолчанию</option>
										<option value="id"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'id') {?> selected<?php }?>>Сортировка по «id»&uarr;</option>
										<option value="-id"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-id') {?> selected<?php }?>>Сортировка по «id»&darr;</option>
										<option value="name"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'name') {?> selected<?php }?>>Сортировка по «наименованию»&uarr;</option>
										<option value="-name"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-name') {?> selected<?php }?>>Сортировка по «наименованию»&darr;</option>
										<option value="strategy"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'strategy') {?> selected<?php }?>>Сортировка по «стратегии»&uarr;</option>
										<option value="-strategy"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-strategy') {?> selected<?php }?>>Сортировка по «стратегии»&darr;</option>
										<option value="maximum"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'maximum') {?> selected<?php }?>>Сортировка по «макс. ставке»&uarr;</option>
										<option value="-maximum"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-maximum') {?> selected<?php }?>>Сортировка по «макс. ставке»&darr;</option>										
										<option value="show"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'show') {?> selected<?php }?>>Сортировка по «показам»&uarr;</option>
										<option value="-show"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-show') {?> selected<?php }?>>Сортировка по «показам»&darr;</option>
										<option value="click"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'click') {?> selected<?php }?>>Сортировка по «кликам»&uarr;</option>
										<option value="-click"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-click') {?> selected<?php }?>>Сортировка по «кликам»&darr;</option>
										<option value="ctr"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'ctr') {?> selected<?php }?>>Сортировка по «CTR»&uarr;</option>
										<option value="-ctr"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-ctr') {?> selected<?php }?>>Сортировка по «CTR»&darr;</option>
										<option value="conversion"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'conversion') {?> selected<?php }?>>Сортировка по «конверсиям»&uarr;</option>
										<option value="-conversion"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-conversion') {?> selected<?php }?>>Сортировка по «конверсиям»&darr;</option>
										<option value="roi"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'roi') {?> selected<?php }?>>Сортировка по «ROI»&uarr;</option>
										<option value="-roi"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-roi') {?> selected<?php }?>>Сортировка по «ROI»&darr;</option>
										<option value="revenue"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == 'revenue') {?> selected<?php }?>>Сортировка по «доходу»&uarr;</option>
										<option value="-revenue"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['sort'] == '-revenue') {?> selected<?php }?>>Сортировка по «доходу»&darr;</option>										

									</select>
								</div>
								<div class="col-md-3">
									<select name="strategy" class="form-control" data-toggle="tooltip" title="Стратегия">
										<option value="0"<?php if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'])) {?> selected<?php }?>>Все стратегии</option>
										<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['STRATEGY']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
											<?php if (empty($_smarty_tpl->tpl_vars['company']->value) || (!empty($_smarty_tpl->tpl_vars['company']->value) && $_smarty_tpl->tpl_vars['VALUE']->value['id'] != -1)) {?><option value="<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == 0) {?>-3<?php } else {
echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy']) && ((!empty($_smarty_tpl->tpl_vars['VALUE']->value['id']) && $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'] == $_smarty_tpl->tpl_vars['VALUE']->value['id']) || (empty($_smarty_tpl->tpl_vars['VALUE']->value['id']) && $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['strategy'] == -3))) {?> selected<?php }?>><?php if (!empty($_smarty_tpl->tpl_vars['company']->value) && $_smarty_tpl->tpl_vars['VALUE']->value['id'] == 0) {?>Не обновлять ставки<?php } else {
echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];
}?></option><?php }?>
										<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

									</select>
								</div>	
								<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where']) && $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 3) {?><div class="col-md-3">
									<select name="place" class="form-control" data-toggle="tooltip" title="Искать по позиции фразы">
										<option value="0"<?php if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'])) {?> selected<?php }?>>Все позиции</option>
										<option class="color_red" value="-1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == -1) {?> selected<?php }?>>Не видно или 2 страница &empty;</option>
										<option class="color_yellow" value="1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 1) {?> selected<?php }?>>1 место спец. &uarr;</option>
										<option class="color_yellow" value="2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 2) {?> selected<?php }?>>2 место спец. &uarr;</option>
										<option class="color_yellow" value="3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 3) {?> selected<?php }?>>3 место спец. &uarr;</option>
										<option class="color_green" value="4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 4) {?> selected<?php }?>>4 место гарантии &darr;</option>
										<option class="color_green" value="5"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 5) {?> selected<?php }?>>5 место гарантии &darr;</option>
										<option class="color_green" value="6"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 6) {?> selected<?php }?>>6 место гарантии &darr;</option>
										<option class="color_green" value="7"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['place'] == 7) {?> selected<?php }?>>7 место гарантии &darr;</option>

									</select>
								</div><?php }?>
								<div class="col-md-1">	
									<button class="btn btn-default" type="submit">&#128269;</button>
								</div>								
							</div>
							<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where']) && $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] == 3) {?><div class="row" style="margin-top: 2px;">							
								<div class="col-md-3">
									<select name="pricelist" class="form-control" data-toggle="tooltip" title="Поиск фраз в диапазоне цен за выбранное место">
										<option value="0"<?php if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'])) {?> selected<?php }?>>Все места</option>
										<option class="color_yellow" value="1"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'] == 1) {?> selected<?php }?>>1 место спец. &uarr;</option>
										<option class="color_yellow" value="2"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'] == 2) {?> selected<?php }?>>2 место спец. &uarr;</option>
										<option class="color_yellow" value="3"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'] == 3) {?> selected<?php }?>>3 место спец. &uarr;</option>
										<option class="color_green" value="4"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'] == 4) {?> selected<?php }?>>4 место гарантии &darr;</option>
										<option class="color_green" value="5"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'] == 5) {?> selected<?php }?>>5 место гарантии &darr;</option>
										<option class="color_green" value="6"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'] == 6) {?> selected<?php }?>>6 место гарантии &darr;</option>
										<option class="color_green" value="7"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['pricelist'] == 7) {?> selected<?php }?>>7 место гарантии &darr;</option>

									</select>
								</div>
								<div class="col-md-1" title="Искать по списываемой цене">
									<span data-toggle="tooltip" title="Отобрать по списываемой цене">Цена</span> <input type="checkbox" name="real" value="1"<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['real'])) {?> checked<?php }?> />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" name="price1" value="<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price1'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price1'];
}?>" data-toggle="tooltip" title="Ставка больше" placeholder=">">
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" name="price2" value="<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price1'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['price2'];
}?>" data-toggle="tooltip" title="Ставка меньше" placeholder="<">
								</div>
								
								<div class="col-md-1">	
									<button class="btn btn-default" type="submit">&#128269;</button>
								</div>
							
							</div><?php }?>
							</form>
							
							<hr>
							<div class="row">
								
								<div class="col-md-3">
									<select id="strategy_id" name="strategy" class="form-control" data-toggle="tooltip" title="Стратегия">
										<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['STRATEGY']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
											<?php if (empty($_smarty_tpl->tpl_vars['company']->value) || (!empty($_smarty_tpl->tpl_vars['company']->value) && $_smarty_tpl->tpl_vars['VALUE']->value['id'] != -1)) {?><option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['VALUE']->value['id'] == 0) {?> selected<?php }?>><?php if (!empty($_smarty_tpl->tpl_vars['company']->value) && $_smarty_tpl->tpl_vars['VALUE']->value['id'] == 0) {?>Не обновлять ставки<?php } else {
echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];
}?></option><?php }?>
										<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

									</select>
								</div>							
								<div class="col-md-1">
									<input type="text" class="form-control" id="strategy_percent" value="" data-toggle="tooltip" title="Наценка в процентах. С галочкой от разницы до 1 места" placeholder="+, %">
								</div>
								<div class="col-md-1">
									% <input type="checkbox" id="strategy_type" value="1"> &#8597;
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" id="strategy_add" value="" data-toggle="tooltip" title="Наценка в валюте кампании" placeholder="&plusmn;, ед.">
								</div>	
								<div class="col-md-1">
									<input type="text" class="form-control" id="strategy_maximum" value="" data-toggle="tooltip" title="Максимальная ставка" placeholder="Макс.">
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" id="strategy_fixed" value="" data-toggle="tooltip" title="Фиксированная ставка" placeholder="Фикс.">
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" id="strategy_budget" value="" data-toggle="tooltip" title="Ограничение дневного бюджета" placeholder="&uarr;, ед.">
								</div>								
								<div class="col-md-3">	
									<button class="btn btn-default" id="strategy_button" type="button">Установить &crarr;</button>
								</div>
							</div>
							<div class="row" style="margin-top: 2px;">								
								<div class="col-md-3">
									&nbsp;РСЯ включить <input type="checkbox" id="context" value="1">
								</div>							
								<div class="col-md-1">
									<input type="text" class="form-control" id="context_percent" value="" data-toggle="tooltip" title="РСЯ: Максимальный охват в процентах, %" placeholder="&#8597;, %">
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" id="context_maximum" value="" data-toggle="tooltip" title="РСЯ: Максимальная ставка" placeholder="Макс.">
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" id="context_fixed" value="" data-toggle="tooltip" title="РСЯ: Ставка для РСЯ при отсутствии информации об охвате аудитории" placeholder="Фикс.">
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" id="context_minimum" value="" data-toggle="tooltip" title="РСЯ: Минимальная ставка" placeholder="Мин.">
								</div>
								<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['xml_user']) && !empty($_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['xml_key'])) {?>
								<div class="col-md-1">
									XML <input type="checkbox" id="position" value="1">
								</div>
								<?php }?>							
							</div>
						</div>
					<?php }
}
