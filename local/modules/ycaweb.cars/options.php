<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');?>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');?>

<?
use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Config\Option,
    Ycaweb\Cars;

global $APPLICATION;
$module_id = 'ycaweb.cars';

\Bitrix\Main\Loader::includeModule($module_id);

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);


$APPLICATION->SetTitle(Loc::getMessage('YCAWEB_CARS_DB_OPTIONS_PAGE_TITLE'));

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);

// get iblocks list
$res = CIBlock::GetList(
    Array(),
    array('ACTIVE' => 'Y', /*"SITE_ID" => SITE_ID*/),
    true
);
$arIblocks[''] = "";
while($ar_res = $res->Fetch()){
    $arIblocks[$ar_res["ID"]] = $ar_res["NAME"];
}

$arAllOptions = array(
    'TAB1'	=> array(
        'TITLE'		=> Loc::getMessage('YCAWEB_CARS_DB_OPTIONS_PAGE_TITLE'),
        'GROUPS'	=> array(
            array(
                'TITLE'		=> Loc::getMessage('YCAWEB_SOURCEBUSTER_OPTION_PLUGIN'),
                'OPTIONS'	=> array(
                    array("carIblock", GetMessage("YCAWEB_CARS_DB_OPTIONS_CAR_IBLOCK_NAME"), 0, Array("select", $arIblocks)),
                ),
            ),
        )
    ),
);

$getCarIblockOption = Option::get($module_id, 'carIblock');

// get iblock properties
if(!empty($getCarIblockOption))
{
    $rsProp = CIBlockProperty::GetList(array("sort" => "asc", "name" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => $getCarIblockOption));
    while ($arr = $rsProp->Fetch())
    {
        $arProperties[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
    }

    $arAllOptions["TAB1"]["GROUPS"][] = array(
        'TITLE'		=> Loc::getMessage('YCAWEB_SOURCEBUSTER_OPTION_GENERAL'),
        'OPTIONS'	=> array(
            array("car_mark", GetMessage("YCAWEB_CARS_DB_OPTIONS_CAR_MARK"), 0, Array("select", $arProperties)),
            array("car_model", GetMessage("YCAWEB_CARS_DB_OPTIONS_CAR_MODEL"), 0, Array("select", $arProperties)),
            array("car_generation", GetMessage("YCAWEB_CARS_DB_OPTIONS_CAR_GENERATION"), 0, Array("select", $arProperties)),
            array("car_serie", GetMessage("YCAWEB_CARS_DB_OPTIONS_CAR_SERIE"), 0, Array("select", $arProperties)),
            array("car_modification", GetMessage("YCAWEB_CARS_DB_OPTIONS_CAR_MODIFICATION"), 0, Array("select", $arProperties)),
        ),
    );
    $carCharacteristics = Cars\Cars::getAllCharacteristics();

    $propOptions = array();
    foreach($carCharacteristics as $characteristic)
    {
        $propOptions[] = array(
            $characteristic["id_car_characteristic"],
            $characteristic["name"],
            0,
            array(
                "select",
                $arProperties
            )
        );
    }
    $arAllOptions["TAB1"]["GROUPS"][] = array(
        "TITLE" => Loc::getMessage('YCAWEB_CARS_DB_OPTIONS_СAR_CHARACTERISTICS_TITLE'),
        "OPTIONS" => $propOptions,
    );
}

$aTabs = array();
foreach($arAllOptions as $tabCode=>$arTab){
    $aTabs[] = array('DIV' => 'edit_'.strtolower($tabCode), 'TAB' => $arTab['TITLE'], 'ICON' => '', 'TITLE' => '');
}
$tabControl = new CAdminTabControl('tabControl', $aTabs);

//Process form
if($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid()){

    foreach($arAllOptions as $arTab){

        foreach($arTab['GROUPS'] as $arGroup)
        {
            foreach($arGroup['OPTIONS'] as $arOption){
                $fieldName = $arOption[0];
                $name = $arOption[0];
                $defaultValue = $arOption[2];
                $type = $arOption[3][0];
                $value = $defaultValue;

                if(isset($_REQUEST[$fieldName])){
                    if($type == 'file'){
                        $oldFileID = Option::get($module_id, $name, $defaultValue);
                        $arFile = array();

                        if($_REQUEST[$fieldName.'_del'] == 'Y'){
                            CFile::Delete($oldFileID);
                            $value = '';
                        }else{
                            if(!empty($_FILES[$fieldName]['name'])){
                                $arFile = $_FILES[$fieldName];
                            }elseif(!empty($_REQUEST[$fieldName])){
                                $filePath = $_REQUEST[$fieldName];
                                $arFile = CFile::MakeFileArray($filePath);
                            }

                            $fileDescription = $_REQUEST[$fieldName.'_descr'];

                            $arAddFile = array(
                                'old_file'		=> $oldFileID,
                                'del'			=> ($arFile['name'] != '' ? 'Y' : ''),
                                'MODULE_ID'		=> $module_id,
                                'description'	=> $fileDescription,
                            );

                            $newFileID = CFile::SaveFile(array_merge($arFile, $arAddFile), '/');
                            $value = ($newFileID > 0 ? $newFileID : $oldFileID);
                        }
                    }else{
                        $value = $_REQUEST[$fieldName];
                        if($type == 'combobox'){
                            $value = serialize($value);
                        }
                    }
                    Option::set($module_id, $name, $value);
                }
            }
        }

    }

}
?>

    <form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($module_id)?>&amp;lang=<?=LANGUAGE_ID?>" name="ycaweb_lottery_settings" enctype="multipart/form-data">
        <?=bitrix_sessid_post()?>
        <?
        $tabControl = new CAdminTabControl('tabControl', $aTabs);
        $tabControl->Begin();
        \Ycaweb\Cars\Cars::getChosenOptions();
        ?>

        <?foreach($arAllOptions as $arTab):?>

            <?$tabControl->BeginNextTab();?>

            <?foreach($arTab['GROUPS'] as $arGroup):?>
                <?if($arGroup['TITLE']):?>
                    <tr class="heading"><td colspan="2"><b><?=$arGroup['TITLE']?></b></td></tr>
                <?endif;?>
                <?foreach($arGroup['OPTIONS'] as $arOption):
                    $fieldName = $arOption[0];
                    $type = $arOption[3];
                    $bReadonly = ($arOption[4] === 'readonly');
                    $val = Option::get($module_id, $arOption[0], $arOption[2]);
                    ?>
                    <?if($type[0] == 'hidden'):?>
                    <input id="<?echo htmlspecialcharsbx($fieldName)?>" type="hidden" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($fieldName)?>">
                    <?else:?>
                    <tr>
                        <td width="40%" nowrap <?if($type[0]=='textarea') echo 'class="adm-detail-valign-top"'?>>
                            <label for="<?echo htmlspecialcharsbx($fieldName)?>"><?echo $arOption[1]?>:</label>
                        <td width="60%">
                            <?if($bReadonly):?>
                                <?=$val?>
                            <?elseif($type[0] == 'checkbox'):?>
                                <input type="checkbox" id="<?echo htmlspecialcharsbx($fieldName)?>" name="<?echo htmlspecialcharsbx($fieldName)?>" value="Y"<?if($val=='Y')echo' checked';?>>
                            <?elseif($type[0] == 'text'):?>
                                <input id="<?echo htmlspecialcharsbx($fieldName)?>" type="text" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($fieldName)?>">
                            <?elseif($type[0] == 'password'):?>
                                <input id="<?echo htmlspecialcharsbx($fieldName)?>" type="password" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($fieldName)?>">
                            <?elseif($type[0] == 'textarea'):?>
                                <textarea id="<?echo htmlspecialcharsbx($fieldName)?>" rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialcharsbx($fieldName)?>"><?echo htmlspecialcharsbx($val)?></textarea>
                            <?elseif($type[0] == 'select'):?>
                                <select name="<?echo htmlspecialcharsbx($fieldName)?>">
                                    <?if(!$val || $val == '0'):?>
                                        <option value="" selected></option>
                                        <?foreach($type[1] as $value=>$name):?>

                                            <option value="<?=$value?>"><?=$name?></option>

                                        <?endforeach;?>
                                    <?else:?>
                                        <option value=""></option>
                                    <?foreach($type[1] as $value=>$name):?>

                                        <option value="<?=$value?>"<?if($value == $val):?> selected<?endif;?>><?=$name?></option>

                                    <?endforeach;?>
                                    <?endif;?>
                                </select>
                            <?elseif($type[0] == 'combobox'):
                                $val = unserialize($val);
                                ?>
                                <select name="<?echo htmlspecialcharsbx($fieldName)?>[]" multiple>
                                    <?foreach($type[1] as $value=>$name):?>
                                        <option value="<?=$value?>"<?if(in_array($value, $val)):?> selected<?endif;?>><?=$name?></option>
                                    <?endforeach;?>
                                </select>
                            <?elseif($type[0] == 'file'):?>
                            <?endif?>
                        </td>
                    </tr>
                <?endif?>
                <?endforeach;?>
            <?endforeach;?>

            <?$tabControl->EndTab();?>

        <?endforeach;?>

        <?$tabControl->Buttons();?>
        <input type="submit" name="Save" value="<?=GetMessage('MAIN_SAVE')?>" title="<?=GetMessage('MAIN_OPT_SAVE_TITLE')?>" class="adm-btn-save">
        <?$tabControl->End();?>
    </form>

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');?>