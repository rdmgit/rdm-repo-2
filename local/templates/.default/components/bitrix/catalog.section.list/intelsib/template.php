<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
global $APPLICATION;
$this->setFrameMode(true);

?>
<div class="category_list">
    <!--h1><?//= $arResult["SECTION"]["IPROPERTY_VALUES"]['SECTION_PAGE_TITLE'] ?></h1-->
    <ul class="column">
        <?
        foreach ($arResult['SECTIONS'] as $key => $item) {
            if ($item["RELATIVE_DEPTH_LEVEL"] == 1) {
                ?>
                <li>
                    <a href="<?= str_replace('auto1/', 'auto/', $item['SECTION_PAGE_URL']) ?>"><?= $item['NAME'] ?></a>
                </li> <? }
        } ?>
    </ul>
</div>
