<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetPageProperty("wrapper_class", "about");
?>
<style>
.page__main-wrapper._search._js-search{display:none;}
.page__header._fix .header{display:none !important}
</style>

<div class="bg_moz" style="background: url(/local/assets/img/banner_company1.jpg);margin-top: -40px; background-size:cover; color:white; min-height:585px;">
	<div class="offers-list__item">
		<div class="top-banner__info-title">
			<div class="b-title">
				<h1>Мы – компания с богатой историей</h1>
			</div>
		</div>
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text t-rgh">
					<p>
						 РДМ-Импорт старейшая компания в Новосибирске по продаже автомобилей. Мы начинали с привоза аукционных автомобилей из Японии, Кореи, Сингапура, Америки, Канады, прошли долгий путь становления и получения опыта. <br>
					</p>
					<p>
						 Сегодня РДМ-Импорт это крупнейший в Сибири автосалон по продаже авто с пробегом, уникальная особенность которого - владение технологией отбора только самых качественных автомобилей. <br>
					</p>
					<p>
						 При неизменной рыночной стоимости машины и без навязывания станции технического обслуживания, мы подтверждаем качество своей гарантией на важнейшие узлы: двигатель, коробку передач, электрическую проводку, и лакокрасочное покрытие.
					</p>
					<span class="nav-link">			
					<a class="link-icon text__color-white more_about" href="/kompany/history/">Узнать больше о нас</a>
					</span>
				</div>
			</div>
		</div>
		 <!-- asdasdasd -->
		<div class="offers-list__item-info">
			<div class="offers-list__item-text">
				<p style="font-size:1.5em;">
 <b>Наши документы </b>
				</p>
 <b> </b>
				<p class="doc">
 <a class="gallery fancy-image" rel="group3" href="../local/assets/img/doc/svidetelstvo_o_reg.jpg">свидетельство о государственной регистрации юридического лица</a>
				</p>
				<p class="doc">
 <a class="gallery fancy-image" rel="group3" href="../local/assets/img/doc/svidetelstvo_post.jpg"> свидетельство о постановке на налоговый учет</a>
				</p>
				<p class="doc">
					 <a href="#recvz" class="reckv_block  fancy-inline" >реквизиты компании</a><a class="gallery" target="_blank" rel="group3" href="../local/files/details.docx"> <!--реквизиты компании--></a>
				</p>
			</div>
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
				</div>
			</div>
		</div>
	</div>
	<div style="max-width:940px; margin:0 auto;margin-top: -50px;">
		<img src="../local/assets/img/line.png" style="width:100%">
	</div>
</div>


<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>У нас работают профессионалы и те, кто стремится к этому</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		<p>
			 Наше предназначение - обеспечивать жителей Новосибирска и регионов Сибири ТОЛЬКО качественными и проверенными автомобилями, чтобы выбор и покупка были удобными, безопасными, и приятными, а с машинами не было сюрпризов и проблем в будущем. <br>
 <br>
			 Для нас это не просто слова - это закон. Мы создаем цивилизованный рынок продажи автотранспорта: без обмана, без лжи, без унижения и хамства. Где продавец не боится смотреть в глаза, и отношения построены на взаимном уважении и выгоде. Мы приглашаем каждого, кому близки эти цели, в нашу команду.
		</p>
	</div>
	<div class="offers-list__item-info" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<p class="car_lg">
 <b>Мы помогли выбрать 285&nbsp;машин </b>
				</p>
 <b> </b>
				<p class="car_lg">
 <b>
					Мы помогли продать 331&nbsp;машину</b>
				</p>
			</div>
		</div>
	</div>
	<div class="offers-list__item-info" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<div class="_read-more">
					<div id="video_smotr" class="offers-list-links__item-link eventVideo" data-src="ut01hN2xfak">
						 Посмотрите видео
					</div>
				</div>
			</div>
		</div>
	</div>
	 <!-- asdasdasd -->
	<div>
		<div class="offers-list__item-text" style="text-align: center;">
			<p>
			</p>
		</div>
		<!--div style="clear:both">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
				</div>
			</div>
		</div-->
	</div>
</div>

<div class="wrap-full banner">
	<img class="banner__img" src="/local/assets/img/banner_company.png">
	<div class="banner__nav">
		<div class="comand eventVideo" data-src="ZdsOB105t_k">
			 Приглашаем в команду
		</div>
	</div>
</div>
<!--div class="offers-list-links__item-link comand eventVideo" data-src="ZdsOB105t_k">Приглашаем в команду</div-->


 <!-- Nagradi -->
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Наши награды</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		<p>
			 Больше всего мы ценим надежность и стабильность в отношениях с партнерами, клиентами и поставщиками. За 14 лет работы РДМ-Импорт получил десятки подтверждений. Нам доверяют серьезные компании, такие как Альфа-Банк, ВТБ24, Сбербанк, Сургутнефтегаз и многие многие другие. С нами вы можете быть уверены в том, что работа будет сделана до конца и своевременно, а результат соответствует ожиданиям.
		</p>
	</div>
	 <!-- asdasdasd -->
	<div style="max-width:1240px; margin:40px auto 15px;">
		 <?$APPLICATION->IncludeComponent(
	"hmweb:medialibrary.slider",
	"template1",
	Array(
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CAROUSEL_WRAP" => "circular",
		"COLLECTIONS" => array(0=>"1",),
		"COMPONENT_TEMPLATE" => ".default",
		"COUNT_ELEMENT_DEFAULT" => "4",
		"RESIZE_TYPE" => "BX_RESIZE_IMAGE_EXACT",
		"START_ELEMENT_SLIDER" => "1",
		"TEMPLATE_THEME" => "blue",
		"THUMB_IMG_HEIGHT" => "300",
		"THUMB_IMG_WIDTH" => "150",
		"USE_AUTOSCROLL" => "N",
		"USE_JQUERY" => "N",
		"USE_PAGINATION" => "N",
		"USE_THUMB_IMG" => "Y"
	)
);?>
	</div>
</div>
 <!--Выданные авто-->
	 <?$APPLICATION->IncludeComponent("bitrix:news.list", "lost-auto1", Array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"COMPONENT_TEMPLATE" => "lost-auto",
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "N",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "NAME",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "15",	// Код информационного блока
		"IBLOCK_TYPE" => "-",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "2",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "Марка",
			2 => "Модель",
			3 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
	),
	false
);?>



<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Отзывы о нашей работе</h2>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"video_otziv_in",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "video_otziv_in",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "16",
		"IBLOCK_TYPE" => "videos_youtube",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"VIDEO_LINK_OTZIV",1=>"Ссылка",2=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
			<div class="all_otzivi">
				<a href="https://www.youtube.com/playlist?list=PLRo6ODoWfWaRWlRyn_7Ds6Qjt8e7jnMNO" target="_blank">Смотреть все отзывы</a>
			</div>
		</div>
	</div>
	<div class="rdm-flamp">
 <a class="flamp-widget" href="https://novosibirsk.flamp.ru/firm/rdm_import_ooo_avtosalon-141265769513599" data-flamp-widget-type="responsive-new" data-flamp-widget-count="1" data-flamp-widget-id="141265769513599" data-flamp-widget-width="100%">Отзывы о нас на Флампе</a><script>!function(d,s){var js,fjs=d.getElementsByTagName(s)[0];js=d.createElement(s);js.async=1;js.src="//widget.flamp.ru/loader.js";fjs.parentNode.insertBefore(js,fjs);}(document,"script");</script>
	</div>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Гарантии</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		 <!--p>
			 Каждый автомобиль проходит тщательную проверку технического состояния, по результатам которой в электронном виде выдается диагностический лист и сертификат качества, гарантирующий исправность. Каждое транспортное средство мы проверяем на юридическую чистоту, ограничения и залог. При покупки проводим должное оформление полного комплекта документов на фирменном бланке в строгом соответствии с законодательством. Мы гарантируем отсутствие проблем при постановке автомобиля на учет и сдаче налоговой отчетности.
		</p>
		<p>
 <br>
			 Для нас гарантии являются незыблемыми при любых обстоятельствах: это больше, чем обязательства перед покупателями, это лицо компании и основа для движения вперед!
		</p-->
	</div>
	<div class="garantii_list">
		<div class="garant_block">
			 Гарантия компетентного обслуживания
		</div>
		<div class="garant_block">
			 Гарантия юридической защищенности
		</div>
		<div class="garant_block">
			 Гарантия технической безопасности
		</div>
 <br>
		<div style="text-align:center;">
			 <iframe style="width:100%; margin:15px auto; height:40vh;" src="https://www.youtube.com/embed/iNL4UQY9Ocs" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
</div>
<div class="black_list" id="bl_list">
	<div class="recveziti" id="recvz">
		<div id="pr">
			<p>
 <b>Управляющий Облецов Роман Алексеевич, на основании Устава</b>
			</p>
			<p>
 <br>
				 ООО "РДМ-Импорт" <br>
				 ИНН 5406352193 / КПП 540101001 <br>
				 ОКПО 94909538 <br>
				 Адрес: 630112 Новосибирск Фрунзе 61/2 А/я 160 <br>
				 р/с 40702810700330001207Новосибирский филиал ПАО АКБ «Связь-Банк» <br>
				 к/с 30101810100000000740 БИК 045004740 <br>
				 тел: (383) 32-812-32
			</p>
		</div>
		<p>
 <a class="gallery" rel="group3" href="../local/files/details.docx">Скачать</a> <a href="#" class="print">Печать</a>
		</p>
	</div>
	<div class="recveziti video_sm">
	</div>
	<div class="recveziti video2_sm">
		 <!--<iframe id="Youtube" style="width:100%; height:240px;" src="https://www.youtube.com/embed/ZdsOB105t_k" frameborder="0" allowfullscreen></iframe>-->
	</div>
</div>

<?if(true){?>
 <script src="//www.youtube.com/player_api"></script> <script>
	
$(document).ready(function(){
	var html_to_print=$('#pr').html();
	var iframe=$('<iframe id="print_frame">');
	$('body').append(iframe); //добавляем эту переменную с iframe в наш body (в самый конец)
	var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
	var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
	$('#print_frame').hide();
	doc.getElementsByTagName('body')[0].innerHTML=html_to_print;
	$('a.print').click(function(){
		win.print();
	})
	
	function onYouTubePlayerAPIReady() {
	  player = new YT.Player('Youtube', {
		events: {'onReady': onPlayerReady}
	  });
	}

	/*$('.reckv_block').click(function(){
		$('.black_list').show();
		$('.recveziti').show();
		$('.video2_sm').hide();
		$('.video_sm').hide();
	})*/

	//document.getElementById('bl_list').onclick = function() {play.pauseVideo();};
})
<!--  AUTO -->

</script> <br>
  <style>
#map{height:340px;overflow:hidden; margin-bottom:30px;}
ymaps:nth-child(3) {
    display: none;
}
</style>
<div id="map"></div>
<?}?>
<?$APPLICATION->IncludeComponent("vegas:winVideo",".default",array(),false);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
