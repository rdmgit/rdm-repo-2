<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<ul class="header__menu">
	<?
	foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
			continue;
	?>
		<?if($arItem["SELECTED"]):?>
			<li class="header__menu-item">
				<a href="<?=$arItem["LINK"]?>" class="header__menu-item-link" <?if(isset($arItem["PARAMS"]["target"])){?>target="<?=$arItem["PARAMS"]["target"]?>"<?}?> <?if(isset($arItem["PARAMS"]["mode"])){?>mode="<?=$arItem["PARAMS"]["mode"]?>"<?}?>>
					<?=$arItem["TEXT"]?>
				</a>
			</li>
		<?else:?>
			<li class="header__menu-item">
				<a href="<?=$arItem["LINK"]?>" class="header__menu-item-link" <?if(isset($arItem["PARAMS"]["target"])){?>target="<?=$arItem["PARAMS"]["target"]?>"<?}?> <?if(isset($arItem["PARAMS"]["mode"])){?>mode="<?=$arItem["PARAMS"]["mode"]?>"<?}?>>
					<?=$arItem["TEXT"]?>
				</a>
			</li>
		<?endif?>

	<?endforeach?>
		
		<li class="header__menu-item _signin">
			<a href="" class="header__menu-item-link eventOpenAuth auth-hide">
				<?=GetMessage('HEADER_MENU_AUTH');?>
			</a>
		</li>
	</ul>
<?endif?>
