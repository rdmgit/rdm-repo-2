<?php
/* Smarty version 3.1.30, created on 2017-06-09 14:59:36
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/user_edit.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a8da80cdc76_27191212',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7bfa6c703b599b4e83f3347097595e6265a61c20' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/user_edit.html',
      1 => 1496812354,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a8da80cdc76_27191212 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Your awesome content goes here -->
				<div class="row">
					
					
				
				
				<div class="box-info animated fadeInDown">
				<h2><strong><?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['USER']['ELEMENT'])) {
$_smarty_tpl->_assignInScope('VALUE', $_smarty_tpl->tpl_vars['DATA']->value['USER']['ELEMENT'][0]);
?>Редактирование<?php } else { ?>Добавление<?php }?> <?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account') {?>аккаунта Яндекс.Директ<?php } else { ?>пользователя<?php }?></strong> </h2>
					<form class="form-horizontal save" role="form" action="/user/user/save/">
					  <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
" />
					  <input type="hidden" name="status" value="1" />
					  <?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><input type="hidden" name="group" value="<?php if (!empty($_smarty_tpl->tpl_vars['VALUE']->value['group'])) {
echo $_smarty_tpl->tpl_vars['VALUE']->value['group'];
} else {
if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account') {?>4<?php } else { ?>1<?php }
}?>" /><?php }?>
					  <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label">Логин<?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account') {?> от аккаунта Яндекс.Директ<?php }?></label>
						<div class="col-sm-10"<?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account') {?> data-toggle="tooltip" title="Логин от аккаунта Яндекс.Директ указывается без @yandex.ru. Обязательное поле"<?php }?>>
						  <input type="text" class="form-control" id="input-text" name="login" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['login'];?>
" placeholder="Логин<?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account') {?> от аккаунта Яндекс.Директ<?php }?>"<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru' && $_smarty_tpl->tpl_vars['VALUE']->value['login'] == 'admin') {?> readonly<?php }?>>
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label">Пароль</label>
						<div class="col-sm-10" data-toggle="tooltip" title="Пароль для входа в программу Яндекс.Директ автомат (может отличаться от пароля яндекс, шифруется MD5)">
						  <input type="password" class="form-control" id="input-text" name="password" value="" placeholder="Пароль для входа в программу Яндекс.Директ автомат"<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru' && $_smarty_tpl->tpl_vars['VALUE']->value['login'] == 'admin') {?> readonly<?php }?>>
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label">Фамилия</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="input-text" name="lastname" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['lastname'];?>
" placeholder=""<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru' && $_smarty_tpl->tpl_vars['VALUE']->value['login'] == 'admin') {?> readonly<?php }?>>
						</div>
					  </div>
					  
					 <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label">Имя</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="input-text" name="name" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];?>
" placeholder=""<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru' && $_smarty_tpl->tpl_vars['VALUE']->value['login'] == 'admin') {?> readonly<?php }?>>
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label">Отчество</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="input-text" name="middlename" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['middlename'];?>
" placeholder=""<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru' && $_smarty_tpl->tpl_vars['VALUE']->value['login'] == 'admin') {?> readonly<?php }?>>
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="input-text" name="email" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['email'];?>
" placeholder=""<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru' && $_smarty_tpl->tpl_vars['VALUE']->value['login'] == 'admin') {?> readonly<?php }?>>
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="input-text" class="col-sm-2 control-label">Телефон</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="input-text" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['phone'];?>
" placeholder=""<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru' && $_smarty_tpl->tpl_vars['VALUE']->value['login'] == 'admin') {?> readonly<?php }?>>
						</div>
					  </div>
					  
					  <div class="form-group">
						<label class="col-sm-2 control-label">Права</label>
						<div class="col-sm-10" data-toggle="tooltip" title="">
							<div class="radio">
							  <label>
								<input type="radio" name="right" id="optionsRadios1" value="1"<?php if (!isset($_smarty_tpl->tpl_vars['VALUE']->value['right']) || !empty($_smarty_tpl->tpl_vars['VALUE']->value['right'])) {?> checked<?php }?>>
								Редактирование
							  </label>
							</div>
							<div class="radio">
							  <label>
								<input type="radio" name="right" id="optionsRadios2" value="0"<?php if (isset($_smarty_tpl->tpl_vars['VALUE']->value['right']) && empty($_smarty_tpl->tpl_vars['VALUE']->value['right'])) {?> checked<?php }?>>
								Просмотр
							  </label>
							</div>
						</div>
					  </div>
					  
					    <?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru') {
} else { ?><center><button type="submit" class="btn btn-default"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Save<?php } else { ?>Сохранить<?php }?></button> <?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru') {
} else { ?><a class="btn btn-default masked"<?php if (empty($_smarty_tpl->tpl_vars['VALUE']->value['id']) || empty($_smarty_tpl->tpl_vars['VALUE']->value['group']) || $_smarty_tpl->tpl_vars['VALUE']->value['group'] != 4) {?> style="display: none;"<?php }?> data-toggle="tooltip" title="Для получения «Токена» вы должны, сначала авторизоваться в соответствующем аккаунте Яндекс.Директ<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['login']) {?> «<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['login'];?>
»<?php }?> в этом же браузере" href="https://oauth.yandex.ru/authorize?response_type=code&client_id=<?php echo $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['id'];?>
&state=<?php if (!empty($_smarty_tpl->tpl_vars['VALUE']->value['id'])) {
echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];
} else { ?>%id%<?php }?>|<?php echo $_SERVER['HTTP_HOST'];
if (!empty($_smarty_tpl->tpl_vars['VALUE']->value['id'])) {
echo $_SERVER['REQUEST_URI'];
} else { ?>/<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['url'];
echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'];?>
/<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['argument'];?>
/id/%id%/<?php }?>"><?php if (!empty($_smarty_tpl->tpl_vars['VALUE']->value['token'])) {?>Обновить токен<?php } else { ?>Получить токен<?php }?></a><?php }
if (!empty($_smarty_tpl->tpl_vars['VALUE']->value['id'])) {?> <button type="button" class="btn btn-default" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/user/delete/param/id/<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
/'"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Delete<?php } else { ?>Удалить<?php }?></button><?php }?></center><?php }?>
					</form>					
				</div>
				<!-- End of your awesome content --><?php }
}
