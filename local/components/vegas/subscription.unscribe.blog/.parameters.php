<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

CModule::IncludeModule("subscribe");
CModule::IncludeModule("form"); 

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));
//################################

$rsForms = CForm::GetList($by="s_id", $order="desc", array(), $is_filtered);
$arListForm = array();
while ($arForm = $rsForms->Fetch())
{
	$arListForm[$arForm["ID"]] = $arForm["NAME"];
}
//################################
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"FORM_ID" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => "Веб форма",
			"TYPE" => "LIST",
			"VALUES" => $arListForm,
			"DEFAULT" => 'Не выбрано',
			"REFRESH" => "N",
			"MULTIPLE" => "N",
		),

		"CACHE_TIME"  =>  array("DEFAULT"=>36000000),
		"CACHE_FILTER" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("IBLOCK_CACHE_FILTER"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("CP_BNL_CACHE_GROUPS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);


CIBlockParameters::Add404Settings($arComponentParameters, $arCurrentValues);
