<?php

$DIVIDER = 10;

$EXT_ID = $INT_ID = 0;
$LIMIT = 1;
$PATH = $FOLDER = $IMAGE_NAME = $IMAGE_URL = '';
$aDrive = $aGearbox = $aBody = $aFuel = $aColor = $aVolume = [];

function initGlobals()
{
    global $aDrive, $aGearbox, $aBody, $aFuel, $aColor, $aVolume;

    $aDrive = [
        "������" => 104,
        "��������" => 105,
        "������" => 106
    ];
    $aGearbox = [
        "��������" => 48,
        "����" => 48,
        "�������" => 49,
        "����" => 49,
        "��������" => 254,
        "�����" => 255
    ];
    $aBody = [
        "�����" => 36,
        "����" => 37,
        "�������" => 38,
        "���������" => 39,
        "����" => 40,
        "��������" => 41,
        "�����" => 42,
        "�������" => 43,
        "������������" => 43,
    ];
    $aFuel = [
        "������" => 258,
        "������" => 107,
        "������" => 108
    ];
    $aColor = [
        "�������" => 257,
        "�����" => 256,
        "������" => 113,
        "�����" => 46,
        "�����������" => 45,
        "����������" => 166,
        "����������" => 165,
        "������" => 164,
        "�������" => 163,
        "�������" => 162,
        "���������" => 161,
        "�������" => 160,
        "�������" => 159,
        "�����" => 157,
        "�������" => 114
    ];
    $aVolume = [
        "0.6" => 109, "0.7" => 21, "0.8" => 22, "0.9" => 29,
        "1.0" => 30, "1.1" => 31, "1.2" => 32, "1.3" => 33, "1.4" => 34, "1.5" => 35, "1.6" => 36, "1.7" => 37, "1.8" => 38, "1.9" => 39,
        "2.0" => 40, "2.1" => 41, "2.2" => 42, "2.3" => 43, "2.4" => 44, "2.5" => 72, "2.6" => 73, "2.7" => 74, "2.8" => 75, "2.9" => 76,
        "3.0" => 77, "3.1" => 78, "3.2" => 79, "3.3" => 80, "3.4" => 81, "3.5" => 82, "3.6" => 83, "3.7" => 84, "3.8" => 85, "3.9" => 86,
        "4.0" => 87, "4.1" => 88, "4.2" => 89, "4.3" => 90, "4.4" => 91, "4.5" => 92, "4.6" => 93, "4.7" => 94, "4.8" => 95, "4.9" => 96,
        "5.0" => 97, "5.1" => 98, "5.2" => 99, "5.3" => 100, "5.4" => 101, "5.5" => 102, "5.6" => 103, "5.7" => 104, "5.8" => 105, "5.9" => 106,
        "6.0" => 107
    ];

    global $EXT_ID, $INT_ID, $LIMIT;
    $EXT_ID = isset($_REQUEST['ext']) ? $_REQUEST['ext'] : $EXT_ID;
    $INT_ID = isset($_REQUEST['int']) ? $_REQUEST['int'] : $INT_ID;
    $LIMIT = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : $LIMIT;
}

function initDir()
{
    global $INT_ID, $PATH, $FOLDER, $DIVIDER;

    $PATH = 'upload/' . (string)(floor($INT_ID / $DIVIDER) * $DIVIDER + $DIVIDER);

    if (!is_dir('upload')) {
        mkdir('upload', 0777, true);
    }
    if (!is_dir($PATH)) {
        mkdir($PATH, 0777, true);
    }
    if ($FOLDER !== '' && !is_dir($PATH . '/' . $FOLDER)) {
        mkdir($PATH . '/' . $FOLDER, 0777, true);
    }
}

function saveImage($imageUrl)
{
    global $PATH, $FOLDER, $IMAGE_NAME, $IMAGE_URL;

    $imageUrl = 'http://rdm-import.ru/assets/' . str_replace('uri://', '', $imageUrl);
	
	$curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 0);
    curl_setopt($curl,CURLOPT_URL,$imageUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $content=curl_exec($curl);
	curl_close($curl);
	
    if ($content === false) {
        return false;
    }

    $ext = stristr($imageUrl, '.png') === TRUE ? '.png' : '.jpg';

    do {
        $newImageName = md5(rand(pow(10, 6), pow(10, 9)));
        $newImageUrl = $PATH . '/' . $FOLDER . '/' . $newImageName . $ext;
    } while (file_exists($newImageName));

    if (file_put_contents($newImageUrl, $content) === false) {
        return false;
    }

    $IMAGE_NAME = $newImageName . $ext;
    $IMAGE_URL = 'parser/' . $newImageUrl;

    return true;
}

/** settings & init */
initGlobals();

$host = 'localhost';
$database = 'amazuref_old';
$user = 'amazuref_old';
$password = 'amazuref_old';

/** coding */
$intID = $INT_ID + 1;
$extID = null;

initDir();

/** open csv */

if (!is_file("$PATH/old.csv")) {
	$file = fopen("$PATH/old.csv", 'a+') or die("Unable to open csv file!");
    fputcsv($file, ['IE_XML_ID', 'IE_NAME', 'IE_ID', 'IE_ACTIVE', 'IE_ACTIVE_FROM', 'IE_ACTIVE_TO', 'IE_PREVIEW_PICTURE', 'IE_PREVIEW_TEXT', 'IE_PREVIEW_TEXT_TYPE', 'IE_DETAIL_PICTURE', 'IE_DETAIL_TEXT', 'IE_DETAIL_TEXT_TYPE', 'IE_CODE', 'IE_SORT', 'IE_TAGS', 'IP_PROP44', 'IP_PROP17', 'IP_PROP15', 'IP_PROP31', 'IP_PROP11', 'IP_PROP16', 'IP_PROP43', 'IP_PROP19', 'IP_PROP20', 'IP_PROP42', 'IP_PROP26', 'IP_PROP12', 'IP_PROP53', 'IP_PROP96', 'IP_PROP24', 'IP_PROP40', 'IP_PROP18', 'IP_PROP13', 'IP_PROP22', 'IP_PROP41', 'IP_PROP97', 'IP_PROP27', 'IP_PROP30', 'IP_PROP25', 'IP_PROP14', 'IP_PROP21', 'IP_PROP10', 'IP_PROP29', 'IC_GROUP0', 'IC_GROUP1', 'IC_GROUP2',], ';');
} else {
	$file = fopen("$PATH/old.csv", 'a+') or die("Unable to open csv file!");
}
/** put csv header */
/** open logs */
$log = fopen("$PATH/log.txt", 'a+') or die("Unable to open log file!");

$link = mysqli_connect($host, $user, $password, $database) or die("Error " . mysqli_error($link));
mysqli_query($link, "SET NAMES 'cp1251'");
mysqli_query($link, "SET CHARACTER SET 'cp1251'");
$query = <<<END
select 
	tcar.id as id,
	tcar.created as created,
    tmark.title as mark, 
    tmodel.title as model,
    ttext.text as text,
    CONCAT(tuser.last_name," ",tuser.first_name) as manager,
    tuser.phone_1 as phone1,
    tuser.phone_2 as phone2,
    tcolorb.title as color_body,
    tcolori.title as color_in,
    tgearbox.title as gearbox,
    tfuel.title as fuel,
    tdrive.title as drive,
    tbody.title as body,
    tcar.right_drive as rigth,
    tcar.year as year,
    tcar.price as price,
    tcar.volume as volume,
    tcar.mileage as mileage,
    tcar.mileage_rf as mileage_rf,
    tcar.power as power,
    tcar.door as door,
	(SELECT GROUP_CONCAT(uri SEPARATOR ";") from file_manager where file_manager.model_id = tcar.id order by file_manager.delta asc) as images
from car as tcar 
	join car_primary_data as tmark on tcar.mark = tmark.id 
	join car_primary_data as tmodel on tcar.model = tmodel.id 
    join car_primary_data as tcolorb on tcar.color_body = tcolorb.id 
    join car_primary_data as tcolori on tcar.color_in = tcolori.id 
    join car_primary_data as tgearbox on tcar.gearbox = tgearbox.id 
    join car_primary_data as tfuel on tcar.fuel = tfuel.id 
    join car_primary_data as tdrive on tcar.drive = tdrive.id 
    join car_primary_data as tbody on tcar.body_type = tbody.id 
	join field_long_text as ttext on tcar.id = ttext.model_id and ttext.field = "car_text"
    join profile as tuser on tcar.manager_id = tuser.user_id
where 
	tcar.visible=1 and tcar.state=5 and tcar.id > $EXT_ID 
limit $LIMIT;
END;
$result = mysqli_query($link, $query) or die('Request error: ' . mysqli_error($link));
if ($result->num_rows == 0) {
    http_response_code(400);
    die();
}
while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $isPreviewImage = true;
    $previewImageUrl = null;

    $images = array_slice(explode(";", $line['images']), 0, 5);

    foreach ($images as $imageUrl) {
        $FOLDER = (string)$intID;
        initDir();

        /** logs */
        fwrite($log, date('M/d H:m:s') . ' ext=' . $line['id'] . ' int=' . $intID . '; ');

        /** save image */
        if (saveImage($imageUrl) === false) {
            if ($isPreviewImage == false) {
                fwrite($log, 'image[preview] - ; ' . "\r\n");
                continue;
            } else {
                fwrite($log, 'image - ; ');
            }
        } else {
            fwrite($log, 'image + ; ');
        }

        /**  */
        if ($isPreviewImage === true) {
            $previewImageUrl = $IMAGE_URL;
            $isPreviewImage = false;
        }
		
		$volume = substr("" . $line['volume'] / 1000, 0, 3);
		if (strlen($volume) == 1) {
			$volume .= ".0";
		}
		
        $row = [
            $line['id'], // IE_XML_ID
            $line['model'] . ' ' . $line['mark'], // IE_NAME
            $intID, // IE_ID
            'Y',
            date('d.m.Y H:i:s', $line['created']), // IE_ACTIVE_FROM
            '', // IE_ACTIVE_TO
            $previewImageUrl, // IE_PREVIEW_PICTURE
            '', // IE_PREVIEW_TEXT
            'text', // IE_PREVIEW_TEXT_TYPE
            '', // IE_DETAIL_PICTURE
            $line['text'], // IE_DETAIL_TEXT
            'html', // IE_DETAIL_TEXT_TYPE
            mb_strtolower(str_replace(' ', '_', $line['model'] . '_' . $line['mark'])), // IE_CODE
            500, // IE_SORT
            '', // IE_TAGS,
            '',// IP_PROP44,
            '', // IP_PROP17,
            $line['year'], // IP_PROP15
            '�.�����������, ��.������ 61/2', // IP_PROP31
            'Y', // IP_PROP11
            'Y', // IP_PROP16
            $line['manager'], // IP_PROP43
            $line['mark'], // IP_PROP19
            $line['model'], // IP_PROP20
            '', // IP_PROP42
            $line['power'], // IP_PROP26
            '', // IP_PROP12
            '���', // IP_PROP53
            '���', // IP_PROP96
            $volume, // ����� ��������
            '',
            isset($aDrive[mb_strtolower($line['drive'], 'windows-1251')])
                ? $aDrive[mb_strtolower($line['drive'], 'windows-1251')]
                : '',
            $line['mileage'],
            ($line['rigth'] == 1) ? '�����' : '������',
            $line['body'],
            // 'В наличии', // ������ � ������� ����� ��������� ������������
            'Архив', // ������ �����
            $line['price'],
            $line['phone1'] !== '' ? $line['phone1'] : $line['phone2'],
            isset($aGearbox[mb_strtolower($line['gearbox'], 'windows-1251')])
                ? $aGearbox[mb_strtolower($line['gearbox'], 'windows-1251')] // IP_PROP25
                : '',
            isset($aBody[mb_strtolower($line['body'], 'windows-1251')])
                ? $aBody[mb_strtolower($line['body'], 'windows-1251')]
                : '',
            isset($aFuel[mb_strtolower($line['fuel'], 'windows-1251')])
                ? $aFuel[mb_strtolower($line['fuel'], 'windows-1251')]
                : '',
            $PATH . '/' . $IMAGE_NAME,
            isset($aColor[mb_strtolower($line['color_body'], 'windows-1251')])
                ? $aColor[mb_strtolower($line['color_body'], 'windows-1251')] // IP_PROP29
                : '',
            '',
            '',
            ''
        ];

        if (fputcsv($file, $row, ';') === false) {
            fwrite($log, 'csv - ; ' . "\r\n");
        } else {
            fwrite($log, 'csv + ; ' . "\r\n");
        }
    }

    $extID = $line['id'];
    $intID++;
}

mysqli_free_result($result);
mysqli_close($link);

fclose($file);
fclose($log);

/** clear output buffer before return response */
ob_clean();

/** return JSON */
$intID = $intID - 1;
$count = $intID - $INT_ID;
header('Content-Type: application/json');
echo json_encode([
    'ext' => (int)$extID,
    'int' => $intID,
    'rows_loaded' => $count,
]);