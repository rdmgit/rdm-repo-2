<?php
$EXT_ID = 0;
$LIMIT = 1;

$PATH = '';
$IMAGE_NAME = '';
$IMAGE_URL = '';

$aDrive = $aGearbox = $aBody = $aFuel = $aColor = $aVolume = [];

function initGlobals()
{
    global $aDrive, $aGearbox, $aBody, $aFuel, $aColor, $aVolume;

    $aDrive = [
        "полный" => 104,
        "передний" => 105,
        "задний" => 106
    ];
    $aGearbox = [
        "механика" => 48,
        "мкпп" => 48,
        "автомат" => 49,
        "акпп" => 49,
        "вариатор" => 254,
        "робот" => 255
    ];
    $aBody = [
        "седан" => 36,
        "джип" => 37,
        "хетчбэк" => 38,
        "универсал" => 39,
        "купе" => 40,
        "открытый" => 41,
        "пикап" => 42,
        "минивэн" => 43,
        "микроавтобус" => 43,
    ];
    $aFuel = [
        "гибрид" => 258,
        "бензин" => 107,
        "дизель" => 108
    ];
    $aColor = [
        "бежевый" => 257,
        "серый" => 256,
        "черный" => 113,
        "белый" => 46,
        "серебристый" => 45,
        "фиолетовый" => 166,
        "коричневый" => 165,
        "желтый" => 164,
        "золотой" => 163,
        "розовый" => 162,
        "оранжевый" => 161,
        "зеленый" => 160,
        "голубой" => 159,
        "синий" => 157,
        "красный" => 114
    ];
    $aVolume = [
        "0.6" => 109, "0.7" => 21, "0.8" => 22, "0.9" => 29,
        "1.0" => 30, "1.1" => 31, "1.2" => 32, "1.3" => 33, "1.4" => 34, "1.5" => 35, "1.6" => 36, "1.7" => 37, "1.8" => 38, "1.9" => 39,
        "2.0" => 40, "2.1" => 41, "2.2" => 42, "2.3" => 43, "2.4" => 44, "2.5" => 72, "2.6" => 73, "2.7" => 74, "2.8" => 75, "2.9" => 76,
        "3.0" => 77, "3.1" => 78, "3.2" => 79, "3.3" => 80, "3.4" => 81, "3.5" => 82, "3.6" => 83, "3.7" => 84, "3.8" => 85, "3.9" => 86,
        "4.0" => 87, "4.1" => 88, "4.2" => 89, "4.3" => 90, "4.4" => 91, "4.5" => 92, "4.6" => 93, "4.7" => 94, "4.8" => 95, "4.9" => 96,
        "5.0" => 97, "5.1" => 98, "5.2" => 99, "5.3" => 100, "5.4" => 101, "5.5" => 102, "5.6" => 103, "5.7" => 104, "5.8" => 105, "5.9" => 106,
        "6.0" => 107
    ];

    global $EXT_ID;
    $EXT_ID = isset($_REQUEST['ext']) ? $_REQUEST['ext'] : $EXT_ID;
}

/** settings & init */

initGlobals();

$host = 'localhost';
$database = 'amazuref_old';
$user = 'amazuref_old';
$password = 'amazuref_old';

//$_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__) . '/public';
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!CModule::IncludeModule("iblock")) {
    echo "Ошибка iblock\n";
    die;
}

/** coding */
$extID = null;

$link = mysqli_connect($host, $user, $password, $database) or die("Error " . mysqli_error($link));
mysqli_query($link, "SET NAMES 'cp1251'");
mysqli_query($link, "SET CHARACTER SET 'cp1251'");
$query = <<<END
select 
	tcar.id as id,
	tcar.created as created,
    tmark.title as mark, 
    tmodel.title as model,
    ttext.text as text,
    CONCAT(tuser.last_name," ",tuser.first_name) as manager,
    tuser.phone_1 as phone1,
    tuser.phone_2 as phone2,
    tcolorb.title as color_body,
    tcolori.title as color_in,
    tgearbox.title as gearbox,
    tfuel.title as fuel,
    tdrive.title as drive,
    tbody.title as body,
    tcar.right_drive as rigth,
    tcar.year as year,
    tcar.price as price,
    tcar.volume as volume,
    tcar.mileage as mileage,
    tcar.mileage_rf as mileage_rf,
    tcar.power as power,
    tcar.door as door,
	(SELECT GROUP_CONCAT(uri SEPARATOR ";") from file_manager where file_manager.model_id = tcar.id order by file_manager.delta asc) as images
from car as tcar 
	join car_primary_data as tmark on tcar.mark = tmark.id 
	join car_primary_data as tmodel on tcar.model = tmodel.id 
    join car_primary_data as tcolorb on tcar.color_body = tcolorb.id 
    join car_primary_data as tcolori on tcar.color_in = tcolori.id 
    join car_primary_data as tgearbox on tcar.gearbox = tgearbox.id 
    join car_primary_data as tfuel on tcar.fuel = tfuel.id 
    join car_primary_data as tdrive on tcar.drive = tdrive.id 
    join car_primary_data as tbody on tcar.body_type = tbody.id 
	join field_long_text as ttext on tcar.id = ttext.model_id and ttext.field = "car_text"
    join profile as tuser on tcar.manager_id = tuser.user_id
where 
	tcar.visible=1 and tcar.state=5 and tcar.id > $EXT_ID 
order by id
limit $LIMIT;
END;
$result = mysqli_query($link, $query) or die('Request error: ' . mysqli_error($link));
if ($result->num_rows == 0) {
    http_response_code(400);
    die();
}

while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $el = new CIBlockElement;
    $aProps = array(
        'HAS_RF_MILEAGE' => ($line['mileage_rf'] == 1) ? 25 : '',
        'MILEAGE' => $line['mileage'],
        'MODEL_YEAR' => ($line['year'] - 1946),
        'IS_HYBRID' => (mb_strtolower($line['fuel']) == 'гибрид') ? 28 : '',
        'MANUFACTURER' => $line['mark'],
        'MODEL' => $line['model'],
        'STEERING_WHEEL' => ($line['rigth'] == 1) ? 20 : 19,
        'POWER' => $line['power'],
        'PRICE' => $line['price'],
        'PHONE' => array($line['phone1'], $line['phone2']),
        'CITY' => 'г. Новосибирск, ул. Фрунзе 61/2',
        'CONTACT_NAME' => $line['manager'],
        'STATE' => 112
    );

    if (isset($aColor[mb_strtolower($line['color_body'])])) {
        $aProps['COLOR'] = $aColor[mb_strtolower($line['color_body'])];
    }
    if (isset($aGearbox[mb_strtolower($line['gearbox'])])) {
        $aProps['TRANSMISSION'] = $aGearbox[mb_strtolower($line['gearbox'])];
    }
    if (isset($aFuel[mb_strtolower($line['fuel'])])) {
        $aProps['FUEL'] = $aFuel[mb_strtolower($line['fuel'])];
    }
    if (isset($aDrive[mb_strtolower($line['drive'])])) {
        $aProps['DRIVE'] = $aDrive[mb_strtolower($line['drive'])];
    }
    if (isset($aBody[mb_strtolower($line['body'])])) {
        $aProps['CAR_BODY'] = $aBody[mb_strtolower($line['body'])];
    }
    $volume = substr("" . $line['volume'] / 1000, 0, 3);
    if (strlen($volume) == 1) {
        $volume .= ".0";
    }
    if (isset($aVolume[$volume])) {
        $aProps['ENGINE'] = $aVolume[$volume];
    }
	
	
    $aImages = explode(";", $line['images']);
    $aPrev = null; 
	$count = 0; $limit = 3;
	foreach ($aImages as $aImage) {
		if($count >= $limit){
			break;
		} else {
			$count = $count + 1;
		}
		
		$exUrl = "uri://";
		$url = "http://old.rdm-import.ru/assets/";
		
		if ($aPrev == null) {
            $aPrev = CFile::MakeFileArray(str_replace($exUrl, $url, $aImage));
            $aProps['MORE_PHOTO'][] = $aPrev;
        } else {
            $aProps['MORE_PHOTO'][] = CFile::MakeFileArray(str_replace($exUrl, $url, $aImage));
        }
    }

    $arFields = array(
        'IBLOCK_ID' => 5,
        'NAME' => $line['mark'] . ' ' . $line['model'],
        'ACTIVE_FROM' => date('d.m.Y H:i:s', $line['created']),
        'PROPERTY_VALUES' => $aProps,
        'PREVIEW_PICTURE' => $aPrev,
        'ACTIVE' => 'Y',
        'DETAIL_TEXT' => $line['text'],
        'XML_ID' => (int)$line['id'],
    );

    file_put_contents(dirname(__FILE__) . "/data.obj", print_r($arFields, true));
    if ($PRODUCT_ID = $el->Add($arFields)) {
        file_put_contents(dirname(__FILE__) . "/log.txt", $line['id'] . " " . $PRODUCT_ID . "\n", FILE_APPEND);
    } else {
        file_put_contents(dirname(__FILE__) . "/log.txt", $line['id'] . " " . $el->LAST_ERROR . "\n", FILE_APPEND);
    }
    flush();

    $extID = $line['id'];
}

mysqli_free_result($result);
mysqli_close($link);

/** clear output buffer before return response */
ob_clean();

header('Content-Type: application/json');
echo json_encode([
    'ext' => (int)$extID,
    'error' => $error
	]);