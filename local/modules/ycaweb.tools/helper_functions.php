<?php

/*
 * Helper functions
 */

if(!function_exists('_vardump')){
	/*
	 * Custom vardump
	 *
	 * @param mixed $var
	 * @param string $output ('f' or 'd')
	 * @param boolean $checkAccess
	 */
	function _vardump($var = '', $output = 'display', $checkAccess = true){
		global $USER;
		if($checkAccess && !$USER->IsAdmin()) return;

		$dump = print_r($var, true);

		$backtraceInfo = debug_backtrace(false);
		$source = 'File <b>'.$backtraceInfo[0]['file'].'</b> in line <b>'.$backtraceInfo[0]['line'].'</b>';

		switch($output){
			case 'f':
			case 'file':
				$logPath = $_SERVER['DOCUMENT_ROOT'].'/vardump_log.txt';

				$logFile = fopen($logPath, 'a');
				fwrite($logFile, "----- " . date('d.m.Y H:i:s') . " | " . strip_tags($source) . " ------------------------\n" . $dump . "\n\n");
				fclose($logFile);
				break;
			default:
				$dump = '<div style="margin: 20px; background: #fdf5db; border-radius: 4px; padding: 5px; border: 1px solid #ffe69d; box-shadow: 0 0 10px 0px #ccc; font-family: Arial;"><div style="margin-bottom: 5px; font-size: 11px; color: #848484; overflow:hidden;" title="'.strip_tags($source).'">'.$source.'</div><pre style="display: block; margin: 0; padding: 10px;  background: #fff; border: 1px solid #ccc; max-height: 400px; overflow: scroll; font-size: 13px; color: #000;">'.$dump.'</pre></div>';
				echo $dump;
				break;
		}
	}
}

if(!function_exists('_print_r')){
	function _print_r($var, $ret = false){
		global $USER;
		if($USER->IsAdmin()){
			$strRet = '<div class="print_r_a_dump" style="background:#f1f1f1;padding:10px;padding-bottom:3px;"><pre style="background:#fff;boder:1px solid #d3d3d3;max-height:300px;font-size:12px;position:relative;overflow:scroll;">' . print_r($var, true) . '</pre>_print_r($var);</div>';
			if($ret !== false){
				echo $strRet;
			}else{
				return $strRet;
			}
		}
	}
}

if(!function_exists('_fpc')){
	function _fpc($var, $fname = "_fpc_dump.txt"){
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/" . $fname, print_r($var, true));
	}
}

if(!function_exists('_endWord')){
	/*
	 * @param int|float $number
	 * @param array $format
	 * @return string
	 */
	function _endWord($number, $wordForm_1, $wordForm_3, $wordForm_5, $addInt = true){
		$format = array($wordForm_1, $wordForm_3, $wordForm_5);
		$cases = array(2, 0, 1, 1, 1, 2);
		return ($addInt ? $number . ' ' : '') . $format[($number%100 > 4 && $number%100 < 20) ? 2 : $cases[min($number%10, 5)]];
	}
}

if(!function_exists('_getCopyrightYear')){
	/*
	 * Returns string of the form '2010 - 2012'
	 *
	 * @param int $startYear
	 * @return string
	 */
	function _getCopyrightYearRange($startYear = 0){
		$startYear = (int)$startYear;
		$currentYear = (int)date('Y');

		if(empty($startYear))
			$startYear = $currentYear;

		return ($currentYear > $startYear) ? $startYear . ' - ' . $currentYear : $startYear;
	}
}

if(!function_exists('mb_ucfirst')){
	function mb_ucfirst($str, $encoding = 'utf-8'){
		return mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
	}
}