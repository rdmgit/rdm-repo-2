<?php
$MESS ['WEATHER_INFORMER_DISPLAY_DATE'] = 'Показывать дату';
$MESS ['WEATHER_INFORMER_DISPLAY_TIME'] = 'Показывать время';
$MESS ['WEATHER_INFORMER_DISPLAY_WEATHER'] = 'Показывать погоду';
$MESS ['WEATHER_INFORMER_DATE_FORMAT'] = 'Формат даты';
$MESS ['WEATHER_INFORMER_CITY'] = 'Город';