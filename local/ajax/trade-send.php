<?	
	require_once $_SERVER["DOCUMENT_ROOT"]."/local/recaptchalib.php";
	
	$temp = $_REQUEST['fields'];
	$strRespond="";
	$arAnswer = array();
	$autoID=0;
	$msg = "";
	
	/* ------------------------------ */
	// Фильтруем полученные данные
	foreach($temp as $k=>$v){
		
		if(strstr($v["name"],'your_')){
			
			$arUser[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
			
		}else if(strstr($v["name"],'g-recaptcha-response')){
			$strRespond = $v["value"];
		}else{
			//if(is_array()){}
			$arData[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);

			if(strstr($v["name"],'_link')){ $autoID = (int)$v["value"];}	

		}
	
	}
	/* ------------------------------ */
	// Разбор капчи
	// Register API keys at https://www.google.com/recaptcha/admin
	$siteKey = "6Lfy-SgUAAAAAHUFP8mCx8lsf5GowSPmeOKaS7b9";
	$secret = "6Lfy-SgUAAAAAM-effqlqdfMsGHzRxXlZ6-ba9fC";
	
	if(!empty($strRespond)){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$strRespond."&remoteip=".$_SERVER["REMOTE_ADDR"]); // отправляем на
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер    
		$content = curl_exec( $ch );
		curl_close( $ch );
		
		if ($content["success"]) {$status =true;}else{$status = false;}	
			
	}else{
		
		$status = false;
	}


if($status){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");	
    CModule::IncludeModule("form");			
    CModule::IncludeModule("iblock");
	
	/* ------------------------------ */
	// Определяем форму, для почтовых уведомлений
	$rsForms = CForm::GetList($by="s_id", $order="desc", array("SID"=>$_REQUEST["type"]));

	if($arForm = $rsForms->Fetch())
	{
		
		
		$rsFieldList = CFormField::GetList($arForm["ID"], "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	   
	    $arFormFields = array();
	    $arError = array();
		while ($arFieldList = $rsFieldList->GetNext())
		{
			
			$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);
			$arAnswer[$arFieldList["ID"]] = $rsAnswer->Fetch();
			$arFieldList["ANSWER"] = $arAnswer;
			
			$arFormFields[] = $arFieldList;
			
			if($arFieldList["REQUIRED"]=="Y"){
				
				if(empty($arUser[$arFieldList["SID"]])){
					
					$arError[] = $arFieldList["SID"];
				}
				

				
			}
			
			if($arFieldList["SID"] == "your_phone"){
				
				$sPhone = $arUser[$arFieldList["SID"]];
				
				$flag = false;
				if(preg_match('/^[0-9]+$/i', $sPhone)){
					
					if(strlen($sPhone)>=7){													
						$flag=true;
					}else{
						$arError[] = $arFieldList["SID"];
					}
				}else{
					
					$arError[] = $arFieldList["SID"];
				}
			}
		}
		
	/* ------------------------------ */
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID"=>5, "ID"=>$autoID,"ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arRes = $ob->GetFields();
            $arAuto = '<a href="http://'.$_SERVER["HTTP_HOST"].$arRes["DETAIL_PAGE_URL"].'" >'.$arRes["NAME"].'</a>';
        }
	/* ------------------------------ */

		// Проверяем массив с ошибками		
		if(count($arError)==0){
			
				$arWebFormListField = array();
				foreach($arFormFields as $arVal){
               
					$arElem = $arAnswer[$arVal["ID"]];
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] =$arUser[$arVal["SID"]];
					
					if($arVal["SID"] == "auto_link"){
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] = $arAuto;
						
					}
			
					
				}
				
			if ($RESULT_ID = CFormResult::Add($arForm["ID"], $arWebFormListField))
			{
			   $send = "Y";
        
                // создадим почтовое событие для отсылки по EMail данных результата
                if (CFormResult::Mail($RESULT_ID))
                {
                    //echo "Почтовое событие успешно создано.";
                }
                else // ошибка
                {
                    //global $strError;
                    //echo $strError;
                }

			}else{
			   $send = "N";
                global $strError;             
			} 

			$arResult = array("msg"=>$arForm["DESCRIPTION"],"arUser"=>$arUser,"flag"=>$flag,"status"=>true);

		}else{ // Если поля не заполнены
			
			
			$arResult = array("status"=>false,"msg"=>"errorForm","error"=>$arError);
			
			
		}

			
		}else{ // Если поля не заполнены			
			
			$arResult = array("status"=>false,"msg"=>"errorForm","error"=>$arError);			
			
		}
}else{
	
	$arResult = array("status"=>false,"msg"=>"errorForm","error"=>array("captcha"));
}

	/* ------------------------------ */



echo json_encode($arResult);
?>