<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>

    <div class="evo__it-wrap">
<?if(count($arResult["ITEMS"])>0){
    $arItems = $arResult["ITEMS"];
    $ind=0;
    foreach($arItems as $arItem){                
        
        if(count($arItem["ALBUM"])>0){
        $style = ($ind%2)?'evo__it-right':'evo__it-left';

	$this->AddEditAction($arItem['ID'], $arItem['ADD_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_ADD"));
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>    
        <div class="evo__it <?=$style?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="evo_it-media">
                <div class="evo_it-overflow">
                <a id="<?=$arItem["ID"]?>" class="fancybox-EvoPopup" href="javascript:;"><img src="<?=$arItem["ALBUM"][0]["SRC"]?>" alt="" /></a>
                </div>
                <h2><?=$arItem["NAME"]?></h2>
            </div>     
        </div>    
        <?$ind++;}}}?>
             
    </div>
    <script>var jsEvoPic = <?=json_encode($arResult["JSON"]);?></script>