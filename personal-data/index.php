<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Согласие на обработку персональных данных");
?>
<div class="offers-list__item offers-list__item-text full-width-align">
<p>
	 Настоящим Пользователь, действуя своей волей и в своем интересе, принимает согласие на обработку своих персональных данных, отправляя информацию при помощи формы обратной связи на интернет-сайте rdm-import.ru, Принятием (акцептом) оферты Согласия является установка флажка «Подтверждаю согласие на обработку персональных данных». Пользователь дает свое согласие ООО «РДМ-Импорт», которому принадлежит права использования Сайта и которое расположено по адресу: 630112, г.Новосибирск, ул.Фрунзе, дом № 61/2, на обработку своих персональных данных со следующими условиями:
</p>
<ol type="1">
	<li>Данное Согласие дается на обработку персональных данных, как без использования средств автоматизации, так и с их использованием.</li>
	<li>Согласие дается на обработку следующих моих персональных данных:</li>

<p>
</p>
<ul>
	<li>
	<p>
		фамилия, имя, отчество;
	</p>
	</li>
	<li>
	<p>
		номера контактных телефонов;
	</p>
	</li>
	<li>
	<p>
		адрес электронной почты.
	</p>
	</li>
</ul>
<p>
</p>

<li>Цель обработки персональных данных:

<ul>
	<li>
	 обеспечения обратной связи с заинтересованными лицами</li>


	 <li>продвижение товаров и услуг ООО «РДМ-Импорт»</li>


	 <li>получение и исследование статистических данных об объеме продаж и качестве услуг ООО «РДМ-Импорт»</li>


	 <li>совершенствование уровня предоставляемых ООО «РДМ-Импорт»&nbsp; услуг и товаров</li>


	  <li>осуществление других видов деятельности в рамках законодательства РФ с обязательным выполнением требований законодательства РФ в области персональных </li>
</li>
</ul>
	<li>Основанием для обработки персональных данных являются: ст. 24 Конституции Российской Федерации; ст. 6 Федерального закона № 152-ФЗ «О персональных данных»; иные федеральные законы и нормативно-правовые акты.</li>
	<li>В ходе обработки с персональными данными могут быть совершены следующие действия: сбор, запись, систематизация, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передача (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение.</li>
	<li>Передача персональных данных третьим лицам осуществляется на основании законодательства Российской Федерации.</li>
	<li>Персональные данные обрабатываются до ликвидации ООО «РДМ-Импорт» как юридического лица. Также обработка персональных данных может быть прекращена по запросу субъекта персональных данных.</li>
	<li>Согласие может быть отозвано субъектом персональных данных или его представителем путем направления письменного заявления в адрес ООО «РДМ-Импорт» или его представителя.</li>
</ol>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>