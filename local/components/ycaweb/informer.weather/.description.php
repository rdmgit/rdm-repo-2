<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("WEATHER_INFORMER_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("WEATHER_INFORMER_COMPONENT_DESCR"),
	"ICON" => "",
	"PATH" => array(
		"ID" => "ycaweb",
	),
);
?>