<?
//include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

//CHTTP::SetStatus("404 Not Found");
//@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetTitle("Ошибка 404");
?>
<div class="page__main-wrapper">
	<div class="flex-wrapper">
		<p class="page__title">Мы не нашли эту страницу на сайте<br />
		:(</p>
		<p class="page__message">
			Возможно, страница была перемещена или удалена с сайта. Такое бывает, но мы вам поможем!
			<br />
			Скорее всего вы искали что-то из этого:
		</p>
		<div class="buttons">
			<a href="/auto/"class="button">Продажа - более 200 автомобилей в наличии</a>
			<a href="/creditovanie/" class="button">Кредит  от 7,5%</a>
			<a href="/trade-in/" class="button">Обмен вашего автомобиля (Trade-In)</a>
		</div>
		<p class="page__message">
			Если вы не нашли выше, то что искали, предлагаем вам начать с главной страницы:
		</p>
		<a href="/" class="button">Перейти на главную страницу</a>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>