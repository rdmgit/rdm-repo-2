<?
$MESS["AD_DETAIL_ADVERTISMENT"] = "Объявление";
$MESS["AD_DETAIL_YEAR"] = "год";
$MESS["AD_DETAIL_FROM"] = "от";
$MESS["AD_DETAIL_HORSE_POWER"] = "л.с.";
$MESS["AD_DETAIL_CREDIT"] = "Купить в кредит";
$MESS["AD_DETAIL_EXCHANGE"] = "Калькулятор обмена";
$MESS["AD_DETAIL_SHOW_PHONE"] = "Показать телефон";
$MESS["AD_DETAIL_SELLING"] = "Продажа";
$MESS["AD_DETAIL_BACK_TO_LIST"] = "к списку авто";
$MESS["AD_DETAIL_ADD_TO_FAVORITE"] = "В избранное";
$MESS["AD_DETAIL_ADD_TO_COMPARE_LIST_TOP"] = "Список сравнения";
$MESS["AD_DETAIL_ADD_TO_COMPARE_LIST_BOTTOM"] = "В список сравнения";
$MESS["AD_DETAIL_EXPAND_ALL_PHOTOS"] = "Развернуть все фото";
$MESS["AD_DETAIL_AUTO_CHARACTERISTICS"] = "Технические характеристики авто";
$MESS["AD_DETAIL_DIAGNOSTIC_CARD"] = "Диагностическая карта";
$MESS["AD_DETAIL_SIMILAR_PRICE"] = "Похожие по цене";
$MESS["AD_DETAIL_SIMILAR_CHARACTERISTICS"] = "Похожие по тех. характеристикам";
$MESS["AD_DETAIL_RESERVE"] = "Зарезервировать авто";
$MESS["AD_DETAIL_SHARE"] = "Поделиться автомобилем";
$MESS["AD_DETAIL_HAS_RF_MILEAGE"] = "есть";
?>