<? 
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class WebForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {
         CModule::IncludeModule("form");  
		if(isset($_REQUEST["AJAX_CALL"])) {

			$authData = array('message' => 'AJAX',"req"=>$_REQUEST,"arParams"=>$this->arParams);
			
					if(true){

					   if(array_key_exists("respondTo",$_REQUEST)){
						   
							$rsFieldList = CFormField::GetList(4, "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
							$arFieldList = array();
							$arEventFields = array();
							$arFields = array();
							$arErrors = array();
						   
							while ($arField = $rsFieldList->GetNext())
							{
								if(!empty($_REQUEST[$arField["SID"]])){
									$val = htmlspecialchars($_REQUEST[$arField["SID"]], ENT_QUOTES);
									$arFieldList[$arField["SID"]] = $val;
									$arEventFields["form_".$arField["TITLE_TYPE"]."_".$arField["ID"]] = $val;
							
								}else{
									$arErrors[$arField["SID"]] = $arField["SID"];
									
								}
							}
						   
						   
						   $FORM_ID = 4;
						   $arWebTemlate = array(
							
							"respond1" => 48,
							"respond2" => 49,
							"respond3" => 50,
							"respond4" => 51,
						   
						   );
						   
						   $arFields["respondTo"] = htmlspecialchars($_REQUEST["respondTo"], ENT_QUOTES);
										
						   if(isset($arWebTemlate[$arFieldList["respondTo"]])){
							   
								CEvent::Send("SIMPLE_FORM_4", 's1', $arFieldList,"N",$arWebTemlate[$arFieldList["respondTo"]]);

						if ($RESULT_ID = CFormResult::Add($FORM_ID, $arEventFields))
						{
						   $arResult["send"] = "Y";
						}else{
						   $arResult["send"] = "N";
						}   
								$arResult["arEventFields"] = $arEventFields;
								$arResult["arFieldList"] = $arFieldList;
								$arResult["arErrors"] = $arErrors;
								
								echo json_encode($arResult);
							   
						   }
						   

						   
					   }else{
						   
						   echo json_encode(array("status"=>false));
						   
					   }

						
					}

		   
			exit();
		}
    
		$rsForm = CForm::GetByID($this->arParams["FORM_ID"]);
		$arForm = $rsForm->Fetch();
		$this->arResult["BUTTON"] = $arForm["BUTTON"];
		$this->arResult["DESCRIPTION"] = $arForm["DESCRIPTION"];

		$this->includeComponentTemplate();

        }    

        public function onPrepareComponentParams($arParams)
        {
            
            
            if(!isset($arParams["CACHE_TIME"]))
                $arParams["CACHE_TIME"] = 36000000;

            $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
            if(strlen($arParams["IBLOCK_TYPE"])<=0)
                $arParams["IBLOCK_TYPE"] = "news";
            
            $arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
            $arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
            $arParams["AJAX_MODE"] = "N";

            return $arParams;
        }
        
    }
?>