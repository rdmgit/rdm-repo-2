<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$oAssets = \Bitrix\Main\Page\Asset::getInstance();
$arCss = array(
	'jquery.bxslider.css',
	'common.css',
	'custom.css',
);
$arJs = array(
	'jquery-1.9.1.min.js',
	'ajaxprocess.js',
	'plugins/jquery.bxslider.min.js',
	'plugins/selectordie.min.js',
	'jquery.cookie.js',
	'common.js'
);
?>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET;?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=0">
<meta name="HandheldFriendly" content="true">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta name="cmsmagazine" content="ab226615b5c33a7c35bfef2efc5ccdfb" />
<meta property="og:title" content="" />
<meta property="og:url" content="" />
<meta property="og:description" content="" />
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="500" />
<meta property="og:image:height" content="300" />
<meta property="twitter:description" content="" />
<meta name="yandex-verification" content="80669ee7f85afe7b" />
<meta name="format-detection" content="telephone=no">
<?if(strstr($_SERVER["REQUEST_URI"], "auto")){?>
	<title><?$APPLICATION->ShowTitle();?></title>
<?}else{?>
	<title><?$APPLICATION->ShowTitle();?> | <?=\Ycaweb\Tools::getSiteParam('SITE_NAME')?></title>
<?}?>
<?$APPLICATION->ShowMeta("keywords");?>
<?$APPLICATION->ShowMeta("description");?>
<?$APPLICATION->ShowMeta("robots");?>
<link rel="shortcut icon" href="/favicon.ico" />
<?if(!empty($arCss)):
	foreach($arCss as $strStylePath){
		$oAssets->addCss(ASSETS_PATH . '/css/' . $strStylePath);
	}
endif;
$oAssets->addCss(ASSETS_PATH . '/fancy/jquery.fancybox.css');

$APPLICATION->ShowCSS();
$APPLICATION->ShowHeadStrings();?>
<!--[if lt IE 9]>
	<script type="text/javascript" src="<?=ASSETS_PATH?>/js/html5.js"></script>
<![endif]-->
<!--[if lt IE 8]>
	<script type="text/javascript" src="<?=ASSETS_PATH?>/js/json2.js"></script>
<![endif]-->
<?if(!empty($arJs)):
	foreach($arJs as $strScriptPath){
		$oAssets->addJs(ASSETS_PATH . '/js/' . $strScriptPath);
	}
endif;
$oAssets->addJs("//yastatic.net/es5-shims/0.0.2/es5-shims.min.js");
$oAssets->addJs("//yastatic.net/share2/share.js");
$oAssets->addJs("//api-maps.yandex.ru/2.1/?lang=ru_RU");

$oAssets->addJs(ASSETS_PATH . '/fancy/jquery.fancybox.min.js');

$APPLICATION->ShowHeadScripts();