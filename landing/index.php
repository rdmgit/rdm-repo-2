<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Landing</title>
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="social-buttons.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="script.js"></script>
		
	</head>
<body>

<div class="main_layer">

<div class="header">
	<div id="logo">
		<img style="    margin-left: -4%;
    margin-bottom: 17px;" src="img/logo.png">
		<img src="img/marker.png" class="marker" style="margin-left: 6% !important;">
		<!--div class="map_img" style="display:block;width:100%">
			<img src="img/map.png" style="width:100%">
		</div-->
		<div>
			<p style="text-align:center;"><b>Продажа автомобилей с пробегом. Автокредит</b></p>
		</div>
		<div class="phone">
			<p style="line-height: 20px; margin-bottom:5px;text-align:center;">г. Новосибирск, ул. Фрунзе 61/2</p>
			<p style="line-height: 20px; text-align:center;"><a href="tel:89658278641">8-965-827-86-41</a></p>
		</div>
		<div style="width:100%;">
			<!--p>Адрес г. Новосибирск, ул. Фрунзе 61/2</p-->
			<div id="map1" style="width:100%;height:290px;"></div>
		</div>
	</div>
	<nav>
		<ul class="accordeon">
			<li>
				ОБМЕН АВТО <br>(Trade in)
			</li>
			<div class="accordeon_slide">

				<div class="inner_li">
					
					<li><b>Личная и финансовая безопасность</b>
					Берем на себя вопросы по продаже старого автомобиля.</li>

					<li><b>Удобная форма оплаты</b>
					Разница в цене оплачивается наличными или в кредит. Ставки от 9,9%!</li>

					<li><b>Экономия вашего времени</b>
					Для обмена в наличии 200 проверенных автомобилей с гарантией!</li>

					<li><b>Комфорт</b>
					В одном месте можете обменять свой старый автомобиль на новый. В наличии 200 проверенных машин с пробегом.</li>
					<iframe  src="https://www.youtube.com/embed/wEusocKszPw" frameborder="0" allowfullscreen></iframe>
					<button id="btn3">Оставить заявку на обмен</button>
					
					<div class="form form3">
						<p>
							Компания РДМ-Импорт подберет для вас наиболее выгодные варианты обмена, заполните и отправьте форму.
						</p>
						<input type="text" id="form_3_name" placeholder="Имя">
						<input type="text" id="form_3_phone" placeholder="Телефон">
						<textarea placeholder="Информация об автомобиле" id="form_3_comment"></textarea>
						<input type="hidden" id="form3" value="Заявка с формы обмена (лендинг для моб. устройств)">
						
							<input type="checkbox" checked id="check3" value="Посетитель подтвердил согласие на обработку персональных данных"> <label>Подтверждаю согласие на обработку <span class="person_sogl">персональных данных</span>
						</label>
						<button>Отправить</button>
					</div>
				</div>

			</div>
			<li>
				БЫСТРАЯ ПРОДАЖА АВТО <br>(комиссия и выкуп)
			</li>
			<div class="accordeon_slide">
				<div class="inner_li">
					
					<li><b>Отсутствие проблем с налоговой</b>
					Оформление документов согласно законодательству.</li>

					<li><b>Рыночная оценка автомобиля</b>
					Оценка на основание технической и юридический проверки.</li>

					<li><b>Комфортно и быстро</b>
					Проведение проверок и расчет в одном месте.</li>
					<iframe src="https://www.youtube.com/embed/_jaB_6Q_3Uc" frameborder="0" allowfullscreen></iframe>
					<button id="btn4">Оставить заявку на продажу</button>
					
					<div class="form form4">
						<p>
							Напишите о своем автомобиле для его предварительной оценки.
						</p>
						<input type="text" id="form_4_name" placeholder="Имя">
						<input type="text" id="form_4_phone" placeholder="Телефон">
						<textarea placeholder="Информация об автомобиле" id="form_4_comment"></textarea>
						<input type="hidden" id="form4" value="С формы продажи авто (с лендинга для моб. устройств)">
						
							<input type="checkbox" checked id="check4" value="Посетитель подтвердил согласие на обработку персональных данных"> <label>Подтверждаю согласие на <span class="person_sogl">персональных данных</span>
						</label>
						<button>Отправить</button>
					</div>
				</div>
			</div>
			<li>
				ПОКУПКА АВТО <br>(200 автомобилей в наличии)
			</li>
			<div class="accordeon_slide">
				<div class="inner_li">
					<li><b>200 автомобилей в наличии</b>
					Ежедневное обновление парка. Спец условия: автокредит от 9,9%!<br></li>
					<a href="http://vam1.ru" target="_blank"><img src="http://rdmauto.ru/img/baner_auto2.png" style="width:100%"></a>
					<li><b>Техническая гарантия</b>
					Гарантия на коробку, двигатель, электрику и лакокрасочное покрытие.</li>

					<li><b>Юридическая гарантия</b>
					Отсутствие проблем с постановкой на учет и налоговой отчетностью.</li>
					<iframe src="https://www.youtube.com/embed/JIETCKHBsdo" frameborder="0" allowfullscreen></iframe><br><br>
					
					<button id="btn5">Назначить встречу менеджеру</button>
					
					<div class="form form5">
						<p>
							Менеджер будет ждать Вас, напишите нам о своем визите.
						</p>
						<input type="text" id="form_5_name" placeholder="Имя">
						<input type="text" id="form_5_phone" placeholder="Телефон">
						<textarea placeholder="Информация об автомобиле" id="form_5_comment"></textarea>
						<input type="hidden" id="form5" value="С формы покупки авто (с лендинга для моб. устройств)">
						
							<input type="checkbox" checked id="check5" value="Посетитель подтвердил согласие на обработку персональных данных"> <label>Подтверждаю согласие на обработку <span class="person_sogl">персональных данных</span>
						</label>
						<button>Отправить</button>
					</div>
				</div>
			</div>
			<li>
				АВТОКРЕДИТ ОТ 9,9% <br>(заявка во все банки)
			</li>
			<div class="accordeon_slide">
				<div class="inner_li">
					
					<li><b>Отправка заявки в 23 банка</b>
					Из всех кредитных решений выбор лучшего: без КАСКО и первоначального взноса.</li>

					<li><b>Выгодные партнерские условия</b>
					Банки-партнеры дают низкие ставки от 9,9%. Одобрение 1 день!</li>
					
					<li><b>Тщательная проверка условий кредитных договоров</b>
					Убережем от скрытых комиссий и дополнительных платежей банкам.</li>
					
					<li><b>Индивидуальный подбор кредитной программы</b>
					Без КАСКО, без первоначального взноса и подтверждения дохода.</li>

					
					<iframe src="https://www.youtube.com/embed/VMip5sJrKZs" frameborder="0" allowfullscreen></iframe><br>
					<!--a href="http://vam1.ru" target="_blank"><img src="http://rdmauto.ru/img/baner_auto2.png" style="width:100%"></a-->
					<button id="btn6">Отправить заявку во все банки</button>
					<div style="text-align:center;">
						<img style="width:33%;margin:2% 3%; vertical-align: -webkit-baseline-middle;" src="img/1a.png">
						<img style="width:33%;margin:2% 3%; vertical-align: -webkit-baseline-middle;" src="img/2a.png">
						<img style="width:33%;margin:2% 3%; vertical-align: -webkit-baseline-middle;" src="img/3a.png">
						<img style="width:33%;margin:2% 3%; vertical-align: -webkit-baseline-middle;" src="img/4a.png">
						<img style="width:33%;margin:2% 3%; vertical-align: -webkit-baseline-middle;" src="img/5a.png">
						<img style="width:33%;margin:2% 3%; vertical-align: -webkit-baseline-middle;" src="img/6a.png">
					</div>
					<div class="form form6">
						<p>
							Компания РДМ-Импорт подберет для вас наиболее выгодные варианты кредитных программ, заполните и отправьте форму.
кнопка “отправить заявку во все банки

						</p>
						<input type="text" id="form_6_name" placeholder="Имя">
						<input type="text" id="form_6_phone" placeholder="Телефон">
						<textarea placeholder="Информация об автомобиле" id="form_6_comment"></textarea>
						<input type="hidden" id="form6" value="С формы автокредита (с лендинга для моб. устройств)">
						
							<input type="checkbox" checked id="check6" value="Посетитель подтвердил согласие на обработку персональных данных"> <label>Подтверждаю согласие на обработку <span class="person_sogl">персональных данных</span>
						</label>
						<button>Отправить</button>
					</div>
				</div>
			</div>			
		</ul>
	</nav>
	<div class="socials_list">
		<a class="soc" target="_blank" href="https://vk.com/rdmimport">
			<img src="img/vk.png">
		</a>
		<a class="soc" target="_blank" href="https://ok.ru/group/51230024597582">
			<img src="img/ok.png">
		</a>
		<a class="soc" target="_blank" href="https://www.facebook.com/%D0%90%D0%B2%D1%82%D0%BE%D1%81%D0%B0%D0%BB%D0%BE%D0%BD-RDM-Import-440746535952043/">
			<img src="img/fb.png">
		</a>
		<a class="soc" target="_blank" href="https://novosibirsk.flamp.ru/firm/rdm_import_ooo_avtosalon-141265769513599">
			<img src="img/Flamp2.jpg">
		</a>
		<a class="soc" target="_blank" href="https://www.youtube.com/channel/UCAc8BkGqUL1TlaLrHEsdAyQ">
			<img src="img/you.jpg">
		</a>
	</div>
</div>
				<div>
					<div class="car_img" style="margin-top:15px; text-align:justify;">
						Мы помогли выбрать 285 машин 
					</div>
					<div class="car_img" style="margin-top:15px; text-align:justify;">
						Мы помогли продать 331 машину
					</div>
					<button class="btn_rad" id="btn2">Остались вопросы - напишите нам!</button>
					<div class="form form2">
		<p>
			Если у Вас остались вопросы, то напишите нам! В ближайшее время Вам будет предоставлена информация!
		</p>
		<input type="text" id="form_2_name" placeholder="Имя">
		<input type="text" id="form_2_phone" placeholder="Телефон">
		<textarea placeholder="Комментарии" id="form_2_comment"></textarea>
		<input type="hidden" id="form2" value="Заявка с формы о компании (с лендинга для мобю устройств)">
		
			<input type="checkbox" checked id="check2" value="Посетитель подтвердил согласие на обработку персональных данных"> <label>Подтверждаю согласие на обработку <span class="person_sogl">персональных данных</span>
		</label>
		<button>Отправить</button>
	</div>
					<iframe style="width:100%; margin-bottom:15px;" src="https://www.youtube.com/embed/ut01hN2xfak" frameborder="0" allowfullscreen></iframe>
				</div>
<div class="block about">
	<h2>О компании <br>РДМ-Импорт</h2>
	<img src="img/line2.png">
	<p>
		Наше предназначение - обеспечивать жителей Новосибирска и регионов Сибири ТОЛЬКО качественными и проверенными автомобилями, чтобы выбор и покупка были удобными, безопасными, и приятными, а с машинами не было сюрпризов и проблем в будущем.
	</p>
	<p>
		Мы создаем цивилизованный рынок продажи автотранспорта.
	</p>
	<!--p>
		Для нас это не просто слова - это закон. Мы создаем цивилизованный рынок продажи автотранспорта: без обмана и хамства, где продавец не боится смотреть в глаза покупателю, и отношения построены на взаимном уважении и выгоде.
	</p-->
	
	
</div>

<div class="banner">
		<img src="img/banner.png">
</div>
<div class="form form1">
	<p>
		Если у Вас остались вопросы, то напишите нам! В ближайшее время Вам будет предоставлена информация!
	</p>
	<input type="text" id="form_1_name" placeholder="Имя">
	<input type="text" id="form_1_phone" placeholder="Телефон">
	<textarea placeholder="Комментарии" id="form_1_comment"></textarea>
	<input type="hidden" id="form1" value="Форма совета эксперта (с лендинга для моб. устройств)">
	
		<input type="checkbox" checked id="check1" value="Посетитель подтвердил согласие на обработку персональных данных"> <label>Подтверждаю согласие на обработку <span class="person_sogl">персональных данных</span>
	</label>
	<button>Отправить</button>
</div>

<div class="footer">
	<div>
		<p><a href="tel:89658278641">8-965-827-86-41</a></p>
		<p>г. Новосибирск, ул. Фрунзе 61/2</p>
		<div id="map">
			<img src="img/map.png" style="width:100%;">
		</div>
		<div>
			<a target="_blank" class="full_v" href="http://rdm-import.ru">Полная версия сайта</a>
		</div>
	</div>
</div>




</div>

<div class="black_layer">

	<div class="white_list">
		<div class="close">✖</div>
		<p>
	 Настоящим Пользователь, действуя своей волей и в своем интересе, принимает согласие на обработку своих персональных данных, отправляя информацию при помощи формы обратной связи на интернет-сайте rdm-import.ru, Принятием (акцептом) оферты Согласия является установка флажка «Подтверждаю согласие на обработку персональных данных». Пользователь дает свое согласие ООО «РДМ-Импорт», которому принадлежит права использования Сайта и которое расположено по адресу: 630112, г.Новосибирск, ул.Фрунзе, дом № 61/2, на обработку своих персональных данных со следующими условиями:
</p>
<ol type="1">
	<li>Данное Согласие дается на обработку персональных данных, как без использования средств автоматизации, так и с их использованием.</li>
	<li>Согласие дается на обработку следующих моих персональных данных:</li>

<p>
</p>
<ul>
	<li>
	<p>
		фамилия, имя, отчество;
	</p>
	</li>
	<li>
	<p>
		номера контактных телефонов;
	</p>
	</li>
	<li>
	<p>
		адрес электронной почты.
	</p>
	</li>
</ul>
<p>
</p>

<li>Цель обработки персональных данных:

<ul>
	<li>
	 обеспечения обратной связи с заинтересованными лицами</li>


	 <li>продвижение товаров и услуг ООО «РДМ-Импорт»</li>


	 <li>получение и исследование статистических данных об объеме продаж и качестве услуг ООО «РДМ-Импорт»</li>


	 <li>совершенствование уровня предоставляемых ООО «РДМ-Импорт»&nbsp; услуг и товаров</li>


	  <li>осуществление других видов деятельности в рамках законодательства РФ с обязательным выполнением требований законодательства РФ в области персональных </li>
</li>
</ul>
	<li>Основанием для обработки персональных данных являются: ст. 24 Конституции Российской Федерации; ст. 6 Федерального закона № 152-ФЗ «О персональных данных»; иные федеральные законы и нормативно-правовые акты.</li>
	<li>В ходе обработки с персональными данными могут быть совершены следующие действия: сбор, запись, систематизация, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передача (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение.</li>
	<li>Передача персональных данных третьим лицам осуществляется на основании законодательства Российской Федерации.</li>
	<li>Персональные данные обрабатываются до ликвидации ООО «РДМ-Импорт» как юридического лица. Также обработка персональных данных может быть прекращена по запросу субъекта персональных данных.</li>
	<li>Согласие может быть отозвано субъектом персональных данных или его представителем путем направления письменного заявления в адрес ООО «РДМ-Импорт» или его представителя.</li>
</ol>
	</div>

</div>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script> <script>
ymaps.ready(function () {
    var myMap = new ymaps.Map('map1', {
            center: [55.040636, 82.952120],
            zoom: 14.8
        }, {
            searchControlProvider: 'yandex#search'
        }),

        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'РДМ-ИМПОРТ',
            balloonContent: 'РДМ-ИМПОРТ'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38]
        });

    myMap.geoObjects
        .add(myPlacemark)
        .add(myPlacemarkWithContent);
		
		var myMap1 = new ymaps.Map('map', {
            center: [55.040636, 82.952120],
            zoom: 14.8
        }, {
            searchControlProvider: 'yandex#search'
        }),

        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),

        myPlacemark1 = new ymaps.Placemark(myMap1.getCenter(), {
            hintContent: 'РДМ-ИМПОРТ',
            balloonContent: 'РДМ-ИМПОРТ'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38]
        });
		myMap.behaviors.disable('scrollZoom');
    myMap.geoObjects
        .add(myPlacemark1)
        .add(myPlacemarkWithContent);
});

</script> 
<body>
</html>