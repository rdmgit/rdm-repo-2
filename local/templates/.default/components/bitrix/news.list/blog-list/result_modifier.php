<?
	$arComments = array();
	$arShow = array();
	$arElements = array();
	
	// Список элементов
	if(count($arResult["ITEMS"])){
		
		foreach($arResult["ITEMS"] as $arItem){
			
			$arElements[] = $arItem["ID"];
						
		}
		
	}

	//Получение количества просмотров у элементов
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","SHOW_COUNTER","PROPERTY_ID_ELEMENT");
	$arFilter = Array("IBLOCK_ID"=>$arResult["ID"],"ID"=>$arElements, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

	while($ob = $res->GetNextElement())
	{
	$arFields = $ob->GetFields();
	$arShow[$arFields["ID"]]=$arFields["SHOW_COUNTER"];	
	}

	//Список комментариев у элементов
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_ID_ELEMENT");
	$arFilter = Array("IBLOCK_ID"=>21, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","=PROPERTY_ID_ELEMENT"=>$arElements);

	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

	while($ob = $res->GetNextElement())
	{
	$arFields = $ob->GetFields();
	$arComments[$arFields["PROPERTY_ID_ELEMENT_VALUE"]] [] = "comment"; 

	}

	//Сохраняем для каждого элемента количество просмотров и комментариев
	foreach($arResult["ITEMS"] as $key => $arItem){
		
		
		if(isset($arComments[$arItem["ID"]])){
			
			$arResult["ITEMS"][$key]["COMMENTS"] = count($arComments[$arItem["ID"]]);
			$arResult["ITEMS"][$key]["SHOW_COUNTER"] = count($arShow[$arItem["ID"]]);
			
		}else{
			
			$arResult["ITEMS"][$key]["COMMENTS"] = 0;
		}
		
		if(isset($arShow[$arItem["ID"]])){

			$arResult["ITEMS"][$key]["SHOW_COUNTER"] = $arShow[$arItem["ID"]];
			
		}	
		
		/* Разбор тегов*/
		
		if(!empty($arItem["TAGS"])){
			
			$tags = str_replace(", ",",",$arItem["TAGS"]);
			$tags = explode(",",$tags);
			$txt = "";
			foreach($tags as $v){
				
				$txt .='<a href="/blog/?tag='.$v.'">#'.$v.'</a>';
				
			}
			$arResult["ITEMS"][$key]["TAGS_HTML"] = $txt;
			//$tags = trim($tags," ");
			//echo"<pre>";
			//print_r($tags);
			//echo"</pre>";
		}
	}
	
	
	
?>