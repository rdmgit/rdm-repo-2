$(document).ready(function(){
    
    /*
     *  Open popup of picture
     */

    $(".fancybox-HistoryPopup").click(function() {
        
        var id = $(this).attr("id");
        if(jsHistoryPic[id] != undefined){
                          
            ob = jsHistoryPic[id];
            var group = new Array();
            
            for(var i in ob){
				if(ob[i]["show"]=="Y"){
					group[i] = {src:ob[i]["src"],opts : {caption : ob[i]["description"]}};
				}else{
					group[i] = {src:ob[i]["src"]};					
				}
            }
            
            $.fancybox.open(group, 
            {
                helpers : {
                    thumbs : {
                        width: 75,
                        height: 50
                    },
                    overlay: {
                        locked: false
                    }
                }
            });

        }

    });
    
});