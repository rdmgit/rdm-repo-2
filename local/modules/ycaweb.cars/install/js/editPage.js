/**
 * Created by LaDi666 on 23.12.2016.
 */
$(function(){
    $('body').on('change', '._js-select', function(e){
        e.preventDefault();
        var propCode = $(this).data('prop');
        var propValue = $(this).find('option:selected').data('val');

        if($(this).hasClass('_modification'))
        {
            getChosenPropsValue(propCode, propValue);
        }

        var nextSelect =  $(this).closest('._js-prop-row').next().find('._js-select');
        var currentSelect = $(this);
        clearNextSelects(currentSelect);

        if(propValue)
        {
            getAjaxProp(nextSelect, propCode, propValue);
        }
        else
        {
            nextSelect.prop('disabled', 'disabled');
        }

        var manufacturerVal = $('._js-select._manufacturer').val();
        var modelVal = $('._js-select._model').val();

        if(manufacturerVal != '' && modelVal != '')
        {
            $('.submit-btn').prop('disabled', '');
        }
        else
        {
            $('.submit-btn').prop('disabled', 'disabled');
        }
    });


    $('#ad-form').on('submit', function(e){
        e.preventDefault();
        var formData = $(this).serialize();

        $.ajax({
            url: "/local/ajax/editAd.php",
            method: 'POST',
            data: formData,
            success: function(data){
                var response = JSON.parse(data);
                if(response.ELEMENT_ID)
                {
                    console.log(response.RESULT_URL);
                    window.location = response.RESULT_URL;
                }
                else
                {
                    console.log(response.ERROR);
                }
            },
            error: function(str){
                console.log(str);
            }
        });
    });
});

function getAjaxProp(selectToAdd, code, value)
{
    if(value) {
        $.ajax({
            url: "/local/ajax/getProp.php",
            method: 'POST',
            data: {code: code, value: value},
            success: function (data) {
                var response = JSON.parse(data);

                for (var i = 0; i < response.length; i++)
                {
                    var years = '';
                    if(response[i].YEAR_BEGIN)
                    {
                        years = ' (' +  response[i].YEAR_BEGIN + '-' + response[i].YEAR_END + ')';
                    }
                    $("<option data-val='" + response[i].ID + "'>" + response[i].NAME + years + "</option>").val(response[i].NAME).appendTo(selectToAdd);
                }

                if(selectToAdd.find('option').size() == 1)
                {
                    var optionValue = selectToAdd.find('option:selected').data('val');
                    var selectCode = selectToAdd.data('prop');
                    var select =  $(selectToAdd).closest('._js-prop-row').next().find('._js-select');

                    if(select.length)
                    {
                        select.find('option').remove();
                    }
                    getAjaxProp(select, selectCode, optionValue);
                }
                selectToAdd.prop('disabled', '');
            }
        });
    }
}

function getChosenPropsValue(propCode, modificationID)
{
    $.ajax({
        url: "/local/ajax/getProp.php",
        method: 'POST',
        data: {code: propCode, value: modificationID},
        success: function (data)
        {
            var response = JSON.parse(data);
            for (var i = 0; i < response.length; i++)
            {
                $("input[data-prop='" + response[i].IBLOCK_CODE + "']").val(response[i].VALUE);
            }
        }
    });
}

function clearNextSelects(currentSelect)
{
    var nextSelect =  currentSelect.closest('._js-prop-row').next().find('._js-select');

    if(nextSelect.length)
    {
        nextSelect.find('option').remove();
        clearNextSelects(nextSelect);
    }
}