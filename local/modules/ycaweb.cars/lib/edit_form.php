<?

namespace Ycaweb\Cars;

use Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

Loader::includeModule('iblock');
Loc::loadMessages(__FILE__);
/**
 * Class EditForm
 *
 * @package ycaweb.cars
 */

class EditForm{

    const MODULE_ID = 'ycaweb.cars';

    public static function CreateEditFormBtn(&$aContext){

        $adIblockID = Option::get(self::MODULE_ID, 'carIblock');
		
        if(!empty($adIblockID))
        {
            $dbIblocks = \CIBlock::GetByID($adIblockID);

            if($arIblock = $dbIblocks->fetch())
            {
                $adIblockType = $arIblock["IBLOCK_TYPE_ID"];
            }

            if ($GLOBALS["APPLICATION"]->GetCurPage(true) == "/bitrix/admin/iblock_list_admin.php" && $_REQUEST['IBLOCK_ID'] == $adIblockID)
            {
                $addMenuExists = false;
                foreach ($aContext as $arMenu) {
                    if($arMenu["TEXT"] == Loc::getMessage("YCAWEB_CARS_EDIT_FORM_ADD_BUTTON_NAME"))
                    {
                        $addMenuExists = true;
                    }
                }

                if(!$addMenuExists)
                {					
                    $aContext[] =
                        array(
                            "TEXT" => Loc::getMessage("YCAWEB_CARS_EDIT_FORM_ADD_BUTTON_NAME"),
                            "LINK" => "ad_element_edit.php?IBLOCK_ID=".$adIblockID."&type=".$adIblockType."&ID=0&lang=" . LANG,
                            "TITLE" => Loc::getMessage("YCAWEB_CARS_EDIT_FORM_ADD_BUTTON_TOOLTIP"),
                    );
                }

            }
            else if($GLOBALS["APPLICATION"]->GetCurPage(true) == "/bitrix/admin/iblock_element_edit.php" && $_REQUEST['IBLOCK_ID'] == $adIblockID)
            {
                $editMenuExists = false;
                foreach ($aContext as $arMenu) {
                    if($arMenu["TEXT"] == Loc::getMessage("YCAWEB_CARS_EDIT_FORM_EDIT_BUTTON_NAME"))
                    {
                        $editMenuExists = true;
                    }
                }
                if(!$editMenuExists) {
                    $aContext[] =
                        array(
                            "TEXT" => Loc::getMessage("YCAWEB_CARS_EDIT_FORM_EDIT_BUTTON_NAME"),
                            "LINK" => "ad_element_edit.php?IBLOCK_ID=" . $adIblockID . "&type=".$adIblockType."&ID=" . $_REQUEST["ID"] . "&lang=" . LANG,
                            "TITLE" => Loc::getMessage("YCAWEB_CARS_EDIT_FORM_EDIT_BUTTON_TOOLTIP"),
                        );
                }
            }
        }
    }
}