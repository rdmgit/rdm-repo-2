<?php
$filename = 'old.csv';

$content  = file_get_contents($filename);

$file = fopen($filename, "w") or die("Unable to open csv[w] file!");
fputcsv($file, [
    'IE_XML_ID', 'IE_NAME', 'IE_ID', 'IE_ACTIVE', 'IE_ACTIVE_FROM', 'IE_ACTIVE_TO',
    'IE_PREVIEW_PICTURE', 'IE_PREVIEW_TEXT', 'IE_PREVIEW_TEXT_TYPE', 'IE_DETAIL_PICTURE',
    'IE_DETAIL_TEXT', 'IE_DETAIL_TEXT_TYPE', 'IE_CODE', 'IE_SORT', 'IE_TAGS', 'IP_PROP44',
    'IP_PROP17', 'IP_PROP15', 'IP_PROP31', 'IP_PROP11', 'IP_PROP16', 'IP_PROP43', 'IP_PROP19',
    'IP_PROP20', 'IP_PROP42', 'IP_PROP26', 'IP_PROP12', 'IP_PROP53', 'IP_PROP96', 'IP_PROP24',
    'IP_PROP40', 'IP_PROP18', 'IP_PROP13', 'IP_PROP22', 'IP_PROP41', 'IP_PROP97', 'IP_PROP27',
    'IP_PROP30', 'IP_PROP25', 'IP_PROP14', 'IP_PROP21', 'IP_PROP10', 'IP_PROP29',
    'IC_GROUP0', 'IC_GROUP1', 'IC_GROUP2',
], ';');
fwrite($file, $content);
fclose($file);