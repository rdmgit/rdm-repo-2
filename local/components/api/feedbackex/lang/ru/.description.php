<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$MESS['CP_NODE_NAME']   = 'Тюнинг-Софт';
$MESS['CP_NAME']        = 'TS Расширенная форма обратной связи';
$MESS['CP_DESCRIPTION'] = 'Расширенная форма обратной связи на ajax для отправки сообщений с сайта';