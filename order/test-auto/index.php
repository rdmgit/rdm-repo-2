<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?><script>
﻿$(document).ready(function(){
      var manufacturerID = 'Honda';
            var ajaxUrl =  '/local/ajax/getModels.php';
            var params = { 'manufacturerID' : manufacturerID };

            $.post(ajaxUrl, params, function(data)
            {
                var result = JSON.parse(data);
                console.log(result);
                if(result.ERRORS)
                {
                    console.log(result.ERRORS);
                }
                else if(result.MODELS)
                {
                    $(modelsSelect).attr('disabled', false).find('option').remove();

                    if(result.MODELS.length)
                    {
                        $("<option>--</option>").appendTo(modelsSelect).val('');

                        for (var i=0; i< result.MODELS.length; i++)
                        {
                            $("<option class='auto-form__item-select-var'>" + result.MODELS[i].label + "</option>").appendTo(modelsSelect).val(result.MODELS[i].value);
                        }
                        $(modelsSelect).selectOrDie("update");
                        $('._model .sod_select').removeClass('disabled').find('.sod_list').css({'max-height': '165px'});
                    }

                }
            })
})
﻿</script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>