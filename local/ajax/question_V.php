<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 	?>
<?
global $USER;

$CAPTCHA_CODE = htmlspecialchars($GLOBALS["APPLICATION"]->CaptchaGetCode()); 
?>	

<div class="modalbox_question" id="id_modalbox_question" >
    <div class="quick-feedback">

        <form action="" method="POST" class="b-form">

            <div class="title">Cпасибо за вашу помощь: что бы вы хотели изменить или улучшить на нашем сайте?</div>
            
            <input type="hidden" value="V" name="feadback-id" id="feadback_id"/>
            
            <textarea value="Сообщение" name="MESSAGE" class="text_hint" id="QUESTION_TEXT" placeholder="Ваше сообщение:"></textarea>
			
			<div id="captchaBlock" class="bxCaptcha">
	
			   <input id="captchaSid" type="hidden" name="captcha_sid" value="<?=$CAPTCHA_CODE?>" />
			   <a id="reloadCaptcha"  class="reloadCaptcha" ><img class="reloadCaptcha-img" src="/local/assets/img/refresh-button.svg" /></a>
			   <img id="captchaImg" class="captchaImg" src="/bitrix/tools/captcha.php?captcha_sid=<?=$CAPTCHA_CODE?>" width="180" height="40" alt="CAPTCHA" />
			   
			   <div class="inputWord">
			   <span class="bxCaptcha-text">Введите код с картинки:</span>
					 <input class="bxCaptcha-word" type="text" name="captcha_word" maxlength="50" value="" />
				</div>
			</div>
			
            <div class="button-wrap">
                <span id="submitForm_question" class="submit-form-question">
                    <span>Отправить</span>
                </span>
            </div>
            <div class="questMsgError" id="questMsgError"></div>
        </form>
    </div>
    <div class="clr"><!-- --></div>
        
</div>
