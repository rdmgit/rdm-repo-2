<?
namespace Classes\Handlers;

class Iblock
{
	
	private static $aсtiveElement = false;

	/**
     * OnAfterIBlockElementUpdate - вызывается после попытки изменения элемента информационного блока методом CIBlockElement::Update.
	  @param &arFields
	 OnAfterIBlockElementAdd - вызывается после попытки добавления нового элемента информационного блока методом CIBlockElement::Add. 
	 @param &arFields
     * @return void
    */
	public static function BXIBlockAfterSave(&$arFields)
	{
////arraytofile($_REQUEST,$_SERVER['DOCUMENT_ROOT']."/local/php_interface/classes/handlers/ImportLog.txt", "_REQUEST");	
		
		if (@$_REQUEST['mode'] == 'import')
		{
            if($arFields["IBLOCK_ID"] == 5)
            {			
                $arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_MANUFACTURER', 'PROPERTY_MANUFACTURER_OLD', 'PROPERTY_MODIFICATION', 'PROPERTY_GENERATION', 'PROPERTY_SERIE', 'PROPERTY_MODEL');
                $arFilter = array ('IBLOCK_ID'  => 5, 'ID'  => $arFields['ID']);
                $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                $hLCarMark = new \HLBlock('CarMark');
                $hLCarModel = new \HLBlock('CarModel');
                $hLCarGeneration = new \HLBlock('CarGeneration');
                $hLCarSerie = new \HLBlock('CarSerie');
                $hLCarModification = new \HLBlock('CarModification');
                if ($arEl = $res->fetch())
                {
                    $arCarMark = $hLCarMark->getField(array('UF_XML_ID' => $arEl['PROPERTY_MANUFACTURER_OLD_VALUE']));
                    $arCarMark = $hLCarMark->getField(array('UF_XML_ID' => $arEl['PROPERTY_MANUFACTURER_VALUE']));
                    if($arCarMark['UF_NAME'])
                        $arProps['MANUFACTURER'] = $arCarMark['UF_NAME'];
                    $arCarModel = $hLCarModel->getField(array('UF_XML_ID' => $arEl['PROPERTY_MODEL_VALUE']));
                    if($arCarModel['UF_NAME'])
                        $arProps['MODEL'] = $arCarModel['UF_NAME'];
                
                    $arCarGeneration = $hLCarGeneration->getField(array('UF_XML_ID' => $arEl['PROPERTY_GENERATION_VALUE']));
                    if($arCarGeneration['UF_NAME'])
                        $arProps['GENERATION'] = $arCarGeneration['UF_NAME'];

                    $arCarSerie = $hLCarSerie->getField(array('UF_XML_ID' => $arEl['PROPERTY_SERIE_VALUE']));
                    if($arCarSerie['UF_NAME'])
                        $arProps['SERIE'] = $arCarSerie['UF_NAME'];

                
                    $arCarModification = $hLCarModification->getField(array('UF_XML_ID' => $arEl['PROPERTY_MODIFICATION_VALUE']));
                    if($arCarModification['UF_NAME'])
                        $arProps['MODIFICATION'] = $arCarModification['UF_NAME'];

                    \CIBlockElement::SetPropertyValuesEx($arFields["ID"],$arFields["IBLOCK_ID"], $arProps);
                    //p($arProps, 'arProps',1);  
                }  
            }
        }
	}

}