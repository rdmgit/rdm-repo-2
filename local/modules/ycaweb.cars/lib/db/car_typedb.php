<?php

namespace Ycaweb\Cars;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class CarTypeTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_type';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
             'id_car_type' => array(
                'data_type' => 'integer',
                'primary' => true,                
             ),
             'name' => array(
                "data_type" => "string",
                "required" => false,
                "title"	=> Loc::getMessage("YCAWEB_CAR_TYPE_NAME")
             )
        );
    }
}