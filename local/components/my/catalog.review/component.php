<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Data\TaggedCache;

//$elementID = $arParams["ELEMENT_ID"];
$elementID = 0;



	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_ID_ELEMENT");
	$arFilter = Array("IBLOCK_ID"=>22, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","CODE"=>$_REQUEST["ELEMENT_CODE"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	
	if($ob = $res->GetNext())
	{
		$elementID = $ob["ID"];
	}


if (0 < $elementID)
{
	$obCache = new CPHPCache; 
	$iLifeTime = $arParams["CACHE_TIME"];
	$sCacheID = 'review_'.$elementID;
	$cacheDir = '/s1/my/catalog.element.reviews';
	
	$arReviews= array();	
	if($obCache->InitCache($iLifeTime, $sCacheID, $cacheDir) ) 
	{
		$arReviews = $obCache->GetVars();		
	}
	elseif($obCache->StartDataCache())
	{

        if (Bitrix\Main\Loader::includeModule('iblock')) {
            //выбираем все предложения, относящиеся к товару
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_CREATE", "PREVIEW_TEXT", 'PROPERTY_ID_ELEMENT', 'PROPERTY_NAME', 'PROPERTY_PHOTO', 'PROPERTY_ANSWER_REVIEW');
            $res = CIBlockElement::GetList(Array('DATE_CREATE'=>'DESC'), Array('IBLOCK_ID'=>REVIEWS_IBLOCK_ID, 'ACTIVE' => 'Y', 'PROPERTY_ID_ELEMENT'=>$elementID), false, false, $arSelect);
            if($res->SelectedRowsCount()) { 
                while($ob = $res->GetNextElement())
                {
                    $reviews = $ob->GetFields();
                    $arReviews[$reviews['ID']] = $reviews;
                    
                    if (defined('BX_COMP_MANAGED_CACHE')) {
                        $tagCache = new TaggedCache();
                        $tagCache->startTagCache($cacheDir);
                        $tagCache->registerTag(sprintf('review_%s', $elementID));
                        $tagCache->endTagCache();
                    }
                }
            }else{
                $obCache->abortDataCache();
            }	
        }
		$obCache->EndDataCache($arReviews);
    }
	
    $arResult['REVIEWS'] = $arReviews;
    /*$count_review = count($arResult['REVIEWS']);   

    if($count_review>= 10 && $count_review <= 20 || $count_review%10 >= 6 && $count_review%10 <= 9 || $count_review%10 == 0)
        $ending_review = 'ов'; 
    elseif($count_review % 10 > 1 && $count_review%10 < 5)
        $ending_review = 'а';
    elseif($count_review % 10 == 1)
        $ending_review = '';
    ?>
    <span id="count_comments_container" class="display_none"><? echo sprintf('%s отзыв%s', $count_review, $ending_review);?></span>
 	<?*/
	if(isset($_SESSION['SESS_AUTH']) && is_array($_SESSION['SESS_AUTH']) ){
		$nameUser = $_SESSION['SESS_AUTH']['FIRST_NAME'];
		$emailUser = $_SESSION['SESS_AUTH']['EMAIL'];				
	}
	
	$arResult['NAME_USER'] = $nameUser;
	$arResult['EMAIL_USER'] = $emailUser;
	$arResult['ELEMENT_ID'] = $elementID;

	$this->IncludeComponentTemplate();	
}
?>  
   