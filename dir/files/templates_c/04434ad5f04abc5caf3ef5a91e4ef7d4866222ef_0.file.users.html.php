<?php
/* Smarty version 3.1.30, created on 2017-06-09 14:59:31
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/users.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a8da37829c3_67121556',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '04434ad5f04abc5caf3ef5a91e4ef7d4866222ef' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/users.html',
      1 => 1496812354,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:user_limit.html' => 1,
  ),
),false)) {
function content_593a8da37829c3_67121556 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/home/a/amazuref/new.rdm-import.ru/public_html/dir/framework/module/template/template/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_replace')) require_once '/home/a/amazuref/new.rdm-import.ru/public_html/dir/framework/module/template/template/smarty/plugins/modifier.replace.php';
?>
				<!-- Your awesome content goes here -->				
				
				<div class="box-info full">
					<h2><strong><?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account') {?>Аккаунты Яндекс.Директ<?php } else { ?>Пользователи<?php }?></strong><?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['USER']['message'])) {?><br> <strong class="color_red">&#9888; <?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['message'];?>
</strong><?php }?></h2>
						
						<?php if (($_smarty_tpl->tpl_vars['USER']->value['group'] == 1 || $_smarty_tpl->tpl_vars['USER']->value['group'] == 4) && ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account' || $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'admin')) {?><div class="data-table-toolbar">
							<div class="row">
								<div class="col-md-4">
									
								</div>
								<div class="col-md-8">
									<div class="toolbar-btn-action">
										<a class="btn btn-success" href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/edit/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/"><i class="fa fa-plus-circle"></i> Добавить <?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'] == 'account') {?>аккаунт Яндекс.Директ<?php } else { ?>пользователя<?php }?></a>
									</div>
								</div>
							</div>
						</div><?php }?>
						
					<div class="table-responsive"><form class="save" action="">
						<table data-sortable class="table table-hover table-striped">
							<thead>
								<tr>
									<th>ID</th>
									
									<th>ФИО</th>
									<th>Логин</th>
									<th>Email</th>
									<th>Телефон</th>
									<th>Запросы</th>
									<th>Обновить токен</th>
									<th>Статус</th>
									
									<th data-sortable="false">Редактировать</th>									
								</tr>
							</thead>
							
							<tbody>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['USER']['ELEMENT'], 'ELEMENT');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ELEMENT']->value) {
?>
								<tr>
									<td><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['status'] == 2) {?><font class="color_red"><?php }
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
</font></td>
									
									<td data-toggle="tooltip" title="Последний вход: <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ELEMENT']->value['time'],'%H:%M %d.%m.%Y');?>
. Последняя синхронизация:  <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ELEMENT']->value['timestamp'],'%H:%M %d.%m.%Y');?>
"><strong><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/edit/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
 <?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['lastname'];?>
</a></strong></td>
									<td><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/edit/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['login'];?>
</a></td>
									<td><a href="mailto: <?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['email'];?>
"><?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['ELEMENT']->value['email'],'@',' @');?>
</a></td>
									<td><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['phone'];?>
</td>
									<td><?php $_smarty_tpl->_subTemplateRender("file:user_limit.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
</td>
									<td><?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru') {
} else {
if ($_smarty_tpl->tpl_vars['ELEMENT']->value['group'] == 4) {?><a class="btn btn-default" data-toggle="tooltip" title="Для получения «Токена» вы должны, сначала авторизоваться в аккаунте Яндекс.Директ: <?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['login'];?>
" href="https://oauth.yandex.ru/authorize?response_type=code&client_id=<?php echo $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['id'];?>
&state=<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
|<?php echo $_SERVER['HTTP_HOST'];
echo $_SERVER['REQUEST_URI'];?>
"><?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['token'])) {?>Обновить токен<?php } else { ?>Получить токен<?php }?></a><?php }
}?></td>
									<td><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['status'] == 2) {?><font class="color_red">выключен</font><?php } else { ?>включен<?php }?></td>
									
									<td>
										<div class="btn-group btn-group-xs">
											<a data-toggle="tooltip" title="Редактировать" href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/edit/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/" class="btn btn-default"><i class="fa fa-edit"></i></a>
											<?php if (($_smarty_tpl->tpl_vars['ELEMENT']->value['group'] == 2 || $_smarty_tpl->tpl_vars['ELEMENT']->value['group'] == 4) && $_SERVER['HTTP_HOST'] != 'demo.direct-automate.ru') {?><a data-toggle="tooltip" title="Пересинхронизировать" href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/refresh/1/" class="btn btn-default"><i class="fa fa-refresh"></i></a>
											<button type="button" data-toggle="tooltip" title="Сбросить все настройки стратегий"  onclick="if (confirm('Сбросить все настройки стратегий?')) location.href='<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/reset/1/'" class="btn btn-default"><i class="fa fa-gears"></i></button><?php }?>
											<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['group'] == 4 && $_SERVER['HTTP_HOST'] != 'demo.direct-automate.ru') {?><button type="button" data-toggle="tooltip" title="Сбросить все настройки стратегий"  onclick="if (confirm('Сбросить все настройки стратегий?')) location.href='<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/reset/1/'" class="btn btn-default"><i class="fa fa-gears"></i></button>
											<button type="button" data-toggle="tooltip" title="Удалить" onclick="if (confirm('Удалить?')) location.href='<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PARAM']['group'];?>
/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/delete/1/'" class="btn btn-default"><i class="fa fa-times-circle"></i></button>
											<a data-toggle="tooltip" title="Отозвать токен" href="https://passport.yandex.ru/profile/access" class="btn btn-default" target="_blank"><i class="fa fa-ban"></i></a><?php }?>
										</div>
									</td>
								</tr>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</tbody>
						</table>
						</form>
					</div>
	
					<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['pages']) && $_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['pages'] > 1) {?>	
					<div class="data-table-toolbar">
						<ul class="pagination">
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['first']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['first'];?>
/">&laquo;</a></li>
						  <?php
$__section_page_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_page']) ? $_smarty_tpl->tpl_vars['__smarty_section_page'] : false;
$__section_page_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['pages']) ? count($_loop) : max(0, (int) $_loop));
$__section_page_0_start = min(0, $__section_page_0_loop);
$__section_page_0_total = min(($__section_page_0_loop - $__section_page_0_start), $__section_page_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_page'] = new Smarty_Variable(array());
if ($__section_page_0_total != 0) {
for ($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] = 1, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] = $__section_page_0_start; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] <= $__section_page_0_total; $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']++, $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']++){
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['current'] == (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null)) {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['index'] : null);?>
/"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_page']->value['iteration'] : null);?>
</a></li>
						  <?php
}
}
if ($__section_page_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_page'] = $__section_page_0_saved;
}
?>
						  <li<?php if ($_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['page'] == $_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['last']) {?> class="disabled"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'])) {?>group/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['group'];?>
/<?php }?>page/<?php echo $_smarty_tpl->tpl_vars['DATA']->value['USER']['PAGE']['last'];?>
/">&raquo;</a></li>
						</ul>
					</div>
					<?php }?>
					
				</div>
				
				
				
				
				
				<!-- End of your awesome content --><?php }
}
