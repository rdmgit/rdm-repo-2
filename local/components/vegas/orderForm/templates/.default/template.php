<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS($templateFolder."/jquery.validate.js");?>

<div class="orderForm" id="_js-orderForm">
    <div class="modalForm__header">
    <img class="modalForm__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
    
    </div>

<form class="order-form" id="order-form" method="post">
    
    <div class="order-input-fullrow">

          <input type="text"  name="order_name" placeholder="Ваше имя" class="order-in-text" id="name">

    </div>
	
    <div class="order-input-fullrow">
       
              <input type="text" name="order_email" placeholder="Ваш E-mail" class="order-in-text" id="email">
     
    </div> 
	
    <div class="order-input-fullrow">
      
              <input type="text"  name="order_phone" placeholder="Мобильный телефон" class="order-in-text" id="phone">
     
    </div>
			
    <div class="order-input-fullrow">        
        <textarea class="order-input-tarea"  name="order_comment" id="info" placeholder="Сообщение"></textarea>
    </div>		
    <div class="order-input-fullrow edata">					
        <input type="checkbox" class="in-text-data"  name="order_edata"  id="edata" checked="checked">Подтверждаю согласие на обработку <a href="/personal-data/" target="_blank">персональных данных</a>
    </div>
    <div class="order-input-fullrow order-input-row-center">    
        <button id="go_mail">Отправить</button>
    </div>
</form>
</div>

<div class="winCallback">

    <div class="modalForm__header"><img class="modalForm__logo" src="/local/assets/img/assets/header/logo.svg" alt="" /></div>
    <div class="modalForm__body">
    
	<span>Спасибо, за то что выбрали нас</span>
   
   </div>
</div>