<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");	
    CModule::IncludeModule("form");			
	$arUser = array();
	$arParams = array();

	$msg = "";
	
	/* ------------------------------ */ 
if(count($_REQUEST)>0)
{		
	if($_REQUEST["params"]) {
        foreach ($_REQUEST["params"] as $k => $v) {
            $arParams[$k] = htmlspecialchars((int)$v, ENT_QUOTES);
        }
    }

    foreach($_REQUEST as $k=>$v){
        $req[$k] = htmlspecialchars($v, ENT_QUOTES);
    }

	if(!empty($req["type"])){
		
	$rsForms = CForm::GetList($by="s_id", $order="desc", array("SID"=>$req["type"]));
	
	if ($arForm = $rsForms->Fetch())
	{	
		$rsFieldList = CFormField::GetList($arForm["ID"], "ALL", $by="s_sort", $order="asc", array("ACTIVE"=>"Y"), $is_filtered);
	   
	    $arFormFields = array();
	    $arError = array();
	    $isRequired = '';
		while ($arFieldList = $rsFieldList->GetNext())
		{
			
			$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);

			$arFieldList["ANSWER"] = $rsAnswer->Fetch();
			
			$arFormFields[] = $arFieldList;
			
			if($arFieldList["REQUIRED"]=="Y"){
				
				if(empty($arUser[$arFieldList["SID"]])){					
					$arError[] = $arFieldList["SID"];              
                    $isRequired = '';
				}				
			}
		}

		$description = "";
		$text = "";
		$captcha = "";
	    if($req["type"]=="credit"){
   
        $description .= '<div class="complexForm__title">Оставить заявку на кредит</div>';
        $description .= '<div class="credit__links">';
        $description .= '<button class="credit__link onAdvanced" id="onCondition">Условия кредитования</button>';
        $description .= '<button class="credit__link onAdvanced" id="onDiff">Чем мы отличаемся?</button>';
        $description .= '<button class="credit__link" id="onCalculator">Калькулятор рассчитать кредит</button>';        
        $description .= '</div>';
		
		$text = '<div class="complexForm__title3">Заказать консультацию кредитного специалиста</div>';
		
		$captcha =
		'<div class="captcha__wrap formfield" id="captcha">
		<div id="recapchaWidget" class="g-recaptcha" data-sitekey="6Lfy-SgUAAAAAHUFP8mCx8lsf5GowSPmeOKaS7b9"></div>
		<div class="error-block trade__error">Вы пропустили капчу</div>
		</div>';
		
        }elseif($req["type"]=="creditadv"){

        $description .= '<div class="complexForm__title">Оставить заявку на кредит</div>';
        $description .= '<div class="credit__links">';
        $description .= '<button class="credit__link onAdvanced" id="onCondition">Условия кредитования</button>';
        $description .= '<button class="credit__link onAdvanced" id="onDiff">Чем мы отличаемся?</button>';
        $description .= '<button class="credit__link" id="onCalculator">Калькулятор рассчитать кредит</button>';
        $description .= '</div>';
		
		
        $text = 
		'<div class="complexForm__title2">Рассчитать <span class="text__color-red">выгодный</span> кредит на этот автомобиль:</div>
        <div class="credit__calc">
		<div class="credit__autos" id="autoid"></div>	
        <span class="credit__price" id="auto__price">Стоимость автомобиля, руб:&nbsp;2&nbsp;500&nbsp;000</span>
		</div>		
        <div class="credit__calc-row">		
		<label class="credit__calc-label1">Первоначальный взнос (укажите сумму в рублях):</label>
		<input class="credit__calc-input1" type="number" name="result__vznos" id="result__vznos" pattern="\d [0-9]" value="" />
		</div>
		<div class="credit__calc-row">		
		<label class="credit__calc-label2">Желаемый срок кредита (лет):</label>
		<input class="credit__calc-input2" type="number" name="result__srok" id="result__srok" pattern="^[ 0-9]+$" value="" />
		</div>
		<div class="credit__name">Результаты расчета:</div>
		<div class="credit__desc" id="result_credit">
		<span class="credit__desc-msg">Чтобы увидеть сумму платежа в месяц заполните первоначальный взнос и срок кредита</span>
		<span class="credit__desc-min" id="result__min">От: 22 000,00</span>
		<span class="credit__desc-max" id="result__max">До: 42 030,00</span>
		<input type="hidden" name="result__min" value="" />
		<input type="hidden" name="result__max" value="" />
		<input type="hidden" name="result__price" value="" />
		<input type="hidden" name="_autolink" value="" />
		</div>
		<div class="complexForm__title3">Закажите <span class="text__color-red">бесплатную</span> консультацию кредитного специалиста</div>
		';
        
		$captcha =
		'<div class="captcha__wrap formfield" id="captcha">
		<div id="recapchaWidget" class="g-recaptcha" data-sitekey="6Lfy-SgUAAAAAHUFP8mCx8lsf5GowSPmeOKaS7b9"></div>
		<div class="error-block trade__error">Вы пропустили капчу</div>
		</div>';
		
        }elseif($req["type"]=="reserve"){
            $description = "Оставьте ваши контакты и мы зарезервируем данный автомобиль для вас.";
        }elseif($req["type"]=="testdrive"){
            $description = "Оставьте ваши контакты и мы запишем вас на тест-драйв данного автомобиля.";
		}

		ob_start();?>
		<div class="complex__header">
			<img class="complex__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
		</div>		
		<form class="complexForm-form" id="complex-form" method="post">	
			<div class="complexForm-group<?=$isRequired?>">
			
			<div class="complexForm__description" id="complexForm__description"><?=$description?></div>
			<div class="complexForm__wrp">
			<?=$text?>
			<?foreach($arFormFields as $arFields){

				$placeholder=!empty($arFields["ANSWER"]["FIELD_PARAM"])?$arFields["ANSWER"]["FIELD_PARAM"]:$arFields["TITLE"];

				if($arFields["ANSWER"]["FIELD_TYPE"]=="text"){
			?>
			<div class="complexForm-input-fullrow field <?if($arFields["REQUIRED"]=="Y"){?>required<?}?>" id="<?=$arFields["SID"]?>">
			
				<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?> value="" />
			<?if($arFields["REQUIRED"]=="Y"){?>
				<div class="error-block">Заполните это поле</div>
			<?}?>
			</div>
			
				<?}elseif($arFields["ANSWER"]["FIELD_TYPE"]=="textarea"){?>
					<div class="complexForm-input-fullrow field <?if($arFields["REQUIRED"]=="Y"){?>required<?}?>" id="<?=$arFields["SID"]?>">

					<textarea name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?>></textarea>
				<?if($arFields["REQUIRED"]=="Y"){?>
					<div class="error-block">Заполните это поле</div>
				<?}?>
			</div>
		
			<?}else{?>			
				<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" value="" />
			  <?}
			}

			foreach($arParams as $k=>$v){?>
				<input type="hidden" name="<?=$k?>" value="<?=$v?>" />
			<?}?>

			<div class="complexForm-input-fullrow edata">
				<input type="checkbox" class="in-text-data data-style onData" name="order_edata" checked="checked">
				<span class="data-text">Подтверждаю согласие на обработку <a href="/personal-data/" target="_blank">персональных&nbsp;данных</a></span>
			</div>
			<?=$captcha?>
			<div class="complexForm-input-fullrow complexForm-input-row-center">
				<button class="btn-submit onSubmit">Отправить</button>
			</div>
			</div>
			</div>
		</form>
        <?
		$html = ob_get_contents();ob_end_clean();

		//$arResult = array("status"=>true,"req"=>$req,"request"=>$_REQUEST,"arParams"=>$arParams,"arForm"=>$arForm,"arFormFields"=>$arFormFields,"html"=>$html);
		$arResult = array("status"=>true,"html"=>$html);
		
	}else{
		
		$arResult = array("status"=>false,"msg"=>"not webform type");
	}
	
	
	
	}else{
		
	$arResult = array("status"=>false,"msg"=>"not type");	
	}
	
}else{
	
	$arResult = array("status"=>false,"msg"=>"not data");
}			

	
echo json_encode($arResult);