<?
function p($arr, $name = '', $die = false) {
	global $USER;
	if ($USER->IsAdmin() /*|| strpos($_SERVER["HTTP_HOST"], ":6448") !== false*/) {
		echo '<pre>'.$name.' ';
		print_r($arr);
		echo '</pre>';
		if($die)
			die();
	}
}

function arraytofile($array, $filename = 0, $arrname, $file = 0, $recursive = false)
{	

	if(is_array($array) || $recursive == true)
	{
		$level = 1;
		if($file == 0)
		{
			$level = 0;
			$file = fopen($filename, "a");
			if(!$file)
			{
				return false;
			}
			fwrite($file, "<" . "?\n\$".$arrname." = ");
		}
		$cnt = count($array);
		$i = 0;
		fwrite($file, "\narray(\n");
		foreach($array as $key => $value)
		{
			if($i++ != 0)
			{
				fwrite($file, ",\n");
			}
			if(is_array($array[$key]))
			{
				fwrite($file, "'$key' => ");
				arraytofile($array[$key], 0,"_array", $file, true);
			}
			else
			{
				$value = addcslashes($value, "'"."\\\\");
				fwrite($file, str_repeat(' ', ($level + 1) * 2) . "'$key' => '$value'");
			}
		}
		fwrite($file, ")");
		if($level == 0)
		{
			fwrite($file, ";\n?".">");
			fclose($file);
			return true;
		}
	}
	else
	{
		if($file == 0)
		{
			$file = fopen($filename, "a");
			if(!$file)
			{
				return false;
			}
			
		}
		//fwrite($file, "<" . "?\n\$".$arrname." = ");
		fwrite($file, "\n\$"."$arrname = ".$array);
		fclose($file);
		return true;
		
	}
}
?>