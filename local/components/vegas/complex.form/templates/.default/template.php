<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS("/local/assets/js/jquery.mask.min.js");?>
<div class="complexForm" id="complex__form" style="display:none">	
	<div class="complexForm-html" id="complexForm__html"></div>
</div>
<div class="complexForm__callback" id="complexForm__callback" style="display:none">
    <div class="complex__header"><img class="complex__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="" /></div>
    <div id="complexForm__callback-html" class="complexForm__callback-html"></div>
</div>