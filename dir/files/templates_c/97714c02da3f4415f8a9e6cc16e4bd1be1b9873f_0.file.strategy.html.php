<?php
/* Smarty version 3.1.30, created on 2017-06-09 15:38:08
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/strategy.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a96b0653502_06041470',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '97714c02da3f4415f8a9e6cc16e4bd1be1b9873f' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/strategy.html',
      1 => 1496812353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a96b0653502_06041470 (Smarty_Internal_Template $_smarty_tpl) {
?>
									<td>
										<select name="strategy[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" class="strategy_id" style="width: 130px;">
										<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['STRATEGY']['ELEMENT'], 'VALUE');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['VALUE']->value) {
?>
											<?php if (empty($_smarty_tpl->tpl_vars['company']->value) || (!empty($_smarty_tpl->tpl_vars['company']->value) && $_smarty_tpl->tpl_vars['VALUE']->value['id'] != -1)) {?><option value="<?php echo $_smarty_tpl->tpl_vars['VALUE']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['strategy'] == $_smarty_tpl->tpl_vars['VALUE']->value['id']) {?> selected<?php }?>><?php if (!empty($_smarty_tpl->tpl_vars['company']->value) && $_smarty_tpl->tpl_vars['VALUE']->value['id'] == 0) {?>Не обновлять ставки<?php } else {
echo $_smarty_tpl->tpl_vars['VALUE']->value['name'];
}?></option><?php }?>
										<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

										</select>
										<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['name'])) {?><br><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['name'];
}?>
									</td>
									<?php if (empty($_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where']) || $_smarty_tpl->tpl_vars['DATA']->value['PARAM']['where'] != 4) {?><td>	
										<nobr>+<input type="text" name="percent[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['percent'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['percent'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['percent'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['percent'];?>
"<?php }?> size="3" class="strategy_percent" title="Наценка в процентах («percent»). С галочкой - от разницы до первого места(«type»).">%<input type="checkbox" name="type[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" class="strategy_type" value="1" <?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['type']) {?> checked<?php }?>><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['type']) {?><strike><?php }?>&#8597;</strike></nobr><br>
										<nobr>&plusmn;<input type="text" name="add[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['add'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['add'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['add'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['add'];?>
"<?php }?> size="3" class="strategy_add" title="Наценка в валюте компании («add»)"> <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?></nobr><br>
										<nobr><<input type="text" name="maximum[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['maximum'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['maximum'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['maximum'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['maximum'];?>
"<?php }?> size="3" class="strategy_maximum<?php if ((empty($_smarty_tpl->tpl_vars['ELEMENT']->value['maximum']) || !round($_smarty_tpl->tpl_vars['ELEMENT']->value['maximum'],2) > 0) && (empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['maximum']) || !round($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['maximum'],2) > 0)) {?> bgcolor_pink<?php }?>" title="Максимально возможная ставка («maximum»)"> <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?></nobr><br>
										<nobr>&ge;<input type="text" name="fixed[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['fixed'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['fixed'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['fixed'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['fixed'];?>
"<?php }?> size="3" class="strategy_fixed" title="Минимальная ставка («fixed»)"> <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?></nobr><br>
										<nobr>&uarr;<input type="text" name="budget[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['budget'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['budget'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['budget'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['budget'];?>
"<?php }?> size="3" class="strategy_budget" title="Ограничение бюджета расходов за день для поиска и РСЯ («company_budget»)"> <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?> 
										<strong><a href="#" id="strategy_param_<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
" class="strategy_param_show dashed" title="Дополнительные параметры"><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['param1'] != 0 || $_smarty_tpl->tpl_vars['ELEMENT']->value['param2'] != 0 || $_smarty_tpl->tpl_vars['ELEMENT']->value['param3'] != 0) {?>-<?php } else { ?>+<?php }?>3</a></strong></nobr>
										<span id="strategy_param_<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
_block" class="strategy_param"<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['param1'] != 0 || $_smarty_tpl->tpl_vars['ELEMENT']->value['param2'] != 0 || $_smarty_tpl->tpl_vars['ELEMENT']->value['param3'] != 0) {?> style="display: inline;"<?php }?>><br><nobr>1<input type="text" name="param1[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['param1'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['param1'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['param1'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['param1'];?>
"<?php }?> size="3" class="strategy_param1" title="Пользовательский параметр «param1»"> ед.</nobr><br>
										<nobr>2<input type="text" name="param2[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['param2'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['param2'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['param2'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['param2'];?>
"<?php }?> size="3" class="strategy_param2" title="Пользовательский параметр «param2»"> ед.</nobr><br>
										<nobr>3<input type="text" name="param3[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['param3'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['param3'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['param3'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['param3'];?>
"<?php }?> size="3" class="strategy_param3" title="Пользовательский параметр «param3»"> ед.</nobr></span>
										
									</td><?php }?>
									<td>
										<nobr>&oplus;<input type="checkbox" name="context[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="1" class="context" <?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['context']) {?> checked<?php }?>> <?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context'])) {?><strike>вкл.</strike><?php } else { ?>вкл.<?php }?></nobr><br>
										<nobr>&#8597;<input type="text" name="context_percent[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['context_percent'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['context_percent'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_percent'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_percent'];?>
"<?php }?> size="2" class="context_percent" title="Охват («context_percent»)">%</nobr><br>
										<nobr><<input type="text" name="context_maximum[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['context_maximum'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['context_maximum'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_maximum'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_maximum'];?>
"<?php }?> size="2" class="context_maximum" title="Максимальная ставка для РСЯ («context_maximum»)"> <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?></nobr><br>
										<nobr>0<input type="text" name="context_fixed[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['context_fixed'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['context_fixed'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_fixed'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_fixed'];?>
"<?php }?> size="2" class="context_fixed" title="Ставка для РСЯ при отсутствии информации об охвате аудитории («context_fixed»)"> <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?></nobr><br>
										<nobr>><input type="text" name="context_minimum[<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['context_minimum'] != 0) {
echo round($_smarty_tpl->tpl_vars['ELEMENT']->value['context_minimum'],2);
}?>"<?php if (!empty($_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_minimum'])) {?> placeholder="<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['STRATEGY']['context_minimum'];?>
"<?php }?> size="2" class="context_minimum" title="Минимальная ставка для РСЯ («context_minimum»)"> <?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['name'])) {
echo $_smarty_tpl->tpl_vars['DATA']->value['CURRENCY']['ID'][$_smarty_tpl->tpl_vars['ELEMENT']->value['currency']]['code'];
} else { ?>y.e.<?php }?></nobr><br>
									</td><?php }
}
