<?php
/* Smarty version 3.1.30, created on 2017-06-07 08:18:22
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/first.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59378c9e6ed151_68527857',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8099556a565b55061ab8fc7ea823affd2444df91' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/first.html',
      1 => 1496812350,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:menu.html' => 1,
    'file:path.html' => 1,
    'file:content.html' => 1,
  ),
),false)) {
function content_59378c9e6ed151_68527857 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/home/a/amazuref/new.rdm-import.ru/public_html/dir/framework/module/template/template/smarty/plugins/modifier.date_format.php';
?>
	<!-- Begin page -->
	<div class="container">
	
	<!-- Modal Dialog -->
	
	<!-- Modal Logout Primary -->
	<div class="md-modal md-fall" id="logout-modal">
		<div class="md-content">
			<h3><strong><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Logout<?php } else { ?>Выход<?php }?></strong> <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Confirmation<?php } else { ?>Подтверждение<?php }?></h3>
			<div>
				<p class="text-center"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Are you sure want to logout from this awesome system?<?php } else { ?>Вы уверены, что хотите выйти?<?php }?></p>
				<p class="text-center">
				<button class="btn btn-danger md-close"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Nope!<?php } else { ?>Нет<?php }?></button>
				<a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/controller/quit/" class="btn btn-success md-close"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Yeah, I'm sure<?php } else { ?>Да<?php }?></a>
				</p>
			</div>
		</div>
	</div>
	
	<!-- Modal Logout Alternatif -->
	<div class="md-modal md-just-me" id="logout-modal-alt">
		<div class="md-content">
			<h3><strong><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Logout<?php } else { ?>Выход<?php }?></strong> <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Confirmation<?php } else { ?>Подтверждение<?php }?></h3>
			<div>
				<p class="text-center"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Are you sure want to logout from this awesome system?<?php } else { ?>Вы уверены, что хотите выйти?<?php }?></p>
				<p class="text-center">
				<button class="btn btn-danger md-close"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Nope!<?php } else { ?>Нет<?php }?></button>
				<a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/controller/quit/" class="btn btn-success md-close"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Yeah, I'm sure<?php } else { ?>Да<?php }?></a>
				</p>
			</div>
		</div>
	</div>
	
	<!-- Modal save -->
	<div class="md-modal" id="save-modal">
		<div class="md-content">
			<h3><strong class="md-title"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Save<?php } else { ?>Сохранение<?php }?></strong></h3>
			<div>
				<p class="text-center md-message"></p>
				<p class="text-center">
				<button class="btn btn-success md-close"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Ok<?php } else { ?>Ок<?php }?></button>
				</p>
			</div>
		</div>
	</div>
	
	<!-- Modal Task Progress -->	
	<div class="md-modal md-slide-stick-top" id="task-progress">
		<div class="md-content">
			<h3><strong>Task Progress</strong> Information</h3>
			<div>
				<p>CLEANING BUGS</p>
				<div class="progress progress-xs for-modal">
				  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
					<span class="sr-only">80&#37; Complete</span>
				  </div>
				</div>
				<p>POSTING SOME STUFF</p>
				<div class="progress progress-xs for-modal">
				  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
					<span class="sr-only">65&#37; Complete</span>
				  </div>
				</div>
				<p>BACKUP DATA FROM SERVER</p>
				<div class="progress progress-xs for-modal">
				  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
					<span class="sr-only">95&#37; Complete</span>
				  </div>
				</div>
				<p>RE-DESIGNING WEB APPLICATION</p>
				<div class="progress progress-xs for-modal">
				  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					<span class="sr-only">100&#37; Complete</span>
				  </div>
				</div>
				<p class="text-center">
				<button class="btn btn-danger btn-sm md-close">Close</button>
				</p>
			</div>
		</div>
	</div>
		
		
		<?php $_smarty_tpl->_subTemplateRender("file:menu.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
		
		
		<!-- Start right content -->
        <div class="right content-page">
            <div class="header content rows-content-header">
			
			<!-- Button mobile view to collapse sidebar menu -->
			<button class="button-menu-mobile show-sidebar">
				<i class="fa fa-bars"></i>
			</button>
			
             <div class="navbar navbar-default" role="navigation">
				  <div class="container">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<i class="fa fa-angle-double-down"></i>
					  </button>
					</div>
					<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/config" title="Сборка от <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['release'],'%d.%m.%Y');?>
"><i class="fa fa-cog"> Версия <?php echo $_smarty_tpl->tpl_vars['DATA']->value['CONFIG']['version'];?>
</i></a></li>
					</ul>
					<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['timer'])) {?><ul class="nav navbar-nav">
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/config" title="Текущее время"><i class="fa fa-clock-o"> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['timer'],'<strong>%H:%M</strong> %d.%m.%Y');?>
 <small><?php echo $_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['week_name'];?>
</small></i></a></li>
					</ul><?php }?>
					<?php if (!empty($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['time'])) {?><ul class="nav navbar-nav">
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/config" title="Время последнего обновления ставок"><i class="fa fa-legal"> <font class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['expire']) {?> color_red<?php } else { ?>color_yellow<?php }?>"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['time'],'<strong>%H:%M</strong> %d.%m.%Y');?>
</font></i></a></li>
					</ul><?php }?>
					<?php if (!empty($_smarty_tpl->tpl_vars['USER']->value['group']) && $_smarty_tpl->tpl_vars['USER']->value['group'] == 1 && !empty($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['count'])) {?><ul class="nav navbar-nav">
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/config" title="Процент обновляемых фраз из запланированных (Кол-во обновляемых фраз из общего числа фраз)"><i class="fa fa-search-plus"> <font class="<?php if ($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['progress'] != 100) {?>color_red<?php } else { ?>color_yellow<?php }?>"><?php echo $_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['progress'];?>
%</font> (<font class="color_yellow"><?php echo $_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['auction'];?>
</font> из <?php echo $_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['count'];?>
)</i></a></li>
					</ul><?php }?>
					<?php if (!empty($_smarty_tpl->tpl_vars['USER']->value['group']) && $_smarty_tpl->tpl_vars['USER']->value['group'] == 1 && !empty($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['interval'])) {?><ul class="nav navbar-nav">
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/config" title="Скорость обновления ставок"><i class="fa fa-tachometer"> <font class="color_yellow"><?php echo $_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['interval'];?>
</font> мин.</i></a></li>
					</ul><?php }?>
					<?php if (!empty($_smarty_tpl->tpl_vars['USER']->value['group']) && $_smarty_tpl->tpl_vars['USER']->value['group'] == 1 && !empty($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['report'])) {?><ul class="nav navbar-nav">
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/config" title="Состояние получения статистики по конверсиям"><i class="fa fa-signal"> <font class=""><?php echo round($_smarty_tpl->tpl_vars['DATA']->value['COUNT']['PHRASE']['report']);?>
</font> %</i></a></li>
					</ul><?php }?>
					
					  
					  
					  <ul class="nav navbar-nav navbar-right top-navbar">
						
						<li class="dropdown">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Howdy<?php } else { ?>Здравствуйте<?php }?>, <strong><?php if (!empty($_smarty_tpl->tpl_vars['USER']->value['name'])) {
echo $_smarty_tpl->tpl_vars['USER']->value['name'];
} else {
echo $_smarty_tpl->tpl_vars['USER']->value['login'];
}?></strong> <i class="fa fa-chevron-down i-xs"></i></a>
						  <ul class="dropdown-menu animated half flipInX">
							<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/edit/param/id/<?php echo $_smarty_tpl->tpl_vars['USER']->value['id'];?>
/"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>My Profile<?php } else { ?>Профиль<?php }?></a></li>
							
							<li><a class="md-trigger" data-modal="logout-modal"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Logout<?php } else { ?>Выход<?php }?></a></li>
						  </ul>
						</li>
					  </ul>
					</div><!--/.nav-collapse -->
				  </div>
				</div>
            </div>
			
			
			
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div id="scrolled" class="body content rows scroll-x scroll-y">
			
				<?php $_smarty_tpl->_subTemplateRender("file:path.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

				
				<?php $_smarty_tpl->_subTemplateRender("file:content.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			
				<footer>
				&copy; 2014-<?php echo date('Y');?>
 <a href="http://direct-automate.ru" target="_blank" title="Директ-автомат.рф"><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>«Cloud-automate.ru»<?php } else { ?>«Директ-автомат.рф»<?php }?></a>
				</footer>
			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->
			
			
			
			
        </div>
		<!-- End right content -->
		
		
		
		
		
		<!-- the overlay modal element -->
		<div class="md-overlay"></div>
		<!-- End of eoverlay modal -->
		
		
		
	</div>
	<!-- End of page --><?php }
}
