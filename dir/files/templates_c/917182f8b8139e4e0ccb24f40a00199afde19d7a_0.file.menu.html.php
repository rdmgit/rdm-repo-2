<?php
/* Smarty version 3.1.30, created on 2017-06-07 08:18:22
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/menu.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59378c9e7b23d3_26993628',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '917182f8b8139e4e0ccb24f40a00199afde19d7a' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/menu.html',
      1 => 1496812351,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59378c9e7b23d3_26993628 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Start sidebar menu -->
		<div class="left side-menu">
            <div class="header sidebar rows">
				<div class="logo animated bounceIn">
					<h1><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/"><img src="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/files/templates/assets/img/logo.png" alt="Logo"> <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Admin<?php } else { ?>Директ автомат<?php }?></a></h1>
				</div>
            </div>
            <div class="body rows scroll-y animated fadeInLeftBig">
                <div class="sidebar-inner slimscroller">
					
				
					
				<div id="sidebar-menu">
					<ul>
						<li<?php if (empty($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller']) || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/index" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/company" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/banner" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/phrase" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/auction") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/"><i class="fa fa-legal"></i> <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Dashboard<?php } else { ?>Ставки<?php }?></a></li>
						<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><li<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "user/user/get" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "user/user/edit") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/account"><i class="fa fa-user"></i> Аккаунты</a></li><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1 || $_smarty_tpl->tpl_vars['USER']->value['group'] == 3) {?><li<?php if (($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "user/user/get" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "user/user/edit") && ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['argument'] == "param/group/admin")) {?> class="active"<?php }?>><a href=""><i class="fa fa-users"></i><i class="fa fa-angle-double-down i-right"></i> Пользователи</a>
							<ul>
								<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/admin"><i class="fa fa-angle-right"></i> Администраторы</a></li>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/manager"><i class="fa fa-angle-right"></i> Представители</a></li><?php }?>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/user/get/param/group/user"><i class="fa fa-angle-right"></i> Субклиенты</a></li>
							</ul>
						</li><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><li<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/strategy/get") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/strategy/get/"><i class="fa fa-signal"></i> Стратегии</a></li><?php }?>
						<li<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/index" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/analize" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/clicks" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/price" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/index" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/statistic/count") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/index/"><i class="fa fa-bar-chart-o"></i><i class="fa fa-angle-double-down i-right"></i> Статистика</a>
							<ul>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/index/"><i class="fa fa-angle-right"></i> Статистика</a></li>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/price/"><i class="fa fa-angle-right"></i> Лог ставок</a></li>
								<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/statistic/count/"><i class="fa fa-angle-right"></i> Объем рекламы</a></li><?php }?>
							</ul>
						</li>
						<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><li<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/cron" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/cron_edit") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/cron/"><i class="fa fa-calendar"></i> Задания</a></li><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><li<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/config") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/config/"><i class="fa fa-wrench"></i> Настройки</a></li><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['USER']->value['group'] == 1) {?><li<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/error/index" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/error/table" || $_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/error/config") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/index/"><i class="fa fa-warning"></i><i class="fa fa-angle-double-down i-right"></i> Диагностика</a>
							<ul>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/index/"><i class="fa fa-angle-right"></i> Ошибки</a></li>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/table/"><i class="fa fa-angle-right"></i> Таблицы</a></li>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/config/"><i class="fa fa-angle-right"></i> Конфигурация</a></li>
								<li><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/error/update/"><i class="fa fa-angle-right"></i> Обновление</a></li>
							</ul>
						</li><?php }?>
						<li<?php if ($_smarty_tpl->tpl_vars['FRAMEWORK']->value['controller'] == "direct/index/article") {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/index/article/documentation/"><i class="fa fa-picture-o"></i> Документация</a></li>
						
					</ul>
					<div class="clear"></div>
				</div>
				</div>
            </div>
            
        </div>
		<!-- End of sidebar menu --><?php }
}
