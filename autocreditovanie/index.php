<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?><script type="text/javascript" src="/bitrix/templates/Avtocreditovanie/chili-1.7.pack.js
"></script> <script type="text/javascript" src="/bitrix/templates/Avtocreditovanie/jquery.easing.js
"></script> <script type="text/javascript" src="/bitrix/templates/Avtocreditovanie/jquery.dimensions.js"></script> <script type="text/javascript" src="/bitrix/templates/Avtocreditovanie/jquery.accordion.js"></script>
<div class="bg_moz" style="background: url(/local/assets/img/2bg.png);margin-top: -40px; background-size:cover; color:white; min-height:585px;">
	<div class="offers-list__item">
		<div class="top-banner__info-title">
			<div class="b-title">
				<h1>Автокредитование.</h1>
			</div>
		</div>
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text block-ccc">
					<p>
						 Компания РДМ-Импорт использует свой опыт работы с банками, накопленный с 2007 года, и свои партнерские связи, поэтому получает наиболее выгодные кредитные программы, чем при самостоятельном обращении в банк, а также уберегает заемщиков от скрытых комиссий и платежей. <br>
					</p>
					<p>
						 90% обратившихся получают положительное кредитное решение. <br>
					</p>
					<p>
						 Помощь в получении кредита мы оказываем жителям любого региона РФ, если нет возможности приехать в Новосибирск, то одобрение можно получить дистанционно. <br>
					</p>
					<p>
						 По кредитному решению, полученному в нашей компании, вы можете купить любой автомобиль и не только в нашем автосалоне. <br>
					</p>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more">
 <br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
					<p>
						 <!--img src="../local/assets/img/proto_img.png" style="width:80%"-->
					</p>
					<div class="offers-list-links">
						<div class="offers-list-links__item _read-more">
 <br>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <!-- asdasdasd -->
		<div class="offers-list__item-info">
			<div class="offers-list__item-text">
			</div>
		</div>
	</div>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Преимущества автокредитования у нас:<br>
 </h2>
		</div>
	</div>
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<p class="doc">
					 Без предоплат!
				</p>
				<p class="doc">
					 Ответственность застрахована!
				</p>
			</div>
		</div>
	</div>
	 <!-- asdasdasd -->
	<div class="offers-list__item-info">
		<div class="offers-list__item-text">
			<p class="doc">
				 Без предоплат!
			</p>
			<p class="doc">
				 Ответственность застрахована!
			</p>
		</div>
	</div>
</div>
 <!--      Здесь пример      -->
<div class="offers-list__item vopros-block">
	<div class="top-banner__info-title">
		<div class="b-title">
			 <!--h2>Страхование в Каско</h2-->
		</div>
	</div>
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc ">
			<h2>Самые популярные вопросы об автокредите</h2>
			<div class="offers-list__item-text voprosi accordeon">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:support.faq.element.list",
	"mytpl",
	Array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "14",
		"IBLOCK_TYPE" => "services",
		"PATH_TO_USER" => "",
		"RATING_TYPE" => "",
		"SECTION_ID" => "1",
		"SHOW_RATING" => ""
	)
);?>
				 <!--a href="#">Сколько переплата за 5 лет?</a><br>
 <a href="#">Посоветуйте самый выгодный кредит</a><br>
 <a href="#">Чем отличаются банки</a><br>
 <a href="#">Включена ли в кредит страховка</a><br>
 <a href="#">Какие акции сейчас есть?</a-->
			</div>
		</div>
	</div>
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc" style="margin-top: 15px;">
			 <!--h2>Потребительское кредитование</h2-->
			<div class="offers-list__item-text">
				<div class="calculator">
					 Кредитный калькулятор
				</div>
				<div class="calculator">
					 Подбор банка
				</div>
				<div class="offers-list__item-actions">
					<div class="offers-list__item-action _calc">
						<div href="#" class="offers-list__item-action-link">
							 Оставить заявку
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="offers-list__item">
	<h2>Доступно о кредитах</h2>
	<div class="offers-list__item-desc">
		<div class="offers-list__item-text">
			 Lorem..
		</div>
	</div>
	<div class="list_video">
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/4GN1SmLZtVY" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Отзыв Стаса из Якутии
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/fLZ5u3YBv_Y" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Отзыв Юрия о гарантийном случае
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/FgoRbJ8xxoc" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Отзыв Владимира из Ачинска
			</div>
		</div>
		<div class="offers-list__item-actions">
			<div class="offers-list__item-action _calc">
				<div href="#" class="offers-list__item-action-link">
					 Оставить заявку
				</div>
			</div>
		</div>
	</div>
</div>
 <!--                      BANKS  -->
<div class="offers-list__item">
	<h2>Наши банки-партрнеры</h2>
	<div class="offers-list__item-desc">
		<div class="offers-list__item-text">
			 Lorem..
		</div>
	</div>
	<div class="list_bank">
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="bank_item">
 <img src="../local/assets/img/sber.jpg">
		</div>
		<div class="offers-list__item-actions">
			<div class="offers-list__item-action _calc">
				<div href="#" class="offers-list__item-action-link">
					 Оставить заявку
				</div>
			</div>
		</div>
	</div>
</div>

<div class="black_list">
	<div class="form_back">
 <img src="/local/assets/img/logosm.png"><br>
		 Спасибо за то, что выбрали нас! Оставьте ваши контакты и мы свяжемся с вами в ближайшее время.
	
		 <?$APPLICATION->IncludeComponent(
	"api:feedbackex",
	"uikit",
	Array(
		"API_FEX_FORM_ID" => "FORM9",
		"BCC" => "",
		"COLOR" => "default",
		"COMPONENT_TEMPLATE" => "uikit",
		"DATETIME" => "",
		"DIR_URL" => "",
		"DISABLE_SEND_MAIL" => "N",
		"DISPLAY_FIELDS" => array(0=>"TITLE",1=>"PHONE",2=>"MESSAGE",),
		"EMAIL_ERROR_MESS" => "Указанный E-mail некорректен",
		"EMAIL_TO" => "p.sazonov@rdm-import.ru",
		"FIELD_ERROR_MESS" => "#FIELD_NAME# обязательное",
		"FIELD_NAME_POSITION" => "stacked",
		"FIELD_SIZE" => "default",
		"FORM_AUTOCOMPLETE" => "Y",
		"FORM_CLASS" => "",
		"FORM_FIELD_WIDTH" => "",
		"FORM_LABEL_TEXT_ALIGN" => "left",
		"FORM_LABEL_WIDTH" => "150px",
		"FORM_SUBMIT_CLASS" => "uk-button uk-width-1-1",
		"FORM_SUBMIT_STYLE" => "",
		"FORM_SUBMIT_VALUE" => "Отправить",
		"FORM_TEXTAREA_ROWS" => "5",
		"FORM_TITLE" => "Обратная связь",
		"FORM_TITLE_LEVEL" => "3",
		"FORM_WIDTH" => "",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "Y",
		"MAIL_SEND_USER" => "N",
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы обратной связи",
		"MAIL_SUBJECT_USER" => "#SITE_NAME#: Копия сообщения из формы обратной связи",
		"MODAL_BTN_CLASS" => "api_button",
		"MODAL_BTN_ID" => "shw",
		"MODAL_BTN_SPAN_CLASS" => "api_icon",
		"MODAL_BTN_TEXT" => "Обратная связь",
		"MODAL_FOOTER_TEXT" => "",
		"MODAL_HEADER_TEXT" => "Обратная связь",
		"MODAL_ID" => "#API_FEX_MODAL_FORM9",
		"OK_TEXT" => "Спасибо!",
		"OK_TEXT_AFTER" => "Мы рассмотрим сообщение и обязательно свяжемся с Вами.<br>Пожалуйста, дождитесь ответа.",
		"PAGE_TITLE" => "",
		"PAGE_URL" => "",
		"REPLACE_FIELD_FROM" => "Y",
		"REQUIRED_FIELDS" => array(0=>"TITLE",1=>"PHONE",),
		"SCROLL_SPEED" => "1000",
		"THEME" => "gradient",
		"TITLE_DISPLAY" => "N",
		"USE_AUTOSIZE" => "Y",
		"USE_JQUERY" => "Y",
		"USE_MODAL" => "N",
		"USE_PLACEHOLDER" => "N",
		"USE_SCROLL" => "Y",
		"WRITE_MESS_DIV_STYLE" => "padding:10px;border-bottom:1px dashed #dadada;",
		"WRITE_MESS_DIV_STYLE_NAME" => "font-weight:bold;",
		"WRITE_MESS_DIV_STYLE_VALUE" => "",
		"WRITE_MESS_FILDES_TABLE" => "N",
		"WRITE_MESS_TABLE_STYLE" => "border-collapse: collapse; border-spacing: 0;",
		"WRITE_MESS_TABLE_STYLE_NAME" => "max-width: 200px; color: #848484; vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;",
		"WRITE_MESS_TABLE_STYLE_VALUE" => "vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;"
	)
);?>
	</div>
</div>
<script>
$(".accordeon .accordeon_slide").hide().prev().click(function() {
	$(this).parents(".accordeon").find(".accordeon_slide").not(this).slideUp().prev().removeClass("active");
	$(this).next().not(":visible").slideDown().prev().addClass("active");
});
</script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
