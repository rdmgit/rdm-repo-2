<?php

/** settings & init */

$host = 'localhost';
$database = 'amazuref_old';
$user = 'amazuref_old';
$password = 'amazuref_old';

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!CModule::IncludeModule("iblock")) {
    echo "Ошибка iblock\n";
    die;
}

$arSelect = Array();
$arFilter = Array("IBLOCK_ID"=>5, "STATE"=>111, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
//$list = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>4000, "iNumPage"=>$_REQUEST["page"]), $arSelect);
$list = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

$count = 0 ;

while($element = $list->GetNextElement()){ 
	$arProps = $element->GetProperties();
	$arFields = $element->GetFields();
	
	if(substr($arFields["XML_ID"], 0,1) !== "0" && $arProps["STATE"]["VALUE_ENUM_ID"] !== "112"){
		$count++;
		
		if(!CIBlockElement::SetPropertyValueCode($arFields["ID"], "STATE" , 112 )){	
			echo '<pre>';
			var_dump('error');
			echo '</pre>';
			die();
		}
	}
}

echo '<pre>';
var_dump($count);
echo '</pre>';
die();