<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("not_show_page_title", "Y");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetTitle("РДМ-Импорт");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top-main",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "topmain",
		"USE_EXT" => "N"
	)
);?>
 <?$APPLICATION->IncludeComponent(
	"ycaweb:ads.filter",
	".default",
	Array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"FILTER_NAME" => "arrAdsFilter",
		"SAVE_IN_SESSION" => "N"
	)
);
$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "intelsib",

    array(
        "PARENT_SECTION_CODE" => $parentSectionCode,
        "SECTION_CODE" => $sectionCode,
        "IBLOCK_ID" => '5',
        "LEVEL" => $level,
        'CHAIN_FROM_META' => false,
    ),
    false
);
?>
<div class="page__main-wrapper">
<p class="page__title">
	 Продаем автомобили <span>цивилизованным</span> способом
</p>
<div class="row">
	<div class="item">
		<img src="/local/assets/img/1.jpg">
		<p class="description">Предлагаем только самые популярные, востребованные и полностью проверенные нами автомобили на техничесткое состояние и юридическую чистоту</p>
	</div>
	<div class="item">
		<img src="/local/assets/img/2.jpg">
		<p class="description">Общаемся с каждым пришедшим или позвонившим к нам человеком гостеприимно и с пониманием</p>
	</div>
	<div class="item">
		<img src="/local/assets/img/3.jpg">
		<p class="description">Оформляем сделку в полном соответствии с законом Российской Федерации</p>
	</div>
</div>
<div class="row">
	<div class="item">
		<img src="/local/assets/img/4.jpg">
		<p class="description">Отвечаем деньгами и репутацией за все продаваемые нами машины в полном объеме, предоставляя на каждую сделку сертификат гарантии качества</p>
	</div>
	<div class="item">
		<img src="/local/assets/img/5.jpg">
		<p class="description">Добиваемся, чтобы вместе с высоким качеством цена оставалась рыночной</p>
	</div>
	<div class="item">
		<img src="/local/assets/img/6.jpg">
		<p class="description">Предоставляем клиентам полную и достоверную информацию о автомобиле до покупки</p>
	</div>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>