<?php if(!defined("AJX_PRCS") || AJX_PRCS !== true) die();

/**
 * Description of kernel
 *
 * @author r.zaycev
 */
class kernel extends \Ycaweb\ajaxModules{
	public function getNewSessid(){
		$newSessId = bitrix_sessid();
		
		$this->moduleResult(array(
			'newSessId' => $newSessId
		));
	}

	public function getCaptchaCode(){
		global $APPLICATION;
		$this->moduleResult(array(
			'code' => $APPLICATION->CaptchaGetCode()
		));
	}

	public function auth(){
		global $USER;
		if(!is_object($USER)){
			$USER = new CUser;
		}

		$this->_checkRequired(array(
			"EMAIL",
			"PASSWORD",
		));

		$arAuthResult = $USER->Login($_POST['EMAIL'], $_POST['PASSWORD'], 'Y', 'Y');
		if($arAuthResult === true){
			$this->moduleResult(array(
				'success' => true
			));
		}else{
			$this->returnError(0, array(
				'type'		=> 'auth',
				'message'	=> '<i class="icon icon-info"></i> Неверный логин или пароль!'
			));
		}
	}

	public function signup(){
		global $USER, $DB;
		if(!is_object($USER)){
			$USER = new CUser;
		}

		$arReg = array(
			"FIRST_NAME" => 'Поле "Имя" не может быть пустым',
			"SECOND_NAME",
			"LAST_NAME",
			"PHONE",
			"EMAIL",
			"PASSWORD",
			"RETYPE_PASSWORD"
		);
		$this->_checkRequired($arReg);

		if(filter_var($_POST['EMAIL'], FILTER_VALIDATE_EMAIL) === false){
			$this->returnError(0,array(
				'type'		=> 'field',
				'field'		=> 'EMAIL',
				'message'	=> 'Пожалуйста, введите действительный адрес электронной почты'
			));
		}

		$rsUser = CUser::GetByLogin($DB->ForSql($_POST['EMAIL']));
		if($rsUser->Fetch()){
			$this->returnError(0,array(
				'type'		=> 'field',
				'field'		=> 'EMAIL',
				'message'	=> 'Пользователь с таким адресом уже существует'
			));
		}

		if(strlen(trim($_POST['PASSWORD'])) < 6){
			$this->returnError(0,array(
				'type'		=> 'field',
				'field'		=> 'PASSWORD',
				'message'	=> 'Пожалуйста, введите 6 и более символов.'
			));
		}

		if(trim($_POST['PASSWORD']) !== trim($_POST['RETYPE_PASSWORD'])){
			$this->returnError(0,array(
				'type'		=> 'field',
				'field'		=> 'RETYPE_PASSWORD',
				'message'	=> 'Пароли не совпадают'
			));
		}

		$strEmail = $DB->ForSql(htmlspecialchars(strip_tags($_POST['EMAIL'])));

		$arFields = Array(
			"NAME"				=> $DB->ForSql(htmlspecialchars(strip_tags($_POST['FIRST_NAME']))),
			"SECOND_NAME"		=> $DB->ForSql(htmlspecialchars(strip_tags($_POST['SECOND_NAME']))),
			"LAST_NAME"			=> $DB->ForSql(htmlspecialchars(strip_tags($_POST['LAST_NAME']))),
			"EMAIL"				=> $strEmail,
			"LOGIN"				=> $strEmail,
			"LID"				=> "s1",
			"ACTIVE"			=> "Y",
			"GROUP_ID"			=> array(5),
			"PASSWORD"			=> $DB->ForSql(trim($_POST['PASSWORD'])),
			"CONFIRM_PASSWORD"	=> $DB->ForSql(trim($_POST['RETYPE_PASSWORD']))
		);

		$userId = $USER->Add($arFields);
		if(intval($userId) > 0){
			$USER->Authorize($userId);
			$this->moduleResult(array(
				'success' => true
			));
		}else{
			$this->returnError(0, array(
				'type' => 'signup',
				'message' => strip_tags($USER->LAST_ERROR)
			));
		}
	}

	public function forgot(){
		global $USER, $DB;
		if(!is_object($USER)){
			$USER = new CUser;
		}

		$this->_checkRequired(array('EMAIL'));

		if(filter_var($_POST['EMAIL'], FILTER_VALIDATE_EMAIL) === false){
			$this->returnError(0,array(
				'type'		=> 'field',
				'field'		=> 'EMAIL',
				'message'	=> 'Пожалуйста, введите действительный адрес электронной почты'
			));
		}

		$strEmail = $DB->ForSql($_POST['EMAIL']);

		$arResult = $USER->SendPassword($strEmail, $strEmail);
		if($arResult["TYPE"] == "OK"){
			$this->moduleResult(array(
				'success' => true,
				'message' => 'Письмо с инструкциями по смене пароля были отправлены на адрес вашей электронной почты.'
			));
		}else{
			$this->returnError(0, array(
				'type'		=> 'field',
				'field'		=> 'EMAIL',
				'message'	=> 'Пользователь не найден'
			));
		}
	}

	/**
	 * @param array  $arFields
	 * @param string $defaultMessage
	 * @private
	 */
	private function _checkRequired($arFields, $defaultMessage = "Это поле не может быть пустым"){
		foreach($arFields as $k => $v){
			if(is_numeric($k)){
				$field = $v;
				$message = $defaultMessage;
			}else{
				$field = $k;
				$message = $v;
			}

			if(!isset($_POST[$field]) || empty($_POST[$field])){
				$this->returnError(0, array(
					'type'		=> 'field',
					'field'		=> $field,
					'message'	=> $message
				));
			}
		}
	}
}
