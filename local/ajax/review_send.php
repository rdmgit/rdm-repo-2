<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if ($_REQUEST['text'] && /*$_REQUEST['phone'] &&*/ $_REQUEST['id_el'] && $_REQUEST['name'] && !$_REQUEST['name2']) {
    if (Bitrix\Main\Loader::includeModule('iblock')) {
    
        $el = new CIBlockElement;
			
        $PROP = array();
        $PROP['NAME'] = $_REQUEST['name']; 		
        $PROP['ID_ELEMENT'] = $_REQUEST['id_el']; 		
        $PROP['AUTHOR_IP'] = $_SERVER['REMOTE_ADDR']; 		
        		
		$res = CIBlockElement::GetByID($_REQUEST['id_el']);
        if($arEl = $res->GetNext())
		{
			$PROP['NAME_TOVAR'] = $arEl['NAME']; 
			$PROP['DETAIL_URL'] =  'https://'.SITE_SERVER_NAME.$arEl['DETAIL_PAGE_URL']; 
		}	
        $arSect = CIBlockSection::GetList(array(), array('CODE'=>'kommentarii', 'IBLOCK_ID'=>REVIEWS_IBLOCK_ID), false, array('IBLOCK_ID', 'ID') )->fetch();
        $arLoadReviewArray = Array(
            "IBLOCK_SECTION_ID" => $arSect['ID'],  
            "IBLOCK_ID"      => REVIEWS_IBLOCK_ID,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => 'Предоложение от '.strip_tags(trim($_REQUEST['name'])),
            "ACTIVE"         => "N",         
            "PREVIEW_TEXT"   => strip_tags(trim($_REQUEST['text'])),
        );

        if($el->Add($arLoadReviewArray))
            echo json_encode(Array('STATUS' => 'OK'));
        else
            echo json_encode(Array('STATUS' => 'ERROR','MESSAGE' => 'Не удалось добавить предложение, попробуйте ещё раз. '."Error: ".$el->LAST_ERROR));
    }
}
elseif($_REQUEST['name2']){
    echo json_encode(Array('STATUS' => 'BOAT'));
}
else{
    $error_text = '';
    if( !$_REQUEST['text'])
        $arr_error[] = '"Ваше предложение"';
    if( !$_REQUEST['name'])
        $arr_error[] = '"Имя"';

    $cnt_ar = count($arr_error);
    for($i=0; $i < $cnt_ar; $i++){
        if($i>0) $error_text .= ', ';
        $error_text .= $arr_error[$i];
    }
    if($error_text != '')
        echo json_encode(Array('STATUS' => 'ERROR', 'MESSAGE' => 'Необходимо заполнить следующие поля: '.$error_text));
}