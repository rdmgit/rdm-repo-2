<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<section <?if($arParams['BLOCK_ID']):?>id="<?=$arParams['BLOCK_ID']?>" <?endif;?>class="l-landing-section l-page-wrapper<?if($arParams['BLOCK_CLASSNAME']):?> <?=$arParams['BLOCK_CLASSNAME']?><?endif;?>">
	<div class="l-page-wrapper__inner">
		<?
			$component->IncludeTypeComponentTemplate($arParams['BLOCK_TYPE']);
		?>
	</div>
</section>