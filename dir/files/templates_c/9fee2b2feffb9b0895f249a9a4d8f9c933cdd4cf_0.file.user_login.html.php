<?php
/* Smarty version 3.1.30, created on 2017-06-07 08:18:18
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/user_login.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59378c9ab48966_00501034',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9fee2b2feffb9b0895f249a9a4d8f9c933cdd4cf' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/user_login.html',
      1 => 1496812354,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59378c9ab48966_00501034 (Smarty_Internal_Template $_smarty_tpl) {
if ((empty($_smarty_tpl->tpl_vars['USER']->value))) {?>
	<!-- Begin page -->
	<div class="container">
		<div class="full-content-center animated fadeInDownBig">
			<a href=""><img src="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/files/templates/assets/img/logo-login.png" class="logo-login img-circle" alt="Logo"></a>
			<div class="login-wrap">
				<div class="box-info">
				<h2 class="text-center"><strong><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Login<?php } else { ?>Авторизация<?php }?></strong> <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>form<?php } else {
}?></h2>
				
					<form role="form" class="login" action="/user/controller/authorize/">
						<div class="form-group login-input">
						<i class="fa fa-sign-in overlay"></i>
						<input type="text" name="login" value="<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru') {?>admin<?php }?>" class="form-control text-input" placeholder="<?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Username<?php } else { ?>Логин<?php }?>">
						</div>
						<div class="form-group login-input">
						<i class="fa fa-key overlay"></i>
						<input type="password" name="password" value="<?php if ($_SERVER['HTTP_HOST'] == 'demo.direct-automate.ru') {?>admin<?php }?>" class="form-control text-input" placeholder="<?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Password<?php } else { ?>Пароль<?php }?>">
						</div>
						
						
						<div class="row">
							<div class="col-sm-6">
							<button type="submit" class="btn btn-success btn-block"><i class="fa fa-unlock"></i> <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Login<?php } else { ?>Войти<?php }?></button>
							</div>
							
						</div>
					</form>
					
				</div>
				<p class="text-center"><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/user/controller/remember/"><i class="fa fa-lock"></i> <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Forgot password?<?php } else { ?>Забыли пароль?<?php }?></a></p>
				<br><br><br><br><br><p class="text-center"><a href="http://direct-automate.ru" target="_blank" title="Директ-автомат.рф">&copy; <?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Direct-automate.ru<?php } else { ?>«Директ-автомат.рф»<?php }?></a></p>
			</div>
			
		</div>
	</div>
	<!-- End of page -->
	<?php } else { ?>
				<center>
                  <p><?php if ($_smarty_tpl->tpl_vars['language']->value == 'en') {?>Logged<?php } else { ?>Вы авторизованы!<?php }?></p>
				 </center>
	<?php }
}
}
