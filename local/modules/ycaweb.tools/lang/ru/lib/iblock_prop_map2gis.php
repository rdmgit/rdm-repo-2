<?php
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS'] = 'Привязка к карте 2GIS';
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_INSTR_VALUE'] = 'Выберите точку на карте двойным щелчком мыши.';
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_INSTR'] = 'Перетащите точку по карте или выберите новое положение двойным щелчком мыши.';
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_GOTO_POINT'] = 'Перейти к точке';
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_REMOVE_POINT'] = 'Очистить значение';
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_SEARCH'] = 'Поиск по адресу';

$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_SETTINGS_WARNING'] = 'Дополнительные настройки карты применимы только в публичной части. Размеры карты можно задавать как в % так и px.';
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_SETTINGS_WIDTH'] = 'Ширина карты';
$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_SETTINGS_HEIGHT'] = 'Высота карты';

$MESS['IBLOCK_PROP_YCAWEB_MAP2GIS_MAP_LOADING'] = 'Загрузка карты ...';