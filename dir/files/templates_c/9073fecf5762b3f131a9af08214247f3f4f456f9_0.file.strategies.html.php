<?php
/* Smarty version 3.1.30, created on 2017-06-09 15:55:56
  from "/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/strategies.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_593a9adce787e7_95913516',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9073fecf5762b3f131a9af08214247f3f4f456f9' => 
    array (
      0 => '/home/a/amazuref/new.rdm-import.ru/public_html/dir/files/templates/strategies.html',
      1 => 1496812353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_593a9adce787e7_95913516 (Smarty_Internal_Template $_smarty_tpl) {
?>
				<!-- Your awesome content goes here -->				
				
				<div class="box-info full">
					<h2><strong>Стратегии</strong></h2>
						
						<div class="data-table-toolbar">
							<div class="row">
								<div class="col-md-4">
									
								</div>
								<div class="col-md-8">
									<div class="toolbar-btn-action">
										<a class="btn btn-success" href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/strategy/get/param/id/0/"><i class="fa fa-plus-circle"></i> Добавить стратегию</a>
									</div>
								</div>
							</div>
						</div>
						
					<div class="table-responsive"><form class="save" action="">
						<table data-sortable class="table table-hover table-striped">
							<thead>
								<tr>
									<th>ID</th>
									
									<th>Наименование</th>
									<th data-sortable="false">Редактировать</th>									
								</tr>
							</thead>
							
							<tbody>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['DATA']->value['STRATEGY']['ELEMENT'], 'ELEMENT');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ELEMENT']->value) {
?>
								<tr>
									<td><?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
</td>
									
									<td data-toggle="tooltip" title="<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['status'] != 2) {?>Редактировать<?php } else { ?>Предустановленная стратегия<?php }?>"><strong><?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['status'] != 2) {?><a href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/strategy/get/param/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/"><?php }
echo $_smarty_tpl->tpl_vars['ELEMENT']->value['name'];?>
</a></strong></td>
									<td>
										<div class="btn-group btn-group-xs">
											<?php if ($_smarty_tpl->tpl_vars['ELEMENT']->value['status'] != 2) {?><a data-toggle="tooltip" title="Редактировать" href="<?php echo $_smarty_tpl->tpl_vars['FRAMEWORK']->value['http_dir'];?>
/direct/strategy/get/param/id/<?php echo $_smarty_tpl->tpl_vars['ELEMENT']->value['id'];?>
/" class="btn btn-default"><i class="fa fa-edit"></i></a><?php }?>
										</div>
									</td>
								</tr>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</tbody>
						</table>
						</form>
					</div>
	
					
					
				</div>
				
				
				
				
				
				<!-- End of your awesome content --><?php }
}
