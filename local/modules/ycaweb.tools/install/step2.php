<?if(!check_bitrix_sessid()) return;
use Bitrix\Main\Localization\Loc;
global $errors;

if($errors===false):
	echo CAdminMessage::ShowNote(Loc::getMessage('MOD_INST_SUCCESS'));
else:
	for($i=0; $i<count($errors); $i++)
		$alErrors .= $errors[$i]."<br>";
	echo CAdminMessage::ShowMessage(array('TYPE' => 'ERROR', 'MESSAGE' => Loc::getMessage('MOD_INST_ERROR'), 'DETAILS' => $alErrors, 'HTML' => true));
endif;
?>